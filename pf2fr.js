class Translator {
    static get() {
        if (!Translator.instance) {
            Translator.instance = new Translator();
        }
        return Translator.instance;
    }

    // Initialize translator
    async initialize() {

        // Signalize translator is ready
        Hooks.callAll("pf2FR.ready");
    }

    constructor() {
        this.initialize();
    }

}

Hooks.once("init", () => {
    game.langFRPf2e = Translator.get();

    game.settings.register("pf2-fr", "name-display", {
        name: "Affichage des noms",
        hint: "Vous pouvez choisir ici la manière dont les noms des acteurs, objets et journaux issus des compendiums seront traduits et affichés",
        scope: "world",
        type: String,
        choices: {
            "vf-vo": "VF (VO)",
            "vo-vf": "VO (VF)",
            "vf": "VF",
            "vo": "VO"
        },
        default: "vf-vo",
        config: true,
        onChange: foundry.utils.debouncedReload
    });

    Babele.get().register({
        module: "pf2-fr",
        lang: 'fr',
        dir: "babele/"+game.settings.get('pf2-fr', 'name-display')+"/fr/"
    });
});

Hooks.once("babele.ready", () => {
    game.pf2e.ConditionManager.initialize();

    if (game.modules.get("lang-fr-pf2e")?.active){
        ui.notifications.error("Le package \"Système PF2 Français\" est encore installé sur cette partie ; il n'est plus utile et peut donc être désinstallé.")
    }
});
