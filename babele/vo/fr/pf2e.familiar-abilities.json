{
    "label": "Pouvoirs des familiers",
    "reference": [],
    "entries": {
        "Accompanist": {
            "name": "Accompanist",
            "description": "<p>Votre familier aide votre représentation. Lorsque vous tentez un test de Représentation, si votre familier est à proximité et peut agir, il vous accompagne avec des gazouillements, des applaudissement ou son propre instrument miniature. Cela vous donne un bonus de circonstances de +1 ou de +2 si vous êtes maître en Représentation.</p>"
        },
        "Wavesense": {
            "name": "Wavesense",
            "description": "<p>Votre familier peut sentir les vibrations dans l'eau. Il acquiert Perception des ondes (imprécis) avec une portée de 9 m.</p>"
        },
        "Master's Form": {
            "name": "Master's Form",
            "description": "<p>Votre familier peut changer de forme par une unique action, se transformant en un humanoïde de votre héritage avec le même âge, le même genre et la même constitution que celle de sa véritable forme, bien qu'il présente toujours un reste surnaturel évident de sa nature, comme des yeux de chat ou une langue de serpent. Cette forme est toujours la même chaque fois qu'il utilise cette capacité. Cela utilise autrement les effets de <em>@UUID[Compendium.pf2e.spells-srd.2qGqa33E4GPUCbMV]{Forme humanoïde}</em>, sauf que le changement est purement cosmétique. Il apparaît seulement comme humanoïde mais ne gagne aucune capacité nouvelle. Votre familier doit avoir les capacités @UUID[Compendium.pf2e.familiar-abilities.cT5octWchU4gjrhP]{dextérité manuelle} et @UUID[Compendium.pf2e.familiar-abilities.zyMRLQnFCQVpltiR]{parole} pour pouvoir sélectionner cette capacité.</p>"
        },
        "Fast Movement: Fly": {
            "name": "Fast Movement: Fly",
            "description": "<p>Augmentez la Vitesse de vol de votre familier de 7,5 à 12 m.</p>"
        },
        "Spell Delivery": {
            "name": "Spell Delivery",
            "description": "<p>Si votre familier se trouve dans votre case, vous pouvez lancer un sort avec une portée de contact, transférer son pouvoir à votre familier et ordonner à votre familier de délivrer le sort. Si vous procédez ainsi, le familier utilise ses 2 actions pour le round afin de se déplacer vers une cible de votre choix et aller toucher cette cible. S'il n'arrive pas au contact de la cible ce tour-ci, le sort n'a aucun effet.</p>"
        },
        "Fast Movement: Swim": {
            "name": "Fast Movement: Swim",
            "description": "<p>Augmentez la Vitesse de nage de votre familier de 7,5 à 12 m.</p>"
        },
        "Darkeater": {
            "name": "Darkeater",
            "description": "<p>Votre familier récupère naturellement dans les ombres. Après avoir passé 10 minutes consécutives dans une zone de lumière faible ou d'obscurité, votre familier récupère un nombre de points de vie égal à la moitié de votre niveau.</p>\n<p>Cette capacité est destinée à un familier de l'ombre. Cependant, un incantateur de l'ombre peut sélectionner cette capacité pour n'importe quel type de familier.</p>"
        },
        "Toolbearer": {
            "name": "Toolbearer",
            "description": "<p>Votre familier peut transporter un jeu d'outils correspondant à un Encombrement léger. Tant que votre familier est adjacent à vous, vous pouvez retirer et replacer les outils durant l'action au cours de laquelle vous les utilisez comme si vous les maniez. Votre familier doit avoir la capacité @UUID[Compendium.pf2e.familiar-abilities.cT5octWchU4gjrhP]{dextérité manuelle} pour utiliser cette capacité.</p>"
        },
        "Resistance": {
            "name": "Resistance",
            "description": "<p>Choisissez deux des dégâts suivants : acide, électricité, feu, froid, poison ou son. Votre familier obtient une résistance égale à la moitié de votre niveau contre les types de dégâts choisis.</p>"
        },
        "Scent": {
            "name": "Scent",
            "description": "<p>Votre familier gagne odorat (imprécis, 9 m).</p>"
        },
        "Spell Battery": {
            "name": "Spell Battery",
            "description": "<p>Vous gagnez un emplacement de sort supplémentaire d'au moins 3 niveaux inférieurs à celui de votre emplacement de sort le plus élevé ; vous devez pouvoir lancer des sorts de niveau 4 utilisant des emplacements de sorts pour sélectionner ce pouvoir de maître.</p>"
        },
        "Damage Avoidance: Reflex": {
            "name": "Damage Avoidance: Reflex",
            "description": "<p>Votre familier ne subit aucun dégât lorsqu'il réussit un jet de Réflexes ; cela n'empêche pas les effets autres que les dégâts.</p>"
        },
        "Greater Resistance": {
            "name": "Greater Resistance",
            "description": "<p>Votre familier augmente la résistance qu'il a obtenu grâce au pouvoir de familier résistance à 3 + la moitié de votre niveau. Votre familier doit avoir le pouvoir @UUID[Compendium.pf2e.familiar-abilities.FcQQLMAJMgOLjnSv]{Résistance} pour pouvoir le sélectionner.</p>"
        },
        "Touch Telepathy": {
            "name": "Touch Telepathy",
            "description": "<p>Votre familier peut communiquer télépathiquement avec vous par le contact. S'il possède également la capacité parole, il peut télépathiquement communiquer par le contact avec toute créature avec laquelle il partage une langue.</p>"
        },
        "Radiant": {
            "name": "Radiant",
            "description": "<p>Votre familier est rempli d'un rayonnement divin qui le rend résistant aux forces du mal. Votre familier gagne une résistance au mal et aux dégâts négatifs égale à la moitié de votre niveau.</p>\n<hr />\n<p><em>Note : Comme les effets placés sur un acteur ne peuvent référencer des données d'un autre acteur, la résistance de la moitié du niveau du PJ n'est pas automatisable. Merci de mettre à jour les résistances des familiers en conséquence.</em></p>"
        },
        "Verdant Burst": {
            "name": "Verdant Burst",
            "description": "<p><strong>Prérequis</strong> @UUID[Compendium.pf2e.feats-srd.baz18CdB13DVMHV9]{Secrets du familier léchi}</p>\n<hr />\n<p>Lorsque votre familier meurt, il relâche son énergie primordiale pour lancer la version de <em>@UUID[Compendium.pf2e.spells-srd.rfZpqmj0AIIdkVIs]{Guérison}</em> à trois actions, intensifié à un niveau inférieur de 1 à votre plus haut emplacement de sort. Le sort <em>Guérison</em> obtient aussi un bonus de statut égal à deux fois le niveau de sort au nombre de Points de vie qu'il redonne aux plantes. Vous devez être capable de lancer les sorts de niveau 2 en utilisant des emplacements de sort pour choisir cette aptitude de familier.</p>"
        },
        "Familiar Focus": {
            "name": "Familiar Focus",
            "description": "<p>Une fois par jour, votre familier peut utiliser 2 actions avec le trait concentration pour regagner 1 point de focalisation, jusqu'à votre maximum habituel. Vous devez disposer d'une réserve de focalisation pour sélectionner ce pouvoir.</p>"
        },
        "Threat Display": {
            "name": "Threat Display",
            "description": "<p>Votre familier vous aide à transmettre des menaces sans parler par le biais du langage corporel.</p>\n<p>Lorsque vous tentez un test d'Intimidation pour @UUID[Compendium.pf2e.actionspf2e.2u915NdUyQan6uKF]{Démoraliser} une créature, si votre familier se trouve à moins de 9 m de votre cible et peut agir, il vous accompagne par des grognements, des sifflements ou en levant les crocs.</p>\n<p>S'il peut le faire, vous ne subissez pas la pénalité normale de -4 au test d'intimidation si votre cible ne comprend pas la langue que vous parlez.</p>"
        },
        "Tough": {
            "name": "Tough",
            "description": "<p>Le nombre maximal de Point de vie de votre familier augmente de 2 par niveau</p>"
        },
        "Second Opinion": {
            "name": "Second Opinion",
            "description": "<p>Votre familier est votre confident académique.</p>\n<p>En dépit du fait d'être un sbire, votre familier obient 1 réaction au début de ses tours, qu'il ne peut seulement utiliser que pour vous @UUID[Compendium.pf2e.actionspf2e.HCl3pzVefiv9ZKQW]{Aider} sur un test de compétence pour Vous souvenir pour une compétence dans laquelle il a le pouvoir de familier @UUID[Compendium.pf2e.familiar-abilities.jevzf9JbJJibpqaI]{Compétent} (il a toujours à se préparer pour vous Aider comme à l'ordinaire avec la réaction Aider).</p>\n<p>Il réussit automatiquement à son test pour vous Aider avec ces compétences ou obtient automatiquement un succès critique si vous êtes maître avec la compétence en question. Votre familier doit avoir le pouvoir compétent pour choisir ceci.</p>"
        },
        "Soul Sight": {
            "name": "Soul Sight",
            "description": "<p>Votre familier obtient @UUID[Compendium.pf2e.bestiary-ability-glossary-srd.sebk9XseMCRkDqRg]{Perception de la vie} avec une portée de 9 m.</p>"
        },
        "Tremorsense": {
            "name": "Tremorsense",
            "description": "<p>Votre familier a une conscience aiguë de toutes les vibrations qui se propagent sur une surface. Il obtient Perception des vibrations (imprécis) avec une portée de 9 m.</p>"
        },
        "Share Senses": {
            "name": "Share Senses",
            "description": "<p>Une fois toutes les 10 minutes, vous pouvez dépenser une action unique avec le trait concentration pour projeter vos sens à l'intérieur de votre familier. Lorsque vous le faites, vous perdez toutes les informations sensorielles de votre propre corps, mais pouvez sentir celles de votre familier durant un maximum de 1 minute. Vous pouvez Révoquer cet effet.</p>"
        },
        "Independent": {
            "name": "Independent",
            "description": "<p>Lors d'une rencontre, si vous ne donnez pas d'ordre à votre familier, il obtient toujours 1 action chaque round. Typiquement, vous décidez toujours comment il dépense cette action, mais le MJ peut déterminer que votre familier choisit sa propre tactique plutôt qu'accomplir l'action que vous préférez. Cela ne fonctionne pas avec @UUID[Compendium.pf2e.familiar-abilities.9PsptrEoCC4QdM23]{Valet} ou des capacités similaires qui requièrent un ordre, si vous êtes capable de monter votre familier ou situations similaires.</p>"
        },
        "Augury": {
            "name": "Augury",
            "description": "<p>Votre familier peut entrevoir les fils du destin pour vous donner un indice cryptique concernant votre avenir. Votre familier peut lancer <em>@UUID[Compendium.pf2e.spells-srd.41TZEjhO6D1nWw2X]{Augure}</em> une fois par jour en utilisant votre tradition magique et votre DC de sort. Vous devez être au moins de 8ème niveau pour choisir cette capacité.</p>"
        },
        "Climber": {
            "name": "Climber",
            "description": "<p>Votre familier gagne une Vitesse d'escalade de 7,5 m.</p>"
        },
        "Fast Movement: Land": {
            "name": "Fast Movement: Land",
            "description": "<p>Augmentez la Vitesse de votre familier de 7,5 à 12 m.</p>"
        },
        "Partner in Crime": {
            "name": "Partner in Crime",
            "description": "<p>Votre familier est votre associé criminel. En dépit d'être un sbire, votre familier obtient une réaction au début de son tour, qu'il ne peut utiliser que pour vous aider sur un test de compétence de Duperie ou de Vol or Thievery (il doit toujours se préparer à vous aider comme habituellement avec la réaction Aider). Il obtient automatiquement un succès à son test pour vous Aider avec ces compétences ou obtient un succès critique si vous êtes maître dans la compétence en question.</p>"
        },
        "Recall Familiar": {
            "name": "Recall Familiar",
            "description": "<p>Vous pouvez convoquer votre familier à vos côtés. Une fois par jour, vous pouvez utiliser une activité de 3 actions, qui a le trait concentration, pour téléporter votre familier dans votre case. Votre familier doit être à moins de 1,5 kilomètre ou la tentative de convocation est perdue. Ceci est un effet d'invocation et de téléportation.</p>"
        },
        "Innate Surge": {
            "name": "Innate Surge",
            "description": "<p>Une fois par jour, vous pouvez puiser dans la magie innée de votre familier pour alimenter la vôtre. Vous pouvez lancer un sort inné octroyé par un don ancestral que vous avez déjà lancé aujourd'hui. Vous devez tout de même Lancer le sort et remplir ses autres conditions.</p>"
        },
        "Gills": {
            "name": "Gills",
            "description": "<p>Votre familier développe un ensemble de branchies, ce qui lui permet de respirer de l'eau en plus de l'air.</p>"
        },
        "Cantrip Connection": {
            "name": "Cantrip Connection",
            "description": "<p>Vous pouvez préparer un tour de magie supplémentaire ou, si vous possédez un répertoire, désigner à la place un tour de magie à lui ajouter à chaque fois que vous sélectionnez ce pouvoir ; vous pouvez le réapprendre, mais vous ne pouvez pas autrement le changer. Pour sélectionner ce pouvoir, vous devez être capable de préparer des tours de magie ou de les ajouter à votre répertoire.</p>"
        },
        "Erudite": {
            "name": "Erudite",
            "description": "<p>Votre familier possède d'incroyables connaissances religieuses. Bien qu'il soit un sbire, votre familier obtient 1 réaction au début de ses tours, qu'il ne peut utiliser que pour @UUID[Compendium.pf2e.actionspf2e.HCl3pzVefiv9ZKQW]{Vous aider} sur un test de Religion (il doit toujours se préparer à vous aider normalement pour la réaction Aider). Il réussit automatiquement son test pour Aider aux tests de Religion ou obtient un succès critique si vous êtes maître en Religion.</p>"
        },
        "Luminous": {
            "name": "Luminous",
            "description": "<p>Votre familier brille d'une lumière réconfortante. Il diffuse une lumière vive dans un rayon de 9 m et une lumière faible dans les 9 m qui suivent. Il peut supprimer ou reprendre cette lumière en utilisant une action, qui possède le trait concentration.</p>"
        },
        "Amphibious": {
            "name": "Amphibious",
            "description": "<p>Votre familier gagne une Vitesse de nage de 7,5 m (ou une Vitesse de 7,5 m s'il a déjà une Vitesse de nage).</p>"
        },
        "Flier": {
            "name": "Flier",
            "description": "<p>Votre familier gagne une Vitesse de vol de 7,5 m.</p>"
        },
        "Tattoo Transformation": {
            "name": "Tattoo Transformation",
            "description": "<p>Votre familier peut se transformer en tatouage que vous portez sur votre peau. Lorsqu'il est transformé en tatouage, le familier est représenté sous forme de portrait stylisé et coloré et ne dispose d'aucune action sauf celle consistant à redevenir un familier. Il n'est pas affecté par les effets de zone et doit être ciblé indépendamment pour être affecté, ce qui demande de connaître le fait que le tatouage est une créature. Cela signifie que vous et vos alliés pouvez guérir et porter assistance au familier, alors que la plupart des ennemis ne connaissent pas sa vraie nature. Les créatures doivent tenter un jet de @Check[type:perception|dc:20|showDC:all]{Perception} DD 20 en utilisant l'action Chercher pour réaliser que c'est un familier (peu d'ennemis le tenteront). Votre familier peut encore communiquer ses sensations de manière empathique. Se transformer en tatouage ou revenir à sa forme de familier est une activité qui dure 1 minute et qui possède le trait concentration.</p>"
        },
        "Manual Dexterity": {
            "name": "Manual Dexterity",
            "description": "<p>Votre familier peut utiliser jusqu'à deux de ses membres comme s'il s'agissait de mains pour les actions Manipulation.</p>"
        },
        "Lifelink": {
            "name": "Lifelink",
            "description": "<p>Si votre familier est réduit par des dégâts à 0 PV, vous pouvez encaisser les dégâts à sa place en tant que réaction avec le trait concentration. Si vous agissez ainsi, vous subissez tous les dégâts et votre familier aucun. Cependant, les effets spéciaux qui surviennent lorsqu'un coup inflige des dégâts à votre familier (comme le venin de serpent) continuent de s'appliquer.</p>"
        },
        "Extra Reagents": {
            "name": "Extra Reagents",
            "description": "<p>Votre familier développe des réactifs imprégnés supplémentaires sur ou à l'intérieur de son corps. Vous gagnez un lot supplémentaire de réactifs imprégnés. Vous devez posséder la capacité réactifs imprégnés pour sélectionner ce pouvoir.</p>"
        },
        "Darkvision": {
            "name": "Darkvision",
            "description": "<p>Votre familier gagne vision dans le noir</p>"
        },
        "Valet": {
            "name": "Valet",
            "description": "<p>Vous pouvez ordonner à votre familier de vous donner des objets avec une grande efficacité. Votre familier n'utilise pas ses deux actions immédiatement lorsque vous lui donnez un ordre. À la place, jusqu'à deux fois avant la fin de votre tour, votre familier peut utiliser deux actions Interagir pour récupérer un objet d'encombrement léger ou négligeable que vous êtes en train de manier et le placer dans une de vos mains libres. Le familier ne peut utiliser cette capacité pour récupérer des objets rangés. Si le familier possède un nombre d'actions différentes, il peut retirer un objet pour chaque action qu'il possède lorsque vous lui avez donné des ordres de cette manière.</p>"
        },
        "Speech": {
            "name": "Speech",
            "description": "<p>Votre familier comprend et parle une langue que vous connaissez.</p>"
        },
        "Burrower": {
            "name": "Burrower",
            "description": "<p>Votre familier gagne une Vitesse de creusement de 1,5 m, qui lui permet de creuser des trous très Petits.</p>"
        },
        "Kinspeech": {
            "name": "Kinspeech",
            "description": "<p>Votre familier peut comprendre et communiquer avec des animaux de la même espèce que lui. Pour sélectionner ce pouvoir, votre familier doit être un animal, être doué de parole et vous devez être au moins de niveau 6.</p>"
        },
        "Medic": {
            "name": "Medic",
            "description": "<p>Votre familier peut faire appel au pouvoir de votre divinité pour @UUID[Compendium.pf2e.actionspf2e.aBQ8ajvEBByv45yz]{Lancer un sort} une fois par jour. Le familier peut lancer la version à 1 action de @UUID[Compendium.pf2e.spells-srd.rfZpqmj0AIIdkVIs]{Guérison} à un niveau inférieur de 2 niveaux à votre emplacement de sort de plus haut niveau qu'il ne peut lancer que sur vous. Vous devez être capable de lancer des sorts de niveau 3 à partir d'emplacements de sorts pour sélectionner cette capacité.</p>"
        },
        "Plant Form": {
            "name": "Plant Form",
            "description": "<p>Votre familier plante peut changer de forme par une unique action, en transformant en une Très petite plante d'un type à peu près similaire à la nature du familier. Cela utilise autrement les effets de <em>@UUID[Compendium.pf2e.spells-srd.dileJ0Yxqg76LMvu]{Morphologie d'arbre}</em>. Vous devez avoir un familier avec le trait plante, tel qu'un léchi, pour choisir cette capacité.</p>"
        },
        "Major Resistance": {
            "name": "Major Resistance",
            "description": "<p>Votre familier augmente la résistance qu'il obtient de son pouvoir de familier résistance à une valeur égale à votre niveau. Pour choisir ce pouvoir, votre familier doit avoir le pouvoir @UUID[Compendium.pf2e.familiar-abilities.zKTL9y9et0oTHEYS]{Résistance supérieure} et vous devez être au moins de niveau 8.</p>"
        },
        "Fast Movement: Climb": {
            "name": "Fast Movement: Climb",
            "description": "<p>Augmentez la Vitesse d'escalade de votre familier de 7,5 à 12 m.</p>"
        },
        "Purify Air": {
            "name": "Purify Air",
            "description": "<p><strong>Prérequis</strong> @UUID[Compendium.pf2e.feats-srd.baz18CdB13DVMHV9]{Secrets du familier léchi}</p>\n<hr />\n<p>Votre familier recycle l'air, fournissant assez d'oxygène à une créature de taille Moyenne dans les zones avec de l'air vicié, tel qu'une chambre scellée ou un espace extradimensionnel. Si le léchi est dans la zone d'un effet de poison inhalé ou d'un effet qui est basé sur l'odorat, les créatures dans une @Template[type:emanation|distance:15]{émanation de 4,5 m} du léchi obtiennent un bonus de circonstances de +2 à leur jet de sauvegarde contre l'effet.</p>"
        },
        "Spellcasting": {
            "name": "Spellcasting",
            "description": "<p>Choisissez un sort dans votre répertoire ou que vous avez préparé dans la journée qui est d'au moins 5 niveaux inférieur à votre plus haut niveau d'emplacement de sort. Votre familier peut Lancer ce sort une fois par jour en utilisant votre tradition magique, votre modificateur de sort et votre DD de sort. Si le sort a un inconvénient qui affecte l'incantateur, vous êtes affectés tous les deux. Vous devez être capable de lancer des sorts de niveau 6 en utilisant des emplacements de sort pour choisir ce pouvoir.</p>"
        },
        "Damage Avoidance: Fortitude": {
            "name": "Damage Avoidance: Fortitude",
            "description": "<p>Votre familier ne subit aucun dégât lorsqu'il réussit un jet de Vigueur ; cela n'empêche pas les effets autres que les dégâts.</p>"
        },
        "Mask Freeze": {
            "name": "Mask Freeze",
            "description": "<p><strong>Masque figé</strong> Lorsqu'il est sous forme de masque, votre familier peut cacher ses qualités surnaturelles évidentes pour passer pour un simple masque sans prétention. Il n'a pas besoin de Se déguiser pour tromper quelqu'un qui n'y prête pas attention et il bénéficie d'un bonus de circonstances de +4 à son DD de Duperie contre un observateur actif qui Cherche ou qui l'étudie.</p>"
        },
        "Snoop": {
            "name": "Snoop",
            "description": "<p>Votre familier garde les yeux et les oreilles ouverts, prêt à relayer chaque bribe de ragot qu'il capte, pour vous aider à recueillir des informations.</p>\n<p>Bien qu'il soit un sbire, votre familier gagne 1 réaction au début de ses tours, qu'il ne peut utiliser que pour vous @UUID[Compendium.pf2e.actionspf2e.HCl3pzVefiv9ZKQW]{Aider} sur un test de Diplomatie en vue de @UUID[Compendium.pf2e.actionspf2e.plBGdZhqq5JBl1D8]{Recueillir des informations} (il doit toujours se préparer à vous aider comme à l'ordinaire avec la réaction Aider, qui nécessite sa participation tout au long de l'activité).</p>\n<p>Il obtient automatiquement un succès à son test pour vous Aider avec ces compétences ou automatiquement un succès critique si vous êtes un maître de la compétence en question.</p>"
        },
        "Poison Reservoir": {
            "name": "Poison Reservoir",
            "description": "<p>Votre familier homoncule possède un réservoir pour le poison, permettant d'appliquer un poison de blessure à l'arme exposée d'un allié adjacent par une unique action Interagir. Vous devez fournir le poison et l'instiller dans le réservoir en utilisant deux actions Interagir consécutives. Vous devez posséder un familier homoncule pour choisir cette capacité.</p>"
        },
        "Skilled": {
            "name": "Skilled",
            "description": "<p>Choisissez une compétence autre que Acrobaties ou Vol. Le modificateur de votre familier dans cette compétence est égale à votre niveau auquel vous ajoutez votre modificateur de caractéristique d'incantation au lieu simplement de votre niveau. Vous pouvez choisir cette capacité plusieurs fois, en choisissant une compétence différente à chaque fois.</p>"
        },
        "Restorative Familiar": {
            "name": "Restorative Familiar",
            "description": "<p>Une fois par jour, votre familier peut utiliser 2 actions avec le trait Concentration pour partager un peu de son énergie vitale et vous guérir. Il doit être dans votre case pour pouvoir le faire. Il vous permet de récupérer un nombre de points de vie égal à [[/r max(floor(@actor.level/2)d8, 1d8)]]{1d8 fois la moitié de votre niveau} (minimum 1d8).</p>"
        },
        "Lab Assistant": {
            "name": "Lab Assistant",
            "description": "<p>Votre familier peut utiliser votre action Alchimie rapide. Vous devez posséder Alchimie rapide et votre familier doit se trouver dans votre case. Il l'accomplit au même coût et aux mêmes conditions que si vous l'utilisiez. Votre familier doit avoir le pouvoir Dextérité manuelle pour pouvoir sélectionner ce pouvoir.</p>"
        },
        "Grasping Tendrils": {
            "name": "Grasping Tendrils",
            "description": "<p>Prérequis @UUID[Compendium.pf2e.feats-srd.baz18CdB13DVMHV9]{Secrets du familier léchi}</p>\n<p>Votre familier peut faire pousser des lianes ou des vrilles similaires, augmentant son allonge à 4,5 m.</p>"
        },
        "Ambassador": {
            "name": "Ambassador",
            "description": "<p>Votre familier sait comment agir de manière mignonne ou concentrée sur demande, ce qui vous aide à faire bonne impression.</p>\n<p>Bien qu'il soit un sbire, votre familier gagne 1 réaction au début de ses tours, qu'il ne peut utiliser que pour vous Aider lors d'un test de Diplomatie pour @UUID[Compendium.pf2e.actionspf2e.OX4fy22hQgUHDr0q]{Faire bonne impression} (il doit toujours se préparer à vous @UUID[Compendium.pf2e.actionspf2e.HCl3pzVefiv9ZKQW]{Aider} comme d'habitude pour la réaction Aider, qui exige qu'il participe à toute l'activité).</p>\n<p>Il réussit automatiquement son test pour vous Aider avec ces compétences ou réussit automatiquement de manière critique si vous êtes maître dans la compétence en question.</p>"
        },
        "Spirit Touch": {
            "name": "Spirit Touch",
            "description": "<p>Votre familier peut toucher les créatures intangibles. Si vous avez le pouvoir de maître transfert de sort de votre familier, tout sort que le familier délivre avec lui obtient les avantages de la rune de propriété <em>@UUID[Compendium.pf2e.equipment-srd.JQdwHECogcTzdd8R]{Spectrale}</em>.</p>"
        },
        "Shadow Step": {
            "name": "Shadow Step",
            "description": "<p>Ce pouvoir est pour les Familiers d'ombre. Un @UUID[Compendium.pf2e.feats-srd.ncrNQcwm4gOQRAA3]{Incantateur d'ombre}, quoi qu'il en soit, peut choisir ce pouvoir pour toute espèce de familier. Votre familier obtient l'action @UUID[Compendium.pf2e.actionspf2e.VQ5OxaDKE0lCj8Mr]{Pas d'ombre}.</p>\n<p>Vous devez être au moins de niveau 7 pour choisir ce pouvoir de familier pour votre familier.</p>"
        },
        "Damage Avoidance: Will": {
            "name": "Damage Avoidance: Will",
            "description": "<p>Votre familier ne subit aucun dégât lorsqu'il réussit un jet de Volonté ; cela n'empêche pas les effets autres que les dégâts.</p>"
        },
        "Focused Rejuvenation": {
            "name": "Focused Rejuvenation",
            "description": "<p>Lorsque vous Refocalisez, vous générez de l'énergie magique qui soigne votre familier. Votre familier récupère 1 Point de Vie par niveau lorsque vous Refocalisez.</p>"
        }
    },
    "mapping": {
        "description": "system.description.value"
    }
}