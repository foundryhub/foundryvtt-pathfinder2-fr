# État de la traduction (kingmaker-bestiary)

 * **vide**: 128
 * **libre**: 5
 * **aucune**: 102


Dernière mise à jour: 2023-03-05 17:26 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[3gtQv6Mkr7CUlG7W.htm](kingmaker-bestiary/3gtQv6Mkr7CUlG7W.htm)|Wild Hunt Archer|
|[3QkgiJJ0IS6oTT0t.htm](kingmaker-bestiary/3QkgiJJ0IS6oTT0t.htm)|River Elasmosaurus|
|[3Tb42wMuwjtP3iYN.htm](kingmaker-bestiary/3Tb42wMuwjtP3iYN.htm)|The Lonely Warrior|
|[6BFUxkEBKPr89SNt.htm](kingmaker-bestiary/6BFUxkEBKPr89SNt.htm)|Mandragora Swarm|
|[6zWcjvPya0wWDZv2.htm](kingmaker-bestiary/6zWcjvPya0wWDZv2.htm)|Risen Fetch|
|[7os3GIj69tIqXTGl.htm](kingmaker-bestiary/7os3GIj69tIqXTGl.htm)|Wild Hunt Horse|
|[8cRXmQ9vd93T0m2N.htm](kingmaker-bestiary/8cRXmQ9vd93T0m2N.htm)|Cyclops Zombie|
|[8D3xvMiuOLJ3p6FU.htm](kingmaker-bestiary/8D3xvMiuOLJ3p6FU.htm)|Hooktongue Hydra|
|[8eTfnYg0xmcgmDVQ.htm](kingmaker-bestiary/8eTfnYg0xmcgmDVQ.htm)|Irovetti's Fetch|
|[8IYynuqZxDpXZwBE.htm](kingmaker-bestiary/8IYynuqZxDpXZwBE.htm)|Tartuccio|
|[AA3hdVlJhALthghl.htm](kingmaker-bestiary/AA3hdVlJhALthghl.htm)|Enormous Flame Drake|
|[aEz68WBHT9F5YlXz.htm](kingmaker-bestiary/aEz68WBHT9F5YlXz.htm)|Castruccio Irovetti|
|[BEo6xNEhEKPsUzIn.htm](kingmaker-bestiary/BEo6xNEhEKPsUzIn.htm)|Hooktongue|
|[bF0FHdZMWl1OuRae.htm](kingmaker-bestiary/bF0FHdZMWl1OuRae.htm)|Hill Giant Butcher|
|[BJbe6BzLSIx8Jsix.htm](kingmaker-bestiary/BJbe6BzLSIx8Jsix.htm)|Storm-Struck Arboreal|
|[bjMn6cRNFK9jgfe0.htm](kingmaker-bestiary/bjMn6cRNFK9jgfe0.htm)|Bog Mummy Cultist|
|[Bq2eST0Ke4MnKXgR.htm](kingmaker-bestiary/Bq2eST0Ke4MnKXgR.htm)|Grigori|
|[bwEYizrdHs8lmvxE.htm](kingmaker-bestiary/bwEYizrdHs8lmvxE.htm)|Leng Envoy|
|[Cj2zn7JazTB9ZnIq.htm](kingmaker-bestiary/Cj2zn7JazTB9ZnIq.htm)|Tiger Lord Hill Giant (TL2)|
|[cjQVmbjqONihI9SR.htm](kingmaker-bestiary/cjQVmbjqONihI9SR.htm)|Fen Pudding|
|[cPcregujkG0LUHiG.htm](kingmaker-bestiary/cPcregujkG0LUHiG.htm)|Evindra|
|[dZEl1W8zV3rj5D9O.htm](kingmaker-bestiary/dZEl1W8zV3rj5D9O.htm)|Old Crackjaw|
|[eAu4SOvMaNISD3RZ.htm](kingmaker-bestiary/eAu4SOvMaNISD3RZ.htm)|Vordakai|
|[etIP2Mdv3Xnr0wto.htm](kingmaker-bestiary/etIP2Mdv3Xnr0wto.htm)|Ancient Wisp|
|[EvNCuVyY5sEGM4ZL.htm](kingmaker-bestiary/EvNCuVyY5sEGM4ZL.htm)|Hannis Drelev|
|[F0uz9YV8ILAM8fIg.htm](kingmaker-bestiary/F0uz9YV8ILAM8fIg.htm)|Windchaser|
|[fbsy6zPV4HjHiis7.htm](kingmaker-bestiary/fbsy6zPV4HjHiis7.htm)|Bloomborn Athach|
|[fQ9FuovHuRt6vtcq.htm](kingmaker-bestiary/fQ9FuovHuRt6vtcq.htm)|Wild Hunt Scout|
|[GDBEHLicn4kKggis.htm](kingmaker-bestiary/GDBEHLicn4kKggis.htm)|Murder of Crows|
|[h73Up6EQZqtgh6gP.htm](kingmaker-bestiary/h73Up6EQZqtgh6gP.htm)|Jamandi Aldori|
|[hLBHFloWuXLjCQYH.htm](kingmaker-bestiary/hLBHFloWuXLjCQYH.htm)|Primal Bandersnatch|
|[hywUu3wVDuXePx3e.htm](kingmaker-bestiary/hywUu3wVDuXePx3e.htm)|Centaur Scout|
|[I8IPTHEU1zF5KmAB.htm](kingmaker-bestiary/I8IPTHEU1zF5KmAB.htm)|Boggard Warden|
|[IGVqtFsNIyghuVsD.htm](kingmaker-bestiary/IGVqtFsNIyghuVsD.htm)|Agai|
|[j7fPCy71EfQL1KmU.htm](kingmaker-bestiary/j7fPCy71EfQL1KmU.htm)|Nyrissa|
|[jUNnWUuiVueOTvHt.htm](kingmaker-bestiary/jUNnWUuiVueOTvHt.htm)|The Misbegotten Troll|
|[K61HTqmdHRPHQz1x.htm](kingmaker-bestiary/K61HTqmdHRPHQz1x.htm)|Tulvak|
|[kiUlJn4FzWMzgkbW.htm](kingmaker-bestiary/kiUlJn4FzWMzgkbW.htm)|Talon Peak Roc|
|[Kmr8s4sEMn365d5M.htm](kingmaker-bestiary/Kmr8s4sEMn365d5M.htm)|Prank Workshop Mitflit|
|[kSR0D8dLXTlw09NT.htm](kingmaker-bestiary/kSR0D8dLXTlw09NT.htm)|Gedovius|
|[L3q7yQ0jKqH2IWy7.htm](kingmaker-bestiary/L3q7yQ0jKqH2IWy7.htm)|Nightmare Rook|
|[L6ANJQoCyk5dWcdH.htm](kingmaker-bestiary/L6ANJQoCyk5dWcdH.htm)|Elder Elemental Tsunami|
|[LdIVntI4ho9eKTVt.htm](kingmaker-bestiary/LdIVntI4ho9eKTVt.htm)|The Dancing Lady|
|[LF67gTrSqSb7h4KZ.htm](kingmaker-bestiary/LF67gTrSqSb7h4KZ.htm)|Boggard Cultist|
|[lRxISnNmcfUm7AfG.htm](kingmaker-bestiary/lRxISnNmcfUm7AfG.htm)|Grabbles|
|[lZtkMlyix3kaTO0j.htm](kingmaker-bestiary/lZtkMlyix3kaTO0j.htm)|The Beast|
|[M7hMPdEbaC1RNwfY.htm](kingmaker-bestiary/M7hMPdEbaC1RNwfY.htm)|Kob Moleg|
|[m8kwG6NskYDlBSCy.htm](kingmaker-bestiary/m8kwG6NskYDlBSCy.htm)|Ankou Assassin|
|[mbAhAq5OyZAv6lq5.htm](kingmaker-bestiary/mbAhAq5OyZAv6lq5.htm)|Darivan|
|[MUuXMpUGEnqmElgT.htm](kingmaker-bestiary/MUuXMpUGEnqmElgT.htm)|Vicious Army Ant Swarm|
|[n82GZhM6joceE91v.htm](kingmaker-bestiary/n82GZhM6joceE91v.htm)|Ilthuliak|
|[nDFogS2qQJomjfmR.htm](kingmaker-bestiary/nDFogS2qQJomjfmR.htm)|Fetch Stalker|
|[ndoXVn6MPPxSJvcC.htm](kingmaker-bestiary/ndoXVn6MPPxSJvcC.htm)|Black Smilodon|
|[ni0RSuVeUgs5WmlY.htm](kingmaker-bestiary/ni0RSuVeUgs5WmlY.htm)|Hillstomper|
|[NMCee869vhJjP5Ri.htm](kingmaker-bestiary/NMCee869vhJjP5Ri.htm)|The Wriggling Man|
|[NSY6VAGs9VrKzyRX.htm](kingmaker-bestiary/NSY6VAGs9VrKzyRX.htm)|Armag Twice-Born|
|[NxVi4ot2bzNOk6Zj.htm](kingmaker-bestiary/NxVi4ot2bzNOk6Zj.htm)|Aecora Silverfire|
|[obT2KQ8YYlRxvSWr.htm](kingmaker-bestiary/obT2KQ8YYlRxvSWr.htm)|Tree that Weeps|
|[OhuPV1g5LfejhtAz.htm](kingmaker-bestiary/OhuPV1g5LfejhtAz.htm)|The Horned Hunter|
|[oM1AvORITfhzwrDk.htm](kingmaker-bestiary/oM1AvORITfhzwrDk.htm)|Barbtongued Wyvern|
|[OnHIutiVLt1czwWL.htm](kingmaker-bestiary/OnHIutiVLt1czwWL.htm)|Wild Hunt Hound|
|[P1kWLRlEPTcZ3uzD.htm](kingmaker-bestiary/P1kWLRlEPTcZ3uzD.htm)|Kellid Graveknight|
|[pD5Y7gJtqlr2A4a2.htm](kingmaker-bestiary/pD5Y7gJtqlr2A4a2.htm)|Hargulka|
|[pfvUhfy0VHNlTyvN.htm](kingmaker-bestiary/pfvUhfy0VHNlTyvN.htm)|Immense Mandragora|
|[pT5hfxcsG7eV5oxh.htm](kingmaker-bestiary/pT5hfxcsG7eV5oxh.htm)|Niodrhast|
|[pZosztihhMtCLinT.htm](kingmaker-bestiary/pZosztihhMtCLinT.htm)|Wild Hunt Monarch|
|[QkGk4GMq3pCtBbLS.htm](kingmaker-bestiary/QkGk4GMq3pCtBbLS.htm)|Troll Guard|
|[qLfHY6uNUQ99NZei.htm](kingmaker-bestiary/qLfHY6uNUQ99NZei.htm)|Werendegar|
|[rnFjLc6xYYPtXS6a.htm](kingmaker-bestiary/rnFjLc6xYYPtXS6a.htm)|Defaced Naiad Queen|
|[s0BfmFWAhLQkQEbg.htm](kingmaker-bestiary/s0BfmFWAhLQkQEbg.htm)|Kargstaad's Giant|
|[S4oIMaVPzQuRaTpK.htm](kingmaker-bestiary/S4oIMaVPzQuRaTpK.htm)|Phomandala|
|[sedubjznhIbVfCkD.htm](kingmaker-bestiary/sedubjznhIbVfCkD.htm)|Sepoko|
|[SfFMqKTUQ1Dwu5lT.htm](kingmaker-bestiary/SfFMqKTUQ1Dwu5lT.htm)|Whimwyrm|
|[SjU0oB6pOk0XY8VN.htm](kingmaker-bestiary/SjU0oB6pOk0XY8VN.htm)|Minognos-Ushad|
|[SVUjDSZHYmwbQgnq.htm](kingmaker-bestiary/SVUjDSZHYmwbQgnq.htm)|The Knurly Witch|
|[sw32QdZlsWnmWaVY.htm](kingmaker-bestiary/sw32QdZlsWnmWaVY.htm)|Mastiff Of Tindalos|
|[tjq87ghubOcPAXjj.htm](kingmaker-bestiary/tjq87ghubOcPAXjj.htm)|Fetch Behemoth|
|[TLoNfIIhS7YGdV54.htm](kingmaker-bestiary/TLoNfIIhS7YGdV54.htm)|Thylacine|
|[tq87VRZjkGBmW8kf.htm](kingmaker-bestiary/tq87VRZjkGBmW8kf.htm)|Rezatha|
|[ugzdSsP9U0gGLZ3v.htm](kingmaker-bestiary/ugzdSsP9U0gGLZ3v.htm)|Rigg Gargadilly|
|[uLzD70CB7Bh2XxQf.htm](kingmaker-bestiary/uLzD70CB7Bh2XxQf.htm)|Shambler|
|[UPESZZbXchcuqI1r.htm](kingmaker-bestiary/UPESZZbXchcuqI1r.htm)|Engelidis|
|[UXXEOnvp2MDaS9Sc.htm](kingmaker-bestiary/UXXEOnvp2MDaS9Sc.htm)|Vilderavn Herald|
|[v2cJC9tdjRHexMwa.htm](kingmaker-bestiary/v2cJC9tdjRHexMwa.htm)|Irahkatu|
|[vBugpZnpxQcrrWoo.htm](kingmaker-bestiary/vBugpZnpxQcrrWoo.htm)|Bloom of Lamashtu|
|[vrZrha0Gz14Zd4tA.htm](kingmaker-bestiary/vrZrha0Gz14Zd4tA.htm)|Korog|
|[vs8QT4LYEcQfA6Us.htm](kingmaker-bestiary/vs8QT4LYEcQfA6Us.htm)|Skeletal Tiger Lord|
|[VSffsyt5RONB4k2U.htm](kingmaker-bestiary/VSffsyt5RONB4k2U.htm)|General Avinash Jurrg|
|[vTzbfxtvhhmS7KWr.htm](kingmaker-bestiary/vTzbfxtvhhmS7KWr.htm)|Bloom Wyvern|
|[w8jUzPPGLQECT4j7.htm](kingmaker-bestiary/w8jUzPPGLQECT4j7.htm)|Overgrown Viper Vine|
|[wD6nctHffSaMdyag.htm](kingmaker-bestiary/wD6nctHffSaMdyag.htm)|Lizardfolk Warrior|
|[WGHV95WkNnlY70Sn.htm](kingmaker-bestiary/WGHV95WkNnlY70Sn.htm)|Lintwerth|
|[wHQmyXnG4Yax4KcK.htm](kingmaker-bestiary/wHQmyXnG4Yax4KcK.htm)|Enormous Dragonfly|
|[WJ0bMCZUHJVwKYG1.htm](kingmaker-bestiary/WJ0bMCZUHJVwKYG1.htm)|Freshly Bloomed Basilisk|
|[wJDtBtvRtyxtyqHS.htm](kingmaker-bestiary/wJDtBtvRtyxtyqHS.htm)|Kargstaad|
|[Wyqsf3qDt7PqQ8OM.htm](kingmaker-bestiary/Wyqsf3qDt7PqQ8OM.htm)|Brush Thylacine|
|[xqTVUqUs5UsGcSuH.htm](kingmaker-bestiary/xqTVUqUs5UsGcSuH.htm)|Winged Owlbear|
|[XQyXoOe7FCpZilaF.htm](kingmaker-bestiary/XQyXoOe7FCpZilaF.htm)|Lesser Jabberwock|
|[y68kqNXmr1BnjZtc.htm](kingmaker-bestiary/y68kqNXmr1BnjZtc.htm)|Oversized Chimera|
|[Y9N8MaTYupFhCUuN.htm](kingmaker-bestiary/Y9N8MaTYupFhCUuN.htm)|Jaggedbriar Hag|
|[ZbgFOoDCOXqhJWhz.htm](kingmaker-bestiary/ZbgFOoDCOXqhJWhz.htm)|Ngara|
|[ZoUIXzygFDuHKebr.htm](kingmaker-bestiary/ZoUIXzygFDuHKebr.htm)|Spirit of Stisshak|

## Liste des éléments vides ne pouvant pas être traduits

| Fichier   | Nom (EN)    | État |
|-----------|-------------|:----:|
|[0EO5vP2gCliAxLrF.htm](kingmaker-bestiary/0EO5vP2gCliAxLrF.htm)|Nok-Nok (Level 5)|vide|
|[0EQySWHT7D68yrrx.htm](kingmaker-bestiary/0EQySWHT7D68yrrx.htm)|Jubilost (Level 8)|vide|
|[0qc6h3jgLFNhX1tG.htm](kingmaker-bestiary/0qc6h3jgLFNhX1tG.htm)|Ballista Defense|vide|
|[1SEyDYO9l6mcFhoy.htm](kingmaker-bestiary/1SEyDYO9l6mcFhoy.htm)|Ekundayo (Level 1)|vide|
|[2deePNLiJcnSZQhd.htm](kingmaker-bestiary/2deePNLiJcnSZQhd.htm)|Test of Strength|vide|
|[3HnIluPWsKm3eEYB.htm](kingmaker-bestiary/3HnIluPWsKm3eEYB.htm)|Flooding Room|vide|
|[3kyS4KzEmG8Eyl4N.htm](kingmaker-bestiary/3kyS4KzEmG8Eyl4N.htm)|Thresholder Hermeticist|vide|
|[3xHHKI7PGQN218aL.htm](kingmaker-bestiary/3xHHKI7PGQN218aL.htm)|Eobald|vide|
|[48o307dry03xazvd.htm](kingmaker-bestiary/48o307dry03xazvd.htm)|Corax|vide|
|[554zNFkbYXBdmgAy.htm](kingmaker-bestiary/554zNFkbYXBdmgAy.htm)|Sister of the Bloodshot Eye|vide|
|[5npzwOJSQLSPhmx2.htm](kingmaker-bestiary/5npzwOJSQLSPhmx2.htm)|Pavetta Stroon-Drelev|vide|
|[5Xslhvyh3MSzgIXN.htm](kingmaker-bestiary/5Xslhvyh3MSzgIXN.htm)|Volodmyra|vide|
|[7bWjM5e9sKxaYbOw.htm](kingmaker-bestiary/7bWjM5e9sKxaYbOw.htm)|Thorn River Bandit|vide|
|[7PkMXEkBrxhTCw4s.htm](kingmaker-bestiary/7PkMXEkBrxhTCw4s.htm)|Horagnamon|vide|
|[8YJiyhWSWxMdGJV6.htm](kingmaker-bestiary/8YJiyhWSWxMdGJV6.htm)|The Power of Faith|vide|
|[9nGhRYarE5KkwOwc.htm](kingmaker-bestiary/9nGhRYarE5KkwOwc.htm)|Nilak|vide|
|[AGNRcnrftglAtlbN.htm](kingmaker-bestiary/AGNRcnrftglAtlbN.htm)|Gromog|vide|
|[AjpJNvt4nwzDAhqT.htm](kingmaker-bestiary/AjpJNvt4nwzDAhqT.htm)|Villamor Koth|vide|
|[aoICPc2KqxIP19m2.htm](kingmaker-bestiary/aoICPc2KqxIP19m2.htm)|Valerie (Level 9)|vide|
|[Awlsx9BtgkQ39x6N.htm](kingmaker-bestiary/Awlsx9BtgkQ39x6N.htm)|Aldori Sister|vide|
|[AXmQ8rUKsJFZUKb6.htm](kingmaker-bestiary/AXmQ8rUKsJFZUKb6.htm)|Collapsing Bridge|vide|
|[B0EZMtvXsIE3SYiu.htm](kingmaker-bestiary/B0EZMtvXsIE3SYiu.htm)|Locking Alarm|vide|
|[bHnGsHHymPUC1XGc.htm](kingmaker-bestiary/bHnGsHHymPUC1XGc.htm)|Auchs|vide|
|[BWI2CbtRo2lUcraX.htm](kingmaker-bestiary/BWI2CbtRo2lUcraX.htm)|Bloom Cultist|vide|
|[ce5vC049lfuXnPSy.htm](kingmaker-bestiary/ce5vC049lfuXnPSy.htm)|Orb Blast Trap|vide|
|[defXhBIK4TtoZXGK.htm](kingmaker-bestiary/defXhBIK4TtoZXGK.htm)|The Stag Lord|vide|
|[Dxn6t9aoWUwPn6CE.htm](kingmaker-bestiary/Dxn6t9aoWUwPn6CE.htm)|Virthad|vide|
|[E70Drr6CmSaJQ01v.htm](kingmaker-bestiary/E70Drr6CmSaJQ01v.htm)|Spiral Seal|vide|
|[Ea0Edd9XNA17yj9n.htm](kingmaker-bestiary/Ea0Edd9XNA17yj9n.htm)|Stag Lord Bandit|vide|
|[EI8wQc9kzooDHQoJ.htm](kingmaker-bestiary/EI8wQc9kzooDHQoJ.htm)|Weakened Floor|vide|
|[EM2mwJzZeu5rWIQS.htm](kingmaker-bestiary/EM2mwJzZeu5rWIQS.htm)|Ekundayo (Level 6)|vide|
|[etrlNmJRUWDKbKwG.htm](kingmaker-bestiary/etrlNmJRUWDKbKwG.htm)|Terrion Numesti|vide|
|[eVHkWtrdGJMVMob7.htm](kingmaker-bestiary/eVHkWtrdGJMVMob7.htm)|Dovan from Nisroch|vide|
|[fiBhPqM2lomswplt.htm](kingmaker-bestiary/fiBhPqM2lomswplt.htm)|Kereek|vide|
|[FkSxn7QSbVqA3dMy.htm](kingmaker-bestiary/FkSxn7QSbVqA3dMy.htm)|Test of Endurance|vide|
|[fs1iFoZmJF1iUWwX.htm](kingmaker-bestiary/fs1iFoZmJF1iUWwX.htm)|Davik Nettles|vide|
|[gdTJOwXUwwhKAzlR.htm](kingmaker-bestiary/gdTJOwXUwwhKAzlR.htm)|Nishkiv the Knife|vide|
|[GiRckUiMamrdgjXQ.htm](kingmaker-bestiary/GiRckUiMamrdgjXQ.htm)|Cutthroat Haunt|vide|
|[gNtXGquzueNJLvFJ.htm](kingmaker-bestiary/gNtXGquzueNJLvFJ.htm)|Dread Aura|vide|
|[GviFe34FuTpo8AT0.htm](kingmaker-bestiary/GviFe34FuTpo8AT0.htm)|Scalding Tar Lake|vide|
|[gXbDaY9ci2u22ptT.htm](kingmaker-bestiary/gXbDaY9ci2u22ptT.htm)|Ghostly Guard|vide|
|[hdFT5WIarw2Do3Sy.htm](kingmaker-bestiary/hdFT5WIarw2Do3Sy.htm)|Tristian (Level 1)|vide|
|[hFHdGOMRuhXIuAJo.htm](kingmaker-bestiary/hFHdGOMRuhXIuAJo.htm)|Test of Agility|vide|
|[hGLp8mkvx8J8DDL8.htm](kingmaker-bestiary/hGLp8mkvx8J8DDL8.htm)|Malgorzata Niska|vide|
|[hGQ4uxhxwtrnGfj0.htm](kingmaker-bestiary/hGQ4uxhxwtrnGfj0.htm)|Rickety Bridge|vide|
|[HkdsqQLNb9XwzYIH.htm](kingmaker-bestiary/HkdsqQLNb9XwzYIH.htm)|Tiger Lord|vide|
|[hLvxvLere6DruDLJ.htm](kingmaker-bestiary/hLvxvLere6DruDLJ.htm)|Annamede Belavarah|vide|
|[hQ0aR4oXug0yoTbT.htm](kingmaker-bestiary/hQ0aR4oXug0yoTbT.htm)|Unstable Pit|vide|
|[HRLmXOCMwyw5HAfw.htm](kingmaker-bestiary/HRLmXOCMwyw5HAfw.htm)|Camouflaged Spiked Pit|vide|
|[htHgsx1COWOfhE3D.htm](kingmaker-bestiary/htHgsx1COWOfhE3D.htm)|Thresholder Mystic|vide|
|[hW40C78kV4MBDs4v.htm](kingmaker-bestiary/hW40C78kV4MBDs4v.htm)|Teleport Trap|vide|
|[hX3uMf6KxgObJ9ec.htm](kingmaker-bestiary/hX3uMf6KxgObJ9ec.htm)|Valerie (Level 1)|vide|
|[I8pvGB7SiXQ6SnGn.htm](kingmaker-bestiary/I8pvGB7SiXQ6SnGn.htm)|Stygian Fires|vide|
|[jFA0q3g3MsFxS3xO.htm](kingmaker-bestiary/jFA0q3g3MsFxS3xO.htm)|Goblin Bat-Dog|vide|
|[JLfy3tVKEhLqT2j5.htm](kingmaker-bestiary/JLfy3tVKEhLqT2j5.htm)|Shelyn's Shame|vide|
|[jmH2nbWRJSYgzx5z.htm](kingmaker-bestiary/jmH2nbWRJSYgzx5z.htm)|Jin Durwhimmer|vide|
|[k9eR5UwCSrfpPPAv.htm](kingmaker-bestiary/k9eR5UwCSrfpPPAv.htm)|Hateful Hermit|vide|
|[KP7uf1E8CQgzTuQy.htm](kingmaker-bestiary/KP7uf1E8CQgzTuQy.htm)|Nugrah|vide|
|[KqWZZBucIAA1MzjF.htm](kingmaker-bestiary/KqWZZBucIAA1MzjF.htm)|Jubilost (Level 1)|vide|
|[Lm30gxMpkRJ2Y43d.htm](kingmaker-bestiary/Lm30gxMpkRJ2Y43d.htm)|Satinder Morne|vide|
|[lutSSPcXOzDDqIGj.htm](kingmaker-bestiary/lutSSPcXOzDDqIGj.htm)|Gurija|vide|
|[MABh0eh1VKh3izdf.htm](kingmaker-bestiary/MABh0eh1VKh3izdf.htm)|Eldritch Echoes|vide|
|[mgh7E2Mh0ZRaniCc.htm](kingmaker-bestiary/mgh7E2Mh0ZRaniCc.htm)|King Vesket|vide|
|[MgUBst46K9Hv0qsJ.htm](kingmaker-bestiary/MgUBst46K9Hv0qsJ.htm)|Amiri (Level 1, Kingmaker)|vide|
|[MlWXttLN4MjyzTGr.htm](kingmaker-bestiary/MlWXttLN4MjyzTGr.htm)|Dropping Web Trap|vide|
|[mMfMs5PlNYkwe55s.htm](kingmaker-bestiary/mMfMs5PlNYkwe55s.htm)|Alasen|vide|
|[mNKAaSBWbZHQRdo9.htm](kingmaker-bestiary/mNKAaSBWbZHQRdo9.htm)|Endless Struggle|vide|
|[MqxA7COGMXc5CNsZ.htm](kingmaker-bestiary/MqxA7COGMXc5CNsZ.htm)|Black Tear Cutthroat|vide|
|[myNEeBzXVmWbHk2X.htm](kingmaker-bestiary/myNEeBzXVmWbHk2X.htm)|Azure Lilies|vide|
|[Nx6vcagWXhYToIdC.htm](kingmaker-bestiary/Nx6vcagWXhYToIdC.htm)|Jurgrindor|vide|
|[O5EEnXdrKPcODuwh.htm](kingmaker-bestiary/O5EEnXdrKPcODuwh.htm)|Sir Fredero Sinnet|vide|
|[oBPdL0icCG5zmknB.htm](kingmaker-bestiary/oBPdL0icCG5zmknB.htm)|Glyph of Warding (Kingmaker)|vide|
|[oOPf7VG4tuMvzrgA.htm](kingmaker-bestiary/oOPf7VG4tuMvzrgA.htm)|Dog (Ekundayo's Companion)|vide|
|[oqlZNnsV8XMGn7JN.htm](kingmaker-bestiary/oqlZNnsV8XMGn7JN.htm)|Praise of Yog-Sothoth|vide|
|[ovjnD3aiPgRi2C7u.htm](kingmaker-bestiary/ovjnD3aiPgRi2C7u.htm)|The Gardener|vide|
|[pcct13qdrriJf3OL.htm](kingmaker-bestiary/pcct13qdrriJf3OL.htm)|Akuzhail|vide|
|[PK8yBANFyMqFZ3IY.htm](kingmaker-bestiary/PK8yBANFyMqFZ3IY.htm)|Zorek|vide|
|[QF2AIby1vQRq5b9E.htm](kingmaker-bestiary/QF2AIby1vQRq5b9E.htm)|Ameon Trask|vide|
|[qpkpPFlN0dSKJxaR.htm](kingmaker-bestiary/qpkpPFlN0dSKJxaR.htm)|Breeg's Traps|vide|
|[qS7JwIPqsjNKKALK.htm](kingmaker-bestiary/qS7JwIPqsjNKKALK.htm)|The First Faithful|vide|
|[RfIipAkVucpR0f0x.htm](kingmaker-bestiary/RfIipAkVucpR0f0x.htm)|Oleg|vide|
|[RL6cxasbeQMtCDvV.htm](kingmaker-bestiary/RL6cxasbeQMtCDvV.htm)|Chief Sootscale|vide|
|[RN3Fiz9AZzUuqb9z.htm](kingmaker-bestiary/RN3Fiz9AZzUuqb9z.htm)|Smoke-Filled Hallway|vide|
|[rsm5ZSX6oKJWQRvf.htm](kingmaker-bestiary/rsm5ZSX6oKJWQRvf.htm)|Nyrissa's Tempest|vide|
|[RTd4FwqGq8gBjdAO.htm](kingmaker-bestiary/RTd4FwqGq8gBjdAO.htm)|Melianse|vide|
|[rUmPNDqvptyp5Ob4.htm](kingmaker-bestiary/rUmPNDqvptyp5Ob4.htm)|Test of Tactics|vide|
|[S3jESLRaGeoaHG7t.htm](kingmaker-bestiary/S3jESLRaGeoaHG7t.htm)|Exploding Bloom Pods|vide|
|[SaSXYUpSWIFHIzET.htm](kingmaker-bestiary/SaSXYUpSWIFHIzET.htm)|Pitax Warden|vide|
|[sF7JAULkMpi1QMz2.htm](kingmaker-bestiary/sF7JAULkMpi1QMz2.htm)|Amiri (Level 11, Kingmaker)|vide|
|[sMCEMlNngFINMX8y.htm](kingmaker-bestiary/sMCEMlNngFINMX8y.htm)|Elga Verniex|vide|
|[sNqAajzeDA9BUkfa.htm](kingmaker-bestiary/sNqAajzeDA9BUkfa.htm)|Explosion Bear|vide|
|[so1XCkNLe2tuNbzW.htm](kingmaker-bestiary/so1XCkNLe2tuNbzW.htm)|Tristian (Level 10)|vide|
|[sY8owbk9TFeygFL9.htm](kingmaker-bestiary/sY8owbk9TFeygFL9.htm)|Nok-Nok (Level 1)|vide|
|[SZVTHAwTVXDwIOqC.htm](kingmaker-bestiary/SZVTHAwTVXDwIOqC.htm)|Gaetane|vide|
|[TAEgPgivuUOyuEU5.htm](kingmaker-bestiary/TAEgPgivuUOyuEU5.htm)|Falling Portcullis|vide|
|[TfwvPnETjUhEUQ82.htm](kingmaker-bestiary/TfwvPnETjUhEUQ82.htm)|Trapped Portcullis|vide|
|[TOW9azHYIoaNSavI.htm](kingmaker-bestiary/TOW9azHYIoaNSavI.htm)|Hidden Pressure Plate|vide|
|[UAlHSl6Cpujld1dx.htm](kingmaker-bestiary/UAlHSl6Cpujld1dx.htm)|Logger|vide|
|[UBwmJpIyIV65U7R2.htm](kingmaker-bestiary/UBwmJpIyIV65U7R2.htm)|Kressle|vide|
|[UfB3NfSgZIkN5Rjx.htm](kingmaker-bestiary/UfB3NfSgZIkN5Rjx.htm)|Cleansed Cultist|vide|
|[uQbzVX7DWDbxLONd.htm](kingmaker-bestiary/uQbzVX7DWDbxLONd.htm)|Paranoia Well|vide|
|[V7FoP8iIcehuiF20.htm](kingmaker-bestiary/V7FoP8iIcehuiF20.htm)|Cursed Guardian|vide|
|[VEWrS5u71szMrhs4.htm](kingmaker-bestiary/VEWrS5u71szMrhs4.htm)|Quintessa Maray|vide|
|[vff5VzjJpRMmg4Hx.htm](kingmaker-bestiary/vff5VzjJpRMmg4Hx.htm)|Cephal Lorentus|vide|
|[vONZlReozVCabXhq.htm](kingmaker-bestiary/vONZlReozVCabXhq.htm)|Collapsing Floor|vide|
|[wdoRpYImTQuVGZSQ.htm](kingmaker-bestiary/wdoRpYImTQuVGZSQ.htm)|Fionn|vide|
|[wMfnwJoLvxdZZAwr.htm](kingmaker-bestiary/wMfnwJoLvxdZZAwr.htm)|Drelev Guards|vide|
|[WmSZELnHCZ9g9Nq2.htm](kingmaker-bestiary/WmSZELnHCZ9g9Nq2.htm)|Svetlana|vide|
|[xMuLJmx51eBv9FcE.htm](kingmaker-bestiary/xMuLJmx51eBv9FcE.htm)|Jewel|vide|
|[XOiNuunTFGbDYeu2.htm](kingmaker-bestiary/XOiNuunTFGbDYeu2.htm)|Lights of the Lost|vide|
|[XydYK1C7RFDqKine.htm](kingmaker-bestiary/XydYK1C7RFDqKine.htm)|Imeckus Stroon|vide|
|[XzFifthQr0V5nEJe.htm](kingmaker-bestiary/XzFifthQr0V5nEJe.htm)|Ilora Nuski|vide|
|[y5W2rrHzQmeSE6LU.htm](kingmaker-bestiary/y5W2rrHzQmeSE6LU.htm)|Happs Bydon|vide|
|[YFnWOu9edWhj6vV6.htm](kingmaker-bestiary/YFnWOu9edWhj6vV6.htm)|Akiros Ismort|vide|
|[ylMXDs2f7C0YKdZY.htm](kingmaker-bestiary/ylMXDs2f7C0YKdZY.htm)|Xae|vide|
|[Ypxj2FdUPQqpWPf3.htm](kingmaker-bestiary/Ypxj2FdUPQqpWPf3.htm)|Ntavi|vide|
|[YyUVzTucO99JFDnm.htm](kingmaker-bestiary/YyUVzTucO99JFDnm.htm)|Rigged Climbing Loops|vide|
|[yzlu5YX7Oxo7TVvK.htm](kingmaker-bestiary/yzlu5YX7Oxo7TVvK.htm)|Prazil|vide|
|[z1Hk6z6RU4sF9aJU.htm](kingmaker-bestiary/z1Hk6z6RU4sF9aJU.htm)|Thresholder Disciple|vide|
|[ZaA9oQXOWne0IXSG.htm](kingmaker-bestiary/ZaA9oQXOWne0IXSG.htm)|Linzi (Level 1)|vide|
|[ZDt1rLh1VUFnHj5S.htm](kingmaker-bestiary/ZDt1rLh1VUFnHj5S.htm)|Lickweed|vide|
|[ZEpByav3dMCyvJJu.htm](kingmaker-bestiary/ZEpByav3dMCyvJJu.htm)|Void Pit|vide|
|[ZGVQcWl03kBIStS0.htm](kingmaker-bestiary/ZGVQcWl03kBIStS0.htm)|Kundal|vide|
|[ZiNVsXL5DJ4Ekd5v.htm](kingmaker-bestiary/ZiNVsXL5DJ4Ekd5v.htm)|Breath of Despair|vide|
|[ZlEhOqdwPDpU3jvO.htm](kingmaker-bestiary/ZlEhOqdwPDpU3jvO.htm)|Stinging Nettles|vide|
|[ZPm7WqgQTjysuTiT.htm](kingmaker-bestiary/ZPm7WqgQTjysuTiT.htm)|False Priestess|vide|
|[zvhw5Qx6gU7e39he.htm](kingmaker-bestiary/zvhw5Qx6gU7e39he.htm)|Linzi (Level 7)|vide|
|[zZrKk6wh7av4nU1z.htm](kingmaker-bestiary/zZrKk6wh7av4nU1z.htm)|Phantasmagoric Fog Trap|vide|

## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[1upVC05DY7YzxNsr.htm](kingmaker-bestiary/1upVC05DY7YzxNsr.htm)|Chew Spider|Araignée mâchouilleur|libre|
|[1UWbR2WkeP0kl1nQ.htm](kingmaker-bestiary/1UWbR2WkeP0kl1nQ.htm)|Giant Trapdoor Spider|Trappe d'araignée géante|libre|
|[1vsQlCVwa9kVmgRi.htm](kingmaker-bestiary/1vsQlCVwa9kVmgRi.htm)|Avatar of the Lantern King|Avatar du Roi lanterne|libre|
|[1ZKOTqqzeorIv7BB.htm](kingmaker-bestiary/1ZKOTqqzeorIv7BB.htm)|Trapdoor Ogre Spider|Trappe d'araignée ogre|libre|
|[2mkfF43tP8IGVfBz.htm](kingmaker-bestiary/2mkfF43tP8IGVfBz.htm)|Foras|Foras|libre|
