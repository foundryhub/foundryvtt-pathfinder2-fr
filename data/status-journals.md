# État de la traduction (journals)

 * **libre**: 3
 * **officielle**: 1


Dernière mise à jour: 2023-03-05 17:26 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[EEZvDB1Z7ezwaxIr.htm](journals/EEZvDB1Z7ezwaxIr.htm)|Domains|Domaines|libre|
|[S55aqwWIzpQRFhcq.htm](journals/S55aqwWIzpQRFhcq.htm)|GM Screen|Écran du MJ|libre|
|[vx5FGEG34AxI2dow.htm](journals/vx5FGEG34AxI2dow.htm)|Archetypes|Archétypes|officielle|
|[xtrW5GEtPPuXR6k2.htm](journals/xtrW5GEtPPuXR6k2.htm)|Deep Backgrounds|Backgrounds/Historiques approfondis|libre|
