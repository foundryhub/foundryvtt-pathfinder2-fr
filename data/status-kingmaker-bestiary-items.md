# État de la traduction (kingmaker-bestiary-items)

 * **aucune**: 537
 * **vide**: 369
 * **libre**: 30


Dernière mise à jour: 2023-03-05 17:26 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[0gCnA8mF12gNsipi.htm](kingmaker-bestiary-items/0gCnA8mF12gNsipi.htm)|Attack of Opportunity|
|[0ivwHldip6F8T0gA.htm](kingmaker-bestiary-items/0ivwHldip6F8T0gA.htm)|Attack of Opportunity (Fangs Only)|
|[0pHw83Qkw39rMcIx.htm](kingmaker-bestiary-items/0pHw83Qkw39rMcIx.htm)|Contingency|
|[0rYILxhM4TiZQaeA.htm](kingmaker-bestiary-items/0rYILxhM4TiZQaeA.htm)|Sudden Charge|
|[0TyxIsxCmJlWFTk6.htm](kingmaker-bestiary-items/0TyxIsxCmJlWFTk6.htm)|Evasion|
|[137OFGe9srUCXPpN.htm](kingmaker-bestiary-items/137OFGe9srUCXPpN.htm)|Closing Volley|
|[1bKTp47s4lhPF0Ix.htm](kingmaker-bestiary-items/1bKTp47s4lhPF0Ix.htm)|Darkvision|
|[1CVzuimA1fYyvCBq.htm](kingmaker-bestiary-items/1CVzuimA1fYyvCBq.htm)|Cruel Anatomist|
|[1fxLVAyfm2PZ7Zxg.htm](kingmaker-bestiary-items/1fxLVAyfm2PZ7Zxg.htm)|Attack of Opportunity|
|[1Ht8zfzmHL4tULd5.htm](kingmaker-bestiary-items/1Ht8zfzmHL4tULd5.htm)|Shield Rip and Chew|
|[1jsdM4qpD0smQsMV.htm](kingmaker-bestiary-items/1jsdM4qpD0smQsMV.htm)|Low-Light Vision|
|[1kX9bpy0yIk83pGw.htm](kingmaker-bestiary-items/1kX9bpy0yIk83pGw.htm)|Rejuvenation|
|[1V9pafG73kkZDmNM.htm](kingmaker-bestiary-items/1V9pafG73kkZDmNM.htm)|Negative Healing|
|[2c38HSvjdnaJgeI2.htm](kingmaker-bestiary-items/2c38HSvjdnaJgeI2.htm)|Awesome Blow|
|[2I9ZCEqUKoC0AAqQ.htm](kingmaker-bestiary-items/2I9ZCEqUKoC0AAqQ.htm)|At-Will Spells|
|[2IqWrJj9edOVS3Gp.htm](kingmaker-bestiary-items/2IqWrJj9edOVS3Gp.htm)|Triple Opportunity|
|[2pEzXozrOXVdVg0A.htm](kingmaker-bestiary-items/2pEzXozrOXVdVg0A.htm)|Life Bloom|
|[2prGxYGRkpCNqmdI.htm](kingmaker-bestiary-items/2prGxYGRkpCNqmdI.htm)|Shelyn's Thanks|
|[2YaktCYgfrnvuUbE.htm](kingmaker-bestiary-items/2YaktCYgfrnvuUbE.htm)|Slash Throat|
|[3AidCm7XJT968Swi.htm](kingmaker-bestiary-items/3AidCm7XJT968Swi.htm)|Choke|
|[3pzII2xLdduTjJrG.htm](kingmaker-bestiary-items/3pzII2xLdduTjJrG.htm)|Quickened Casting|
|[3Ygn2F1yeN3UTO2S.htm](kingmaker-bestiary-items/3Ygn2F1yeN3UTO2S.htm)|Pain|
|[3ZL7oyw6PKNMzEhf.htm](kingmaker-bestiary-items/3ZL7oyw6PKNMzEhf.htm)|Grant Desire|
|[45aANCYgNwQS8Ega.htm](kingmaker-bestiary-items/45aANCYgNwQS8Ega.htm)|Hooktongue Venom|
|[48zaDpaZFzUKLJHL.htm](kingmaker-bestiary-items/48zaDpaZFzUKLJHL.htm)|Sudden Charge|
|[4EsbaXbLbmfGNLmn.htm](kingmaker-bestiary-items/4EsbaXbLbmfGNLmn.htm)|Constant Spells|
|[4mK32Hq1EHPgHPW4.htm](kingmaker-bestiary-items/4mK32Hq1EHPgHPW4.htm)|Dread Striker|
|[4SgwfAqj7PXlo4NI.htm](kingmaker-bestiary-items/4SgwfAqj7PXlo4NI.htm)|Kob's Ruinous Strike|
|[4WbCPkQrjR5xvIUs.htm](kingmaker-bestiary-items/4WbCPkQrjR5xvIUs.htm)|Temporal Strikes|
|[5AVIS7Cfz12DClSG.htm](kingmaker-bestiary-items/5AVIS7Cfz12DClSG.htm)|Darkvision|
|[5EExyaDy8xArOFh0.htm](kingmaker-bestiary-items/5EExyaDy8xArOFh0.htm)|Fast Healing 10|
|[5G6Es9pOfgRiXtpy.htm](kingmaker-bestiary-items/5G6Es9pOfgRiXtpy.htm)|Power Attack|
|[5m1MoZTkyC5yL2jL.htm](kingmaker-bestiary-items/5m1MoZTkyC5yL2jL.htm)|Double Strike|
|[5nbfijONNe4T5HFM.htm](kingmaker-bestiary-items/5nbfijONNe4T5HFM.htm)|Draining Touch|
|[5O8YD9thn6myrfWy.htm](kingmaker-bestiary-items/5O8YD9thn6myrfWy.htm)|Pollen Burst|
|[5uF0UI8Ihj8qhfy5.htm](kingmaker-bestiary-items/5uF0UI8Ihj8qhfy5.htm)|Support Benefit|
|[5uI2L7wMgW34OuXg.htm](kingmaker-bestiary-items/5uI2L7wMgW34OuXg.htm)|Vengeful Rage|
|[5UTWzAXIsjMqThHw.htm](kingmaker-bestiary-items/5UTWzAXIsjMqThHw.htm)|Unfair Aim|
|[5w3fouC3WVuTBtWK.htm](kingmaker-bestiary-items/5w3fouC3WVuTBtWK.htm)|Trample|
|[5z29EkkGtyaSoB1a.htm](kingmaker-bestiary-items/5z29EkkGtyaSoB1a.htm)|Attack of Opportunity|
|[66WdJU8fGY3RHFZN.htm](kingmaker-bestiary-items/66WdJU8fGY3RHFZN.htm)|Blinded|
|[6aSDaFMEcz58NAqP.htm](kingmaker-bestiary-items/6aSDaFMEcz58NAqP.htm)|Despairing Breath|
|[6cprEKOmbMfF47Lu.htm](kingmaker-bestiary-items/6cprEKOmbMfF47Lu.htm)|Shame|
|[6gDHZ6VPfZDrKCnp.htm](kingmaker-bestiary-items/6gDHZ6VPfZDrKCnp.htm)|Sneak Attack|
|[6ILB5ny6782Ujs7o.htm](kingmaker-bestiary-items/6ILB5ny6782Ujs7o.htm)|Change Shape|
|[6kvbcAoDPilYf0fI.htm](kingmaker-bestiary-items/6kvbcAoDPilYf0fI.htm)|At-Will Spells|
|[6mO0FIwfW82Cszej.htm](kingmaker-bestiary-items/6mO0FIwfW82Cszej.htm)|Curse of the Weretiger|
|[6qAaC77CfOknbaMv.htm](kingmaker-bestiary-items/6qAaC77CfOknbaMv.htm)|Quick Recovery|
|[6qhXb73HUuWsXVlw.htm](kingmaker-bestiary-items/6qhXb73HUuWsXVlw.htm)|Aldori Riposte|
|[6QtfXfEtO9Jk9Ceg.htm](kingmaker-bestiary-items/6QtfXfEtO9Jk9Ceg.htm)|Greensight|
|[6xM67gan9MlmS1dN.htm](kingmaker-bestiary-items/6xM67gan9MlmS1dN.htm)|Tilt and Roll|
|[6YFC1LK4zBkiKg1N.htm](kingmaker-bestiary-items/6YFC1LK4zBkiKg1N.htm)|Beyond the Barrier|
|[6znbpXWxevbdvTl2.htm](kingmaker-bestiary-items/6znbpXWxevbdvTl2.htm)|Rip and Rend|
|[70wPP2Jajb7NN04d.htm](kingmaker-bestiary-items/70wPP2Jajb7NN04d.htm)|Change Shape|
|[73DVUYU49Oqo474k.htm](kingmaker-bestiary-items/73DVUYU49Oqo474k.htm)|Nymph's Tragedy|
|[73zfyyrBBuzM9Dh9.htm](kingmaker-bestiary-items/73zfyyrBBuzM9Dh9.htm)|Terrifying Croak|
|[77XHZv9nEQwJTaiO.htm](kingmaker-bestiary-items/77XHZv9nEQwJTaiO.htm)|Drain Oculus|
|[7ff6eVjIgZUezVKm.htm](kingmaker-bestiary-items/7ff6eVjIgZUezVKm.htm)|Jabberwock Bloodline|
|[7fVgYQK2ZSqvG5KV.htm](kingmaker-bestiary-items/7fVgYQK2ZSqvG5KV.htm)|Twin Feint|
|[7fwg0KKHJSx50lBW.htm](kingmaker-bestiary-items/7fwg0KKHJSx50lBW.htm)|Battlefield Command|
|[7iMYJiq8cs6BSSdN.htm](kingmaker-bestiary-items/7iMYJiq8cs6BSSdN.htm)|Nature's Edge|
|[7j0otQgc8z5zzALv.htm](kingmaker-bestiary-items/7j0otQgc8z5zzALv.htm)|Site Bound|
|[7Lzy2YrsBlDo3O23.htm](kingmaker-bestiary-items/7Lzy2YrsBlDo3O23.htm)|Regeneration 25 (Deactivated by Acid or Fire)|
|[7N9zMdYcPW2J4Hzt.htm](kingmaker-bestiary-items/7N9zMdYcPW2J4Hzt.htm)|Nyrissa's Favor|
|[7O2rhLBXoMyayaTV.htm](kingmaker-bestiary-items/7O2rhLBXoMyayaTV.htm)|Distracting Yipping|
|[7omOmpvwPwKMmBfB.htm](kingmaker-bestiary-items/7omOmpvwPwKMmBfB.htm)|Intimidating Strike|
|[7pIVt1wT5IWY1dOP.htm](kingmaker-bestiary-items/7pIVt1wT5IWY1dOP.htm)|Flurry of Blows|
|[7QpzxU7FsF8lN6FP.htm](kingmaker-bestiary-items/7QpzxU7FsF8lN6FP.htm)|Shameful Memories|
|[7s2ezouHGaqFCuhW.htm](kingmaker-bestiary-items/7s2ezouHGaqFCuhW.htm)|Corrupting Gaze|
|[7SDmYIdMAER2dLGK.htm](kingmaker-bestiary-items/7SDmYIdMAER2dLGK.htm)|Invoke Stisshak|
|[7tW8esj9IVdwLqwS.htm](kingmaker-bestiary-items/7tW8esj9IVdwLqwS.htm)|Primal Weapon|
|[7Y8YE3i8NiZAOPkt.htm](kingmaker-bestiary-items/7Y8YE3i8NiZAOPkt.htm)|Collapse|
|[7ZhJ9zRnsiGY2LMq.htm](kingmaker-bestiary-items/7ZhJ9zRnsiGY2LMq.htm)|Change Shape|
|[88hwkEhv8cx338VM.htm](kingmaker-bestiary-items/88hwkEhv8cx338VM.htm)|Attack of Opportunity|
|[89ANJyvglhWZiKZv.htm](kingmaker-bestiary-items/89ANJyvglhWZiKZv.htm)|Deploy Flytraps|
|[8fXr0ygvqmKMJQ7y.htm](kingmaker-bestiary-items/8fXr0ygvqmKMJQ7y.htm)|Ambush Strike|
|[8GzTttsR8tPUpy3a.htm](kingmaker-bestiary-items/8GzTttsR8tPUpy3a.htm)|Manifest Fetch Weapon|
|[8k6w6YpDwJuyU7Lj.htm](kingmaker-bestiary-items/8k6w6YpDwJuyU7Lj.htm)|Twin Feint|
|[8x5ve1XWJQ7yIA7F.htm](kingmaker-bestiary-items/8x5ve1XWJQ7yIA7F.htm)|Attack of Opportunity|
|[907AYCZJGYIzNC2a.htm](kingmaker-bestiary-items/907AYCZJGYIzNC2a.htm)|Quickened Casting|
|[9a3n2YfibJlQxctv.htm](kingmaker-bestiary-items/9a3n2YfibJlQxctv.htm)|Unseen Sight|
|[9AVnb90miimzc7rS.htm](kingmaker-bestiary-items/9AVnb90miimzc7rS.htm)|Sneak Attack|
|[9khUkSjPZRFIilUZ.htm](kingmaker-bestiary-items/9khUkSjPZRFIilUZ.htm)|Rend|
|[9ODQkquyZvS7PFNK.htm](kingmaker-bestiary-items/9ODQkquyZvS7PFNK.htm)|Knockdown|
|[9qzHAK95CCa74dtM.htm](kingmaker-bestiary-items/9qzHAK95CCa74dtM.htm)|Absorb the Bloom|
|[9ST6BDxKH8vXvpEq.htm](kingmaker-bestiary-items/9ST6BDxKH8vXvpEq.htm)|Attack of Opportunity (Worm Jaws Only)|
|[9uNCXDfw6yzI9yL5.htm](kingmaker-bestiary-items/9uNCXDfw6yzI9yL5.htm)|Release Spell|
|[9VA9Qjcx5YJxs4P1.htm](kingmaker-bestiary-items/9VA9Qjcx5YJxs4P1.htm)|Jangle the Chain|
|[A8rxcOUACAmsVmTH.htm](kingmaker-bestiary-items/A8rxcOUACAmsVmTH.htm)|Command the Undead|
|[AaMjBE4xQc5ZqroJ.htm](kingmaker-bestiary-items/AaMjBE4xQc5ZqroJ.htm)|Sneak Attack|
|[aan6tOAkYHhETUdL.htm](kingmaker-bestiary-items/aan6tOAkYHhETUdL.htm)|Deadly Aim|
|[AB7mMjajOUtvqlxD.htm](kingmaker-bestiary-items/AB7mMjajOUtvqlxD.htm)|Evasion|
|[AfvMfBPmJB8eecwr.htm](kingmaker-bestiary-items/AfvMfBPmJB8eecwr.htm)|Sneak Attack|
|[afWpHJ6qqzsRORUX.htm](kingmaker-bestiary-items/afWpHJ6qqzsRORUX.htm)|Steady Spellcasting|
|[AFz5oCbdGYZQVrDD.htm](kingmaker-bestiary-items/AFz5oCbdGYZQVrDD.htm)|Agonized Wail|
|[Ag07kBthiVumPkcV.htm](kingmaker-bestiary-items/Ag07kBthiVumPkcV.htm)|Focus Hatred|
|[ai3N8T91JXo3Hx5e.htm](kingmaker-bestiary-items/ai3N8T91JXo3Hx5e.htm)|Negative Healing|
|[aLcLFpXMpgdiDrzO.htm](kingmaker-bestiary-items/aLcLFpXMpgdiDrzO.htm)|Symbiotic Swarm|
|[aM4rXRFUMH2Ufswh.htm](kingmaker-bestiary-items/aM4rXRFUMH2Ufswh.htm)|Improved Grab|
|[AMuN6VT4RTloaWQk.htm](kingmaker-bestiary-items/AMuN6VT4RTloaWQk.htm)|Bomber|
|[ao5De1dHxQjNJ8za.htm](kingmaker-bestiary-items/ao5De1dHxQjNJ8za.htm)|Souleater|
|[AoTJ2NwBcO5rItKq.htm](kingmaker-bestiary-items/AoTJ2NwBcO5rItKq.htm)|Darkvision|
|[apQdVdQWSEbQzCw2.htm](kingmaker-bestiary-items/apQdVdQWSEbQzCw2.htm)|Focus Gaze|
|[aQsTI9u9o9zMbaHa.htm](kingmaker-bestiary-items/aQsTI9u9o9zMbaHa.htm)|Resolve|
|[AtiJbhL8pY43Rvv3.htm](kingmaker-bestiary-items/AtiJbhL8pY43Rvv3.htm)|Smash Kneecaps|
|[auvnM9kZNqXdqAIR.htm](kingmaker-bestiary-items/auvnM9kZNqXdqAIR.htm)|Outside of Time|
|[AvThDzMVhWejEVsL.htm](kingmaker-bestiary-items/AvThDzMVhWejEVsL.htm)|Low-Light Vision|
|[aW63u7ufl1uEOBQh.htm](kingmaker-bestiary-items/aW63u7ufl1uEOBQh.htm)|Furious Finish|
|[AwAeIIlDkmCoVoca.htm](kingmaker-bestiary-items/AwAeIIlDkmCoVoca.htm)|Regeneration 20 (Deactivated by Acid or Fire)|
|[AwKlC3yGdtDzjPWM.htm](kingmaker-bestiary-items/AwKlC3yGdtDzjPWM.htm)|Masterful Hunter|
|[AX9lSAPNk2PJCat1.htm](kingmaker-bestiary-items/AX9lSAPNk2PJCat1.htm)|Outside of Time|
|[ay85G9ub5DX6nf12.htm](kingmaker-bestiary-items/ay85G9ub5DX6nf12.htm)|No Escape|
|[Az48BJ0aNRNzQotH.htm](kingmaker-bestiary-items/Az48BJ0aNRNzQotH.htm)|Wounded Arm|
|[aZke0WE5mklrrBK7.htm](kingmaker-bestiary-items/aZke0WE5mklrrBK7.htm)|Draining Inhalation|
|[b07B1akTo84FKZUe.htm](kingmaker-bestiary-items/b07B1akTo84FKZUe.htm)|Attack of Opportunity|
|[BA3fqrqPyWZ59YSs.htm](kingmaker-bestiary-items/BA3fqrqPyWZ59YSs.htm)|Deadeye's Shame|
|[bekh1R8LAluGLNaA.htm](kingmaker-bestiary-items/bekh1R8LAluGLNaA.htm)|Low-Light Vision|
|[BFpgenVI0pBLyV0S.htm](kingmaker-bestiary-items/BFpgenVI0pBLyV0S.htm)|Infused Items|
|[BKsKbanuSDh8osp9.htm](kingmaker-bestiary-items/BKsKbanuSDh8osp9.htm)|Hope or Despair|
|[BltNCa3ZEsbxzhf7.htm](kingmaker-bestiary-items/BltNCa3ZEsbxzhf7.htm)|Buck|
|[bMbUyfSqVR9lWDB7.htm](kingmaker-bestiary-items/bMbUyfSqVR9lWDB7.htm)|Rejuvenation|
|[BO3hN1F0bkrNu60q.htm](kingmaker-bestiary-items/BO3hN1F0bkrNu60q.htm)|Fast Healing 15|
|[bOpAJRLpemL4HwNd.htm](kingmaker-bestiary-items/bOpAJRLpemL4HwNd.htm)|Swamp Stride|
|[BOya1lhwnf2RNvGy.htm](kingmaker-bestiary-items/BOya1lhwnf2RNvGy.htm)|Juggernaut|
|[BtJvNGAGxO2VOuXB.htm](kingmaker-bestiary-items/BtJvNGAGxO2VOuXB.htm)|Mind-Numbing Breath|
|[bvBZPt5ocFCNJflm.htm](kingmaker-bestiary-items/bvBZPt5ocFCNJflm.htm)|Ready and Load|
|[BxuCFIo6yswNsArJ.htm](kingmaker-bestiary-items/BxuCFIo6yswNsArJ.htm)|Hunting Cry|
|[c1sQmMq46Aeg6en6.htm](kingmaker-bestiary-items/c1sQmMq46Aeg6en6.htm)|Axe Specialization|
|[c2r3T8G5zXq6UOBS.htm](kingmaker-bestiary-items/c2r3T8G5zXq6UOBS.htm)|Mobility|
|[C31XU8XsrO1R112R.htm](kingmaker-bestiary-items/C31XU8XsrO1R112R.htm)|Focused Target|
|[c3vIbIBnVpy9fjsE.htm](kingmaker-bestiary-items/c3vIbIBnVpy9fjsE.htm)|Rage|
|[C9Ds23BokMiIUMi5.htm](kingmaker-bestiary-items/C9Ds23BokMiIUMi5.htm)|Release Jewel|
|[C9ESEAYQaJUciIba.htm](kingmaker-bestiary-items/C9ESEAYQaJUciIba.htm)|Sneak Attack|
|[cA2cBXkewzB0Fwtj.htm](kingmaker-bestiary-items/cA2cBXkewzB0Fwtj.htm)|Change Shape|
|[CBuSOJ26ZXTgXAze.htm](kingmaker-bestiary-items/CBuSOJ26ZXTgXAze.htm)|Constant Spells|
|[CcrqR1iP71tzTA5V.htm](kingmaker-bestiary-items/CcrqR1iP71tzTA5V.htm)|Collapse|
|[cD3KZDeLep4rM0aB.htm](kingmaker-bestiary-items/cD3KZDeLep4rM0aB.htm)|Darkvision|
|[CeBgXo66gTiufqL5.htm](kingmaker-bestiary-items/CeBgXo66gTiufqL5.htm)|Nature's Edge|
|[Ch2vwQBo7w7hd8ou.htm](kingmaker-bestiary-items/Ch2vwQBo7w7hd8ou.htm)|Harrying Swordfighter|
|[cJIYg8gvnBbvoiuI.htm](kingmaker-bestiary-items/cJIYg8gvnBbvoiuI.htm)|Mobility|
|[CJravyxL4AFAla1i.htm](kingmaker-bestiary-items/CJravyxL4AFAla1i.htm)|Curse of the Wererat|
|[cQluYUjYjTeHDhTp.htm](kingmaker-bestiary-items/cQluYUjYjTeHDhTp.htm)|Spinning Sweep|
|[CquGBdxcC31YxWhu.htm](kingmaker-bestiary-items/CquGBdxcC31YxWhu.htm)|Rotting Stench|
|[D6qVDUiJjJhA5CIX.htm](kingmaker-bestiary-items/D6qVDUiJjJhA5CIX.htm)|Wild Reincarnation|
|[d7qSq3HVLR2AAayU.htm](kingmaker-bestiary-items/d7qSq3HVLR2AAayU.htm)|Low-Light Vision|
|[d8wBT2ExQFfDG2Mt.htm](kingmaker-bestiary-items/d8wBT2ExQFfDG2Mt.htm)|Wild Hunt Link|
|[DbU5apk8FlJos4xz.htm](kingmaker-bestiary-items/DbU5apk8FlJos4xz.htm)|Critical Debilitating Strike|
|[DFgNddcsAbZVlWSH.htm](kingmaker-bestiary-items/DFgNddcsAbZVlWSH.htm)|Pounce|
|[dJzydVxO1TtPnaho.htm](kingmaker-bestiary-items/dJzydVxO1TtPnaho.htm)|Deny Advantage|
|[dNOthX618UT1hZN6.htm](kingmaker-bestiary-items/dNOthX618UT1hZN6.htm)|Distracting Shot|
|[DoCmnJxIOLLTb9Xm.htm](kingmaker-bestiary-items/DoCmnJxIOLLTb9Xm.htm)|Greater Acid Flask|
|[DOviMcCQeyu6rmjx.htm](kingmaker-bestiary-items/DOviMcCQeyu6rmjx.htm)|Attack of Opportunity|
|[DrW6hlIWaGCF1pU4.htm](kingmaker-bestiary-items/DrW6hlIWaGCF1pU4.htm)|Juggernaut|
|[dtTve1TvHU0al6Es.htm](kingmaker-bestiary-items/dtTve1TvHU0al6Es.htm)|Darkvision|
|[du4VcQ2P0kOpRVrS.htm](kingmaker-bestiary-items/du4VcQ2P0kOpRVrS.htm)|Buck|
|[DuDuSE2EKfZt9r6m.htm](kingmaker-bestiary-items/DuDuSE2EKfZt9r6m.htm)|Quickened Casting|
|[DVsGmUvb4tqHJrCw.htm](kingmaker-bestiary-items/DVsGmUvb4tqHJrCw.htm)|Rejuvenation|
|[dWpKdYaRxbAJiHSi.htm](kingmaker-bestiary-items/dWpKdYaRxbAJiHSi.htm)|Bloodbird|
|[E2BfR5UhdImAFitO.htm](kingmaker-bestiary-items/E2BfR5UhdImAFitO.htm)|Shyka's Judgement|
|[e2cSvAso2wePkLAK.htm](kingmaker-bestiary-items/e2cSvAso2wePkLAK.htm)|Korog's Command|
|[EaWA2WkRjbpm2GiI.htm](kingmaker-bestiary-items/EaWA2WkRjbpm2GiI.htm)|Duplicate Victim|
|[ECXG4lnlaSBXuqcP.htm](kingmaker-bestiary-items/ECXG4lnlaSBXuqcP.htm)|Defensive Slice|
|[EdoAyjfImWlnNF3Z.htm](kingmaker-bestiary-items/EdoAyjfImWlnNF3Z.htm)|End the Charade|
|[Eeclk1UrcRUe4od5.htm](kingmaker-bestiary-items/Eeclk1UrcRUe4od5.htm)|Lock and Shriek|
|[EGY18Yebn9ZVwEbP.htm](kingmaker-bestiary-items/EGY18Yebn9ZVwEbP.htm)|Rat Empathy|
|[EgymgxzMTQGUvdBD.htm](kingmaker-bestiary-items/EgymgxzMTQGUvdBD.htm)|Telekinetic Assault|
|[eHtfWWTVUSw5Zu4F.htm](kingmaker-bestiary-items/eHtfWWTVUSw5Zu4F.htm)|Forceful Focus|
|[ek9L2OPkQLBaP0Ml.htm](kingmaker-bestiary-items/ek9L2OPkQLBaP0Ml.htm)|Bound to the Mortal Night|
|[emb5f7dGdRTiUTnN.htm](kingmaker-bestiary-items/emb5f7dGdRTiUTnN.htm)|Scent (Imprecise) 30 feet|
|[eMFelq83Tb6uIwqb.htm](kingmaker-bestiary-items/eMFelq83Tb6uIwqb.htm)|Moon Frenzy|
|[Enk2lR0Vry7rabGe.htm](kingmaker-bestiary-items/Enk2lR0Vry7rabGe.htm)|Site Bound|
|[eQQryZfmKwV4meJL.htm](kingmaker-bestiary-items/eQQryZfmKwV4meJL.htm)|Quickened Casting|
|[EtUgChhYGZ37rp9z.htm](kingmaker-bestiary-items/EtUgChhYGZ37rp9z.htm)|Sure Possession|
|[eUDWR9NZHLWYc7Dj.htm](kingmaker-bestiary-items/eUDWR9NZHLWYc7Dj.htm)|Darkvision|
|[EVaLrmDugpw1gfO1.htm](kingmaker-bestiary-items/EVaLrmDugpw1gfO1.htm)|Scent (Imprecise) 30 feet|
|[EwwHk83vJUlZ0cbV.htm](kingmaker-bestiary-items/EwwHk83vJUlZ0cbV.htm)|Improved Grab|
|[exrZ9BjFfwPvWYPf.htm](kingmaker-bestiary-items/exrZ9BjFfwPvWYPf.htm)|Ripping Grab|
|[Ez68XkpqwOnuPfr3.htm](kingmaker-bestiary-items/Ez68XkpqwOnuPfr3.htm)|Shield Block|
|[eznh9ydPPecUq1K1.htm](kingmaker-bestiary-items/eznh9ydPPecUq1K1.htm)|Steady Spellcasting|
|[F0yeghQAPNgRWKCf.htm](kingmaker-bestiary-items/F0yeghQAPNgRWKCf.htm)|Deafening Cry|
|[f42wFnhs1khcDf8G.htm](kingmaker-bestiary-items/f42wFnhs1khcDf8G.htm)|Fast Healing 20|
|[F4qi1llZbd9QZy4d.htm](kingmaker-bestiary-items/F4qi1llZbd9QZy4d.htm)|Raise a Shield|
|[f7AUQc1tVLR8FHEA.htm](kingmaker-bestiary-items/f7AUQc1tVLR8FHEA.htm)|Fog Vision|
|[fbNFZNeEcQgHVKW4.htm](kingmaker-bestiary-items/fbNFZNeEcQgHVKW4.htm)|Darkvision|
|[FCU5JyYgiHDot5rm.htm](kingmaker-bestiary-items/FCU5JyYgiHDot5rm.htm)|Swarming Bites|
|[FEC49j9OjVBivyUX.htm](kingmaker-bestiary-items/FEC49j9OjVBivyUX.htm)|Planar Acclimation|
|[FktubLHfxuouP7QK.htm](kingmaker-bestiary-items/FktubLHfxuouP7QK.htm)|Stubborn Conviction|
|[FMwhWB2KeMuU3kYQ.htm](kingmaker-bestiary-items/FMwhWB2KeMuU3kYQ.htm)|Scent (Imprecise) 30 feet|
|[FQyPQNxKCzuNCQHu.htm](kingmaker-bestiary-items/FQyPQNxKCzuNCQHu.htm)|Wolf Empathy|
|[fRaI1Z66oWzSAz2Q.htm](kingmaker-bestiary-items/fRaI1Z66oWzSAz2Q.htm)|Hit 'Em Hard|
|[FrmCI6DzaoSpGyxI.htm](kingmaker-bestiary-items/FrmCI6DzaoSpGyxI.htm)|Twin Parry|
|[fS0rXdbVRA1IMM8E.htm](kingmaker-bestiary-items/fS0rXdbVRA1IMM8E.htm)|Attack of Opportunity|
|[fTVvztwtM7PZgob6.htm](kingmaker-bestiary-items/fTVvztwtM7PZgob6.htm)|Grab|
|[FuvkjEpm8ZZnjZ46.htm](kingmaker-bestiary-items/FuvkjEpm8ZZnjZ46.htm)|Sneak Attack|
|[Fv9HoAW6MyuvLWBs.htm](kingmaker-bestiary-items/Fv9HoAW6MyuvLWBs.htm)|At-Will Spells|
|[Fw6GAT5RmaN6VMII.htm](kingmaker-bestiary-items/Fw6GAT5RmaN6VMII.htm)|Giant's Lunge|
|[FwhFzYJLEkQ3J68c.htm](kingmaker-bestiary-items/FwhFzYJLEkQ3J68c.htm)|Low-Light Vision|
|[FwN998JxnTCCkJw3.htm](kingmaker-bestiary-items/FwN998JxnTCCkJw3.htm)|Reveal the Enemy|
|[fXlB9FRj3C3aY3Vk.htm](kingmaker-bestiary-items/fXlB9FRj3C3aY3Vk.htm)|Form Control|
|[fYhoTq6AnWKAwKBM.htm](kingmaker-bestiary-items/fYhoTq6AnWKAwKBM.htm)|Release the Inmost Worm|
|[FZAkeuRb3tyRnlmH.htm](kingmaker-bestiary-items/FZAkeuRb3tyRnlmH.htm)|Hurried Retreat|
|[G0x7fr9ZPMuqw2RG.htm](kingmaker-bestiary-items/G0x7fr9ZPMuqw2RG.htm)|Weeping Aura|
|[GAwPcYxItK5M24Ve.htm](kingmaker-bestiary-items/GAwPcYxItK5M24Ve.htm)|Scent (Imprecise) 30 feet|
|[GcSM5ZkG8OONvY0G.htm](kingmaker-bestiary-items/GcSM5ZkG8OONvY0G.htm)|Fey Conduit|
|[gfH6Km7ijznOKHzF.htm](kingmaker-bestiary-items/gfH6Km7ijznOKHzF.htm)|Negative Healing|
|[ghqAZgEVPVGBLtZW.htm](kingmaker-bestiary-items/ghqAZgEVPVGBLtZW.htm)|Tongue Grab|
|[GMKTkSGoth8sggbH.htm](kingmaker-bestiary-items/GMKTkSGoth8sggbH.htm)|Call Glaive|
|[gsoYzQxRygC654Xf.htm](kingmaker-bestiary-items/gsoYzQxRygC654Xf.htm)|Pounce|
|[GxlQBNruDU25fJkQ.htm](kingmaker-bestiary-items/GxlQBNruDU25fJkQ.htm)|Open Doors|
|[GZ9qL58QMmBtlHul.htm](kingmaker-bestiary-items/GZ9qL58QMmBtlHul.htm)|Electric Healing|
|[gzziZdYQCjJ1pCM1.htm](kingmaker-bestiary-items/gzziZdYQCjJ1pCM1.htm)|Reminder of Doom|
|[h2wSXBnK6CA96ypS.htm](kingmaker-bestiary-items/h2wSXBnK6CA96ypS.htm)|Reach Spell|
|[h4yqrCg7sP5DCU4F.htm](kingmaker-bestiary-items/h4yqrCg7sP5DCU4F.htm)|Darkvision|
|[H8mIfS4BQ0XpEAtY.htm](kingmaker-bestiary-items/H8mIfS4BQ0XpEAtY.htm)|Launch Marbles|
|[H8UomLORlnQErtR4.htm](kingmaker-bestiary-items/H8UomLORlnQErtR4.htm)|Gnawing Bite|
|[hhOHcOSPkhLdvric.htm](kingmaker-bestiary-items/hhOHcOSPkhLdvric.htm)|Confusing Gaze|
|[hID13CF39qYYQ2rX.htm](kingmaker-bestiary-items/hID13CF39qYYQ2rX.htm)|Collapse|
|[HiL2TCMI2Rzmy2SW.htm](kingmaker-bestiary-items/HiL2TCMI2Rzmy2SW.htm)|Juggernaut|
|[HiZTEnboQEoek6kL.htm](kingmaker-bestiary-items/HiZTEnboQEoek6kL.htm)|Three-Headed Strike|
|[HKEKbaIpMla2LGS8.htm](kingmaker-bestiary-items/HKEKbaIpMla2LGS8.htm)|Sneak Attack|
|[hO1dPgyMTWEGJRxH.htm](kingmaker-bestiary-items/hO1dPgyMTWEGJRxH.htm)|Rejuvenation|
|[HQAGq6saSYHzJC0I.htm](kingmaker-bestiary-items/HQAGq6saSYHzJC0I.htm)|Three Headed|
|[hqm0z6fZAXCCTDOs.htm](kingmaker-bestiary-items/hqm0z6fZAXCCTDOs.htm)|Wild Stride|
|[HrP24kDK1jVss45v.htm](kingmaker-bestiary-items/HrP24kDK1jVss45v.htm)|Fast Healing 10|
|[HtScT1S9mxNqMylg.htm](kingmaker-bestiary-items/HtScT1S9mxNqMylg.htm)|Fast Healing 15|
|[HWNQMuf5GV8rRSMu.htm](kingmaker-bestiary-items/HWNQMuf5GV8rRSMu.htm)|Frightful Presence|
|[HYAfgOkdktmrrKGf.htm](kingmaker-bestiary-items/HYAfgOkdktmrrKGf.htm)|Stygian Fire|
|[HYnUw0h1bJQMkzBc.htm](kingmaker-bestiary-items/HYnUw0h1bJQMkzBc.htm)|Planar Acclimation|
|[hznpxgtC8SziycgN.htm](kingmaker-bestiary-items/hznpxgtC8SziycgN.htm)|Hatchet Onslaught|
|[HZvbzYmbccVoGV1B.htm](kingmaker-bestiary-items/HZvbzYmbccVoGV1B.htm)|Maul Mastery|
|[HZwLWbZdPHnymsUr.htm](kingmaker-bestiary-items/HZwLWbZdPHnymsUr.htm)|Darkvision|
|[i42oMxLUkfr8rkMc.htm](kingmaker-bestiary-items/i42oMxLUkfr8rkMc.htm)|Open Doors|
|[i5DVxzz2vN9zcxsC.htm](kingmaker-bestiary-items/i5DVxzz2vN9zcxsC.htm)|Attack of Opportunity|
|[IDIM2wkSDDgMI06V.htm](kingmaker-bestiary-items/IDIM2wkSDDgMI06V.htm)|Grasping Roots|
|[iE3cMVqsLzS8aqvH.htm](kingmaker-bestiary-items/iE3cMVqsLzS8aqvH.htm)|Rejuvenation|
|[ifhXNlwgfGtUNABs.htm](kingmaker-bestiary-items/ifhXNlwgfGtUNABs.htm)|Reactive Charge|
|[iGnRoOsUlKx1zO7Q.htm](kingmaker-bestiary-items/iGnRoOsUlKx1zO7Q.htm)|Death Throes|
|[ihGm6kH3jaNxYb3p.htm](kingmaker-bestiary-items/ihGm6kH3jaNxYb3p.htm)|Tremorsense (Precise) 30 feet|
|[ik0FmQbY7s9wzZh2.htm](kingmaker-bestiary-items/ik0FmQbY7s9wzZh2.htm)|Wild Shape|
|[ILS10PAagocQkyqT.htm](kingmaker-bestiary-items/ILS10PAagocQkyqT.htm)|Hunt Prey|
|[ImYiua3KKxG5XU61.htm](kingmaker-bestiary-items/ImYiua3KKxG5XU61.htm)|Play the Pipes|
|[ImzpAPq1SgMcf7t3.htm](kingmaker-bestiary-items/ImzpAPq1SgMcf7t3.htm)|Hunt Prey|
|[inAbFfjyYxWyFk2e.htm](kingmaker-bestiary-items/inAbFfjyYxWyFk2e.htm)|Prostrate|
|[Io2JD6hMiVboalmM.htm](kingmaker-bestiary-items/Io2JD6hMiVboalmM.htm)|Cunning|
|[iOA6wNMnIzBStwhB.htm](kingmaker-bestiary-items/iOA6wNMnIzBStwhB.htm)|Return to Pavetta|
|[ipARSA7VFfyApjl1.htm](kingmaker-bestiary-items/ipARSA7VFfyApjl1.htm)|Throw Rock|
|[iqKVeVAvsVEgW29e.htm](kingmaker-bestiary-items/iqKVeVAvsVEgW29e.htm)|Rejuvenation|
|[iVaCyzg2MMKJ1SIu.htm](kingmaker-bestiary-items/iVaCyzg2MMKJ1SIu.htm)|Darkvision|
|[IW0bpDD32dNAHnum.htm](kingmaker-bestiary-items/IW0bpDD32dNAHnum.htm)|Frumious Charge|
|[Iyw7GZBX5oMU1kR0.htm](kingmaker-bestiary-items/Iyw7GZBX5oMU1kR0.htm)|Darkvision|
|[IzVf6cehi9NajoXy.htm](kingmaker-bestiary-items/IzVf6cehi9NajoXy.htm)|Trident Twist|
|[J3lzyo3f2n6RNBG4.htm](kingmaker-bestiary-items/J3lzyo3f2n6RNBG4.htm)|Rushing Water|
|[jAX00kRCiI1ahLJA.htm](kingmaker-bestiary-items/jAX00kRCiI1ahLJA.htm)|Bow Specialist|
|[jBAYmOeepk3w4EgS.htm](kingmaker-bestiary-items/jBAYmOeepk3w4EgS.htm)|Rend|
|[jga4jzrMzJRkNouq.htm](kingmaker-bestiary-items/jga4jzrMzJRkNouq.htm)|Formation Fighters|
|[JjphR7198vaXiAJo.htm](kingmaker-bestiary-items/JjphR7198vaXiAJo.htm)|Defensive Roll|
|[jlJLCgnS5JMrSDgN.htm](kingmaker-bestiary-items/jlJLCgnS5JMrSDgN.htm)|Tear Free|
|[jmL1vjBAG3jha7pe.htm](kingmaker-bestiary-items/jmL1vjBAG3jha7pe.htm)|Stabbing Fit|
|[Jn2HBbRFuiRxKRbB.htm](kingmaker-bestiary-items/Jn2HBbRFuiRxKRbB.htm)|Goblin Scuttle|
|[JoOzphwUQC4ZFm7J.htm](kingmaker-bestiary-items/JoOzphwUQC4ZFm7J.htm)|Saving Slash|
|[JPmOqF1x4Au8drWv.htm](kingmaker-bestiary-items/JPmOqF1x4Au8drWv.htm)|Spectral Uprising|
|[jU73I507kdECl13G.htm](kingmaker-bestiary-items/jU73I507kdECl13G.htm)|Contingency|
|[JXtD0GzHXfGW7Aan.htm](kingmaker-bestiary-items/JXtD0GzHXfGW7Aan.htm)|Deny Advantage|
|[jXwGpQmNRAqEcUQN.htm](kingmaker-bestiary-items/jXwGpQmNRAqEcUQN.htm)|Quickened Casting|
|[jyTZDCNVazHG1tSk.htm](kingmaker-bestiary-items/jyTZDCNVazHG1tSk.htm)|Burst|
|[jZfqY5quDvHq9tmh.htm](kingmaker-bestiary-items/jZfqY5quDvHq9tmh.htm)|Wild Empathy|
|[k1LT9AcDMUPeGPOy.htm](kingmaker-bestiary-items/k1LT9AcDMUPeGPOy.htm)|Righteous Certainty|
|[k2R72XvX3USofjXB.htm](kingmaker-bestiary-items/k2R72XvX3USofjXB.htm)|Lash Out|
|[K3AfPmsD7hzdbhv0.htm](kingmaker-bestiary-items/K3AfPmsD7hzdbhv0.htm)|Sneak Attack|
|[K5qMhCPST2MiSV7J.htm](kingmaker-bestiary-items/K5qMhCPST2MiSV7J.htm)|Inveigled|
|[K7J1kJsdpeoiTUqZ.htm](kingmaker-bestiary-items/K7J1kJsdpeoiTUqZ.htm)|Attack of Opportunity|
|[k9kbOoeLWV3Bx8l5.htm](kingmaker-bestiary-items/k9kbOoeLWV3Bx8l5.htm)|Moment of Solitude|
|[kfdzBPs02IvVMabf.htm](kingmaker-bestiary-items/kfdzBPs02IvVMabf.htm)|Focus Gaze|
|[kh9EPJwZWvajnZBV.htm](kingmaker-bestiary-items/kh9EPJwZWvajnZBV.htm)|At-Will Spells|
|[klmOYsNgGYaAMROh.htm](kingmaker-bestiary-items/klmOYsNgGYaAMROh.htm)|Knockback|
|[kMVtB4pZd0LYoYOm.htm](kingmaker-bestiary-items/kMVtB4pZd0LYoYOm.htm)|Slam Doors|
|[KngnsUrW0RbmKrce.htm](kingmaker-bestiary-items/KngnsUrW0RbmKrce.htm)|Focus Beauty|
|[KNywleLC7uqltyig.htm](kingmaker-bestiary-items/KNywleLC7uqltyig.htm)|Focus Gaze|
|[krTcoq8OKd5gFAL0.htm](kingmaker-bestiary-items/krTcoq8OKd5gFAL0.htm)|Sneak Attack|
|[KtA9MV7RA5nWRmko.htm](kingmaker-bestiary-items/KtA9MV7RA5nWRmko.htm)|Blinding Sickness|
|[KUcCUd82J1JqXWE5.htm](kingmaker-bestiary-items/KUcCUd82J1JqXWE5.htm)|Breath Weapon|
|[kv7A4qqpl2mG1nHt.htm](kingmaker-bestiary-items/kv7A4qqpl2mG1nHt.htm)|Quickened Casting|
|[kZiYWe3uWt1JBdbS.htm](kingmaker-bestiary-items/kZiYWe3uWt1JBdbS.htm)|Familiar|
|[lCn44iSRdDgu2GdC.htm](kingmaker-bestiary-items/lCn44iSRdDgu2GdC.htm)|Attack of Opportunity|
|[leYjDFOKDNCIZOBf.htm](kingmaker-bestiary-items/leYjDFOKDNCIZOBf.htm)|Sneak Attack|
|[lffL96H4Twel1YRH.htm](kingmaker-bestiary-items/lffL96H4Twel1YRH.htm)|Darkvision|
|[lGRlzXnOSNGW7djU.htm](kingmaker-bestiary-items/lGRlzXnOSNGW7djU.htm)|Shame|
|[lLQgAigQauz0Z3sx.htm](kingmaker-bestiary-items/lLQgAigQauz0Z3sx.htm)|Designate Blood Foe|
|[Lq9tywTCezzL2UYy.htm](kingmaker-bestiary-items/Lq9tywTCezzL2UYy.htm)|Lamashtu's Bloom|
|[lqfoMvtMondVCDSV.htm](kingmaker-bestiary-items/lqfoMvtMondVCDSV.htm)|Steady Spellcasting|
|[Lri10NA5TEU7fZeX.htm](kingmaker-bestiary-items/Lri10NA5TEU7fZeX.htm)|Call to the Hunt|
|[LsUtyTnBZPF6Hvnp.htm](kingmaker-bestiary-items/LsUtyTnBZPF6Hvnp.htm)|Frightful Presence|
|[LTLa6hS15bgHkOBi.htm](kingmaker-bestiary-items/LTLa6hS15bgHkOBi.htm)|Protection from Decapitation|
|[lUMpyYokJ5dTTdu9.htm](kingmaker-bestiary-items/lUMpyYokJ5dTTdu9.htm)|Blood Drain|
|[LXvqvVhorWXVewtI.htm](kingmaker-bestiary-items/LXvqvVhorWXVewtI.htm)|Twist Time|
|[lYCCv7NRnl6ZnNjT.htm](kingmaker-bestiary-items/lYCCv7NRnl6ZnNjT.htm)|Constant Spells|
|[lYx7vXEHaPIDLbWp.htm](kingmaker-bestiary-items/lYx7vXEHaPIDLbWp.htm)|Sickle|
|[Lz7LqoEOMbBrTosy.htm](kingmaker-bestiary-items/Lz7LqoEOMbBrTosy.htm)|Repulsing Blow|
|[LZhDKd6gpJJ5o2RO.htm](kingmaker-bestiary-items/LZhDKd6gpJJ5o2RO.htm)|Aura of Disquietude|
|[m4LUwAovVkqwRsiG.htm](kingmaker-bestiary-items/m4LUwAovVkqwRsiG.htm)|Vengeful Rage|
|[m5He7mR6hyUEterZ.htm](kingmaker-bestiary-items/m5He7mR6hyUEterZ.htm)|Regeneration 30 (Deactivated by Fire or Good)|
|[M9wyjXrmWfCD7nBk.htm](kingmaker-bestiary-items/M9wyjXrmWfCD7nBk.htm)|Blinding Beauty|
|[mCooSIipdGY8hP2o.htm](kingmaker-bestiary-items/mCooSIipdGY8hP2o.htm)|Living Bow|
|[mDmFiG2gsRPiENrP.htm](kingmaker-bestiary-items/mDmFiG2gsRPiENrP.htm)|Warp Teleportation|
|[MfVHl3j1UyN4VFks.htm](kingmaker-bestiary-items/MfVHl3j1UyN4VFks.htm)|Attack of Opportunity|
|[MgV9p8g40O0uwDjs.htm](kingmaker-bestiary-items/MgV9p8g40O0uwDjs.htm)|Wild Gaze|
|[MgVuD5shNkwpUCdM.htm](kingmaker-bestiary-items/MgVuD5shNkwpUCdM.htm)|Frightful Moan|
|[Mkzdftre3Tzz3aVM.htm](kingmaker-bestiary-items/Mkzdftre3Tzz3aVM.htm)|Drop From Ceiling|
|[moA510VYoGxOfach.htm](kingmaker-bestiary-items/moA510VYoGxOfach.htm)|Recall Lintwerth|
|[MQbQ4WqtcjmvDsJS.htm](kingmaker-bestiary-items/MQbQ4WqtcjmvDsJS.htm)|Seeking Shots|
|[Mqryx0fWVqgV0RiU.htm](kingmaker-bestiary-items/Mqryx0fWVqgV0RiU.htm)|Mauler|
|[MqUTUiuHEWGtwSkQ.htm](kingmaker-bestiary-items/MqUTUiuHEWGtwSkQ.htm)|Instinctive Cooperation|
|[mTecTRC1ab5f2qOh.htm](kingmaker-bestiary-items/mTecTRC1ab5f2qOh.htm)|Negative Healing|
|[MUZm9jerelh5o9lK.htm](kingmaker-bestiary-items/MUZm9jerelh5o9lK.htm)|Royal Command|
|[mVgSuJseTKH5wKIO.htm](kingmaker-bestiary-items/mVgSuJseTKH5wKIO.htm)|Twin Takedown|
|[mwIVxTyDPXVZaEcf.htm](kingmaker-bestiary-items/mwIVxTyDPXVZaEcf.htm)|Fearsome Gaze|
|[MwxI4D0saKJpksvK.htm](kingmaker-bestiary-items/MwxI4D0saKJpksvK.htm)|Shoulder Slam|
|[MxkYw0L8AzlUayt9.htm](kingmaker-bestiary-items/MxkYw0L8AzlUayt9.htm)|Rush|
|[MZGshvRJ0ZlhdQ5k.htm](kingmaker-bestiary-items/MZGshvRJ0ZlhdQ5k.htm)|Rhetorical Spell|
|[n1p0bJc7I91avM4S.htm](kingmaker-bestiary-items/n1p0bJc7I91avM4S.htm)|Wild Empathy|
|[N4dy1EmZsnQD8XR3.htm](kingmaker-bestiary-items/N4dy1EmZsnQD8XR3.htm)|Chill Breath|
|[N4EVKfsc2JGoy6hL.htm](kingmaker-bestiary-items/N4EVKfsc2JGoy6hL.htm)|Rider Synergy|
|[n5PTMIe2knU6k8WD.htm](kingmaker-bestiary-items/n5PTMIe2knU6k8WD.htm)|Spiked Pitfall|
|[N7BomXiXSkV8IDmE.htm](kingmaker-bestiary-items/N7BomXiXSkV8IDmE.htm)|Perpetual Hangover|
|[N8EKVbUAmiDxtOlF.htm](kingmaker-bestiary-items/N8EKVbUAmiDxtOlF.htm)|Sneak Attack|
|[nC28cnIAmXUrJhdf.htm](kingmaker-bestiary-items/nC28cnIAmXUrJhdf.htm)|Rage|
|[nCPtTG7EkbC5KQpL.htm](kingmaker-bestiary-items/nCPtTG7EkbC5KQpL.htm)|Furious Power Attack|
|[nHt7MnXhByHOX78E.htm](kingmaker-bestiary-items/nHt7MnXhByHOX78E.htm)|Tandem Chop|
|[nkAaDbVaSoGjlqCp.htm](kingmaker-bestiary-items/nkAaDbVaSoGjlqCp.htm)|Poison Spray|
|[NKFCPYANKZVEHAVK.htm](kingmaker-bestiary-items/NKFCPYANKZVEHAVK.htm)|No Escape|
|[nNdV2vrrzVUlwrhp.htm](kingmaker-bestiary-items/nNdV2vrrzVUlwrhp.htm)|Fast Healing 15|
|[nsDXE0HKZhrqgV54.htm](kingmaker-bestiary-items/nsDXE0HKZhrqgV54.htm)|Moon Frenzy|
|[ntcF2Dw0m1DqvDnw.htm](kingmaker-bestiary-items/ntcF2Dw0m1DqvDnw.htm)|Dream of Rulership|
|[NTFiXa55jt87djrs.htm](kingmaker-bestiary-items/NTFiXa55jt87djrs.htm)|Attack of Opportunity|
|[NTRKdsufuPkE5ERS.htm](kingmaker-bestiary-items/NTRKdsufuPkE5ERS.htm)|Aldori Parry|
|[Nu5hdhS5ABZ0CZjq.htm](kingmaker-bestiary-items/Nu5hdhS5ABZ0CZjq.htm)|Twin Butchery|
|[NWflOL2JDt8KPMH6.htm](kingmaker-bestiary-items/NWflOL2JDt8KPMH6.htm)|Rejuvenation|
|[Nzx1vI7LkdpuIwph.htm](kingmaker-bestiary-items/Nzx1vI7LkdpuIwph.htm)|Unseen Sight|
|[o3sdERN8Ehqn7jo0.htm](kingmaker-bestiary-items/o3sdERN8Ehqn7jo0.htm)|Low-Light Vision|
|[ocYHO8rNnbCQGplD.htm](kingmaker-bestiary-items/ocYHO8rNnbCQGplD.htm)|Bewildering Hoofbeats|
|[OenA92WNHdH1I94w.htm](kingmaker-bestiary-items/OenA92WNHdH1I94w.htm)|Conjure Daemon|
|[ogXJyvypuEP8U1hd.htm](kingmaker-bestiary-items/ogXJyvypuEP8U1hd.htm)|Bloom Venom|
|[oHxfnh9KIO2dQcTN.htm](kingmaker-bestiary-items/oHxfnh9KIO2dQcTN.htm)|Darkvision|
|[on7BQSeRUgbsFSSE.htm](kingmaker-bestiary-items/on7BQSeRUgbsFSSE.htm)|Knockdown|
|[onRMNTdGzo2Qq4rE.htm](kingmaker-bestiary-items/onRMNTdGzo2Qq4rE.htm)|Sneak Attack|
|[OtqpSI4VXEPSxQox.htm](kingmaker-bestiary-items/OtqpSI4VXEPSxQox.htm)|Intimidating Prowess|
|[otVXldOceOZtEV2x.htm](kingmaker-bestiary-items/otVXldOceOZtEV2x.htm)|Frightening Rattle|
|[oVDdlUuHM54d6gAP.htm](kingmaker-bestiary-items/oVDdlUuHM54d6gAP.htm)|At-Will Spells|
|[OvgHtZCLvGtWHyg9.htm](kingmaker-bestiary-items/OvgHtZCLvGtWHyg9.htm)|Raging Resistance|
|[OZRGnwXPUQq6AsKe.htm](kingmaker-bestiary-items/OZRGnwXPUQq6AsKe.htm)|Force Bolt|
|[P143e4Kl2GktAt4b.htm](kingmaker-bestiary-items/P143e4Kl2GktAt4b.htm)|Greatsword Critical Specialization|
|[p3LP4p7cOQkgP2pl.htm](kingmaker-bestiary-items/p3LP4p7cOQkgP2pl.htm)|Befuddle|
|[pc3Nk8nioNXX1wuM.htm](kingmaker-bestiary-items/pc3Nk8nioNXX1wuM.htm)|Flash of Insight|
|[pg54945zKnAqECAM.htm](kingmaker-bestiary-items/pg54945zKnAqECAM.htm)|Darkvision|
|[PG62mZgFRnBReYcw.htm](kingmaker-bestiary-items/PG62mZgFRnBReYcw.htm)|Word of Recall|
|[pQowCb4EVsOhUgK2.htm](kingmaker-bestiary-items/pQowCb4EVsOhUgK2.htm)|Breath Weapon|
|[pQr1ecKdOpSlfSnw.htm](kingmaker-bestiary-items/pQr1ecKdOpSlfSnw.htm)|Spiteful Command|
|[pQrymwjXMI3vqcOk.htm](kingmaker-bestiary-items/pQrymwjXMI3vqcOk.htm)|Negative Healing|
|[PueAKB1MJBXmbhHV.htm](kingmaker-bestiary-items/PueAKB1MJBXmbhHV.htm)|Formation Fighter|
|[pugi4AZBmL8aH1oF.htm](kingmaker-bestiary-items/pugi4AZBmL8aH1oF.htm)|Vengeful Sting|
|[pWFsPT5b6xUAwvpD.htm](kingmaker-bestiary-items/pWFsPT5b6xUAwvpD.htm)|Scent (Imprecise) 30 feet|
|[PxCKKRzmpOzQRHlN.htm](kingmaker-bestiary-items/PxCKKRzmpOzQRHlN.htm)|Tiger Empathy|
|[pyyskEw6HoHX0DBp.htm](kingmaker-bestiary-items/pyyskEw6HoHX0DBp.htm)|Site Bound|
|[Q0WHpEJkqbeT0KZr.htm](kingmaker-bestiary-items/Q0WHpEJkqbeT0KZr.htm)|Waves of Fear|
|[Q78tMeRUfO7vehcO.htm](kingmaker-bestiary-items/Q78tMeRUfO7vehcO.htm)|Regeneration 45 (Deactivated by Fire or Negative)|
|[Q7g8pfqUSNczyqNo.htm](kingmaker-bestiary-items/Q7g8pfqUSNczyqNo.htm)|Attack of Opportunity|
|[q7jtffxL9IJqb4Pq.htm](kingmaker-bestiary-items/q7jtffxL9IJqb4Pq.htm)|Change Shape|
|[Q8CDVoVPdXpwkoEA.htm](kingmaker-bestiary-items/Q8CDVoVPdXpwkoEA.htm)|Constant Spells|
|[qAjzrkRNotMQvgjw.htm](kingmaker-bestiary-items/qAjzrkRNotMQvgjw.htm)|Infuse Arrow|
|[QebQalR7fIq41wKr.htm](kingmaker-bestiary-items/QebQalR7fIq41wKr.htm)|Attack of Opportunity|
|[qEenLpsXQfSrqeJh.htm](kingmaker-bestiary-items/qEenLpsXQfSrqeJh.htm)|Scent (Imprecise) 30 feet|
|[Qg6K3SNkS9IdmxUm.htm](kingmaker-bestiary-items/Qg6K3SNkS9IdmxUm.htm)|Blessed Life|
|[qiU4iKX6FIHuopg7.htm](kingmaker-bestiary-items/qiU4iKX6FIHuopg7.htm)|Sting|
|[qn7XE1djceSKfkuH.htm](kingmaker-bestiary-items/qn7XE1djceSKfkuH.htm)|Scent (Imprecise) 30 feet|
|[qQD7sRQC5iCPloAd.htm](kingmaker-bestiary-items/qQD7sRQC5iCPloAd.htm)|Ectoplasmic Maneuver|
|[QrCROiN8OwsCLDUj.htm](kingmaker-bestiary-items/QrCROiN8OwsCLDUj.htm)|Skirmish Strike|
|[QXYemYqhU6zdoBsz.htm](kingmaker-bestiary-items/QXYemYqhU6zdoBsz.htm)|Reach Spell|
|[qztKctN0HIhpmOSy.htm](kingmaker-bestiary-items/qztKctN0HIhpmOSy.htm)|Low-Light Vision|
|[r0B1Lotd2T85M2u2.htm](kingmaker-bestiary-items/r0B1Lotd2T85M2u2.htm)|Low-Light Vision|
|[R6qqK2TgPEbgEcH0.htm](kingmaker-bestiary-items/R6qqK2TgPEbgEcH0.htm)|Defensive Burst|
|[Ral7Zi8ooe0f3emu.htm](kingmaker-bestiary-items/Ral7Zi8ooe0f3emu.htm)|Moon Frenzy|
|[rcNrrQtcdNppDhtd.htm](kingmaker-bestiary-items/rcNrrQtcdNppDhtd.htm)|Feasting Bite|
|[rl3BJy5yXHIijmZz.htm](kingmaker-bestiary-items/rl3BJy5yXHIijmZz.htm)|Darkvision|
|[RLutvx6GldUXGhjl.htm](kingmaker-bestiary-items/RLutvx6GldUXGhjl.htm)|Twin Parry|
|[rlwsvNyOSTobwhA9.htm](kingmaker-bestiary-items/rlwsvNyOSTobwhA9.htm)|Centipede Swarm Venom|
|[rNPNkpT1CEzgR8Ry.htm](kingmaker-bestiary-items/rNPNkpT1CEzgR8Ry.htm)|Unnerving Prowess|
|[rnQSJr976QIURT9w.htm](kingmaker-bestiary-items/rnQSJr976QIURT9w.htm)|Songstrike|
|[roURyHTV2DxU7WTQ.htm](kingmaker-bestiary-items/roURyHTV2DxU7WTQ.htm)|Darkvision|
|[rTLmI9nr5Sji2tZD.htm](kingmaker-bestiary-items/rTLmI9nr5Sji2tZD.htm)|Axe Critical Specialization|
|[rtPLskLwq9ldQzDC.htm](kingmaker-bestiary-items/rtPLskLwq9ldQzDC.htm)|Scalding Burst|
|[rwJegRqmKknX4v9P.htm](kingmaker-bestiary-items/rwJegRqmKknX4v9P.htm)|Attack of Opportunity|
|[RXMeyIF1blCFYaFi.htm](kingmaker-bestiary-items/RXMeyIF1blCFYaFi.htm)|Darkvision|
|[s2robi6KwZM7XeVw.htm](kingmaker-bestiary-items/s2robi6KwZM7XeVw.htm)|Falling Portcullis|
|[S9OYItI2DPSsb4Za.htm](kingmaker-bestiary-items/S9OYItI2DPSsb4Za.htm)|Switch Fables|
|[sCOLNwAp0vM1QRy9.htm](kingmaker-bestiary-items/sCOLNwAp0vM1QRy9.htm)|Baleful Gaze|
|[Sd3o6E29DT50HqQ5.htm](kingmaker-bestiary-items/Sd3o6E29DT50HqQ5.htm)|Greater Darkvision|
|[sED3umw26WGHjqa6.htm](kingmaker-bestiary-items/sED3umw26WGHjqa6.htm)|Consume Memories|
|[sIdAMHJe8g1PQUF7.htm](kingmaker-bestiary-items/sIdAMHJe8g1PQUF7.htm)|Low-Light Vision|
|[SKOscQwReKF5ZqAY.htm](kingmaker-bestiary-items/SKOscQwReKF5ZqAY.htm)|Maming Chop|
|[smt8XqXd55pl6Gwi.htm](kingmaker-bestiary-items/smt8XqXd55pl6Gwi.htm)|Clamp Shut|
|[snWh3FVKpgmQAHQz.htm](kingmaker-bestiary-items/snWh3FVKpgmQAHQz.htm)|Disfigure|
|[spF1Kzai1HQbME5t.htm](kingmaker-bestiary-items/spF1Kzai1HQbME5t.htm)|Bloom Regeneration|
|[SPiKrNADm6u2qbhl.htm](kingmaker-bestiary-items/SPiKrNADm6u2qbhl.htm)|Triumphant Roar|
|[SpVGiNHO8YKLWtpn.htm](kingmaker-bestiary-items/SpVGiNHO8YKLWtpn.htm)|Quick Alchemy|
|[SqomM8yPUdX1yPY8.htm](kingmaker-bestiary-items/SqomM8yPUdX1yPY8.htm)|Attack of Opportunity|
|[sT3ZX2Zurexp4hLH.htm](kingmaker-bestiary-items/sT3ZX2Zurexp4hLH.htm)|Steady Spellcasting|
|[sU4JRJE89RWZ5Z6a.htm](kingmaker-bestiary-items/sU4JRJE89RWZ5Z6a.htm)|Scent (Precise) 120 feet|
|[sw2mWVFaQb93J9el.htm](kingmaker-bestiary-items/sw2mWVFaQb93J9el.htm)|Forager|
|[swHSxsAoATAgMMX2.htm](kingmaker-bestiary-items/swHSxsAoATAgMMX2.htm)|Ride Tick|
|[Swn94Gh9zgVIcHmO.htm](kingmaker-bestiary-items/Swn94Gh9zgVIcHmO.htm)|Ankle Bite|
|[SYMyLr9FwZW5kVcr.htm](kingmaker-bestiary-items/SYMyLr9FwZW5kVcr.htm)|Attack of Opportunity|
|[SZOur85Nka45xVe7.htm](kingmaker-bestiary-items/SZOur85Nka45xVe7.htm)|Frightful Moan|
|[T0IRXqqR5Zr2FXsp.htm](kingmaker-bestiary-items/T0IRXqqR5Zr2FXsp.htm)|Negative Healing|
|[T1JhJ4gf00b500cI.htm](kingmaker-bestiary-items/T1JhJ4gf00b500cI.htm)|Attack of Opportunity|
|[t49UcMd23mTLwrj5.htm](kingmaker-bestiary-items/t49UcMd23mTLwrj5.htm)|Paralyzing Touch|
|[TGbnXBgCIKg1wFwT.htm](kingmaker-bestiary-items/TGbnXBgCIKg1wFwT.htm)|Targeting Shot|
|[TILteUNBjDkjtfg6.htm](kingmaker-bestiary-items/TILteUNBjDkjtfg6.htm)|Inspiration|
|[TIWU41UBtl2ksfmx.htm](kingmaker-bestiary-items/TIWU41UBtl2ksfmx.htm)|Twin Riposte|
|[tJSXokhRWx6GoMJe.htm](kingmaker-bestiary-items/tJSXokhRWx6GoMJe.htm)|Bleed for Lamashtu|
|[tKFw0mI1NIfpLYBn.htm](kingmaker-bestiary-items/tKFw0mI1NIfpLYBn.htm)|Unbalancing Blow|
|[tkP4RVEwAR4aHYbk.htm](kingmaker-bestiary-items/tkP4RVEwAR4aHYbk.htm)|Darkvision|
|[TqvPH7FdjdwawFbW.htm](kingmaker-bestiary-items/TqvPH7FdjdwawFbW.htm)|Shield Block|
|[ttXiy5Zys3PzCa8f.htm](kingmaker-bestiary-items/ttXiy5Zys3PzCa8f.htm)|Tortuous Touch|
|[TUJZ5ff9G3KgBsCN.htm](kingmaker-bestiary-items/TUJZ5ff9G3KgBsCN.htm)|Darkvision|
|[TvcbacSr4PLH15CQ.htm](kingmaker-bestiary-items/TvcbacSr4PLH15CQ.htm)|Curse of the Werewolf|
|[tvr5xWTm58t3Dgy3.htm](kingmaker-bestiary-items/tvr5xWTm58t3Dgy3.htm)|Crumbling Edge|
|[TwMRnjbKEUPZwBKx.htm](kingmaker-bestiary-items/TwMRnjbKEUPZwBKx.htm)|Divine Font|
|[TwVHeJqYT1io5LxX.htm](kingmaker-bestiary-items/TwVHeJqYT1io5LxX.htm)|Summon Pack|
|[TxCWNmfsEkdTzNvw.htm](kingmaker-bestiary-items/TxCWNmfsEkdTzNvw.htm)|Outside of Time|
|[tz6uqrNAY2ySZ2TY.htm](kingmaker-bestiary-items/tz6uqrNAY2ySZ2TY.htm)|Of Three Minds|
|[u0gmeeDEvRzdbvFs.htm](kingmaker-bestiary-items/u0gmeeDEvRzdbvFs.htm)|Negative Healing|
|[U22LSFuhWJzxq7TO.htm](kingmaker-bestiary-items/U22LSFuhWJzxq7TO.htm)|Damsel Act|
|[uCIO2gyqCCw5HMiZ.htm](kingmaker-bestiary-items/uCIO2gyqCCw5HMiZ.htm)|Quick Bomber|
|[UdmjBD1i3DxTYEep.htm](kingmaker-bestiary-items/UdmjBD1i3DxTYEep.htm)|Telekinetic Assault|
|[UGbMxXUr68ITj7ND.htm](kingmaker-bestiary-items/UGbMxXUr68ITj7ND.htm)|Drain Lintwerth|
|[UIGEr5LUogZmy8ln.htm](kingmaker-bestiary-items/UIGEr5LUogZmy8ln.htm)|Phantasmagoric Fog|
|[uiZk5k4gZbLlAo44.htm](kingmaker-bestiary-items/uiZk5k4gZbLlAo44.htm)|Rejuvenation|
|[UJnXwBbuFjAcC01q.htm](kingmaker-bestiary-items/UJnXwBbuFjAcC01q.htm)|Unsettling Attention|
|[ulr3xQ9vjVzN0JcV.htm](kingmaker-bestiary-items/ulr3xQ9vjVzN0JcV.htm)|Constant Spells|
|[UlSgJk73FttvDVKx.htm](kingmaker-bestiary-items/UlSgJk73FttvDVKx.htm)|Attack of Opportunity|
|[uMg5j8kbgsMRkJNO.htm](kingmaker-bestiary-items/uMg5j8kbgsMRkJNO.htm)|Invoke Old Sharptooth|
|[uox8BdqT7SHwPAXF.htm](kingmaker-bestiary-items/uox8BdqT7SHwPAXF.htm)|Backlash|
|[UqivmeGpdoq5TEFp.htm](kingmaker-bestiary-items/UqivmeGpdoq5TEFp.htm)|Manifest Fetch Weapon|
|[utrBYZarwPMS7ACv.htm](kingmaker-bestiary-items/utrBYZarwPMS7ACv.htm)|Resolve|
|[v4dtDXWYnbH5R47w.htm](kingmaker-bestiary-items/v4dtDXWYnbH5R47w.htm)|Fearsome Blizzard|
|[V6fjZPcQEMD98LxA.htm](kingmaker-bestiary-items/V6fjZPcQEMD98LxA.htm)|Dramatic Disarm|
|[v8fWAG80JbMt4nOy.htm](kingmaker-bestiary-items/v8fWAG80JbMt4nOy.htm)|Scent (Imprecise) 30 feet|
|[vCJLJPrm6qXxNRzd.htm](kingmaker-bestiary-items/vCJLJPrm6qXxNRzd.htm)|Divine Font (Harm)|
|[VCxhoISThjwcy55r.htm](kingmaker-bestiary-items/VCxhoISThjwcy55r.htm)|Attack of Opportunity|
|[vLKZN5X8t0pUwZJd.htm](kingmaker-bestiary-items/vLKZN5X8t0pUwZJd.htm)|Improved Knockdown|
|[vllPNnBmoSwFEtl9.htm](kingmaker-bestiary-items/vllPNnBmoSwFEtl9.htm)|Guarded Movement|
|[VmmlUbjFsbcN9dM8.htm](kingmaker-bestiary-items/VmmlUbjFsbcN9dM8.htm)|Sneak Attack|
|[VPg7zeVby8h8yTMX.htm](kingmaker-bestiary-items/VPg7zeVby8h8yTMX.htm)|Bark Orders|
|[vrZuMJAZcSYNFRp1.htm](kingmaker-bestiary-items/vrZuMJAZcSYNFRp1.htm)|Woodland Stride|
|[vvB4kICd5X8HaFue.htm](kingmaker-bestiary-items/vvB4kICd5X8HaFue.htm)|Rage|
|[VViv6cOIpMtNUZzW.htm](kingmaker-bestiary-items/VViv6cOIpMtNUZzW.htm)|Brutal Gore|
|[vYdUPrjuB8HrzGBA.htm](kingmaker-bestiary-items/vYdUPrjuB8HrzGBA.htm)|Attack of Opportunity|
|[vYngXSj2DDVpfWFe.htm](kingmaker-bestiary-items/vYngXSj2DDVpfWFe.htm)|Meddling Tail|
|[wAjwXDiTkvisDxZR.htm](kingmaker-bestiary-items/wAjwXDiTkvisDxZR.htm)|Survival of the Fittest|
|[wb6prflG67LiVXh1.htm](kingmaker-bestiary-items/wb6prflG67LiVXh1.htm)|Trackless Step|
|[WDpA7ff1gJiokx9N.htm](kingmaker-bestiary-items/WDpA7ff1gJiokx9N.htm)|Hunted Shot|
|[WF3kud5WAVewvKL0.htm](kingmaker-bestiary-items/WF3kud5WAVewvKL0.htm)|Low-Light Vision|
|[wFieL2rsS0tFWUAR.htm](kingmaker-bestiary-items/wFieL2rsS0tFWUAR.htm)|Regeneration 20 (Deactivated by Cold Iron or Lawful)|
|[WhCqHDOg0A8FK3IF.htm](kingmaker-bestiary-items/WhCqHDOg0A8FK3IF.htm)|Relentless Tracker|
|[WIPJ7Xl3dOhNuLlq.htm](kingmaker-bestiary-items/WIPJ7Xl3dOhNuLlq.htm)|Counterspell|
|[Wjfmdyuddn5fCyVZ.htm](kingmaker-bestiary-items/Wjfmdyuddn5fCyVZ.htm)|Insult Challenges|
|[wO3hSmCvgcCzSah3.htm](kingmaker-bestiary-items/wO3hSmCvgcCzSah3.htm)|Bloom Curse|
|[WpEtlM3mpICnsXPm.htm](kingmaker-bestiary-items/WpEtlM3mpICnsXPm.htm)|Steady Spellcasting|
|[WrmnFHWCqCQMpQlB.htm](kingmaker-bestiary-items/WrmnFHWCqCQMpQlB.htm)|Pin to the Sky|
|[wSYY3PhHktTGw8ua.htm](kingmaker-bestiary-items/wSYY3PhHktTGw8ua.htm)|Baroness's Scream|
|[Wt9KDemV1jGIVjE5.htm](kingmaker-bestiary-items/Wt9KDemV1jGIVjE5.htm)|Catch Rock|
|[WvKgLpntjekJJHp7.htm](kingmaker-bestiary-items/WvKgLpntjekJJHp7.htm)|Sneak Attack|
|[wXPOAgEeixFbagOW.htm](kingmaker-bestiary-items/wXPOAgEeixFbagOW.htm)|Swift Summon|
|[wyAVtZ4X5ErNqi3C.htm](kingmaker-bestiary-items/wyAVtZ4X5ErNqi3C.htm)|Sneak Attack|
|[wzltAJwbP73KlolU.htm](kingmaker-bestiary-items/wzltAJwbP73KlolU.htm)|Itchy|
|[X1qtaRb6bkYt1N3s.htm](kingmaker-bestiary-items/X1qtaRb6bkYt1N3s.htm)|Attack of Opportunity|
|[X3HpUq80mQWyiHZ3.htm](kingmaker-bestiary-items/X3HpUq80mQWyiHZ3.htm)|Hunt Prey|
|[x46WnJD9YKQXpRkE.htm](kingmaker-bestiary-items/x46WnJD9YKQXpRkE.htm)|Raging Storm|
|[x8mIj7POmkmnt5WS.htm](kingmaker-bestiary-items/x8mIj7POmkmnt5WS.htm)|Divine Recovery|
|[XCvqYacJqKAyKyBy.htm](kingmaker-bestiary-items/XCvqYacJqKAyKyBy.htm)|Change Shape|
|[xeEI4dqDE4PmxOeA.htm](kingmaker-bestiary-items/xeEI4dqDE4PmxOeA.htm)|Forest Walker|
|[XFTxhLhYbwJ3pxdt.htm](kingmaker-bestiary-items/XFTxhLhYbwJ3pxdt.htm)|Unseen Sight|
|[XfUDUv0iccZ8hNjg.htm](kingmaker-bestiary-items/XfUDUv0iccZ8hNjg.htm)|Negative Healing|
|[XHQxmGEm71C14Nci.htm](kingmaker-bestiary-items/XHQxmGEm71C14Nci.htm)|Grab|
|[XhrxL6SNJhTojhys.htm](kingmaker-bestiary-items/XhrxL6SNJhTojhys.htm)|Manifest Crystals|
|[xI9jqkNAsWJ8GrxC.htm](kingmaker-bestiary-items/xI9jqkNAsWJ8GrxC.htm)|Collapse|
|[XJXxOQSoroFvm4m9.htm](kingmaker-bestiary-items/XJXxOQSoroFvm4m9.htm)|Constant Spells|
|[xkPOrd9a16OIHTyl.htm](kingmaker-bestiary-items/xkPOrd9a16OIHTyl.htm)|Second Wind|
|[XKVb3v9QlngaKRQV.htm](kingmaker-bestiary-items/XKVb3v9QlngaKRQV.htm)|Sneak Attack|
|[xMM6S0SqKD29GPxJ.htm](kingmaker-bestiary-items/xMM6S0SqKD29GPxJ.htm)|Site Bound|
|[XnVq5NAwuLiDa4dD.htm](kingmaker-bestiary-items/XnVq5NAwuLiDa4dD.htm)|Sudden Stormburst|
|[XR8SUq7u8lPNwfsz.htm](kingmaker-bestiary-items/XR8SUq7u8lPNwfsz.htm)|Overhand Smash|
|[xtGHTPAuSTsudXMG.htm](kingmaker-bestiary-items/xtGHTPAuSTsudXMG.htm)|Enhanced Familiar|
|[xtitjNKcLXuYUcnS.htm](kingmaker-bestiary-items/xtitjNKcLXuYUcnS.htm)|Reel In|
|[XU7E8CJIc6dwyRml.htm](kingmaker-bestiary-items/XU7E8CJIc6dwyRml.htm)|Knockback|
|[Xva9qQPkH1v1bsG1.htm](kingmaker-bestiary-items/Xva9qQPkH1v1bsG1.htm)|Floor Collapse|
|[xYyZLfzVCUdoVnQn.htm](kingmaker-bestiary-items/xYyZLfzVCUdoVnQn.htm)|Reach Spell|
|[xZwLtaE6SEA50Rqf.htm](kingmaker-bestiary-items/xZwLtaE6SEA50Rqf.htm)|Twin Chop|
|[Y0nwjk1NkdGHxBxi.htm](kingmaker-bestiary-items/Y0nwjk1NkdGHxBxi.htm)|Change Shape|
|[y1uZ9I98YpDpmpQx.htm](kingmaker-bestiary-items/y1uZ9I98YpDpmpQx.htm)|At-Will Spells|
|[y6KKrN8Aiff22yLS.htm](kingmaker-bestiary-items/y6KKrN8Aiff22yLS.htm)|Bloodline Magic|
|[y8OK5g64U5E8qC0q.htm](kingmaker-bestiary-items/y8OK5g64U5E8qC0q.htm)|Vicious Ranseur|
|[Y9nkyrHVb6qQd6hs.htm](kingmaker-bestiary-items/Y9nkyrHVb6qQd6hs.htm)|Shame|
|[yEmvQZDwtr985C2Y.htm](kingmaker-bestiary-items/yEmvQZDwtr985C2Y.htm)|Hatchet Flurry|
|[YeVYuXNHpkcHdBo2.htm](kingmaker-bestiary-items/YeVYuXNHpkcHdBo2.htm)|Attack of Opportunity (Tail Only)|
|[yGkx5YIsqb7P1b34.htm](kingmaker-bestiary-items/yGkx5YIsqb7P1b34.htm)|Effortless Concentration|
|[Yh6PceHFS60uaThW.htm](kingmaker-bestiary-items/Yh6PceHFS60uaThW.htm)|Release Boulders|
|[yI8AXFiHc0FyemjU.htm](kingmaker-bestiary-items/yI8AXFiHc0FyemjU.htm)|Ominous Mien|
|[yIiN4CzNAnrIGY5R.htm](kingmaker-bestiary-items/yIiN4CzNAnrIGY5R.htm)|Low-Light Vision|
|[YJesTEWYLdHelRVi.htm](kingmaker-bestiary-items/YJesTEWYLdHelRVi.htm)|At-Will Spells|
|[ykE4J2hEBa7gWmVx.htm](kingmaker-bestiary-items/ykE4J2hEBa7gWmVx.htm)|Bloom Step|
|[YMNKladez96kdBX8.htm](kingmaker-bestiary-items/YMNKladez96kdBX8.htm)|Nimble Dodge|
|[yPpDjeNgrvnyJ092.htm](kingmaker-bestiary-items/yPpDjeNgrvnyJ092.htm)|Distort Senses|
|[Yq84rD7Ts0Wko9Pf.htm](kingmaker-bestiary-items/Yq84rD7Ts0Wko9Pf.htm)|Rip and Drag|
|[yR0motfxtDNCXbQv.htm](kingmaker-bestiary-items/yR0motfxtDNCXbQv.htm)|Felling Blow|
|[yS5bYDhKr2Koa3mm.htm](kingmaker-bestiary-items/yS5bYDhKr2Koa3mm.htm)|No Time To Die|
|[yvOTSeGrhPgBSOBZ.htm](kingmaker-bestiary-items/yvOTSeGrhPgBSOBZ.htm)|Capsize|
|[z3giiwwqKX9RBgD8.htm](kingmaker-bestiary-items/z3giiwwqKX9RBgD8.htm)|Spew Seed Pod|
|[Z5F5Xx9nQAfj1TtM.htm](kingmaker-bestiary-items/Z5F5Xx9nQAfj1TtM.htm)|Fireball|
|[Z65zRWuULtfR4enK.htm](kingmaker-bestiary-items/Z65zRWuULtfR4enK.htm)|Attack of Opportunity|
|[Z9mCi4uEoEQi6uMv.htm](kingmaker-bestiary-items/Z9mCi4uEoEQi6uMv.htm)|Quick Block|
|[zdtGqzcFir4opahs.htm](kingmaker-bestiary-items/zdtGqzcFir4opahs.htm)|Refocus Curse|
|[zfVISHBP6rc5o8Rv.htm](kingmaker-bestiary-items/zfVISHBP6rc5o8Rv.htm)|Exile's Curse|
|[ZG61qjm93pm36Iw5.htm](kingmaker-bestiary-items/ZG61qjm93pm36Iw5.htm)|Darkvision|
|[ZH9w82rcAvicGUV6.htm](kingmaker-bestiary-items/ZH9w82rcAvicGUV6.htm)|Menace Prey|
|[ziktYNU02MOBNRm7.htm](kingmaker-bestiary-items/ziktYNU02MOBNRm7.htm)|Infectious Wrath|
|[ZiobchoAZw8EGQAl.htm](kingmaker-bestiary-items/ZiobchoAZw8EGQAl.htm)|Recovery|
|[ZlCrr2J1Yth6IZEj.htm](kingmaker-bestiary-items/ZlCrr2J1Yth6IZEj.htm)|Lamashtu's Bloom|
|[zm4JTE61At7s9UrX.htm](kingmaker-bestiary-items/zm4JTE61At7s9UrX.htm)|Blow Mists|
|[zOBaEVlReAlrAIKE.htm](kingmaker-bestiary-items/zOBaEVlReAlrAIKE.htm)|Lonely Dirge|
|[ZPVlL1UH75oMFTCB.htm](kingmaker-bestiary-items/ZPVlL1UH75oMFTCB.htm)|Armag's Rage|
|[zquNaeOnacgZG38j.htm](kingmaker-bestiary-items/zquNaeOnacgZG38j.htm)|Darkvision|
|[ZR6JE7Gh0fgDobEm.htm](kingmaker-bestiary-items/ZR6JE7Gh0fgDobEm.htm)|Lurker Stance|
|[ZrwMBFIWJnpx6Utu.htm](kingmaker-bestiary-items/ZrwMBFIWJnpx6Utu.htm)|Dread Striker|
|[ZSab4ZN954h6Ckqb.htm](kingmaker-bestiary-items/ZSab4ZN954h6Ckqb.htm)|Atrophied Lich|
|[zWVvmrN2Kq0dZWsu.htm](kingmaker-bestiary-items/zWVvmrN2Kq0dZWsu.htm)|Freezing Wind|
|[ZYFSO8d8sBeN7WO7.htm](kingmaker-bestiary-items/ZYFSO8d8sBeN7WO7.htm)|Unsettling Revelation|

## Liste des éléments vides ne pouvant pas être traduits

| Fichier   | Nom (EN)    | État |
|-----------|-------------|:----:|
|[0mCmhGYAI47n4kdy.htm](kingmaker-bestiary-items/0mCmhGYAI47n4kdy.htm)|Divine Prepared Spells|vide|
|[0Rjcnl6ynsH5LKjO.htm](kingmaker-bestiary-items/0Rjcnl6ynsH5LKjO.htm)|Warfare Lore|vide|
|[0UqqnUjTvhlc7sGv.htm](kingmaker-bestiary-items/0UqqnUjTvhlc7sGv.htm)|Bardic Lore|vide|
|[0VYJH6vuFSZm9NIX.htm](kingmaker-bestiary-items/0VYJH6vuFSZm9NIX.htm)|Primal Innate Spells|vide|
|[0WYsG1PdrVoJN2Ue.htm](kingmaker-bestiary-items/0WYsG1PdrVoJN2Ue.htm)|Divine Innate Spells|vide|
|[1lrxLnSpTkBwcSli.htm](kingmaker-bestiary-items/1lrxLnSpTkBwcSli.htm)|Ghostly Battle Axe|vide|
|[1Lvngo9gteYE4ep9.htm](kingmaker-bestiary-items/1Lvngo9gteYE4ep9.htm)|Dagger|vide|
|[1PdKOeeXtOPcFYCb.htm](kingmaker-bestiary-items/1PdKOeeXtOPcFYCb.htm)|Composite Longbow|vide|
|[1VH2ArAHvIhMi57j.htm](kingmaker-bestiary-items/1VH2ArAHvIhMi57j.htm)|Fist|vide|
|[1Y7PAPhO3aVpgKoq.htm](kingmaker-bestiary-items/1Y7PAPhO3aVpgKoq.htm)|Ghostly Rapier|vide|
|[28VxmNDvpa40sH4P.htm](kingmaker-bestiary-items/28VxmNDvpa40sH4P.htm)|Divine Prepared Spells|vide|
|[2A5WqL14A7eKaElj.htm](kingmaker-bestiary-items/2A5WqL14A7eKaElj.htm)|Claw|vide|
|[312Fug37MtFpvG0P.htm](kingmaker-bestiary-items/312Fug37MtFpvG0P.htm)|Tongue|vide|
|[38JeDzTzMbmPrydc.htm](kingmaker-bestiary-items/38JeDzTzMbmPrydc.htm)|Survival|vide|
|[3ANSkECTu3y2Lt3C.htm](kingmaker-bestiary-items/3ANSkECTu3y2Lt3C.htm)|Dogslicer|vide|
|[3gmJPQIKZq7dcelo.htm](kingmaker-bestiary-items/3gmJPQIKZq7dcelo.htm)|Composite Shortbow|vide|
|[3hPEjn6DWOXtuQ2I.htm](kingmaker-bestiary-items/3hPEjn6DWOXtuQ2I.htm)|Occult Innate Spells|vide|
|[3VX47OPvg5PAjzsD.htm](kingmaker-bestiary-items/3VX47OPvg5PAjzsD.htm)|Primal Spontaneous Spells|vide|
|[4bbZ6NaZSeW5sE1f.htm](kingmaker-bestiary-items/4bbZ6NaZSeW5sE1f.htm)|Trident|vide|
|[4C0eLAnhYCsQz9KO.htm](kingmaker-bestiary-items/4C0eLAnhYCsQz9KO.htm)|+4 Status to All Saves vs. Mental|vide|
|[4hjXOaTYgTu9yoad.htm](kingmaker-bestiary-items/4hjXOaTYgTu9yoad.htm)|Claws|vide|
|[4KoYrmcXEdMKXapg.htm](kingmaker-bestiary-items/4KoYrmcXEdMKXapg.htm)|Academia Lore|vide|
|[4S2A9APP31jlMK4Q.htm](kingmaker-bestiary-items/4S2A9APP31jlMK4Q.htm)|Arcane Spontaneous Spells|vide|
|[4WaSsGcstf79Y44R.htm](kingmaker-bestiary-items/4WaSsGcstf79Y44R.htm)|Jaws|vide|
|[4z1JD9bhf1R5wpmX.htm](kingmaker-bestiary-items/4z1JD9bhf1R5wpmX.htm)|Trident|vide|
|[5dKTN8eobwXG2iGV.htm](kingmaker-bestiary-items/5dKTN8eobwXG2iGV.htm)|Arcane Spontaneous Spells|vide|
|[5dvPmX8OUqLml3m6.htm](kingmaker-bestiary-items/5dvPmX8OUqLml3m6.htm)|Claw|vide|
|[5LclMp6q589Sl0Mn.htm](kingmaker-bestiary-items/5LclMp6q589Sl0Mn.htm)|Claw|vide|
|[5LgL3UMxFZRmIgXy.htm](kingmaker-bestiary-items/5LgL3UMxFZRmIgXy.htm)|Vulture Beak|vide|
|[5MTbqNxF7xLIRqQY.htm](kingmaker-bestiary-items/5MTbqNxF7xLIRqQY.htm)|Abbadon Lore|vide|
|[5xktm2PrBdKEX5oh.htm](kingmaker-bestiary-items/5xktm2PrBdKEX5oh.htm)|Dagger|vide|
|[61i4ipb2qXp6RshD.htm](kingmaker-bestiary-items/61i4ipb2qXp6RshD.htm)|Lumber Lore|vide|
|[61pWFfMaO4glQNlI.htm](kingmaker-bestiary-items/61pWFfMaO4glQNlI.htm)|Fetch Weapon|vide|
|[64snLNQ9P2c8IffF.htm](kingmaker-bestiary-items/64snLNQ9P2c8IffF.htm)|Guild Lore|vide|
|[6e66VeO6FBdw8Fjn.htm](kingmaker-bestiary-items/6e66VeO6FBdw8Fjn.htm)|Hatchet|vide|
|[6k6LtJmrZAfTImSh.htm](kingmaker-bestiary-items/6k6LtJmrZAfTImSh.htm)|Staff|vide|
|[6k7jBeAiFp27Wcv1.htm](kingmaker-bestiary-items/6k7jBeAiFp27Wcv1.htm)|Bard Composition Spells|vide|
|[6MwtMijY3R7DbxaJ.htm](kingmaker-bestiary-items/6MwtMijY3R7DbxaJ.htm)|Ghostly Attack|vide|
|[6SNxPBIEy6G6pJ5S.htm](kingmaker-bestiary-items/6SNxPBIEy6G6pJ5S.htm)|Fist|vide|
|[6XfWpghskVUmJSVG.htm](kingmaker-bestiary-items/6XfWpghskVUmJSVG.htm)|Aldori Dueling Sword|vide|
|[7ERyflHHinGfODO2.htm](kingmaker-bestiary-items/7ERyflHHinGfODO2.htm)|Primal Prepared Spells|vide|
|[7fPsSiHuHlM11fhF.htm](kingmaker-bestiary-items/7fPsSiHuHlM11fhF.htm)|+1 Status to All Saves vs. Magic|vide|
|[7MElwF26Lym91Wch.htm](kingmaker-bestiary-items/7MElwF26Lym91Wch.htm)|Longsword|vide|
|[7SmLN0kqsTk5kyLK.htm](kingmaker-bestiary-items/7SmLN0kqsTk5kyLK.htm)|Crystal Scimitar|vide|
|[7TT0BqubK5ErNyN3.htm](kingmaker-bestiary-items/7TT0BqubK5ErNyN3.htm)|Ranseur|vide|
|[83BVCoUzow3GIr5W.htm](kingmaker-bestiary-items/83BVCoUzow3GIr5W.htm)|Claw|vide|
|[873UBJDHNOwZqAn0.htm](kingmaker-bestiary-items/873UBJDHNOwZqAn0.htm)|Longsword|vide|
|[8FamBadz3fY1Ktf1.htm](kingmaker-bestiary-items/8FamBadz3fY1Ktf1.htm)|Javelin|vide|
|[8I1oL3XnWQjb1WeF.htm](kingmaker-bestiary-items/8I1oL3XnWQjb1WeF.htm)|Torture Lore|vide|
|[8Ib2hxgIftGdnnC4.htm](kingmaker-bestiary-items/8Ib2hxgIftGdnnC4.htm)|Occult Focus Spells|vide|
|[8lbGHlEEQstmS0rd.htm](kingmaker-bestiary-items/8lbGHlEEQstmS0rd.htm)|Club|vide|
|[8s4nFCFF2c0eNDnN.htm](kingmaker-bestiary-items/8s4nFCFF2c0eNDnN.htm)|Divine Prepared Spells|vide|
|[8Ss1d21OHToUfFNl.htm](kingmaker-bestiary-items/8Ss1d21OHToUfFNl.htm)|Yog-Sothoth Lore|vide|
|[8T1NpFqpuMXKzB51.htm](kingmaker-bestiary-items/8T1NpFqpuMXKzB51.htm)|Jaws|vide|
|[8wo9GPXQVySZtBb5.htm](kingmaker-bestiary-items/8wo9GPXQVySZtBb5.htm)|Arcane Prepared Spells|vide|
|[8zIAec8NlrwPEbab.htm](kingmaker-bestiary-items/8zIAec8NlrwPEbab.htm)|Trident|vide|
|[95EZclhH2zhKgoxm.htm](kingmaker-bestiary-items/95EZclhH2zhKgoxm.htm)|Dagger|vide|
|[9F78hYb3XMI7z4ai.htm](kingmaker-bestiary-items/9F78hYb3XMI7z4ai.htm)|Rapier|vide|
|[9h5jUpLhqi52Tedi.htm](kingmaker-bestiary-items/9h5jUpLhqi52Tedi.htm)|Brevoy Lore|vide|
|[9jMNfcA0u8pECJhS.htm](kingmaker-bestiary-items/9jMNfcA0u8pECJhS.htm)|Dagger|vide|
|[9JQwWcFOpgrzmEIy.htm](kingmaker-bestiary-items/9JQwWcFOpgrzmEIy.htm)|Dagger|vide|
|[9kVpK6J5EVHZcp0K.htm](kingmaker-bestiary-items/9kVpK6J5EVHZcp0K.htm)|Major Staff of Transmutation|vide|
|[9PUgsYyJWDJ9J0sE.htm](kingmaker-bestiary-items/9PUgsYyJWDJ9J0sE.htm)|+1 Status vs. Posion|vide|
|[9rDjTECEOqQb11SQ.htm](kingmaker-bestiary-items/9rDjTECEOqQb11SQ.htm)|Dagger|vide|
|[9VgJpOQ6xJ9gTgCU.htm](kingmaker-bestiary-items/9VgJpOQ6xJ9gTgCU.htm)|Bard Spontaneous Spells|vide|
|[a7WtcnRg6JlsVIDg.htm](kingmaker-bestiary-items/a7WtcnRg6JlsVIDg.htm)|Maul|vide|
|[A9Hfkb1IhZNWZW7U.htm](kingmaker-bestiary-items/A9Hfkb1IhZNWZW7U.htm)|Divine Prepared Spells|vide|
|[AIde6VUDBdxDlQQS.htm](kingmaker-bestiary-items/AIde6VUDBdxDlQQS.htm)|Dagger|vide|
|[AjjeSlBUiJKE8SOB.htm](kingmaker-bestiary-items/AjjeSlBUiJKE8SOB.htm)|Divine Prepared Spells|vide|
|[AksHJ5xCuwQbdOmq.htm](kingmaker-bestiary-items/AksHJ5xCuwQbdOmq.htm)|Claw|vide|
|[ap7UmDIuWShLoSbJ.htm](kingmaker-bestiary-items/ap7UmDIuWShLoSbJ.htm)|Forest Lore|vide|
|[ApHre7Dw1aJJlSiv.htm](kingmaker-bestiary-items/ApHre7Dw1aJJlSiv.htm)|Hatchet|vide|
|[Asj3JNQgw7B1ZXXC.htm](kingmaker-bestiary-items/Asj3JNQgw7B1ZXXC.htm)|Primal Innate Spells|vide|
|[atdIIgSxVkVrP1Ti.htm](kingmaker-bestiary-items/atdIIgSxVkVrP1Ti.htm)|Druid Order Spells|vide|
|[auHvPa2qajS0vG6w.htm](kingmaker-bestiary-items/auHvPa2qajS0vG6w.htm)|Primal Spontaneous Spells|vide|
|[axy7Bj9PMjBKlFdd.htm](kingmaker-bestiary-items/axy7Bj9PMjBKlFdd.htm)|Club|vide|
|[AYlNJO8ziqGDNG6F.htm](kingmaker-bestiary-items/AYlNJO8ziqGDNG6F.htm)|Forest Lore|vide|
|[azD5OELMU31IrNwz.htm](kingmaker-bestiary-items/azD5OELMU31IrNwz.htm)|Dagger|vide|
|[BGZlipUdtmQIe6By.htm](kingmaker-bestiary-items/BGZlipUdtmQIe6By.htm)|Divine Prepared Spells|vide|
|[BMGyGT2KQinKwWfe.htm](kingmaker-bestiary-items/BMGyGT2KQinKwWfe.htm)|Shortsword|vide|
|[BMzs4OQEu3Mt6iZQ.htm](kingmaker-bestiary-items/BMzs4OQEu3Mt6iZQ.htm)|Gambling Lore|vide|
|[BqhAFrC5aDQ1BKoT.htm](kingmaker-bestiary-items/BqhAFrC5aDQ1BKoT.htm)|Blowgun|vide|
|[BuBkGYAPCPnJ03go.htm](kingmaker-bestiary-items/BuBkGYAPCPnJ03go.htm)|Claw|vide|
|[bZEeE1MlmmqIJiiK.htm](kingmaker-bestiary-items/bZEeE1MlmmqIJiiK.htm)|Cleric Domain Spells|vide|
|[C4fZA5gB6JeZf08P.htm](kingmaker-bestiary-items/C4fZA5gB6JeZf08P.htm)|Club|vide|
|[CBKkrJnJ9DcFA6p0.htm](kingmaker-bestiary-items/CBKkrJnJ9DcFA6p0.htm)|Hunting Lore|vide|
|[cc2QQL18PozIgxVb.htm](kingmaker-bestiary-items/cc2QQL18PozIgxVb.htm)|Wizard School Spells|vide|
|[cFKmkUCecFtQweau.htm](kingmaker-bestiary-items/cFKmkUCecFtQweau.htm)|Occult Spontaneous Spells|vide|
|[cjGRYigrrey1TIiS.htm](kingmaker-bestiary-items/cjGRYigrrey1TIiS.htm)|Underworld Lore|vide|
|[ckg1JdT2pmJlw8zC.htm](kingmaker-bestiary-items/ckg1JdT2pmJlw8zC.htm)|Dagger|vide|
|[ckkFk6fhxAXrQHzv.htm](kingmaker-bestiary-items/ckkFk6fhxAXrQHzv.htm)|Composite Shortbow|vide|
|[cpFEKCq7CWkJ3qrw.htm](kingmaker-bestiary-items/cpFEKCq7CWkJ3qrw.htm)|Trident|vide|
|[cu5gzpbsORryK2Zs.htm](kingmaker-bestiary-items/cu5gzpbsORryK2Zs.htm)|Athletics|vide|
|[d2pAqCh02beVW16z.htm](kingmaker-bestiary-items/d2pAqCh02beVW16z.htm)|Jaws|vide|
|[D4f8OzwwMvBLI86y.htm](kingmaker-bestiary-items/D4f8OzwwMvBLI86y.htm)|Cold Iron Shortsword|vide|
|[d9FqMXrNIAm6rnHs.htm](kingmaker-bestiary-items/d9FqMXrNIAm6rnHs.htm)|Divine Innate Spells|vide|
|[dahtG8lHhjQBQemo.htm](kingmaker-bestiary-items/dahtG8lHhjQBQemo.htm)|Hurled Thorn|vide|
|[dCRhF1JVyPGvXOfW.htm](kingmaker-bestiary-items/dCRhF1JVyPGvXOfW.htm)|First World Lore|vide|
|[dCxHG55HkAHx4awZ.htm](kingmaker-bestiary-items/dCxHG55HkAHx4awZ.htm)|Ritual|vide|
|[DdEls9nTJvA7pEi0.htm](kingmaker-bestiary-items/DdEls9nTJvA7pEi0.htm)|Blowgun|vide|
|[DeAD5IPwOAtQ1Zjr.htm](kingmaker-bestiary-items/DeAD5IPwOAtQ1Zjr.htm)|Kukri|vide|
|[dGa7fiQkgtKN8PDA.htm](kingmaker-bestiary-items/dGa7fiQkgtKN8PDA.htm)|Crafting|vide|
|[dhHfUP1VQHRPtvdU.htm](kingmaker-bestiary-items/dhHfUP1VQHRPtvdU.htm)|Stealth|vide|
|[dHrjeuan3T6fH0jz.htm](kingmaker-bestiary-items/dHrjeuan3T6fH0jz.htm)|Rituals|vide|
|[dhTLYiCmWDkONUN5.htm](kingmaker-bestiary-items/dhTLYiCmWDkONUN5.htm)|Jaws|vide|
|[DksWsSv1BvpLeNhJ.htm](kingmaker-bestiary-items/DksWsSv1BvpLeNhJ.htm)|Jaws|vide|
|[DLI2axhKyDzeg5Ub.htm](kingmaker-bestiary-items/DLI2axhKyDzeg5Ub.htm)|Primal Innate Spells|vide|
|[DNaScDDIg4t6u0Hw.htm](kingmaker-bestiary-items/DNaScDDIg4t6u0Hw.htm)|Ballista Bolt|vide|
|[dsVVZHTsFi9iFwgD.htm](kingmaker-bestiary-items/dsVVZHTsFi9iFwgD.htm)|Hill Lore|vide|
|[DUWbE68XkGU2jM9J.htm](kingmaker-bestiary-items/DUWbE68XkGU2jM9J.htm)|Fist|vide|
|[dX22wsxoPsGLzOQ0.htm](kingmaker-bestiary-items/dX22wsxoPsGLzOQ0.htm)|Dagger|vide|
|[DxTspLZpQI4zVPoa.htm](kingmaker-bestiary-items/DxTspLZpQI4zVPoa.htm)|Sorcerer Bloodline Spells|vide|
|[e1txRyfONnCuYPny.htm](kingmaker-bestiary-items/e1txRyfONnCuYPny.htm)|+1 Status to All Saves vs. Magic|vide|
|[E3C47mJQhyySioGh.htm](kingmaker-bestiary-items/E3C47mJQhyySioGh.htm)|Hatchet|vide|
|[E3r5U0LkfWOXhgLt.htm](kingmaker-bestiary-items/E3r5U0LkfWOXhgLt.htm)|Shortbow|vide|
|[EihtJU42yTV6LfVg.htm](kingmaker-bestiary-items/EihtJU42yTV6LfVg.htm)|Fetch Weapon|vide|
|[ELQWppSD9MT72Ci1.htm](kingmaker-bestiary-items/ELQWppSD9MT72Ci1.htm)|Staff of Fire (Major)|vide|
|[Epu5XvonDsZQClpu.htm](kingmaker-bestiary-items/Epu5XvonDsZQClpu.htm)|Flechette|vide|
|[ESPLHQJWpOyak6Wa.htm](kingmaker-bestiary-items/ESPLHQJWpOyak6Wa.htm)|Battle Axe|vide|
|[eSXITfBnqJ3s71nu.htm](kingmaker-bestiary-items/eSXITfBnqJ3s71nu.htm)|Divine Focus Spells|vide|
|[fckcc8VGCaiue1sX.htm](kingmaker-bestiary-items/fckcc8VGCaiue1sX.htm)|Divine Prepared Spells|vide|
|[FKNzpxoNtzzC5ytc.htm](kingmaker-bestiary-items/FKNzpxoNtzzC5ytc.htm)|Dagger|vide|
|[Fl8QSyuys5Y8YCrK.htm](kingmaker-bestiary-items/Fl8QSyuys5Y8YCrK.htm)|Ghostly Hand|vide|
|[FMyBd24spHonNXKf.htm](kingmaker-bestiary-items/FMyBd24spHonNXKf.htm)|Occult Spontaneous Spells|vide|
|[fqcdNor1vTHZe5Zl.htm](kingmaker-bestiary-items/fqcdNor1vTHZe5Zl.htm)|Occult Spontaneous Spells|vide|
|[fqO0yIYBu1FrLvB9.htm](kingmaker-bestiary-items/fqO0yIYBu1FrLvB9.htm)|Scythe|vide|
|[ftnV0syBwJfqGk6Z.htm](kingmaker-bestiary-items/ftnV0syBwJfqGk6Z.htm)|Thorn|vide|
|[fwojd1PspBIG8CEY.htm](kingmaker-bestiary-items/fwojd1PspBIG8CEY.htm)|Shortsword|vide|
|[fxgXQNNgTLwJ2JoU.htm](kingmaker-bestiary-items/fxgXQNNgTLwJ2JoU.htm)|Arcane Prepared Spells|vide|
|[FzjT88a1Bn4XsEYM.htm](kingmaker-bestiary-items/FzjT88a1Bn4XsEYM.htm)|Battle Axe|vide|
|[g6XydGLF6RtVUp5L.htm](kingmaker-bestiary-items/g6XydGLF6RtVUp5L.htm)|Stealth|vide|
|[gbg7dpE5qccG0uh1.htm](kingmaker-bestiary-items/gbg7dpE5qccG0uh1.htm)|Greater Shock Maul|vide|
|[gDENaXLY2aYA7jYG.htm](kingmaker-bestiary-items/gDENaXLY2aYA7jYG.htm)|Greataxe|vide|
|[gj9hDQvaXekxyv1Q.htm](kingmaker-bestiary-items/gj9hDQvaXekxyv1Q.htm)|Longsword|vide|
|[gS87nm7lmCmY8pzz.htm](kingmaker-bestiary-items/gS87nm7lmCmY8pzz.htm)|First World Lore|vide|
|[GxSpJFY9UJiwry54.htm](kingmaker-bestiary-items/GxSpJFY9UJiwry54.htm)|Jaws|vide|
|[GZ3aIv1wg9M7gGFU.htm](kingmaker-bestiary-items/GZ3aIv1wg9M7gGFU.htm)|Giant Lore|vide|
|[h0MxDLIAI1CLU61G.htm](kingmaker-bestiary-items/h0MxDLIAI1CLU61G.htm)|Thorny Lash|vide|
|[H5KqV1tEsBEfhfvU.htm](kingmaker-bestiary-items/H5KqV1tEsBEfhfvU.htm)|Hunt Prey|vide|
|[hbOLSP9RZmA3hyU8.htm](kingmaker-bestiary-items/hbOLSP9RZmA3hyU8.htm)|+1 Status to All Saves vs. Poison|vide|
|[hE1MVc0Pxfi3nERv.htm](kingmaker-bestiary-items/hE1MVc0Pxfi3nERv.htm)|Greatsword|vide|
|[HfIRzOBnqZx60iSO.htm](kingmaker-bestiary-items/HfIRzOBnqZx60iSO.htm)|Club|vide|
|[hhIAMezYcNkDfXe8.htm](kingmaker-bestiary-items/hhIAMezYcNkDfXe8.htm)|Ritual|vide|
|[HjxDZjwRHwjQx54V.htm](kingmaker-bestiary-items/HjxDZjwRHwjQx54V.htm)|Spear|vide|
|[hsn4E9lEPjjDCMAN.htm](kingmaker-bestiary-items/hsn4E9lEPjjDCMAN.htm)|Occult Focus Spells|vide|
|[htpcG9CC3rJNay4j.htm](kingmaker-bestiary-items/htpcG9CC3rJNay4j.htm)|Fist|vide|
|[HuZ0GwHrpEco9S3C.htm](kingmaker-bestiary-items/HuZ0GwHrpEco9S3C.htm)|Ritual|vide|
|[hxeZesdJLgHPkEJ5.htm](kingmaker-bestiary-items/hxeZesdJLgHPkEJ5.htm)|Spectral Hand|vide|
|[hY6mcAJCKxbG0Bzu.htm](kingmaker-bestiary-items/hY6mcAJCKxbG0Bzu.htm)|Rituals|vide|
|[i98RWuQkObalnsWO.htm](kingmaker-bestiary-items/i98RWuQkObalnsWO.htm)|Shortbow|vide|
|[iGREJT9RZVh5TMqy.htm](kingmaker-bestiary-items/iGREJT9RZVh5TMqy.htm)|Ritual|vide|
|[IkPoWCmWZcyW5osI.htm](kingmaker-bestiary-items/IkPoWCmWZcyW5osI.htm)|Staff|vide|
|[In6OZtLo0Avr0mV1.htm](kingmaker-bestiary-items/In6OZtLo0Avr0mV1.htm)|Trident|vide|
|[IP2DHIvoqbsWaRVU.htm](kingmaker-bestiary-items/IP2DHIvoqbsWaRVU.htm)|Cleaver|vide|
|[ITgYWyiUVLD2SpLl.htm](kingmaker-bestiary-items/ITgYWyiUVLD2SpLl.htm)|Composite Shortbow|vide|
|[IYbU9IiWVPm4CGyc.htm](kingmaker-bestiary-items/IYbU9IiWVPm4CGyc.htm)|Fist|vide|
|[j1GC3KLLJa3BPe0o.htm](kingmaker-bestiary-items/j1GC3KLLJa3BPe0o.htm)|Battle Axe|vide|
|[j4kcdayowXPW4Avm.htm](kingmaker-bestiary-items/j4kcdayowXPW4Avm.htm)|Horns|vide|
|[j8nRwfwxCyxXRet5.htm](kingmaker-bestiary-items/j8nRwfwxCyxXRet5.htm)|Dagger|vide|
|[jEkasU1qDlN76A9i.htm](kingmaker-bestiary-items/jEkasU1qDlN76A9i.htm)|Occult Innate Spells|vide|
|[JGybGGqJOMowAau2.htm](kingmaker-bestiary-items/JGybGGqJOMowAau2.htm)|Divine Rituals|vide|
|[jkgZ1UCUT3j7cOf2.htm](kingmaker-bestiary-items/jkgZ1UCUT3j7cOf2.htm)|Restov Lore|vide|
|[jmgh011n8MXGN953.htm](kingmaker-bestiary-items/jmgh011n8MXGN953.htm)|Dagger|vide|
|[jp5lLzCnJEJ3qCUl.htm](kingmaker-bestiary-items/jp5lLzCnJEJ3qCUl.htm)|Arcane Spells Prepared|vide|
|[JP9GiA6aaQlXRKAE.htm](kingmaker-bestiary-items/JP9GiA6aaQlXRKAE.htm)|Primal Prepared Spells|vide|
|[jUFQA2dWOvLpv6zg.htm](kingmaker-bestiary-items/jUFQA2dWOvLpv6zg.htm)|Fist|vide|
|[jvDyUbBwbo3R1H4x.htm](kingmaker-bestiary-items/jvDyUbBwbo3R1H4x.htm)|Divine Prepared Spells|vide|
|[K470oCZYTiRoTfqf.htm](kingmaker-bestiary-items/K470oCZYTiRoTfqf.htm)|Jaws|vide|
|[k6dnEhWODfyRtg4b.htm](kingmaker-bestiary-items/k6dnEhWODfyRtg4b.htm)|Composite Shortbow|vide|
|[kaPLdey2Dv3F1VUN.htm](kingmaker-bestiary-items/kaPLdey2Dv3F1VUN.htm)|Oathbow|vide|
|[KbYGZddUNQGhrhEf.htm](kingmaker-bestiary-items/KbYGZddUNQGhrhEf.htm)|Composite Longbow|vide|
|[KCOXqXVayJMKPZVz.htm](kingmaker-bestiary-items/KCOXqXVayJMKPZVz.htm)|Composite Longbow|vide|
|[kdl1FTleQZfZf6eP.htm](kingmaker-bestiary-items/kdl1FTleQZfZf6eP.htm)|Primal Spontaneous Spells|vide|
|[KEm1y8hEl4sjQ6Cv.htm](kingmaker-bestiary-items/KEm1y8hEl4sjQ6Cv.htm)|Talon|vide|
|[khNk0fYgnDMF7kKq.htm](kingmaker-bestiary-items/khNk0fYgnDMF7kKq.htm)|First World Lore|vide|
|[kJ9e1DXfbX04Qfoe.htm](kingmaker-bestiary-items/kJ9e1DXfbX04Qfoe.htm)|+1 Status to All Saves vs. Magic|vide|
|[Kok7cEEC0PeallmB.htm](kingmaker-bestiary-items/Kok7cEEC0PeallmB.htm)|Horns|vide|
|[koqEAUHXo9t7wqVS.htm](kingmaker-bestiary-items/koqEAUHXo9t7wqVS.htm)|Mace|vide|
|[kPaYMeS20gIcKi3E.htm](kingmaker-bestiary-items/kPaYMeS20gIcKi3E.htm)|Primal Innate Spells|vide|
|[KpHIq12RL6K9ukG2.htm](kingmaker-bestiary-items/KpHIq12RL6K9ukG2.htm)|First World Lore|vide|
|[kTN86guVl5qzlzIK.htm](kingmaker-bestiary-items/kTN86guVl5qzlzIK.htm)|Primal Innate Spells|vide|
|[kTXlEGWWz4WnsrwF.htm](kingmaker-bestiary-items/kTXlEGWWz4WnsrwF.htm)|Warfare Lore|vide|
|[kU6LiydL3jldv5KQ.htm](kingmaker-bestiary-items/kU6LiydL3jldv5KQ.htm)|Primal Innate Spells|vide|
|[kX089KrOjeO3aDf3.htm](kingmaker-bestiary-items/kX089KrOjeO3aDf3.htm)|Composite Longbow|vide|
|[LbdwyQgOrSBqDgv9.htm](kingmaker-bestiary-items/LbdwyQgOrSBqDgv9.htm)|Dagger|vide|
|[LC8WDqxmulYb1L4q.htm](kingmaker-bestiary-items/LC8WDqxmulYb1L4q.htm)|Dagger|vide|
|[LCVw81WewWAm0wOB.htm](kingmaker-bestiary-items/LCVw81WewWAm0wOB.htm)|Primal Innate Spells|vide|
|[LEDnvueR2NzH8zkD.htm](kingmaker-bestiary-items/LEDnvueR2NzH8zkD.htm)|Dagger|vide|
|[LHmME2otANk4a79s.htm](kingmaker-bestiary-items/LHmME2otANk4a79s.htm)|Worm Jaws|vide|
|[LHPXbhgsSYCjB4uG.htm](kingmaker-bestiary-items/LHPXbhgsSYCjB4uG.htm)|Club|vide|
|[lim8vhAnHsss3MkL.htm](kingmaker-bestiary-items/lim8vhAnHsss3MkL.htm)|Light Hammer|vide|
|[LK0wAtZtoKTLOgnz.htm](kingmaker-bestiary-items/LK0wAtZtoKTLOgnz.htm)|Claw|vide|
|[LTaIE8s2797qElxg.htm](kingmaker-bestiary-items/LTaIE8s2797qElxg.htm)|Dagger|vide|
|[luiDDR5D3oiCAEZh.htm](kingmaker-bestiary-items/luiDDR5D3oiCAEZh.htm)|Hatchet|vide|
|[M21WXIiu1wI2tftP.htm](kingmaker-bestiary-items/M21WXIiu1wI2tftP.htm)|Ritual|vide|
|[M5azwdU2fCtU2eR5.htm](kingmaker-bestiary-items/M5azwdU2fCtU2eR5.htm)|Gorum Lore|vide|
|[MBYJQAMX2qMwwp1o.htm](kingmaker-bestiary-items/MBYJQAMX2qMwwp1o.htm)|Divine Prepared Spells|vide|
|[mc4qvML9Y9Vngpwo.htm](kingmaker-bestiary-items/mc4qvML9Y9Vngpwo.htm)|Thorny Vine|vide|
|[mcMNYM92nivzmq61.htm](kingmaker-bestiary-items/mcMNYM92nivzmq61.htm)|+2 Status to All Saves vs. Mental|vide|
|[MDHk8qTYvCueXiiR.htm](kingmaker-bestiary-items/MDHk8qTYvCueXiiR.htm)|Dwelling Lore|vide|
|[miMn8Pf7FluNMnzn.htm](kingmaker-bestiary-items/miMn8Pf7FluNMnzn.htm)|Ritual|vide|
|[mkO8uUk9bttHYIRn.htm](kingmaker-bestiary-items/mkO8uUk9bttHYIRn.htm)|Snake Fangs|vide|
|[MMamQCPSgaxFWQd4.htm](kingmaker-bestiary-items/MMamQCPSgaxFWQd4.htm)|Whip|vide|
|[mmgcrA8q8AjJiPoF.htm](kingmaker-bestiary-items/mmgcrA8q8AjJiPoF.htm)|Shuriken|vide|
|[mMPWudSAzEWfl1eN.htm](kingmaker-bestiary-items/mMPWudSAzEWfl1eN.htm)|Composite Longbow|vide|
|[MP8qtlyLZ6pRMVZl.htm](kingmaker-bestiary-items/MP8qtlyLZ6pRMVZl.htm)|Dagger|vide|
|[MrbeKdSngiqGczPC.htm](kingmaker-bestiary-items/MrbeKdSngiqGczPC.htm)|Glaive|vide|
|[mSjLHeaPtyB5Sjcm.htm](kingmaker-bestiary-items/mSjLHeaPtyB5Sjcm.htm)|Fist|vide|
|[MxQXSU7ZH7FLyYf5.htm](kingmaker-bestiary-items/MxQXSU7ZH7FLyYf5.htm)|Scythe|vide|
|[n1fjlb5osV2yl7as.htm](kingmaker-bestiary-items/n1fjlb5osV2yl7as.htm)|+4 Status to All Saves vs. Mental|vide|
|[n5KuZKOcFvPmlkze.htm](kingmaker-bestiary-items/n5KuZKOcFvPmlkze.htm)|Cleric Domain Spells|vide|
|[nHs2Wp0Ifqpenmxk.htm](kingmaker-bestiary-items/nHs2Wp0Ifqpenmxk.htm)|Warhammer|vide|
|[nkFUtIxpiWKJ11ko.htm](kingmaker-bestiary-items/nkFUtIxpiWKJ11ko.htm)|Hand|vide|
|[nnU6e1a4DLTuRnUV.htm](kingmaker-bestiary-items/nnU6e1a4DLTuRnUV.htm)|Academia Lore|vide|
|[NO9VvuwakH04tpcx.htm](kingmaker-bestiary-items/NO9VvuwakH04tpcx.htm)|Hatchet|vide|
|[nP9Bi95YRl4E9QMt.htm](kingmaker-bestiary-items/nP9Bi95YRl4E9QMt.htm)|Greataxe|vide|
|[nrmfu7IJT3ecM1M8.htm](kingmaker-bestiary-items/nrmfu7IJT3ecM1M8.htm)|Beak|vide|
|[nUhHPT76LVNXNYRg.htm](kingmaker-bestiary-items/nUhHPT76LVNXNYRg.htm)|Occult Spontaneous Spells|vide|
|[O2EsiBYHC57XSjTz.htm](kingmaker-bestiary-items/O2EsiBYHC57XSjTz.htm)|Jaws|vide|
|[o2schqbpr94UUldL.htm](kingmaker-bestiary-items/o2schqbpr94UUldL.htm)|Hatchet|vide|
|[O6kjx7rDPPICVkym.htm](kingmaker-bestiary-items/O6kjx7rDPPICVkym.htm)|Rapier|vide|
|[o82vOevA36JTvcnU.htm](kingmaker-bestiary-items/o82vOevA36JTvcnU.htm)|Forest Lore|vide|
|[ohbFrIxS5WF1abVb.htm](kingmaker-bestiary-items/ohbFrIxS5WF1abVb.htm)|Dagger|vide|
|[OiCu7wtLBEPD8y4D.htm](kingmaker-bestiary-items/OiCu7wtLBEPD8y4D.htm)|Spear|vide|
|[oka4i9kQ0yTjUsVF.htm](kingmaker-bestiary-items/oka4i9kQ0yTjUsVF.htm)|Trident|vide|
|[okVVkEEDYOrrlhfS.htm](kingmaker-bestiary-items/okVVkEEDYOrrlhfS.htm)|Cleric Domain Spells|vide|
|[oORyGKlMVkIFWGHJ.htm](kingmaker-bestiary-items/oORyGKlMVkIFWGHJ.htm)|Greatsword|vide|
|[OQv2VZ49tvgCKDhW.htm](kingmaker-bestiary-items/OQv2VZ49tvgCKDhW.htm)|First World Lore|vide|
|[OqVIaOEX3mbFTW9O.htm](kingmaker-bestiary-items/OqVIaOEX3mbFTW9O.htm)|Arcane Spontaneous Spells|vide|
|[OSCP7JLtSYLDVYrO.htm](kingmaker-bestiary-items/OSCP7JLtSYLDVYrO.htm)|Jaws|vide|
|[OvqcWkhFk95fFEEX.htm](kingmaker-bestiary-items/OvqcWkhFk95fFEEX.htm)|Shoulder Spikes|vide|
|[owZSVQs8IkCQjNLE.htm](kingmaker-bestiary-items/owZSVQs8IkCQjNLE.htm)|Composite Longbow|vide|
|[oya1k9x9UZ3zPauA.htm](kingmaker-bestiary-items/oya1k9x9UZ3zPauA.htm)|Rituals|vide|
|[paPaJBwymUtXLyYF.htm](kingmaker-bestiary-items/paPaJBwymUtXLyYF.htm)|Whip|vide|
|[PBGOOwAe8HedCuGy.htm](kingmaker-bestiary-items/PBGOOwAe8HedCuGy.htm)|Longsword|vide|
|[pfic0QiF1jUkDVwR.htm](kingmaker-bestiary-items/pfic0QiF1jUkDVwR.htm)|River Lore|vide|
|[Pfn9Ff6lgtXDae3e.htm](kingmaker-bestiary-items/Pfn9Ff6lgtXDae3e.htm)|Occult Spontaneous Spells|vide|
|[PhsmxJZ4aqEowjOn.htm](kingmaker-bestiary-items/PhsmxJZ4aqEowjOn.htm)|Claws|vide|
|[pllCGrR7E0eAP35u.htm](kingmaker-bestiary-items/pllCGrR7E0eAP35u.htm)|Dagger|vide|
|[PmFUHtH32XT97IlJ.htm](kingmaker-bestiary-items/PmFUHtH32XT97IlJ.htm)|Warfare Lore|vide|
|[pMKjMXZobeZ6zFPm.htm](kingmaker-bestiary-items/pMKjMXZobeZ6zFPm.htm)|Primal Prepared Spells|vide|
|[pmVLSDQuvnEqYH5y.htm](kingmaker-bestiary-items/pmVLSDQuvnEqYH5y.htm)|Fey Lore|vide|
|[po8aw9EkLZ78IlVy.htm](kingmaker-bestiary-items/po8aw9EkLZ78IlVy.htm)|Hatchet|vide|
|[pp6Q7jKBxVkG5uWk.htm](kingmaker-bestiary-items/pp6Q7jKBxVkG5uWk.htm)|Rituals|vide|
|[pPppryZClUfw47fK.htm](kingmaker-bestiary-items/pPppryZClUfw47fK.htm)|+1 Status to All Saves vs. Magic|vide|
|[PqqO1TraA4ak7FIy.htm](kingmaker-bestiary-items/PqqO1TraA4ak7FIy.htm)|Divine Focus Spells|vide|
|[pu63PF0pspvou1kE.htm](kingmaker-bestiary-items/pu63PF0pspvou1kE.htm)|Whiplashing Tail|vide|
|[pu7iTks4TUgGpglG.htm](kingmaker-bestiary-items/pu7iTks4TUgGpglG.htm)|First World Lore|vide|
|[PzkoaPhLG2JLClMM.htm](kingmaker-bestiary-items/PzkoaPhLG2JLClMM.htm)|Longsword|vide|
|[q1z16jklLBVTY0A8.htm](kingmaker-bestiary-items/q1z16jklLBVTY0A8.htm)|Glaive|vide|
|[Q2aaq7yspaPD9kAk.htm](kingmaker-bestiary-items/Q2aaq7yspaPD9kAk.htm)|Nobility Lore|vide|
|[q6QpDgIvY1kfKSWc.htm](kingmaker-bestiary-items/q6QpDgIvY1kfKSWc.htm)|Dagger|vide|
|[q6ZsHR2exUj0eCp7.htm](kingmaker-bestiary-items/q6ZsHR2exUj0eCp7.htm)|Aldori Dueling Sword|vide|
|[QB62e4FfQvymENul.htm](kingmaker-bestiary-items/QB62e4FfQvymENul.htm)|Tail|vide|
|[QDw6f81ZeMuIueQP.htm](kingmaker-bestiary-items/QDw6f81ZeMuIueQP.htm)|Tail|vide|
|[qesdwQLA0OfebLki.htm](kingmaker-bestiary-items/qesdwQLA0OfebLki.htm)|Intimidation|vide|
|[QLqI8GHyMhlnlL3h.htm](kingmaker-bestiary-items/QLqI8GHyMhlnlL3h.htm)|Rock|vide|
|[QQaSJEmqMeqq6kd9.htm](kingmaker-bestiary-items/QQaSJEmqMeqq6kd9.htm)|Lurker Claw|vide|
|[qQY3snkHHYYEQDxW.htm](kingmaker-bestiary-items/qQY3snkHHYYEQDxW.htm)|Primal Spontaneous Spells|vide|
|[QryAkNRvJCn0uaSN.htm](kingmaker-bestiary-items/QryAkNRvJCn0uaSN.htm)|Dagger|vide|
|[QsQBz0EKCXO9IzXT.htm](kingmaker-bestiary-items/QsQBz0EKCXO9IzXT.htm)|Rapier|vide|
|[qTKs06NZl2BkZiYP.htm](kingmaker-bestiary-items/qTKs06NZl2BkZiYP.htm)|+1 Status to All Saves vs. Positive|vide|
|[qVOjbIdihxei6lTm.htm](kingmaker-bestiary-items/qVOjbIdihxei6lTm.htm)|Composite Longbow|vide|
|[QW0dGg59W0r9tJtU.htm](kingmaker-bestiary-items/QW0dGg59W0r9tJtU.htm)|Stealth|vide|
|[QXyRODDmF4tozV5P.htm](kingmaker-bestiary-items/QXyRODDmF4tozV5P.htm)|Longsword|vide|
|[qYTvq8RiCpkT6wG7.htm](kingmaker-bestiary-items/qYTvq8RiCpkT6wG7.htm)|Dagger|vide|
|[R2ZrKwkuQb3XIG4z.htm](kingmaker-bestiary-items/R2ZrKwkuQb3XIG4z.htm)|Dagger|vide|
|[rbZIdY30oD5udoGi.htm](kingmaker-bestiary-items/rbZIdY30oD5udoGi.htm)|Occult Focus Spells|vide|
|[rdf4aOsP7jywGEIC.htm](kingmaker-bestiary-items/rdf4aOsP7jywGEIC.htm)|Primal Innate Spells|vide|
|[RflctXAVkddCvSeq.htm](kingmaker-bestiary-items/RflctXAVkddCvSeq.htm)|Jaws|vide|
|[rkdBPs8QgQ9Tfvpj.htm](kingmaker-bestiary-items/rkdBPs8QgQ9Tfvpj.htm)|Ovinrbaane|vide|
|[RNxWSJIolrbdRbNy.htm](kingmaker-bestiary-items/RNxWSJIolrbdRbNy.htm)|Bardic Lore|vide|
|[rWVM81zPBmywUXvF.htm](kingmaker-bestiary-items/rWVM81zPBmywUXvF.htm)|Occult Spontaneous Spells|vide|
|[rZftbooG5W57BA0g.htm](kingmaker-bestiary-items/rZftbooG5W57BA0g.htm)|Divine Prepared Spells|vide|
|[S2bjckIBFgZGuliH.htm](kingmaker-bestiary-items/S2bjckIBFgZGuliH.htm)|Ogre Hook|vide|
|[S6sguxBDLkRhBNAS.htm](kingmaker-bestiary-items/S6sguxBDLkRhBNAS.htm)|Divine Prepared Spells|vide|
|[sHdlA1TMLwvXBRX0.htm](kingmaker-bestiary-items/sHdlA1TMLwvXBRX0.htm)|Composite Shortbow|vide|
|[Sn0vYKbSWgQmv5g7.htm](kingmaker-bestiary-items/Sn0vYKbSWgQmv5g7.htm)|Cleric Domain Spells|vide|
|[SQ4DUNap1xbzs5Sq.htm](kingmaker-bestiary-items/SQ4DUNap1xbzs5Sq.htm)|Jaws|vide|
|[ssgy6SBt7L8uYlUi.htm](kingmaker-bestiary-items/ssgy6SBt7L8uYlUi.htm)|Arcane Innate Spells|vide|
|[sTjHob8UlAoUPu20.htm](kingmaker-bestiary-items/sTjHob8UlAoUPu20.htm)|Yog-Sothoth Lore|vide|
|[T1dMCJU34dJYVgZw.htm](kingmaker-bestiary-items/T1dMCJU34dJYVgZw.htm)|Club|vide|
|[t4BxYpWMm3lEqjxG.htm](kingmaker-bestiary-items/t4BxYpWMm3lEqjxG.htm)|Occult Spontaneous Spells|vide|
|[T5dyuJhTrUcC9fqv.htm](kingmaker-bestiary-items/T5dyuJhTrUcC9fqv.htm)|Claw|vide|
|[T9wwE2uJLrI8NxyM.htm](kingmaker-bestiary-items/T9wwE2uJLrI8NxyM.htm)|Bear Trap|vide|
|[taxWERmxNkRViJLR.htm](kingmaker-bestiary-items/taxWERmxNkRViJLR.htm)|Greataxe|vide|
|[TCbRZkzWxt6vXhCx.htm](kingmaker-bestiary-items/TCbRZkzWxt6vXhCx.htm)|+2 Status to All Saves vs. Fear Effects|vide|
|[tDlyF5IZNsYAYYhP.htm](kingmaker-bestiary-items/tDlyF5IZNsYAYYhP.htm)|Thrown Object|vide|
|[thieXYs44gA9P3Uo.htm](kingmaker-bestiary-items/thieXYs44gA9P3Uo.htm)|Divine Innate Spells|vide|
|[TjcIBNGiRbsiYxqr.htm](kingmaker-bestiary-items/TjcIBNGiRbsiYxqr.htm)|Ritual|vide|
|[tqOuI2ayN5VhcNyW.htm](kingmaker-bestiary-items/tqOuI2ayN5VhcNyW.htm)|Hand Crossbow|vide|
|[TR472pgOTxUJrN9B.htm](kingmaker-bestiary-items/TR472pgOTxUJrN9B.htm)|Club|vide|
|[TSDKiQYnEhZxGO3K.htm](kingmaker-bestiary-items/TSDKiQYnEhZxGO3K.htm)|Warfare Lore|vide|
|[TWA2MXZuX91plGrL.htm](kingmaker-bestiary-items/TWA2MXZuX91plGrL.htm)|Art Lore|vide|
|[tWbqmLk652jnFNiN.htm](kingmaker-bestiary-items/tWbqmLk652jnFNiN.htm)|Ritual|vide|
|[Tzi4lR0Oigo4U8Wu.htm](kingmaker-bestiary-items/Tzi4lR0Oigo4U8Wu.htm)|Emerald Beam|vide|
|[Ubytn72fPKxfwdab.htm](kingmaker-bestiary-items/Ubytn72fPKxfwdab.htm)|Dagger|vide|
|[ucBBQrbesTH1rIdv.htm](kingmaker-bestiary-items/ucBBQrbesTH1rIdv.htm)|Light Hammer|vide|
|[UGAcwTdmLcqn2A5C.htm](kingmaker-bestiary-items/UGAcwTdmLcqn2A5C.htm)|River Lore|vide|
|[UiApvNrZOpUiRTze.htm](kingmaker-bestiary-items/UiApvNrZOpUiRTze.htm)|Centipede Mandibles|vide|
|[UiU7w5ovykgz63Vf.htm](kingmaker-bestiary-items/UiU7w5ovykgz63Vf.htm)|First World Lore|vide|
|[Up8YexckH0qYu7Mg.htm](kingmaker-bestiary-items/Up8YexckH0qYu7Mg.htm)|Hatchet|vide|
|[UtXatkaqCOacbbnj.htm](kingmaker-bestiary-items/UtXatkaqCOacbbnj.htm)|Jaws|vide|
|[uv3BAfwMvrlyZDI6.htm](kingmaker-bestiary-items/uv3BAfwMvrlyZDI6.htm)|Fist|vide|
|[v0ppjSET59hXq9Z5.htm](kingmaker-bestiary-items/v0ppjSET59hXq9Z5.htm)|Dagger|vide|
|[v76tHhlGlywyinPF.htm](kingmaker-bestiary-items/v76tHhlGlywyinPF.htm)|Shortbow|vide|
|[V7siOMUXReEpRR0u.htm](kingmaker-bestiary-items/V7siOMUXReEpRR0u.htm)|Dagger|vide|
|[Vd2tGIDTvBddHJ36.htm](kingmaker-bestiary-items/Vd2tGIDTvBddHJ36.htm)|Arcane Focus Spells|vide|
|[VdKXa5UE8pjAkhRN.htm](kingmaker-bestiary-items/VdKXa5UE8pjAkhRN.htm)|Staff|vide|
|[VGrJpWKaRTR27Qyu.htm](kingmaker-bestiary-items/VGrJpWKaRTR27Qyu.htm)|Arcane Prepared Spells|vide|
|[viDna1Oudpf22Ieg.htm](kingmaker-bestiary-items/viDna1Oudpf22Ieg.htm)|Blowgun|vide|
|[vJiFJxTfxGmRSWe0.htm](kingmaker-bestiary-items/vJiFJxTfxGmRSWe0.htm)|Composite Shortbow|vide|
|[VPpeBvTnuLzMsNon.htm](kingmaker-bestiary-items/VPpeBvTnuLzMsNon.htm)|Primal Prepared Spells|vide|
|[VQzmSFhPlPCnWRYb.htm](kingmaker-bestiary-items/VQzmSFhPlPCnWRYb.htm)|Arcane Innate Spells|vide|
|[vRSrxRGekX69z1xM.htm](kingmaker-bestiary-items/vRSrxRGekX69z1xM.htm)|Greatsword|vide|
|[VTjvgSGDhiMzsKdY.htm](kingmaker-bestiary-items/VTjvgSGDhiMzsKdY.htm)|Frenzied Fangs|vide|
|[VUyMebZxthD1vUj3.htm](kingmaker-bestiary-items/VUyMebZxthD1vUj3.htm)|Composite Longbow|vide|
|[VVezMNwhjbCrp0yX.htm](kingmaker-bestiary-items/VVezMNwhjbCrp0yX.htm)|Warfare Lore|vide|
|[VzrK9L3fTzGmyHto.htm](kingmaker-bestiary-items/VzrK9L3fTzGmyHto.htm)|Druid Order Spells|vide|
|[W3UNvs3a5bUDjnkS.htm](kingmaker-bestiary-items/W3UNvs3a5bUDjnkS.htm)|Ritual|vide|
|[w5ySqTQIaFATIjDM.htm](kingmaker-bestiary-items/w5ySqTQIaFATIjDM.htm)|Arcane Innate Spells|vide|
|[WBB2qr7XvkQXu1l5.htm](kingmaker-bestiary-items/WBB2qr7XvkQXu1l5.htm)|Ritual|vide|
|[wbtt2m0nhK330JwC.htm](kingmaker-bestiary-items/wbtt2m0nhK330JwC.htm)|Hatchet|vide|
|[we4aKsadr3FYXQCP.htm](kingmaker-bestiary-items/we4aKsadr3FYXQCP.htm)|Primal Prepared Spells|vide|
|[wFE3Lth938xGYdIL.htm](kingmaker-bestiary-items/wFE3Lth938xGYdIL.htm)|Warfare Lore|vide|
|[WJaww98OCHbzRIIP.htm](kingmaker-bestiary-items/WJaww98OCHbzRIIP.htm)|Jaws|vide|
|[wkahGwEkL5eYhtZy.htm](kingmaker-bestiary-items/wkahGwEkL5eYhtZy.htm)|Tail|vide|
|[wll4JHYsNNNsx4lF.htm](kingmaker-bestiary-items/wll4JHYsNNNsx4lF.htm)|Academia Lore|vide|
|[WNANaLjOqPHdTPjN.htm](kingmaker-bestiary-items/WNANaLjOqPHdTPjN.htm)|Dagger|vide|
|[WRLKbO1wNweUuW4n.htm](kingmaker-bestiary-items/WRLKbO1wNweUuW4n.htm)|Longspear|vide|
|[WTjCVJwNHH8vUKzE.htm](kingmaker-bestiary-items/WTjCVJwNHH8vUKzE.htm)|Sorcerer Bloodline Spells|vide|
|[wuin1zchdZrKVQEY.htm](kingmaker-bestiary-items/wuin1zchdZrKVQEY.htm)|Dagger|vide|
|[x1BxQoUjfPHH06yh.htm](kingmaker-bestiary-items/x1BxQoUjfPHH06yh.htm)|Dagger|vide|
|[X3C10c6qy59DlNdh.htm](kingmaker-bestiary-items/X3C10c6qy59DlNdh.htm)|Occult Innate Spells|vide|
|[X41cWt7idOHqasPx.htm](kingmaker-bestiary-items/X41cWt7idOHqasPx.htm)|Greatsword|vide|
|[x46bcv7xjs0irbqk.htm](kingmaker-bestiary-items/x46bcv7xjs0irbqk.htm)|Morningstar|vide|
|[X6qxLgkALLxdJTTq.htm](kingmaker-bestiary-items/X6qxLgkALLxdJTTq.htm)|Cleaver|vide|
|[xBvjgSrMNlbRs2vt.htm](kingmaker-bestiary-items/xBvjgSrMNlbRs2vt.htm)|Crossbow|vide|
|[xdpY6vYKdagXO6bk.htm](kingmaker-bestiary-items/xdpY6vYKdagXO6bk.htm)|Bard Composition Spells|vide|
|[XdUyVqH2LISYTKP4.htm](kingmaker-bestiary-items/XdUyVqH2LISYTKP4.htm)|Sickle|vide|
|[XEeQ6n37g3yEASJB.htm](kingmaker-bestiary-items/XEeQ6n37g3yEASJB.htm)|Dagger|vide|
|[XFWwEVmfkf4Naohf.htm](kingmaker-bestiary-items/XFWwEVmfkf4Naohf.htm)|Jaws|vide|
|[xIXTT704xBEHdGIn.htm](kingmaker-bestiary-items/xIXTT704xBEHdGIn.htm)|Dagger|vide|
|[xj83ymsPlY7u87j1.htm](kingmaker-bestiary-items/xj83ymsPlY7u87j1.htm)|Athletics|vide|
|[xYt0Yl9LAnVB1BZ6.htm](kingmaker-bestiary-items/xYt0Yl9LAnVB1BZ6.htm)|Warfare Lore|vide|
|[y57fpMJDVfTQ3moG.htm](kingmaker-bestiary-items/y57fpMJDVfTQ3moG.htm)|Brevoy Lore|vide|
|[YaqUxn6Ybbvxv3gB.htm](kingmaker-bestiary-items/YaqUxn6Ybbvxv3gB.htm)|Quill|vide|
|[yaWanh7jgvzb4bNA.htm](kingmaker-bestiary-items/yaWanh7jgvzb4bNA.htm)|Jaws|vide|
|[YCWMeM1zCWZqP48p.htm](kingmaker-bestiary-items/YCWMeM1zCWZqP48p.htm)|Rod of Razors|vide|
|[YdimsTpSLxkBXaBh.htm](kingmaker-bestiary-items/YdimsTpSLxkBXaBh.htm)|Arcane Spontaneous Spells|vide|
|[yJ37AcaU1dl5m7KT.htm](kingmaker-bestiary-items/yJ37AcaU1dl5m7KT.htm)|Dagger|vide|
|[yKwcZmq0W6TpvY7z.htm](kingmaker-bestiary-items/yKwcZmq0W6TpvY7z.htm)|Lance|vide|
|[yLgcfIF0Rbavu6Tr.htm](kingmaker-bestiary-items/yLgcfIF0Rbavu6Tr.htm)|Glaive|vide|
|[ypEVZiKiaV4PXVPe.htm](kingmaker-bestiary-items/ypEVZiKiaV4PXVPe.htm)|Yog-Sothoth Lore|vide|
|[yTVotZYCI6abxOvt.htm](kingmaker-bestiary-items/yTVotZYCI6abxOvt.htm)|Bastard Sword|vide|
|[yTWeFCTfaEECHhCs.htm](kingmaker-bestiary-items/yTWeFCTfaEECHhCs.htm)|Falchion|vide|
|[Yutc2Syx2q0EKb1P.htm](kingmaker-bestiary-items/Yutc2Syx2q0EKb1P.htm)|Heraldry Lore|vide|
|[yX5Z8bj03S67Thb5.htm](kingmaker-bestiary-items/yX5Z8bj03S67Thb5.htm)|Primal Spontaneous Spells|vide|
|[YXGsH5DgEDFiRIu7.htm](kingmaker-bestiary-items/YXGsH5DgEDFiRIu7.htm)|Lumber Lore|vide|
|[YxX6oMlTOXzTGheb.htm](kingmaker-bestiary-items/YxX6oMlTOXzTGheb.htm)|Occult Innate Spells|vide|
|[ZaZTsIwbHhL3ZcQy.htm](kingmaker-bestiary-items/ZaZTsIwbHhL3ZcQy.htm)|Battle Axe|vide|
|[ZBxbAEeCdREK43Yu.htm](kingmaker-bestiary-items/ZBxbAEeCdREK43Yu.htm)|Bard Composition Spells|vide|
|[zgdHnMuj8S2a5m8L.htm](kingmaker-bestiary-items/zgdHnMuj8S2a5m8L.htm)|Ghostly Hand|vide|
|[zNJKK7fb7gXv78OV.htm](kingmaker-bestiary-items/zNJKK7fb7gXv78OV.htm)|Warhammer|vide|
|[ZNWj00PO55PVPFmc.htm](kingmaker-bestiary-items/ZNWj00PO55PVPFmc.htm)|Shortsword|vide|
|[ZOJd2q3zsGLrA8Xw.htm](kingmaker-bestiary-items/ZOJd2q3zsGLrA8Xw.htm)|Divine Prepared Spells|vide|
|[ZQWoQnlsIHN6EyFc.htm](kingmaker-bestiary-items/ZQWoQnlsIHN6EyFc.htm)|Wolf Jaws|vide|
|[ztl64mYGjNx9Qab6.htm](kingmaker-bestiary-items/ztl64mYGjNx9Qab6.htm)|Sorcerer Bloodline Spells|vide|
|[zWFmuptIJBa1t8FT.htm](kingmaker-bestiary-items/zWFmuptIJBa1t8FT.htm)|Warfare Lore|vide|

## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0sXAwqL8Tb6XqJ0H.htm](kingmaker-bestiary-items/0sXAwqL8Tb6XqJ0H.htm)|Lantern King's Glow|Lueur du Roi Lanterne|libre|
|[2n6nRtgDNRWONgvp.htm](kingmaker-bestiary-items/2n6nRtgDNRWONgvp.htm)|Drain Luck|Drain de chance|libre|
|[79pJPNMXOyazfHy6.htm](kingmaker-bestiary-items/79pJPNMXOyazfHy6.htm)|Accelerated Existence|Existence accélérée|libre|
|[aHuQg1Wi1gb8KAbH.htm](kingmaker-bestiary-items/aHuQg1Wi1gb8KAbH.htm)|Time Siphon|Siphon du temps|libre|
|[AkE2QdLe1wysN32X.htm](kingmaker-bestiary-items/AkE2QdLe1wysN32X.htm)|Blindness|Aveuglement|libre|
|[CDUrtwX9v7aZEVjj.htm](kingmaker-bestiary-items/CDUrtwX9v7aZEVjj.htm)|Change Shape|Changement de forme|libre|
|[DgApO6iOQpwx3KfR.htm](kingmaker-bestiary-items/DgApO6iOQpwx3KfR.htm)|Resolve|Résolution|libre|
|[dGglOjtXAyHqoW5X.htm](kingmaker-bestiary-items/dGglOjtXAyHqoW5X.htm)|Steady Spellcasting|Incantation fiable|libre|
|[DGNIDiy16GZYbh8b.htm](kingmaker-bestiary-items/DGNIDiy16GZYbh8b.htm)|Trapdoor Lunge|Fonce trappe|libre|
|[DJVCW8WVRR0Prvrs.htm](kingmaker-bestiary-items/DJVCW8WVRR0Prvrs.htm)|Prismatic Burst|Explosion prismatique|libre|
|[dnLiUmNpLT4ZnUQ6.htm](kingmaker-bestiary-items/dnLiUmNpLT4ZnUQ6.htm)|Darkvision|Vision dans le noir|libre|
|[fryglhBup2tRHWFS.htm](kingmaker-bestiary-items/fryglhBup2tRHWFS.htm)|Telepathy 300 feet|Télépathie 90 m|libre|
|[GJxtjkowvzk3cvaE.htm](kingmaker-bestiary-items/GJxtjkowvzk3cvaE.htm)|Constant Spells|Sorts constants|libre|
|[i8ykQ7pFedbfWxeV.htm](kingmaker-bestiary-items/i8ykQ7pFedbfWxeV.htm)|Staff|Bâton|libre|
|[JWMAFJOdy4Odx7RY.htm](kingmaker-bestiary-items/JWMAFJOdy4Odx7RY.htm)|Playful Switch|Échange enjoué|libre|
|[kooXn2rvr04D9fM6.htm](kingmaker-bestiary-items/kooXn2rvr04D9fM6.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[KXuuj04cishnGNpk.htm](kingmaker-bestiary-items/KXuuj04cishnGNpk.htm)|Trapdoor Lunge|Fonce trappe|libre|
|[mGzWQjNPCtlz97Cn.htm](kingmaker-bestiary-items/mGzWQjNPCtlz97Cn.htm)|Sorcerer Bloodline Spells|Sorts de lignage d'ensorceleur|libre|
|[n4uBILCtoQcDwVBY.htm](kingmaker-bestiary-items/n4uBILCtoQcDwVBY.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[NuzA3kAJ0iGNoN0N.htm](kingmaker-bestiary-items/NuzA3kAJ0iGNoN0N.htm)|Ritual|Rituel|libre|
|[P5jo7Pf9aXvKHvel.htm](kingmaker-bestiary-items/P5jo7Pf9aXvKHvel.htm)|Unseen Sight|Vision invisible|libre|
|[q5MeF25mWxrHIqg3.htm](kingmaker-bestiary-items/q5MeF25mWxrHIqg3.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[T3OviChVRRywo6sB.htm](kingmaker-bestiary-items/T3OviChVRRywo6sB.htm)|Mocking Laughter|Rire moqueur|libre|
|[tsZqAzdWTSJww5o9.htm](kingmaker-bestiary-items/tsZqAzdWTSJww5o9.htm)|First World Lore|Connaissance du Premier monde|libre|
|[TxKSVVgJ5OZw3cy0.htm](kingmaker-bestiary-items/TxKSVVgJ5OZw3cy0.htm)|Outside of Time|En-dehors du temps|libre|
|[U69KdW5yZ21jJT4a.htm](kingmaker-bestiary-items/U69KdW5yZ21jJT4a.htm)|Yog-Sothoth Lore|Connaissance Yog-Sothoth|libre|
|[u8YE1BntdMHM1ks0.htm](kingmaker-bestiary-items/u8YE1BntdMHM1ks0.htm)|At-Will Spells|Sorts à volonté|libre|
|[wYKABRKZFADU4eFV.htm](kingmaker-bestiary-items/wYKABRKZFADU4eFV.htm)|Fortune's Friend|Ami de la Chance|libre|
|[XCuiusaM9Pgov7Hy.htm](kingmaker-bestiary-items/XCuiusaM9Pgov7Hy.htm)|Reactive|Réactif|libre|
|[XEczLnuwuxklAds8.htm](kingmaker-bestiary-items/XEczLnuwuxklAds8.htm)|Adjust Temperature|Régler la température|libre|
