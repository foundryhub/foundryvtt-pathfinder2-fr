# État de la traduction (agents-of-edgewatch-bestiary-items)

 * **officielle**: 1134
 * **libre**: 80


Dernière mise à jour: 2023-03-05 17:26 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[00AnAb9FZBwVZGjI.htm](agents-of-edgewatch-bestiary-items/00AnAb9FZBwVZGjI.htm)|Jaws|Mâchoires|officielle|
|[02vYXhCEPmoLEU7V.htm](agents-of-edgewatch-bestiary-items/02vYXhCEPmoLEU7V.htm)|Fast Healing 5|Guérison accélérée 5|officielle|
|[034N8wrms1VjpEX4.htm](agents-of-edgewatch-bestiary-items/034N8wrms1VjpEX4.htm)|Terrible Justice|Justice terrifiante|officielle|
|[09M4q5fVCuvOwcWk.htm](agents-of-edgewatch-bestiary-items/09M4q5fVCuvOwcWk.htm)|Poison Ink|Encre empoisonnée|officielle|
|[0AtXXmHLOdGkB70d.htm](agents-of-edgewatch-bestiary-items/0AtXXmHLOdGkB70d.htm)|Fast Healing 10|Guérison accélérée 10|officielle|
|[0DxZM2ZhwpKrUolW.htm](agents-of-edgewatch-bestiary-items/0DxZM2ZhwpKrUolW.htm)|Sneak Attack|Attaque sournoise|officielle|
|[0Fzs97xBu4GMOwjA.htm](agents-of-edgewatch-bestiary-items/0Fzs97xBu4GMOwjA.htm)|+1 Status to All Saves vs. Magic|+1 de statut aux JdS contre la magie|officielle|
|[0GDvJYPFAaQoah9E.htm](agents-of-edgewatch-bestiary-items/0GDvJYPFAaQoah9E.htm)|Syringe|Seringue|officielle|
|[0Jf7Xd3lnLkiXGLW.htm](agents-of-edgewatch-bestiary-items/0Jf7Xd3lnLkiXGLW.htm)|Library Lore|Connaissance des bibliothèques|officielle|
|[0Jid4uAhNoUlXWoo.htm](agents-of-edgewatch-bestiary-items/0Jid4uAhNoUlXWoo.htm)|Guildmaster's Lead|L'exemple de la maîtresse de guilde|officielle|
|[0jNbTDLglMGWVCtF.htm](agents-of-edgewatch-bestiary-items/0jNbTDLglMGWVCtF.htm)|Pincer|Pince|officielle|
|[0jRv23fTxJTfzMQx.htm](agents-of-edgewatch-bestiary-items/0jRv23fTxJTfzMQx.htm)|Innate Occult Spells|Sorts occultes innés|libre|
|[0KI7dYfaduuCOAlE.htm](agents-of-edgewatch-bestiary-items/0KI7dYfaduuCOAlE.htm)|Rejuvenation|Reconstruction|officielle|
|[0kL2ZW2fqmtImuCu.htm](agents-of-edgewatch-bestiary-items/0kL2ZW2fqmtImuCu.htm)|Grab|Empoignade|officielle|
|[0mlWlB8x9buvEw3g.htm](agents-of-edgewatch-bestiary-items/0mlWlB8x9buvEw3g.htm)|Swarm Mind|Esprit de la nuée|officielle|
|[0rqwxWSqNK0Ikmdw.htm](agents-of-edgewatch-bestiary-items/0rqwxWSqNK0Ikmdw.htm)|Graveknight's Curse|Malédiction du chevalier sépulcre|officielle|
|[0TIsrEfH3mfhYqMB.htm](agents-of-edgewatch-bestiary-items/0TIsrEfH3mfhYqMB.htm)|Eye-Opener|Ouvrir les yeux|officielle|
|[0tqsvUhGYvPuB2ga.htm](agents-of-edgewatch-bestiary-items/0tqsvUhGYvPuB2ga.htm)|Reshape Reality|Remodeler la réalité|officielle|
|[0UpmO63mWa667Q99.htm](agents-of-edgewatch-bestiary-items/0UpmO63mWa667Q99.htm)|Tactical Aura|Aura tactique|officielle|
|[0yo458L980s9JXH6.htm](agents-of-edgewatch-bestiary-items/0yo458L980s9JXH6.htm)|Pest Haven|Abri à nuisibles|officielle|
|[0zLmXZjs6O5tPrZB.htm](agents-of-edgewatch-bestiary-items/0zLmXZjs6O5tPrZB.htm)|Warfare Lore|Connaissance de la guerre|officielle|
|[10AhR44dLRwNyyyT.htm](agents-of-edgewatch-bestiary-items/10AhR44dLRwNyyyT.htm)|Alchemical Rupture|Rupture alchimique|officielle|
|[126ppmFKAd49r2hi.htm](agents-of-edgewatch-bestiary-items/126ppmFKAd49r2hi.htm)|Najra Swarm Attack|Attaque de najra en nuée|officielle|
|[157GGkuQcvXmLwPu.htm](agents-of-edgewatch-bestiary-items/157GGkuQcvXmLwPu.htm)|Hammering Flurry|Déluge martelant|officielle|
|[1a9J9jdVUsp4RIxQ.htm](agents-of-edgewatch-bestiary-items/1a9J9jdVUsp4RIxQ.htm)|Sky and Heaven Stance|Posture du ciel et du paradis|officielle|
|[1bSj6FFb2A1ZOyrZ.htm](agents-of-edgewatch-bestiary-items/1bSj6FFb2A1ZOyrZ.htm)|+2 Status to All Saves vs. Mental|+2 de statut aux JdS contre mental|officielle|
|[1cQlKfzWAd8gXGFa.htm](agents-of-edgewatch-bestiary-items/1cQlKfzWAd8gXGFa.htm)|Malefic Binding|Entrave maléfique|officielle|
|[1d7P7fxvDCam2ALx.htm](agents-of-edgewatch-bestiary-items/1d7P7fxvDCam2ALx.htm)|Vein Walker|Marcheur veineux|officielle|
|[1DABV9KyoaCfVd3F.htm](agents-of-edgewatch-bestiary-items/1DABV9KyoaCfVd3F.htm)|Darkvision|Vision dans le noir|officielle|
|[1eq3XxcWMvDVBheO.htm](agents-of-edgewatch-bestiary-items/1eq3XxcWMvDVBheO.htm)|Telepathy 100 feet|Télépathie à 30 mètres|officielle|
|[1GcnjXs1m3fitCxq.htm](agents-of-edgewatch-bestiary-items/1GcnjXs1m3fitCxq.htm)|Sacrilegious Aura|Aura sacrilège|officielle|
|[1h5CBWyXY0MAHGtm.htm](agents-of-edgewatch-bestiary-items/1h5CBWyXY0MAHGtm.htm)|Bloody Sneak Attack|Attaque sournoise sanglante|officielle|
|[1HjWqCIXUjmknlZE.htm](agents-of-edgewatch-bestiary-items/1HjWqCIXUjmknlZE.htm)|Adamantine Strikes|Frappes d'adamantium|officielle|
|[1jGFJIbrEUb7ObhO.htm](agents-of-edgewatch-bestiary-items/1jGFJIbrEUb7ObhO.htm)|No MAP|Pas de PAM|officielle|
|[1mBHiJQyWY7LwsCN.htm](agents-of-edgewatch-bestiary-items/1mBHiJQyWY7LwsCN.htm)|Rend|Éventration|officielle|
|[1OKW1epSXihm5XRb.htm](agents-of-edgewatch-bestiary-items/1OKW1epSXihm5XRb.htm)|Scent (Imprecise) 30 feet|Odorat (imprécis) 9 m|officielle|
|[1pf3cBO6a0unyMKL.htm](agents-of-edgewatch-bestiary-items/1pf3cBO6a0unyMKL.htm)|Jaws|Mâchoires|officielle|
|[1qQjPJIus3hZOUXY.htm](agents-of-edgewatch-bestiary-items/1qQjPJIus3hZOUXY.htm)|Powerful Swipe|Frappe transversale puissante|officielle|
|[1Rk88GORg0fjreo8.htm](agents-of-edgewatch-bestiary-items/1Rk88GORg0fjreo8.htm)|Chain Up|Enchaîner|officielle|
|[1sozJvnrR9nNQ2qu.htm](agents-of-edgewatch-bestiary-items/1sozJvnrR9nNQ2qu.htm)|Sneak Attack|Attaque sournoise|officielle|
|[1UFXWKQIPIkKudjR.htm](agents-of-edgewatch-bestiary-items/1UFXWKQIPIkKudjR.htm)|Fangs|Crocs|officielle|
|[1vHKAVMhE8gmaKS2.htm](agents-of-edgewatch-bestiary-items/1vHKAVMhE8gmaKS2.htm)|Low-Light Vision|Vision nocturne|officielle|
|[1wPZqdlI8xQ4yfN9.htm](agents-of-edgewatch-bestiary-items/1wPZqdlI8xQ4yfN9.htm)|Fangs|Crocs|officielle|
|[22ts3ClOQFeLq0Jt.htm](agents-of-edgewatch-bestiary-items/22ts3ClOQFeLq0Jt.htm)|Water Shield|Bouclier d'eau|officielle|
|[268BBCle1rCc1g1d.htm](agents-of-edgewatch-bestiary-items/268BBCle1rCc1g1d.htm)|Cannon Fusillade|Fusillade de canon|officielle|
|[26ZZztmP0cFmWp6x.htm](agents-of-edgewatch-bestiary-items/26ZZztmP0cFmWp6x.htm)|Precise Tremorsense 40 feet|Perception des vibrations précise|officielle|
|[281TWDcEhOImRl44.htm](agents-of-edgewatch-bestiary-items/281TWDcEhOImRl44.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[29elTy7XDcAju8yK.htm](agents-of-edgewatch-bestiary-items/29elTy7XDcAju8yK.htm)|Ghaele's Gaze|Regard du ghaéle|officielle|
|[29naBTIemmuYyMKB.htm](agents-of-edgewatch-bestiary-items/29naBTIemmuYyMKB.htm)|Skip Between|Insinuation|officielle|
|[2EL49f3yp855RUpq.htm](agents-of-edgewatch-bestiary-items/2EL49f3yp855RUpq.htm)|Quick Movements|Déplacements rapides|officielle|
|[2FxUvyDjnESqE9bl.htm](agents-of-edgewatch-bestiary-items/2FxUvyDjnESqE9bl.htm)|Quick Brew|Préparation rapide|officielle|
|[2gGx394noy8wbdH3.htm](agents-of-edgewatch-bestiary-items/2gGx394noy8wbdH3.htm)|Constrict|Constriction|officielle|
|[2JJtxgkrLOcIfYQz.htm](agents-of-edgewatch-bestiary-items/2JJtxgkrLOcIfYQz.htm)|Mirror Dart|Fléchette miroir|officielle|
|[2JN5a5P81ZqQkOsD.htm](agents-of-edgewatch-bestiary-items/2JN5a5P81ZqQkOsD.htm)|Painful Light|Lumière douloureuse|officielle|
|[2pMaO2Z0N8Vx4Yy8.htm](agents-of-edgewatch-bestiary-items/2pMaO2Z0N8Vx4Yy8.htm)|Scent (Imprecise) 30 feet|Odorat (imprécis) 9 m|officielle|
|[2S79HfbIxhTAsZFh.htm](agents-of-edgewatch-bestiary-items/2S79HfbIxhTAsZFh.htm)|At-Will Spells|Sorts à volonté|officielle|
|[2TFdxsGPr1xaX73k.htm](agents-of-edgewatch-bestiary-items/2TFdxsGPr1xaX73k.htm)|Syringe|Seringue|officielle|
|[2tZ1okZGWrxi91Ba.htm](agents-of-edgewatch-bestiary-items/2tZ1okZGWrxi91Ba.htm)|Telepathy 100 feet|Télépathie à 30 mètres|officielle|
|[2Vevg87ax4Fd8BnK.htm](agents-of-edgewatch-bestiary-items/2Vevg87ax4Fd8BnK.htm)|Shield Bash|Coup de bouclier|officielle|
|[2w1WQr635JVnsORb.htm](agents-of-edgewatch-bestiary-items/2w1WQr635JVnsORb.htm)|Swift Sneak|Furtivité rapide|officielle|
|[2wSPUQgCOkfTQCUJ.htm](agents-of-edgewatch-bestiary-items/2wSPUQgCOkfTQCUJ.htm)|+2 Status to All Saves vs. Magic|+2 de statut aux JdS contre la magie|officielle|
|[2Wzd91SDBc89gsI9.htm](agents-of-edgewatch-bestiary-items/2Wzd91SDBc89gsI9.htm)|Tail|Queue|officielle|
|[2yp2PpkHqWXuA5k0.htm](agents-of-edgewatch-bestiary-items/2yp2PpkHqWXuA5k0.htm)|Change Shape|Changement de forme|officielle|
|[2zmTtiyeZNvKPOcI.htm](agents-of-edgewatch-bestiary-items/2zmTtiyeZNvKPOcI.htm)|Ink Blood|Sang d'encre|officielle|
|[2zo8BFj6mkav8MPT.htm](agents-of-edgewatch-bestiary-items/2zo8BFj6mkav8MPT.htm)|Ferocious Devotion|Dévotion féroce|officielle|
|[3039pLEWxpDZZrH0.htm](agents-of-edgewatch-bestiary-items/3039pLEWxpDZZrH0.htm)|Attack of Opportunity (Jaws Only)|Attaque d'opportunité (Mâchoires uniquement)|officielle|
|[30qgIKOMzbO7zHQj.htm](agents-of-edgewatch-bestiary-items/30qgIKOMzbO7zHQj.htm)|Norgorber Lore|Connaissance de Norgorber|officielle|
|[31SjrOIjFWX8if4N.htm](agents-of-edgewatch-bestiary-items/31SjrOIjFWX8if4N.htm)|Poison Weapon|Arme empoisonnée|officielle|
|[32FbUxGNpWinXjEq.htm](agents-of-edgewatch-bestiary-items/32FbUxGNpWinXjEq.htm)|Goblin Scuttle|Précipitation gobeline|officielle|
|[345q6v1fOXwCpJpM.htm](agents-of-edgewatch-bestiary-items/345q6v1fOXwCpJpM.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[35yXdy43dCClA3wy.htm](agents-of-edgewatch-bestiary-items/35yXdy43dCClA3wy.htm)|Smooth Operator|Manipulateur invétéré|officielle|
|[3aasDsiXoE4xtSVZ.htm](agents-of-edgewatch-bestiary-items/3aasDsiXoE4xtSVZ.htm)|Distracting Shadows|Ombres distrayantes|officielle|
|[3BvCMBxHiHXEMq2P.htm](agents-of-edgewatch-bestiary-items/3BvCMBxHiHXEMq2P.htm)|Bonesense (Imprecise) 30 feet|Perception des os (imprécis) 9 m|officielle|
|[3bYjif3A9K5vszf9.htm](agents-of-edgewatch-bestiary-items/3bYjif3A9K5vszf9.htm)|Claw|Griffe|officielle|
|[3dd754R1VUxFyZiT.htm](agents-of-edgewatch-bestiary-items/3dd754R1VUxFyZiT.htm)|Secret of Rebirth|Secret de renaissance|officielle|
|[3fSPm7dDpsyRRKqg.htm](agents-of-edgewatch-bestiary-items/3fSPm7dDpsyRRKqg.htm)|Fast Healing 8|Guérison accélérée 8|officielle|
|[3g9fOfW6T5VklwH5.htm](agents-of-edgewatch-bestiary-items/3g9fOfW6T5VklwH5.htm)|Compression|Compression|officielle|
|[3jd4JNaR2XiNoddM.htm](agents-of-edgewatch-bestiary-items/3jd4JNaR2XiNoddM.htm)|Jaws|Mâchoires|officielle|
|[3jh7PEda42lpDxXR.htm](agents-of-edgewatch-bestiary-items/3jh7PEda42lpDxXR.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[3N2FHMvzMRnvpsId.htm](agents-of-edgewatch-bestiary-items/3N2FHMvzMRnvpsId.htm)|Human Hunter|Chasseur d'humains|officielle|
|[3SoJ3oq6Q1vAEy8o.htm](agents-of-edgewatch-bestiary-items/3SoJ3oq6Q1vAEy8o.htm)|Grab|Empoignade|officielle|
|[3sUb7J0bD92GSjki.htm](agents-of-edgewatch-bestiary-items/3sUb7J0bD92GSjki.htm)|Innate Occult Spells|Sorts occultes innés|libre|
|[3TDeHZ2NkeZhRp86.htm](agents-of-edgewatch-bestiary-items/3TDeHZ2NkeZhRp86.htm)|Despair Ray|Rayon de désespoir|officielle|
|[3VSwbSPRGAYY8tg5.htm](agents-of-edgewatch-bestiary-items/3VSwbSPRGAYY8tg5.htm)|Songblade|Chantelame|officielle|
|[3vUeNTCG7OjIBa45.htm](agents-of-edgewatch-bestiary-items/3vUeNTCG7OjIBa45.htm)|Forced Regeneration|Régénération forcée|officielle|
|[3xd9C3zcdYxijFhU.htm](agents-of-edgewatch-bestiary-items/3xd9C3zcdYxijFhU.htm)|Trample|Piétinement|officielle|
|[46OKgjftfLLd55Dz.htm](agents-of-edgewatch-bestiary-items/46OKgjftfLLd55Dz.htm)|Katana|Katana|officielle|
|[4CShIdKgZz5cAVsW.htm](agents-of-edgewatch-bestiary-items/4CShIdKgZz5cAVsW.htm)|Occult Rituals|Rituels occultes|officielle|
|[4dEZdXNmp0qZ2uWV.htm](agents-of-edgewatch-bestiary-items/4dEZdXNmp0qZ2uWV.htm)|Darkvision|Vision dans le noir|officielle|
|[4EqqjuXvtgi5CQMY.htm](agents-of-edgewatch-bestiary-items/4EqqjuXvtgi5CQMY.htm)|Skyward Slash|Taillade vers le ciel|officielle|
|[4fuCvW4VqkpcxMz7.htm](agents-of-edgewatch-bestiary-items/4fuCvW4VqkpcxMz7.htm)|At-Will Spells|Sorts à volonté|officielle|
|[4G2h2y2d0Zz3woFP.htm](agents-of-edgewatch-bestiary-items/4G2h2y2d0Zz3woFP.htm)|Wave|Vague|officielle|
|[4jItoqJFtYwZm9bE.htm](agents-of-edgewatch-bestiary-items/4jItoqJFtYwZm9bE.htm)|Scent (Imprecise) 30 feet|Odorat (imprécis) 9 m|officielle|
|[4LPTg9hErKNAmmv0.htm](agents-of-edgewatch-bestiary-items/4LPTg9hErKNAmmv0.htm)|Negative Healing|Soins négatifs|officielle|
|[4LuKFdOn742r53ct.htm](agents-of-edgewatch-bestiary-items/4LuKFdOn742r53ct.htm)|Dagger|Dague|officielle|
|[4mfr0BXdQQ9ZuRDr.htm](agents-of-edgewatch-bestiary-items/4mfr0BXdQQ9ZuRDr.htm)|Deepen the Wound|Aggraver la blessure|officielle|
|[4nJSWcGlNE3k6uH0.htm](agents-of-edgewatch-bestiary-items/4nJSWcGlNE3k6uH0.htm)|Black Ink Delirium|Délire d'encre noire|officielle|
|[4p4BNBKvDMPM8yRG.htm](agents-of-edgewatch-bestiary-items/4p4BNBKvDMPM8yRG.htm)|Cleric Domain Spells|Sorts de domaine de prêtre|libre|
|[4QRkjb5aTrVTZYRO.htm](agents-of-edgewatch-bestiary-items/4QRkjb5aTrVTZYRO.htm)|Jealous Musician|Musicien jaloux|officielle|
|[4RXnnP8CpvFi5RjK.htm](agents-of-edgewatch-bestiary-items/4RXnnP8CpvFi5RjK.htm)|All-Around Vision|Vision à 360°|officielle|
|[4T0hd15HFgEoLTEv.htm](agents-of-edgewatch-bestiary-items/4T0hd15HFgEoLTEv.htm)|Slow|Lent|officielle|
|[4UhxcsjIkaaA0FnK.htm](agents-of-edgewatch-bestiary-items/4UhxcsjIkaaA0FnK.htm)|Telepathy 100 feet|Télépathie à 30 mètres|officielle|
|[4UovGCnsjker1UNi.htm](agents-of-edgewatch-bestiary-items/4UovGCnsjker1UNi.htm)|Sneak Attack|Attaque sournoise|officielle|
|[4uyjbjTFC0HefTzh.htm](agents-of-edgewatch-bestiary-items/4uyjbjTFC0HefTzh.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[4VKUGZEqMWaJycr4.htm](agents-of-edgewatch-bestiary-items/4VKUGZEqMWaJycr4.htm)|Shield Block|Blocage au bouclier|officielle|
|[4Wo8cEyKXFYXahnb.htm](agents-of-edgewatch-bestiary-items/4Wo8cEyKXFYXahnb.htm)|Entropy Sense (Imprecise) 60 feet|Perception de l'entropie (imprécis) 18 m|officielle|
|[4ZZA8573arWP2MKX.htm](agents-of-edgewatch-bestiary-items/4ZZA8573arWP2MKX.htm)|Aquatic Lore|Connaissance aquatique|officielle|
|[53IhzCwi4y8yO9XX.htm](agents-of-edgewatch-bestiary-items/53IhzCwi4y8yO9XX.htm)|Drain Bonded Item|Drain d'objet lié|officielle|
|[571EUWSacME4MxgN.htm](agents-of-edgewatch-bestiary-items/571EUWSacME4MxgN.htm)|Morningstar|Morgenstern|officielle|
|[5aBlY3dBYmIMAZwP.htm](agents-of-edgewatch-bestiary-items/5aBlY3dBYmIMAZwP.htm)|Swinging Chandelier|Lustre ocillant|officielle|
|[5B9QSKBjBa9tC0SG.htm](agents-of-edgewatch-bestiary-items/5B9QSKBjBa9tC0SG.htm)|Stunning Strike|Frappe étourdissante|officielle|
|[5bx4G4sDLKNJ0Fim.htm](agents-of-edgewatch-bestiary-items/5bx4G4sDLKNJ0Fim.htm)|Frightful Presence|Présence terrifiante|officielle|
|[5DSmWdkGY2DeiYD8.htm](agents-of-edgewatch-bestiary-items/5DSmWdkGY2DeiYD8.htm)|Attach|Fixation|officielle|
|[5GTOdw4CBKeecBvX.htm](agents-of-edgewatch-bestiary-items/5GTOdw4CBKeecBvX.htm)|Spiked Chain|Chaîne cloutée|officielle|
|[5hCSuILSsdP2wKtN.htm](agents-of-edgewatch-bestiary-items/5hCSuILSsdP2wKtN.htm)|Cheek Pouches|Abajoues|officielle|
|[5HdfiD2BmZU6LzvL.htm](agents-of-edgewatch-bestiary-items/5HdfiD2BmZU6LzvL.htm)|Whirlwind Strike|Frappe tourbillonnante|officielle|
|[5Hk5r8bi7Nulj5pY.htm](agents-of-edgewatch-bestiary-items/5Hk5r8bi7Nulj5pY.htm)|Negative Healing|Soins négatifs|officielle|
|[5inXNyZGoLUMii53.htm](agents-of-edgewatch-bestiary-items/5inXNyZGoLUMii53.htm)|Slither|Reptation|officielle|
|[5izc2RKfDYtjIicX.htm](agents-of-edgewatch-bestiary-items/5izc2RKfDYtjIicX.htm)|Rejuvenation|Reconstruction|officielle|
|[5qAf1qV6aq6q3pQl.htm](agents-of-edgewatch-bestiary-items/5qAf1qV6aq6q3pQl.htm)|Little Favors|Services mineurs|officielle|
|[5rYhqUb2YVDE98ag.htm](agents-of-edgewatch-bestiary-items/5rYhqUb2YVDE98ag.htm)|Warpwave Spell|Sort de vagues de distorsion|officielle|
|[5vpT5j3f28sZPdRn.htm](agents-of-edgewatch-bestiary-items/5vpT5j3f28sZPdRn.htm)|Collapse|Effondrement|officielle|
|[5wnaaHpUx6f7lycE.htm](agents-of-edgewatch-bestiary-items/5wnaaHpUx6f7lycE.htm)|Low-Light Vision|Vision nocturne|officielle|
|[5wVIixUHmaim2A4K.htm](agents-of-edgewatch-bestiary-items/5wVIixUHmaim2A4K.htm)|Consume Flesh|Dévorer la chair|officielle|
|[5Yk1UPJggdSuj7Kw.htm](agents-of-edgewatch-bestiary-items/5Yk1UPJggdSuj7Kw.htm)|Claw|Griffe|officielle|
|[5Z1XSibUkx9LaIrv.htm](agents-of-edgewatch-bestiary-items/5Z1XSibUkx9LaIrv.htm)|Claws|Griffes|officielle|
|[64us7Ph5ZQlOh8bU.htm](agents-of-edgewatch-bestiary-items/64us7Ph5ZQlOh8bU.htm)|Sailing Lore|Connaissance de la navigation maritime|officielle|
|[6BA0nR5VEDa0Aw15.htm](agents-of-edgewatch-bestiary-items/6BA0nR5VEDa0Aw15.htm)|Surface-Bound|Lié à la surface|officielle|
|[6cFodLsS9nMte5ts.htm](agents-of-edgewatch-bestiary-items/6cFodLsS9nMte5ts.htm)|Fill Tank|Remplir la cuve|officielle|
|[6DnRipdoNRQWMUyZ.htm](agents-of-edgewatch-bestiary-items/6DnRipdoNRQWMUyZ.htm)|Spit|Crachat|officielle|
|[6gBFZhqoL9eg2XDz.htm](agents-of-edgewatch-bestiary-items/6gBFZhqoL9eg2XDz.htm)|Collective Attack|Attaque collective|officielle|
|[6hx8YYYNNoWo6xwu.htm](agents-of-edgewatch-bestiary-items/6hx8YYYNNoWo6xwu.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[6M89EOsieWisWkK3.htm](agents-of-edgewatch-bestiary-items/6M89EOsieWisWkK3.htm)|Heavy Crossbow|Arbalète lourde|officielle|
|[6nFnVyRyLOu3DldL.htm](agents-of-edgewatch-bestiary-items/6nFnVyRyLOu3DldL.htm)|Crossbow|Arbalète|officielle|
|[6Nu42xqROJzl0XrC.htm](agents-of-edgewatch-bestiary-items/6Nu42xqROJzl0XrC.htm)|Hampering Strike|Frappe ralentisseuse|officielle|
|[6OAI0pZw5WRcrpWw.htm](agents-of-edgewatch-bestiary-items/6OAI0pZw5WRcrpWw.htm)|Dream Lore|Connaissance des rêves|officielle|
|[6pCE969IqFRVqLzJ.htm](agents-of-edgewatch-bestiary-items/6pCE969IqFRVqLzJ.htm)|Engulf|Engloutir|officielle|
|[6pcwqNViNq5cqn8U.htm](agents-of-edgewatch-bestiary-items/6pcwqNViNq5cqn8U.htm)|Claw|Griffe|officielle|
|[6PRImWQWZEOuSAeI.htm](agents-of-edgewatch-bestiary-items/6PRImWQWZEOuSAeI.htm)|Dagger|Dague|officielle|
|[6Tw3xiLUkr4AYhGN.htm](agents-of-edgewatch-bestiary-items/6Tw3xiLUkr4AYhGN.htm)|Quick Bomber|Artificier rapide|officielle|
|[6u1eMdIQZV0dWPQU.htm](agents-of-edgewatch-bestiary-items/6u1eMdIQZV0dWPQU.htm)|Opportune Dodge|Esquive opportune|officielle|
|[6YAq6Oj3jOtxCRWq.htm](agents-of-edgewatch-bestiary-items/6YAq6Oj3jOtxCRWq.htm)|Claw|Griffe|officielle|
|[70miglztK7B97hcW.htm](agents-of-edgewatch-bestiary-items/70miglztK7B97hcW.htm)|Darkvision|Vision dans le noir|officielle|
|[71ypVkmQCnSo1C8X.htm](agents-of-edgewatch-bestiary-items/71ypVkmQCnSo1C8X.htm)|Claw|Griffe|officielle|
|[735O6tGVpk8PcXiV.htm](agents-of-edgewatch-bestiary-items/735O6tGVpk8PcXiV.htm)|Innate Arcane Spells|Sorts arcaniques innés|libre|
|[745PY8vNnU4dRB3J.htm](agents-of-edgewatch-bestiary-items/745PY8vNnU4dRB3J.htm)|Diabolic Certitude|Certitude diabolique|officielle|
|[76fFiqfOfbkyXm8t.htm](agents-of-edgewatch-bestiary-items/76fFiqfOfbkyXm8t.htm)|Spiked Chain|Chaîne cloutée|officielle|
|[79KeB3EaNuz3Wjd2.htm](agents-of-edgewatch-bestiary-items/79KeB3EaNuz3Wjd2.htm)|Spatial Riptide|Contre-courant spatial|officielle|
|[7ArMrPYZDTroaUsN.htm](agents-of-edgewatch-bestiary-items/7ArMrPYZDTroaUsN.htm)|At-Will Spells|Sorts à volonté|officielle|
|[7biZlvn3vY2e7oEj.htm](agents-of-edgewatch-bestiary-items/7biZlvn3vY2e7oEj.htm)|Divert Strike|Dévier une Frappe|officielle|
|[7GSRG0kO6Z9IWhXQ.htm](agents-of-edgewatch-bestiary-items/7GSRG0kO6Z9IWhXQ.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[7iz6trwOzT3IfpHj.htm](agents-of-edgewatch-bestiary-items/7iz6trwOzT3IfpHj.htm)|Constant Spells|Sorts constants|officielle|
|[7KBnvkWiiKe2PzQ5.htm](agents-of-edgewatch-bestiary-items/7KBnvkWiiKe2PzQ5.htm)|Horn|Corne|officielle|
|[7kENjgZrG04Exwhk.htm](agents-of-edgewatch-bestiary-items/7kENjgZrG04Exwhk.htm)|Telepathy 100 Feet|Télépathie à 30 mètres|officielle|
|[7kep2BZKcI8m29QV.htm](agents-of-edgewatch-bestiary-items/7kep2BZKcI8m29QV.htm)|Rejuvenation|Reconstruction|officielle|
|[7l3dJqmfIwNmUyfj.htm](agents-of-edgewatch-bestiary-items/7l3dJqmfIwNmUyfj.htm)|Negative Healing|Soins négatifs|officielle|
|[7m8XF9B86oB7by1T.htm](agents-of-edgewatch-bestiary-items/7m8XF9B86oB7by1T.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[7nwDQbdYpJ5U6EZs.htm](agents-of-edgewatch-bestiary-items/7nwDQbdYpJ5U6EZs.htm)|Call Toxins|Appel de toxines|officielle|
|[7OjXQSbCokE3yjiM.htm](agents-of-edgewatch-bestiary-items/7OjXQSbCokE3yjiM.htm)|Shovel|Pelle|officielle|
|[7PJ0xQKU3T5EtaPW.htm](agents-of-edgewatch-bestiary-items/7PJ0xQKU3T5EtaPW.htm)|Heaven's Thunder|Tonnerre des cieux|officielle|
|[7qqes4mzlHueunJX.htm](agents-of-edgewatch-bestiary-items/7qqes4mzlHueunJX.htm)|Deep Breath|Inspiration profonde|officielle|
|[7RZClAttcFohXVTd.htm](agents-of-edgewatch-bestiary-items/7RZClAttcFohXVTd.htm)|Little Favors|Services mineurs|officielle|
|[7sl37aa7OG77O2mr.htm](agents-of-edgewatch-bestiary-items/7sl37aa7OG77O2mr.htm)|Norgorber Lore|Connaissance de Norgorber|officielle|
|[7UeREmeUuipS0pKu.htm](agents-of-edgewatch-bestiary-items/7UeREmeUuipS0pKu.htm)|Dart|Fléchette|officielle|
|[7UsBYA3xYXL2TFT2.htm](agents-of-edgewatch-bestiary-items/7UsBYA3xYXL2TFT2.htm)|Darkvision|Vision dans le noir|officielle|
|[7vOzE1FHZEotvswQ.htm](agents-of-edgewatch-bestiary-items/7vOzE1FHZEotvswQ.htm)|+2 Status Bonus on Saves vs. Shove|+2 de statut aux JdS contre Pousser|officielle|
|[84E8AqCBiRIBEHuG.htm](agents-of-edgewatch-bestiary-items/84E8AqCBiRIBEHuG.htm)|Dagger|Dague|officielle|
|[87HtA52J9Oqfjsft.htm](agents-of-edgewatch-bestiary-items/87HtA52J9Oqfjsft.htm)|Illusory Persona|Personnage illusoire|officielle|
|[8ADZbtdcgOMgnF3e.htm](agents-of-edgewatch-bestiary-items/8ADZbtdcgOMgnF3e.htm)|Occult Ritual|Rituel occulte|libre|
|[8aw6p0tL8IYj3lNK.htm](agents-of-edgewatch-bestiary-items/8aw6p0tL8IYj3lNK.htm)|Tail|Queue|officielle|
|[8bbFiMBoHc5BG8QA.htm](agents-of-edgewatch-bestiary-items/8bbFiMBoHc5BG8QA.htm)|Darkvision|Vision dans le noir|officielle|
|[8dM3dH69X49kSo82.htm](agents-of-edgewatch-bestiary-items/8dM3dH69X49kSo82.htm)|Dagger|Dague|officielle|
|[8IxYcWmidpRnNMrc.htm](agents-of-edgewatch-bestiary-items/8IxYcWmidpRnNMrc.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[8j0TGPf1kZlcIwpn.htm](agents-of-edgewatch-bestiary-items/8j0TGPf1kZlcIwpn.htm)|Steal Memories|Voler des souvenirs|officielle|
|[8JzS4wK4EWHWFJlY.htm](agents-of-edgewatch-bestiary-items/8JzS4wK4EWHWFJlY.htm)|Chopper|Couperet|officielle|
|[8K2BcPcACmOHHydT.htm](agents-of-edgewatch-bestiary-items/8K2BcPcACmOHHydT.htm)|Dreadsong|Chant d'effroi|officielle|
|[8k6vKZQ086Gc6Yq2.htm](agents-of-edgewatch-bestiary-items/8k6vKZQ086Gc6Yq2.htm)|Hell Lore|Connaissance de l'Enfer|officielle|
|[8k84XkMz9ZT6F3TB.htm](agents-of-edgewatch-bestiary-items/8k84XkMz9ZT6F3TB.htm)|Rapier|Rapière|officielle|
|[8kuBPd4vWsUhaCao.htm](agents-of-edgewatch-bestiary-items/8kuBPd4vWsUhaCao.htm)|Blinding Stream|Flot aveuglant|officielle|
|[8N0oYLqMb0kjQ009.htm](agents-of-edgewatch-bestiary-items/8N0oYLqMb0kjQ009.htm)|Wizard School Spells|Sorts de l'école de magicien|libre|
|[8NRxk6m07y9TpopE.htm](agents-of-edgewatch-bestiary-items/8NRxk6m07y9TpopE.htm)|Sneak Attack|Attaque sournoise|officielle|
|[8QIEpGKWvwKK8mSZ.htm](agents-of-edgewatch-bestiary-items/8QIEpGKWvwKK8mSZ.htm)|Scraping Clamor|Grattage assourdissant|officielle|
|[8QnTTWcdqDNyOtIl.htm](agents-of-edgewatch-bestiary-items/8QnTTWcdqDNyOtIl.htm)|Miasma of Pollution|Miasme de pollution|officielle|
|[8qsC3gMbkSTTQOdY.htm](agents-of-edgewatch-bestiary-items/8qsC3gMbkSTTQOdY.htm)|Impale|Empaler|officielle|
|[8Qz8Y3OYthQCPvSH.htm](agents-of-edgewatch-bestiary-items/8Qz8Y3OYthQCPvSH.htm)|Constant Spells|Sorts constants|officielle|
|[8R9Z6J7bjth011q9.htm](agents-of-edgewatch-bestiary-items/8R9Z6J7bjth011q9.htm)|Norgorber Lore|Connaissance de Norgorber|officielle|
|[8RfMfocN311lqv8N.htm](agents-of-edgewatch-bestiary-items/8RfMfocN311lqv8N.htm)|Spell-Imbued Blade|Lame ensorcelée|officielle|
|[8RQS62riVfrsiZjr.htm](agents-of-edgewatch-bestiary-items/8RQS62riVfrsiZjr.htm)|Radiant Ray|Rayon radiant|officielle|
|[8rukLsOFemgZkuGO.htm](agents-of-edgewatch-bestiary-items/8rukLsOFemgZkuGO.htm)|Scent (Imprecise) 60 feet|Odorat (imprécis) 18 m|officielle|
|[8sPdH0UwMRq9RTHB.htm](agents-of-edgewatch-bestiary-items/8sPdH0UwMRq9RTHB.htm)|Darkvision|Vision dans le noir|officielle|
|[8tU7XeCCuWdlIydb.htm](agents-of-edgewatch-bestiary-items/8tU7XeCCuWdlIydb.htm)|Fist|Poing|officielle|
|[8ubCXo0Jx35U5xxc.htm](agents-of-edgewatch-bestiary-items/8ubCXo0Jx35U5xxc.htm)|Golem Antimagic|Antimagie du golem|officielle|
|[8xAxPvW0rww5NovC.htm](agents-of-edgewatch-bestiary-items/8xAxPvW0rww5NovC.htm)|Greater Warpwave Strike|Frappe de vagues de distorsion supérieure|officielle|
|[90dz4KzJNBG1WfGw.htm](agents-of-edgewatch-bestiary-items/90dz4KzJNBG1WfGw.htm)|+1 Status to All Saves vs. Magic|+1 de statut aux JdS contre la magie|officielle|
|[92BXBHbmRG68ifPd.htm](agents-of-edgewatch-bestiary-items/92BXBHbmRG68ifPd.htm)|Amplify Spell|Amplifier un sort|officielle|
|[933yJ4bfZhr9zTIX.htm](agents-of-edgewatch-bestiary-items/933yJ4bfZhr9zTIX.htm)|Collapse|Effondrement|officielle|
|[97KS2uPGBbRl3fOQ.htm](agents-of-edgewatch-bestiary-items/97KS2uPGBbRl3fOQ.htm)|Engulf|Engloutir|officielle|
|[9Af2sRyOtaTDufyy.htm](agents-of-edgewatch-bestiary-items/9Af2sRyOtaTDufyy.htm)|Maul|Maillet|officielle|
|[9bgmrU7Aa4AWtibm.htm](agents-of-edgewatch-bestiary-items/9bgmrU7Aa4AWtibm.htm)|Legal Lore|Connaissance juridique|officielle|
|[9cRBZZnKohlEmRzR.htm](agents-of-edgewatch-bestiary-items/9cRBZZnKohlEmRzR.htm)|Dagger|Dague|officielle|
|[9duTJhK5paVZrMkr.htm](agents-of-edgewatch-bestiary-items/9duTJhK5paVZrMkr.htm)|At-Will Spells|Sorts à volonté|officielle|
|[9dxZ2UN0t2f32r0f.htm](agents-of-edgewatch-bestiary-items/9dxZ2UN0t2f32r0f.htm)|Bury in Offal|Ensevelir sous les déchets|officielle|
|[9fvVPY7pFKhpLQwU.htm](agents-of-edgewatch-bestiary-items/9fvVPY7pFKhpLQwU.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[9gEiH150yl4bKC6n.htm](agents-of-edgewatch-bestiary-items/9gEiH150yl4bKC6n.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[9gGdxZjwIsTVIuxu.htm](agents-of-edgewatch-bestiary-items/9gGdxZjwIsTVIuxu.htm)|Alchemical Formulas (5th)|Formules alchimiques (5e)|officielle|
|[9HOzCgOAKeHP5EhB.htm](agents-of-edgewatch-bestiary-items/9HOzCgOAKeHP5EhB.htm)|Venom Explosion|Explosion de venin|officielle|
|[9hvKp9EJbq6Log0q.htm](agents-of-edgewatch-bestiary-items/9hvKp9EJbq6Log0q.htm)|Consume Flesh|Dévorer la chair|officielle|
|[9jJY47nOXFMztxEo.htm](agents-of-edgewatch-bestiary-items/9jJY47nOXFMztxEo.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[9KEf9z8ZVsw7qXJg.htm](agents-of-edgewatch-bestiary-items/9KEf9z8ZVsw7qXJg.htm)|Bloody Jab|Piqûre sanglante|officielle|
|[9LK5ORluncHekkZ5.htm](agents-of-edgewatch-bestiary-items/9LK5ORluncHekkZ5.htm)|Swarming Bites|Nuée de morsures|officielle|
|[9m6cGcYmQptppNHf.htm](agents-of-edgewatch-bestiary-items/9m6cGcYmQptppNHf.htm)|Change Shape|Changement de forme|officielle|
|[9n6E9GRbgNOMXBmN.htm](agents-of-edgewatch-bestiary-items/9n6E9GRbgNOMXBmN.htm)|Grab|Empoignade|officielle|
|[9oInACsH1lSDnaHW.htm](agents-of-edgewatch-bestiary-items/9oInACsH1lSDnaHW.htm)|Spinning Blade|Lame rotative|officielle|
|[9ShSRecAk97rJkLS.htm](agents-of-edgewatch-bestiary-items/9ShSRecAk97rJkLS.htm)|Motion Sense 60 feet|Perception du mouvement 18 m,|officielle|
|[9sMCsySJHETUYLfH.htm](agents-of-edgewatch-bestiary-items/9sMCsySJHETUYLfH.htm)|Staff|Bâton|officielle|
|[9WqBdZ30eNWIpXaC.htm](agents-of-edgewatch-bestiary-items/9WqBdZ30eNWIpXaC.htm)|Fangs|Crocs|officielle|
|[A04N1HzBOQCcYnM2.htm](agents-of-edgewatch-bestiary-items/A04N1HzBOQCcYnM2.htm)|Blackfinger Blight|Fléau de noirs doigts|officielle|
|[a1Es9ETdZdfagQRX.htm](agents-of-edgewatch-bestiary-items/a1Es9ETdZdfagQRX.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[A2LL8i2v8LsE6Ilp.htm](agents-of-edgewatch-bestiary-items/A2LL8i2v8LsE6Ilp.htm)|Cleaver|Fendoir|officielle|
|[a34FGdPF5cxVbX4J.htm](agents-of-edgewatch-bestiary-items/a34FGdPF5cxVbX4J.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[A4evoVscksRyehl1.htm](agents-of-edgewatch-bestiary-items/A4evoVscksRyehl1.htm)|Greater Planar Rift|Faille planaire supérieure|officielle|
|[A4o54wh2xPLY1V7q.htm](agents-of-edgewatch-bestiary-items/A4o54wh2xPLY1V7q.htm)|Composite Shortbow|Arc court composite|officielle|
|[a5d4jfdl2s8CiAsa.htm](agents-of-edgewatch-bestiary-items/a5d4jfdl2s8CiAsa.htm)|Breath of Lies|Souffle de mensonges|officielle|
|[A6EBzxn9xWiWavNB.htm](agents-of-edgewatch-bestiary-items/A6EBzxn9xWiWavNB.htm)|Telepathy 100 feet|Télépathie à 30 mètres|officielle|
|[a6jNbmkq4m7CmbSA.htm](agents-of-edgewatch-bestiary-items/a6jNbmkq4m7CmbSA.htm)|Lawkeeper|Gardienne de l'ordre|officielle|
|[A7VgvFTQFpm1Cc79.htm](agents-of-edgewatch-bestiary-items/A7VgvFTQFpm1Cc79.htm)|Smoke Bomb Launcher|Lanceur de bombes fumigènes|officielle|
|[A8r84vdl4zcvpCMg.htm](agents-of-edgewatch-bestiary-items/A8r84vdl4zcvpCMg.htm)|Free Blade|Lame inexorable|officielle|
|[aap2jSjnDkxEKZUI.htm](agents-of-edgewatch-bestiary-items/aap2jSjnDkxEKZUI.htm)|Fist|Poing|officielle|
|[abpRv79lZLaukj8M.htm](agents-of-edgewatch-bestiary-items/abpRv79lZLaukj8M.htm)|Warhammer|Marteau de guerre|officielle|
|[aBQGxKqOuBqF1FkF.htm](agents-of-edgewatch-bestiary-items/aBQGxKqOuBqF1FkF.htm)|Double Stab|Perforation double|officielle|
|[ACezcH9GTIcXGlSB.htm](agents-of-edgewatch-bestiary-items/ACezcH9GTIcXGlSB.htm)|Mortal Shell|Coquille mortelle|officielle|
|[AD2GrziGoOFfzrk7.htm](agents-of-edgewatch-bestiary-items/AD2GrziGoOFfzrk7.htm)|Contingency Plan|Plan de secours|officielle|
|[AIJka2Zew5I1e7Lo.htm](agents-of-edgewatch-bestiary-items/AIJka2Zew5I1e7Lo.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[AiOnTNJ8o76YiJze.htm](agents-of-edgewatch-bestiary-items/AiOnTNJ8o76YiJze.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[aJfhxNbUXiAHn566.htm](agents-of-edgewatch-bestiary-items/aJfhxNbUXiAHn566.htm)|Warfare Lore|Connaissance de la guerre|officielle|
|[aJI86W6ShmBc7gPN.htm](agents-of-edgewatch-bestiary-items/aJI86W6ShmBc7gPN.htm)|Entropy Sense (Imprecise) 120 feet|Perception de l'entropie (imprécis) 36 m|officielle|
|[akEAcbm2fRfGZg8v.htm](agents-of-edgewatch-bestiary-items/akEAcbm2fRfGZg8v.htm)|Conjure Swords|Invocation d'épées|officielle|
|[AkqOlXLB6K0JLoqz.htm](agents-of-edgewatch-bestiary-items/AkqOlXLB6K0JLoqz.htm)|Wail of the Betrayed|Plainte de trahison|officielle|
|[ANZcdjjZfZOfuJYL.htm](agents-of-edgewatch-bestiary-items/ANZcdjjZfZOfuJYL.htm)|Jaws|Mâchoires|officielle|
|[apKzqJtSb9Y34OBk.htm](agents-of-edgewatch-bestiary-items/apKzqJtSb9Y34OBk.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[AQMbWGVUpkE6QLbs.htm](agents-of-edgewatch-bestiary-items/AQMbWGVUpkE6QLbs.htm)|Kiss|Baiser|officielle|
|[aSPT9O0IpzqjiqG0.htm](agents-of-edgewatch-bestiary-items/aSPT9O0IpzqjiqG0.htm)|Seal Room|Sceller la pièce|officielle|
|[aSvIfWvkrhDYQZn3.htm](agents-of-edgewatch-bestiary-items/aSvIfWvkrhDYQZn3.htm)|Regeneration 20 (Deactivated by Fire or Acid)|Régénération 20 (Désactivée par Feu ou Acide)|officielle|
|[aSzrKKXR7hl8ZVhX.htm](agents-of-edgewatch-bestiary-items/aSzrKKXR7hl8ZVhX.htm)|Stone Spike|Pointe de pierre|officielle|
|[at6MabtHqhZPINQ3.htm](agents-of-edgewatch-bestiary-items/at6MabtHqhZPINQ3.htm)|Suction|Succion|officielle|
|[AT8OH6P1ZPmkA0CR.htm](agents-of-edgewatch-bestiary-items/AT8OH6P1ZPmkA0CR.htm)|Paralysis|Paralysie|officielle|
|[AuIi30v2cCwEK9Oy.htm](agents-of-edgewatch-bestiary-items/AuIi30v2cCwEK9Oy.htm)|Flog Mercilessly|Fouetter impitoyablement|officielle|
|[aV20X0Mp1CqH1tqz.htm](agents-of-edgewatch-bestiary-items/aV20X0Mp1CqH1tqz.htm)|Fan of Daggers|Amoureux des dagues|officielle|
|[AV34BEM8xG7BwDYs.htm](agents-of-edgewatch-bestiary-items/AV34BEM8xG7BwDYs.htm)|Crossbow|Arbalète|officielle|
|[Av9NysCBnGty9we0.htm](agents-of-edgewatch-bestiary-items/Av9NysCBnGty9we0.htm)|Furious Wallop|Frappe furieuse|officielle|
|[aVFV0hNNlEPetZWI.htm](agents-of-edgewatch-bestiary-items/aVFV0hNNlEPetZWI.htm)|Poisoner's Staff (Major)|Bâton de l'empoisonneur (majeur)|libre|
|[AvJUZJ1PD1HyuHdf.htm](agents-of-edgewatch-bestiary-items/AvJUZJ1PD1HyuHdf.htm)|Crossbow|Arbalète|officielle|
|[aVyixje5UBQb21sX.htm](agents-of-edgewatch-bestiary-items/aVyixje5UBQb21sX.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[awh4If0CSlamWfUO.htm](agents-of-edgewatch-bestiary-items/awh4If0CSlamWfUO.htm)|Improved Grab|Empoignade améliorée|officielle|
|[awO8dArKZVWslq53.htm](agents-of-edgewatch-bestiary-items/awO8dArKZVWslq53.htm)|Claw|Griffe|officielle|
|[aYZ47LUpvGry49gd.htm](agents-of-edgewatch-bestiary-items/aYZ47LUpvGry49gd.htm)|Ooze Lore|Connaissance des vases|officielle|
|[aZlGemIJeXm9jJEE.htm](agents-of-edgewatch-bestiary-items/aZlGemIJeXm9jJEE.htm)|Knife Thrower|Lanceur de couteau|officielle|
|[aZvRC9a03MwZf7rr.htm](agents-of-edgewatch-bestiary-items/aZvRC9a03MwZf7rr.htm)|Frightening Critical|Critique effrayant|officielle|
|[b0iAXGEWNNI2u5Tg.htm](agents-of-edgewatch-bestiary-items/b0iAXGEWNNI2u5Tg.htm)|War Razor|Rasoir de combat|officielle|
|[B0YPC9dAo09oqw5y.htm](agents-of-edgewatch-bestiary-items/B0YPC9dAo09oqw5y.htm)|Innate Occult Spells|Sorts occultes innés|libre|
|[b1QZ33BjtQXdUVJj.htm](agents-of-edgewatch-bestiary-items/b1QZ33BjtQXdUVJj.htm)|Cannon Arm|Bras canon|officielle|
|[B3Ppv6QafImenNGu.htm](agents-of-edgewatch-bestiary-items/B3Ppv6QafImenNGu.htm)|Claw|Griffe|officielle|
|[B6M6fTBtJwAQDYCs.htm](agents-of-edgewatch-bestiary-items/B6M6fTBtJwAQDYCs.htm)|Swarm Mind|Esprit de la nuée|officielle|
|[B8N51wc1bGZzdZ7d.htm](agents-of-edgewatch-bestiary-items/B8N51wc1bGZzdZ7d.htm)|Hateful Gaze|Regard haineux|officielle|
|[b8sV2RLnt3RmLO2t.htm](agents-of-edgewatch-bestiary-items/b8sV2RLnt3RmLO2t.htm)|Upward Stab|Poignarder vers le haut|officielle|
|[b9gVzcE3UM4ixbBV.htm](agents-of-edgewatch-bestiary-items/b9gVzcE3UM4ixbBV.htm)|Tendrils Come Alive|Éveil des vrilles|officielle|
|[bA6R5JBcAw7StwCO.htm](agents-of-edgewatch-bestiary-items/bA6R5JBcAw7StwCO.htm)|Cane of the Maelstrom|Canne du Maelström|officielle|
|[Ba9gUN2kmGTDwJ7v.htm](agents-of-edgewatch-bestiary-items/Ba9gUN2kmGTDwJ7v.htm)|Bloody Mayhem|Carnage sanglant|officielle|
|[BasU6Iuc7hRxhoYG.htm](agents-of-edgewatch-bestiary-items/BasU6Iuc7hRxhoYG.htm)|Form Tool|Former un outil|officielle|
|[BcDHhgCwCxAUCy1q.htm](agents-of-edgewatch-bestiary-items/BcDHhgCwCxAUCy1q.htm)|Jaws|Mâchoires|officielle|
|[BdWPHFjkOXFGTDgi.htm](agents-of-edgewatch-bestiary-items/BdWPHFjkOXFGTDgi.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[BeG2KYyzwrCfE6KK.htm](agents-of-edgewatch-bestiary-items/BeG2KYyzwrCfE6KK.htm)|Warpwave Strike|Frappe de vagues de distorsion|officielle|
|[BEoBeAAFDzMQN6GW.htm](agents-of-edgewatch-bestiary-items/BEoBeAAFDzMQN6GW.htm)|Darkvision|Vision dans le noir|officielle|
|[bfsuL0uIFF1F4Guy.htm](agents-of-edgewatch-bestiary-items/bfsuL0uIFF1F4Guy.htm)|Fangs|Crocs|officielle|
|[BGk06cTCmFh4ZooN.htm](agents-of-edgewatch-bestiary-items/BGk06cTCmFh4ZooN.htm)|Telepathy 100 feet|Télépathie à 30 mètres|officielle|
|[BHZkmFQkiTD4QBd2.htm](agents-of-edgewatch-bestiary-items/BHZkmFQkiTD4QBd2.htm)|Dagger|Dague|officielle|
|[bi6VcgRWUdub6hl4.htm](agents-of-edgewatch-bestiary-items/bi6VcgRWUdub6hl4.htm)|Subsume|Engouffrer|officielle|
|[Bi7jDnQux0YzmGpg.htm](agents-of-edgewatch-bestiary-items/Bi7jDnQux0YzmGpg.htm)|Telepathy 100 feet|Télépathie à 30 mètres|officielle|
|[biy9pcVadULFQhYw.htm](agents-of-edgewatch-bestiary-items/biy9pcVadULFQhYw.htm)|Reaper's Lancet Sheath|Fourreau du Bistouri de l'éventreur|officielle|
|[blAoRBmaeuiqIsGU.htm](agents-of-edgewatch-bestiary-items/blAoRBmaeuiqIsGU.htm)|Games Lore|Connaissance ludique|officielle|
|[Blq1uGh1lHrneL8W.htm](agents-of-edgewatch-bestiary-items/Blq1uGh1lHrneL8W.htm)|Constant Spells|Sorts constants|officielle|
|[bLsJ5WlwMamd2QGg.htm](agents-of-edgewatch-bestiary-items/bLsJ5WlwMamd2QGg.htm)|Rejuvenation|Reconstruction|officielle|
|[Bm63scXZG8oZql5K.htm](agents-of-edgewatch-bestiary-items/Bm63scXZG8oZql5K.htm)|Fist|Poing|officielle|
|[bN92RThudov2eNQm.htm](agents-of-edgewatch-bestiary-items/bN92RThudov2eNQm.htm)|Legal Lore|Connaissance juridique|officielle|
|[BofxZCq1wekuufql.htm](agents-of-edgewatch-bestiary-items/BofxZCq1wekuufql.htm)|Joro Spider Venom|Venin d'araignée joro|officielle|
|[bplHxNRZHOun4UiH.htm](agents-of-edgewatch-bestiary-items/bplHxNRZHOun4UiH.htm)|Hand Crossbow|Arbalète de poing|officielle|
|[bpTCeqf0WRsdxC9t.htm](agents-of-edgewatch-bestiary-items/bpTCeqf0WRsdxC9t.htm)|Venom Stream|Flot de venin|officielle|
|[bqTN2IAncaogzgo9.htm](agents-of-edgewatch-bestiary-items/bqTN2IAncaogzgo9.htm)|Weapon Master|Maître d'armes|officielle|
|[BrOfryWmR4ITLzIV.htm](agents-of-edgewatch-bestiary-items/BrOfryWmR4ITLzIV.htm)|Flame Dart|Fléchette de flammes|officielle|
|[bSa3Y6h5y9hcXmYa.htm](agents-of-edgewatch-bestiary-items/bSa3Y6h5y9hcXmYa.htm)|Katar|Katar|officielle|
|[BSAdAEHe8VdvYOjg.htm](agents-of-edgewatch-bestiary-items/BSAdAEHe8VdvYOjg.htm)|Bottled Lightning|Foudre en bouteille|officielle|
|[bsbrnl0pxRuL6acz.htm](agents-of-edgewatch-bestiary-items/bsbrnl0pxRuL6acz.htm)|Hose|Lance à eau|officielle|
|[BSfiT0dLYi9Xp9gb.htm](agents-of-edgewatch-bestiary-items/BSfiT0dLYi9Xp9gb.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[BsQZslLQ0FJcYne3.htm](agents-of-edgewatch-bestiary-items/BsQZslLQ0FJcYne3.htm)|Reshape Reality|Remodeler la réalité|officielle|
|[BT64pCzbpph5pC4r.htm](agents-of-edgewatch-bestiary-items/BT64pCzbpph5pC4r.htm)|Darkvision|Vision dans le noir|officielle|
|[BTVYmpw94v3oWTGl.htm](agents-of-edgewatch-bestiary-items/BTVYmpw94v3oWTGl.htm)|Constant Spells|Sorts constants|officielle|
|[bVqJIi6r8cTALSZa.htm](agents-of-edgewatch-bestiary-items/bVqJIi6r8cTALSZa.htm)|Dagger|Dague|officielle|
|[BW9xnDoY4BohVAGZ.htm](agents-of-edgewatch-bestiary-items/BW9xnDoY4BohVAGZ.htm)|Focus Spells|Sorts focalisés|libre|
|[bWtvDdGa2KvPHNmx.htm](agents-of-edgewatch-bestiary-items/bWtvDdGa2KvPHNmx.htm)|Grab|Empoignade|officielle|
|[Bx3FBCXmpwC6wpPZ.htm](agents-of-edgewatch-bestiary-items/Bx3FBCXmpwC6wpPZ.htm)|Fist|Poing|officielle|
|[byUSs0eF5Ctqs4xY.htm](agents-of-edgewatch-bestiary-items/byUSs0eF5Ctqs4xY.htm)|Sea Hag's Bargain|Contrat de la guenaude marine|officielle|
|[bZAhdsbYFuZ2syFl.htm](agents-of-edgewatch-bestiary-items/bZAhdsbYFuZ2syFl.htm)|Easy to Call|Facile à appeler|officielle|
|[C01m9GnZW7b5Uk9K.htm](agents-of-edgewatch-bestiary-items/C01m9GnZW7b5Uk9K.htm)|Shadow Step|Pas de l'ombre|officielle|
|[C2rrPgxtXj2SQdVM.htm](agents-of-edgewatch-bestiary-items/C2rrPgxtXj2SQdVM.htm)|Dagger|Dague|officielle|
|[C5DSJUvGx6T45XYO.htm](agents-of-edgewatch-bestiary-items/C5DSJUvGx6T45XYO.htm)|Attack of Opportunity (Stinger Only)|Attaque d'opportunité (Dard uniquement)|officielle|
|[c6pExwVTDS5Qyrw4.htm](agents-of-edgewatch-bestiary-items/c6pExwVTDS5Qyrw4.htm)|Constant Spells|Sorts constants|officielle|
|[C8jCG9WTW2fP6H0b.htm](agents-of-edgewatch-bestiary-items/C8jCG9WTW2fP6H0b.htm)|Poison Frenzy|Déchaînement de poison|officielle|
|[C8jd840fpmYd3wDB.htm](agents-of-edgewatch-bestiary-items/C8jd840fpmYd3wDB.htm)|Suffocating Fumes|Vapeurs asphyxiantes|officielle|
|[CaQuzIFC9BNqHDUQ.htm](agents-of-edgewatch-bestiary-items/CaQuzIFC9BNqHDUQ.htm)|Dagger|Dague|officielle|
|[CBZ2xBopPRbaMVV0.htm](agents-of-edgewatch-bestiary-items/CBZ2xBopPRbaMVV0.htm)|Capsize|Chavirer|officielle|
|[CcCVlFjqLYk2DTlc.htm](agents-of-edgewatch-bestiary-items/CcCVlFjqLYk2DTlc.htm)|+1 Status to Will Saves vs. Mental|+1 de statut aux JdS contre mental|officielle|
|[CCz3eaJWNZAPwqBS.htm](agents-of-edgewatch-bestiary-items/CCz3eaJWNZAPwqBS.htm)|Master Brawler|Maître bagarreur|officielle|
|[cdoPISuZdSbxOwT1.htm](agents-of-edgewatch-bestiary-items/cdoPISuZdSbxOwT1.htm)|Spell Choke|Étranglement de sort|officielle|
|[ce1K4Mc00gFvS1v8.htm](agents-of-edgewatch-bestiary-items/ce1K4Mc00gFvS1v8.htm)|Telepathy 100 feet|Télépathie à 30 mètres|officielle|
|[cerEbvB6QEc8UcuU.htm](agents-of-edgewatch-bestiary-items/cerEbvB6QEc8UcuU.htm)|No Escape|Pas d'échappatoire|officielle|
|[cg0RVhBfO8MxjPpI.htm](agents-of-edgewatch-bestiary-items/cg0RVhBfO8MxjPpI.htm)|Superior Senses|Sens supérieurs|officielle|
|[CgEMdYYlkqnUPBfB.htm](agents-of-edgewatch-bestiary-items/CgEMdYYlkqnUPBfB.htm)|Perfect Will|Volonté parfaite|officielle|
|[chuMKB6cHYONy4NC.htm](agents-of-edgewatch-bestiary-items/chuMKB6cHYONy4NC.htm)|Toxic Gas|Gaz toxique|officielle|
|[CIh6yDe29eBqfdsD.htm](agents-of-edgewatch-bestiary-items/CIh6yDe29eBqfdsD.htm)|Haphazard Hack|Entaille désordonnée|officielle|
|[ciQ62q9OynsChQSL.htm](agents-of-edgewatch-bestiary-items/ciQ62q9OynsChQSL.htm)|Waves of Sorrow|Vagues de chagrin|officielle|
|[CK9zFRMfz5AvtehE.htm](agents-of-edgewatch-bestiary-items/CK9zFRMfz5AvtehE.htm)|Focus Spells|Sorts focalisés|libre|
|[CkJTSqPlqI5Fj2b7.htm](agents-of-edgewatch-bestiary-items/CkJTSqPlqI5Fj2b7.htm)|Mirror Hand|Main miroir|officielle|
|[ckqlqEZcPT6ZKZTJ.htm](agents-of-edgewatch-bestiary-items/ckqlqEZcPT6ZKZTJ.htm)|Club|Gourdin|officielle|
|[CMV3xH3xyqqm0yJQ.htm](agents-of-edgewatch-bestiary-items/CMV3xH3xyqqm0yJQ.htm)|Athletics|Athlétisme|officielle|
|[cNdgAyzrxvT3Z68j.htm](agents-of-edgewatch-bestiary-items/cNdgAyzrxvT3Z68j.htm)|+2 Status to All Saves vs. Composition Spells|+2 de statut aux JdS contre les sorts de composition|officielle|
|[cneQw5areaRAqZhr.htm](agents-of-edgewatch-bestiary-items/cneQw5areaRAqZhr.htm)|Shortsword|Épée courte|officielle|
|[cNLxEiiU06b04C55.htm](agents-of-edgewatch-bestiary-items/cNLxEiiU06b04C55.htm)|Tentacle|Tentacule|officielle|
|[CnW4ILs2EPOYJl20.htm](agents-of-edgewatch-bestiary-items/CnW4ILs2EPOYJl20.htm)|Legal Lore|Connaissance juridique|officielle|
|[cny7NT0gNZmXhDv6.htm](agents-of-edgewatch-bestiary-items/cny7NT0gNZmXhDv6.htm)|Tentacle|Tentacule|officielle|
|[CpdC4SJqhh9UFhSi.htm](agents-of-edgewatch-bestiary-items/CpdC4SJqhh9UFhSi.htm)|Jaws|Mâchoires|officielle|
|[cSFNzQ52byIdvdWb.htm](agents-of-edgewatch-bestiary-items/cSFNzQ52byIdvdWb.htm)|Broken Quills|Plumes brisées|officielle|
|[Ct2gfJE7OAo8ZR44.htm](agents-of-edgewatch-bestiary-items/Ct2gfJE7OAo8ZR44.htm)|Constant Spells|Sorts constants|officielle|
|[Ctkj7r3YYAFSQEKx.htm](agents-of-edgewatch-bestiary-items/Ctkj7r3YYAFSQEKx.htm)|Disgorge Portal|Expulsion d'un portail|officielle|
|[CU5CXS5c5trwAd9g.htm](agents-of-edgewatch-bestiary-items/CU5CXS5c5trwAd9g.htm)|Steady Spellcasting|Incantation fiable|officielle|
|[CUtiSdjK9OwxIfKR.htm](agents-of-edgewatch-bestiary-items/CUtiSdjK9OwxIfKR.htm)|Shatter Defenses|Briser les défenses|officielle|
|[CUYM17Clq1YTEBxz.htm](agents-of-edgewatch-bestiary-items/CUYM17Clq1YTEBxz.htm)|Maddening Whispers|Murmures aliénants|officielle|
|[cvCrwykv9kKHfIwG.htm](agents-of-edgewatch-bestiary-items/cvCrwykv9kKHfIwG.htm)|Darkvision|Vision dans le noir|officielle|
|[CVEw8LgJymgw5uI2.htm](agents-of-edgewatch-bestiary-items/CVEw8LgJymgw5uI2.htm)|Rend|Éventration|officielle|
|[CVTZnv3ZcE4LbDPg.htm](agents-of-edgewatch-bestiary-items/CVTZnv3ZcE4LbDPg.htm)|Breath Weapon|Souffle|officielle|
|[cW925U5mT8WZIcql.htm](agents-of-edgewatch-bestiary-items/cW925U5mT8WZIcql.htm)|Shortbow|Arc court|officielle|
|[CWcYZHN9MSfxTR2k.htm](agents-of-edgewatch-bestiary-items/CWcYZHN9MSfxTR2k.htm)|Sneak Attack|Attaque sournoise|officielle|
|[CwNHaEk7qrMHHyza.htm](agents-of-edgewatch-bestiary-items/CwNHaEk7qrMHHyza.htm)|Constant Spells|Sorts constants|officielle|
|[CWOEh5RnFG5cysRk.htm](agents-of-edgewatch-bestiary-items/CWOEh5RnFG5cysRk.htm)|Tentacle Trip|Croc-en-jambe de tentacules|officielle|
|[cX2EH9YzIFFgfGra.htm](agents-of-edgewatch-bestiary-items/cX2EH9YzIFFgfGra.htm)|Dagger|Dague|officielle|
|[cXt1NhRBZEXIv1ju.htm](agents-of-edgewatch-bestiary-items/cXt1NhRBZEXIv1ju.htm)|Telepathy 100 feet|Télépathie à 30 mètres|officielle|
|[cYpzRpaINHeW2XxC.htm](agents-of-edgewatch-bestiary-items/cYpzRpaINHeW2XxC.htm)|Poison Weapon|Arme empoisonnée|officielle|
|[czNwyIcrJKwQNfoZ.htm](agents-of-edgewatch-bestiary-items/czNwyIcrJKwQNfoZ.htm)|Constant Spells|Sorts constants|officielle|
|[D27lYuRNmEYB5vjo.htm](agents-of-edgewatch-bestiary-items/D27lYuRNmEYB5vjo.htm)|Wipe Away Cracks|Réparer les fêlures|officielle|
|[DBzG2AKUenn28L2w.htm](agents-of-edgewatch-bestiary-items/DBzG2AKUenn28L2w.htm)|War Razor|Rasoir de combat|officielle|
|[dcDZ5fYx6PsHPo4t.htm](agents-of-edgewatch-bestiary-items/dcDZ5fYx6PsHPo4t.htm)|At-Will Spells|Sorts à volonté|officielle|
|[DdFilDnuHDObT81V.htm](agents-of-edgewatch-bestiary-items/DdFilDnuHDObT81V.htm)|Freeze Floor|Geler le sol|officielle|
|[dDvj6Zila83uSsch.htm](agents-of-edgewatch-bestiary-items/dDvj6Zila83uSsch.htm)|Flying Blade|Lame volante|officielle|
|[deQl75TOphyii6YU.htm](agents-of-edgewatch-bestiary-items/deQl75TOphyii6YU.htm)|Planar Coven|Cercle planaire|officielle|
|[DF3OKBUIdabo7i9j.htm](agents-of-edgewatch-bestiary-items/DF3OKBUIdabo7i9j.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[dff7pIEmGTfCShQo.htm](agents-of-edgewatch-bestiary-items/dff7pIEmGTfCShQo.htm)|Hardness 5|Solidité 5|officielle|
|[dffHcWQsvtoeLOID.htm](agents-of-edgewatch-bestiary-items/dffHcWQsvtoeLOID.htm)|Divine Rituals|Rituels divins|libre|
|[dfm82amOgBxSgH3m.htm](agents-of-edgewatch-bestiary-items/dfm82amOgBxSgH3m.htm)|Longspear|Pique|officielle|
|[DGsILrvl3LJIeZIu.htm](agents-of-edgewatch-bestiary-items/DGsILrvl3LJIeZIu.htm)|Enshroud|Envelopper|officielle|
|[Dh4LCrV41VJjACPD.htm](agents-of-edgewatch-bestiary-items/Dh4LCrV41VJjACPD.htm)|Arcane Cannon|Canon arcanique|officielle|
|[dhD7eewlB6LvvS2M.htm](agents-of-edgewatch-bestiary-items/dhD7eewlB6LvvS2M.htm)|Darkvision|Vision dans le noir|officielle|
|[dhPK5EdGfI5j8Yse.htm](agents-of-edgewatch-bestiary-items/dhPK5EdGfI5j8Yse.htm)|Thunk 'n' Slice|Frapper & couper|officielle|
|[DJUjRfb1YyWOvrSi.htm](agents-of-edgewatch-bestiary-items/DJUjRfb1YyWOvrSi.htm)|At-Will Spells|Sorts à volonté|officielle|
|[DLJfYIHzmCQGAW0s.htm](agents-of-edgewatch-bestiary-items/DLJfYIHzmCQGAW0s.htm)|Rabbit Punch|Coup du lapin|officielle|
|[Dm6lDovIDGIcISuP.htm](agents-of-edgewatch-bestiary-items/Dm6lDovIDGIcISuP.htm)|Magnetize the Living|Magnétiser les vivants|officielle|
|[DNsLOiJgbt3epj8c.htm](agents-of-edgewatch-bestiary-items/DNsLOiJgbt3epj8c.htm)|Mob Rush|Ruée de la foule|officielle|
|[DNVNuZGCllMw1hmA.htm](agents-of-edgewatch-bestiary-items/DNVNuZGCllMw1hmA.htm)|Drowning Grasp|Noyer par contact|officielle|
|[DOYd8fquer3sIStG.htm](agents-of-edgewatch-bestiary-items/DOYd8fquer3sIStG.htm)|Entropic Feedback|Retour entropique|officielle|
|[dPoLgUKZRxlWnRba.htm](agents-of-edgewatch-bestiary-items/dPoLgUKZRxlWnRba.htm)|Swallow Whole|Gober|officielle|
|[DREnRy1SAHY6dCiU.htm](agents-of-edgewatch-bestiary-items/DREnRy1SAHY6dCiU.htm)|Alchemical Formulas (10th)|Formules alchimiques (10e)|officielle|
|[drfXFnc49cl52C2X.htm](agents-of-edgewatch-bestiary-items/drfXFnc49cl52C2X.htm)|Foot|Pied|officielle|
|[DrsLgW5tbXlYEqJ1.htm](agents-of-edgewatch-bestiary-items/DrsLgW5tbXlYEqJ1.htm)|Alien Presence|Présence étrangère|officielle|
|[dRVd3bgtgiGOetEC.htm](agents-of-edgewatch-bestiary-items/dRVd3bgtgiGOetEC.htm)|Darkvision|Vision dans le noir|officielle|
|[DSVJsEHiWFPACcNX.htm](agents-of-edgewatch-bestiary-items/DSVJsEHiWFPACcNX.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[dwuIf4lpimWqI6eV.htm](agents-of-edgewatch-bestiary-items/dwuIf4lpimWqI6eV.htm)|+1 Status to All Saves vs. Magic|+1 de statut aux JdS contre la magie|officielle|
|[Dz4rpuIMNqJy4V90.htm](agents-of-edgewatch-bestiary-items/Dz4rpuIMNqJy4V90.htm)|Inky Imitator|Imitateur d'encre|officielle|
|[E0iLnf03NhzlNZTT.htm](agents-of-edgewatch-bestiary-items/E0iLnf03NhzlNZTT.htm)|Moderate Alchemist's Fire|Feu grégeois moyen|officielle|
|[E0zQqvJo9nRAZs7T.htm](agents-of-edgewatch-bestiary-items/E0zQqvJo9nRAZs7T.htm)|Blowtorch|Chalumeau|officielle|
|[E3ItzWH7n7tJ4AB0.htm](agents-of-edgewatch-bestiary-items/E3ItzWH7n7tJ4AB0.htm)|Entropy Sense (Imprecise) 60 feet|Perception de l'entropie (imprécis) 18 m|officielle|
|[e5D3G73TDu6XWskj.htm](agents-of-edgewatch-bestiary-items/e5D3G73TDu6XWskj.htm)|Twisting Reach|Allonge serpentine|officielle|
|[E5E4EFfOXmYDktze.htm](agents-of-edgewatch-bestiary-items/E5E4EFfOXmYDktze.htm)|War Razor|Rasoir de combat|officielle|
|[E85UiB4OeqzsCKWf.htm](agents-of-edgewatch-bestiary-items/E85UiB4OeqzsCKWf.htm)|Darkvision|Vision dans le noir|officielle|
|[E9Cr9P2L3cp3NzRD.htm](agents-of-edgewatch-bestiary-items/E9Cr9P2L3cp3NzRD.htm)|Aura of Angry Crystals|Aura de cristaux furieux|officielle|
|[EaAkzJuaOgph4evs.htm](agents-of-edgewatch-bestiary-items/EaAkzJuaOgph4evs.htm)|Jaws|Mâchoires|officielle|
|[EaBeGYjdT1VVqvT6.htm](agents-of-edgewatch-bestiary-items/EaBeGYjdT1VVqvT6.htm)|Split|Scission|officielle|
|[eaCjNNNqsgrBB55K.htm](agents-of-edgewatch-bestiary-items/eaCjNNNqsgrBB55K.htm)|Hand Crossbow|Arbalète de poing|officielle|
|[eapXoUtrhnFlcZ2a.htm](agents-of-edgewatch-bestiary-items/eapXoUtrhnFlcZ2a.htm)|Gang Lore|Connaissance des gangs|officielle|
|[EbIhMOd0cHtkhNcM.htm](agents-of-edgewatch-bestiary-items/EbIhMOd0cHtkhNcM.htm)|Zealous Restoration|Restauration zélée|officielle|
|[ecUXFzmWTDeBzbIu.htm](agents-of-edgewatch-bestiary-items/ecUXFzmWTDeBzbIu.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[EEMf91Mnk5n3dYup.htm](agents-of-edgewatch-bestiary-items/EEMf91Mnk5n3dYup.htm)|Frantic Grasp|Étreinte frénétique|officielle|
|[EF6rT8DI1QtqCDmn.htm](agents-of-edgewatch-bestiary-items/EF6rT8DI1QtqCDmn.htm)|Improved Grab|Empoignade améliorée|officielle|
|[EFGYVjmnPR7oeSdb.htm](agents-of-edgewatch-bestiary-items/EFGYVjmnPR7oeSdb.htm)|Spear|Lance|officielle|
|[efKlG6Tj9bAd6SQ9.htm](agents-of-edgewatch-bestiary-items/efKlG6Tj9bAd6SQ9.htm)|Treasure Sense (Imprecise) 30 feet|Perception des trésors (imprécis) 9 m|officielle|
|[EGOkkUlxQlgkV8Ao.htm](agents-of-edgewatch-bestiary-items/EGOkkUlxQlgkV8Ao.htm)|Dagger|Dague|officielle|
|[eGuMBSFkjeJsBOLy.htm](agents-of-edgewatch-bestiary-items/eGuMBSFkjeJsBOLy.htm)|Fangs|Crocs|officielle|
|[eIDlmbUtMfTc108y.htm](agents-of-edgewatch-bestiary-items/eIDlmbUtMfTc108y.htm)|Throw Shadow Blade|Lancer de lame d'ombre|officielle|
|[EjUqgzIjC8XDdB6h.htm](agents-of-edgewatch-bestiary-items/EjUqgzIjC8XDdB6h.htm)|Dagger|Dague|officielle|
|[eJY1bfWIhxuD4h0H.htm](agents-of-edgewatch-bestiary-items/eJY1bfWIhxuD4h0H.htm)|Delay Condition|Retardement d'état|officielle|
|[ekdPtlR0pjDVDM4T.htm](agents-of-edgewatch-bestiary-items/ekdPtlR0pjDVDM4T.htm)|Go for the Eyes|Viser les yeux|officielle|
|[EKp5irpqgnTwEyjX.htm](agents-of-edgewatch-bestiary-items/EKp5irpqgnTwEyjX.htm)|Backdrop|Décor|officielle|
|[EKw9z5qbMiwdPSwH.htm](agents-of-edgewatch-bestiary-items/EKw9z5qbMiwdPSwH.htm)|Darkvision|Vision dans le noir|officielle|
|[ELoLmxNU0ywpt3HM.htm](agents-of-edgewatch-bestiary-items/ELoLmxNU0ywpt3HM.htm)|Stone Spike|Pointe de pierre|officielle|
|[ElQzIcZqeoNNQayH.htm](agents-of-edgewatch-bestiary-items/ElQzIcZqeoNNQayH.htm)|Dagger|Dague|officielle|
|[emBPB2m0d0BQpI2N.htm](agents-of-edgewatch-bestiary-items/emBPB2m0d0BQpI2N.htm)|Leaching Glare|Regard troublant|officielle|
|[EMKkZjLFHbAYJztn.htm](agents-of-edgewatch-bestiary-items/EMKkZjLFHbAYJztn.htm)|Innate Occult Spells|Sorts occultes innés|libre|
|[eosKGTfGo3nA3EVl.htm](agents-of-edgewatch-bestiary-items/eosKGTfGo3nA3EVl.htm)|Recalibrate|Recalibrage|officielle|
|[ePfqkUvCF1uDrS24.htm](agents-of-edgewatch-bestiary-items/ePfqkUvCF1uDrS24.htm)|Darkvision|Vision dans le noir|officielle|
|[ePXkUAkuSGauQ7Jv.htm](agents-of-edgewatch-bestiary-items/ePXkUAkuSGauQ7Jv.htm)|Supersonic Hearing|Ouïe supersonique|officielle|
|[Eq6IkJnS6klg5Kqj.htm](agents-of-edgewatch-bestiary-items/Eq6IkJnS6klg5Kqj.htm)|Target Culprit|Cibler un criminel|officielle|
|[er6I32kkHdLAxCXI.htm](agents-of-edgewatch-bestiary-items/er6I32kkHdLAxCXI.htm)|Stamper|Tamponneuse|officielle|
|[EtuysfUGpQnBFAaU.htm](agents-of-edgewatch-bestiary-items/EtuysfUGpQnBFAaU.htm)|Rapier|Rapière|officielle|
|[eTWouqSZqarzUigD.htm](agents-of-edgewatch-bestiary-items/eTWouqSZqarzUigD.htm)|+1 Status to All Saves vs. Magic|+1 de statut aux JdS contre la magie|officielle|
|[eVjcGt7GJnLCOU3R.htm](agents-of-edgewatch-bestiary-items/eVjcGt7GJnLCOU3R.htm)|Constrict|Constriction|officielle|
|[EWsYBE6UDRYE7p46.htm](agents-of-edgewatch-bestiary-items/EWsYBE6UDRYE7p46.htm)|Skitterstitch Venom|Venin d'araignée de peaux recousues|officielle|
|[EXg9UirCZfna0Tsv.htm](agents-of-edgewatch-bestiary-items/EXg9UirCZfna0Tsv.htm)|Hook Claw|Griffe crochue|officielle|
|[EyiD5AxylSuM4vcO.htm](agents-of-edgewatch-bestiary-items/EyiD5AxylSuM4vcO.htm)|Dagger|Dague|officielle|
|[eYUQ4i2nFz1iBeTO.htm](agents-of-edgewatch-bestiary-items/eYUQ4i2nFz1iBeTO.htm)|Grab|Empoignade|officielle|
|[eyUSuPWwgWZeCeA7.htm](agents-of-edgewatch-bestiary-items/eyUSuPWwgWZeCeA7.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[Ez5LQ3xID56lShet.htm](agents-of-edgewatch-bestiary-items/Ez5LQ3xID56lShet.htm)|Infector|Infector|officielle|
|[f0fmwIGLi4szm1lO.htm](agents-of-edgewatch-bestiary-items/f0fmwIGLi4szm1lO.htm)|Darkvision|Vision dans le noir|officielle|
|[F0wNdQAwukZzTRRS.htm](agents-of-edgewatch-bestiary-items/F0wNdQAwukZzTRRS.htm)|Alchemical Torrent|Torrent alchimique|officielle|
|[f3cXxnZGDx1KqvCZ.htm](agents-of-edgewatch-bestiary-items/f3cXxnZGDx1KqvCZ.htm)|Sneak Attack|Attaque sournoise|officielle|
|[f52vv2ucD59hRBEO.htm](agents-of-edgewatch-bestiary-items/f52vv2ucD59hRBEO.htm)|Constrict|Constriction|officielle|
|[f6aAjLfElEhbsLCC.htm](agents-of-edgewatch-bestiary-items/f6aAjLfElEhbsLCC.htm)|Dirty Bomb|Bombe toxique|officielle|
|[f8FrD7VC9NmfhQ3W.htm](agents-of-edgewatch-bestiary-items/f8FrD7VC9NmfhQ3W.htm)|Warfare Lore|Connaissance de la guerre|officielle|
|[f9SGRyWKHiuofcI9.htm](agents-of-edgewatch-bestiary-items/f9SGRyWKHiuofcI9.htm)|Ravenous Jaws|Mâchoires voraces|officielle|
|[fBWMN8Yh6mm05krO.htm](agents-of-edgewatch-bestiary-items/fBWMN8Yh6mm05krO.htm)|First Step|Premier pas|officielle|
|[fC6dc9AdYYKBeuWn.htm](agents-of-edgewatch-bestiary-items/fC6dc9AdYYKBeuWn.htm)|Daemonic Pledge|Serment daémonique|officielle|
|[FcWnKeqz5Ffy5vxu.htm](agents-of-edgewatch-bestiary-items/FcWnKeqz5Ffy5vxu.htm)|At-Will Spells|Sorts à volonté|officielle|
|[fgprZ9vs1zfRUz3o.htm](agents-of-edgewatch-bestiary-items/fgprZ9vs1zfRUz3o.htm)|Dispel Vulnerability|Dissipation de vulnérabilité|officielle|
|[FH3nsGdSWs8rbUYr.htm](agents-of-edgewatch-bestiary-items/FH3nsGdSWs8rbUYr.htm)|Poisonous Cloud|Nuage empoisonné|officielle|
|[FHNon1tZFK4IcVPe.htm](agents-of-edgewatch-bestiary-items/FHNon1tZFK4IcVPe.htm)|Legal Lore|Connaissance juridique|officielle|
|[fJd0mVVeFIuCWuEo.htm](agents-of-edgewatch-bestiary-items/fJd0mVVeFIuCWuEo.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[fJjXPvTiys4MFIsw.htm](agents-of-edgewatch-bestiary-items/fJjXPvTiys4MFIsw.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[FK02FgrkiyUcMuJp.htm](agents-of-edgewatch-bestiary-items/FK02FgrkiyUcMuJp.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[fkNlQGLkTlnATC6Y.htm](agents-of-edgewatch-bestiary-items/fkNlQGLkTlnATC6Y.htm)|Swallow Whole|Gober|officielle|
|[fkyMMwCkFLHVH5BV.htm](agents-of-edgewatch-bestiary-items/fkyMMwCkFLHVH5BV.htm)|Steady Spellcasting|Incantation fiable|officielle|
|[Fm6lAJIPw6ObpzHZ.htm](agents-of-edgewatch-bestiary-items/Fm6lAJIPw6ObpzHZ.htm)|Pull Under|Par le fond|officielle|
|[FMCAwZcfOhS476dW.htm](agents-of-edgewatch-bestiary-items/FMCAwZcfOhS476dW.htm)|Darkvision|Vision dans le noir|officielle|
|[fMk2JkXosTd2BjxC.htm](agents-of-edgewatch-bestiary-items/fMk2JkXosTd2BjxC.htm)|Darkvision|Vision dans le noir|officielle|
|[fmyoJEwSeBRrb9Ik.htm](agents-of-edgewatch-bestiary-items/fmyoJEwSeBRrb9Ik.htm)|+1 Status Bonus on Saves vs. Divine Magic|+1 de statut aux JdS contre la magie divine|officielle|
|[fORoOGa8xpUS4qM6.htm](agents-of-edgewatch-bestiary-items/fORoOGa8xpUS4qM6.htm)|Dark Tapestry Lore|Connaissance de la Sombre Tapisserie|officielle|
|[fPhaooKcxBRh5Bsx.htm](agents-of-edgewatch-bestiary-items/fPhaooKcxBRh5Bsx.htm)|Sneak Attack|Attaque sournoise|officielle|
|[FqjfY1OlrUKVd4mw.htm](agents-of-edgewatch-bestiary-items/FqjfY1OlrUKVd4mw.htm)|Darkvision|Vision dans le noir|officielle|
|[fqPyHyqdqW4NMFk3.htm](agents-of-edgewatch-bestiary-items/fqPyHyqdqW4NMFk3.htm)|Infused Items|Objets imprégnés|officielle|
|[fRnsLPos2xrH1GzQ.htm](agents-of-edgewatch-bestiary-items/fRnsLPos2xrH1GzQ.htm)|Low-Light Vision|Vision nocturne|officielle|
|[FsASThbyptq4QfWi.htm](agents-of-edgewatch-bestiary-items/FsASThbyptq4QfWi.htm)|Trample|Piétinement|officielle|
|[fSTbX2DA6uu9MkuN.htm](agents-of-edgewatch-bestiary-items/fSTbX2DA6uu9MkuN.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[fu5LQi1ag0KXxEby.htm](agents-of-edgewatch-bestiary-items/fu5LQi1ag0KXxEby.htm)|Prickly Defense|Défense épineuse|officielle|
|[FvZvIio9Hz2dvltI.htm](agents-of-edgewatch-bestiary-items/FvZvIio9Hz2dvltI.htm)|Duck and Weave|Esquive et riposte|officielle|
|[FXTugA9GorPxMpXh.htm](agents-of-edgewatch-bestiary-items/FXTugA9GorPxMpXh.htm)|Designate Apostate|Désigner un apostat|officielle|
|[FY9YGIGBEeh0FNKi.htm](agents-of-edgewatch-bestiary-items/FY9YGIGBEeh0FNKi.htm)|Mother Venom Poison|Poison de Mère Venin|officielle|
|[G406qwAGsa4C7jmR.htm](agents-of-edgewatch-bestiary-items/G406qwAGsa4C7jmR.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[g407ooIFn82B2iXk.htm](agents-of-edgewatch-bestiary-items/g407ooIFn82B2iXk.htm)|Claw|Griffe|officielle|
|[g67xAp632gfQapHl.htm](agents-of-edgewatch-bestiary-items/g67xAp632gfQapHl.htm)|Overwhelming Anguish|Anxiété écrasante|officielle|
|[g8bRqHNpfEqsTYyx.htm](agents-of-edgewatch-bestiary-items/g8bRqHNpfEqsTYyx.htm)|Internal Spell Strike|Frappe magique interne|officielle|
|[G8ORuPOn8J7KCfJc.htm](agents-of-edgewatch-bestiary-items/G8ORuPOn8J7KCfJc.htm)|Prepared Divine Spells|Sorts divins préparés|libre|
|[gCFv5lyk3Wfoe8GB.htm](agents-of-edgewatch-bestiary-items/gCFv5lyk3Wfoe8GB.htm)|Javelin|Javeline|officielle|
|[gczvNivaLBoVgpla.htm](agents-of-edgewatch-bestiary-items/gczvNivaLBoVgpla.htm)|Mirror Jump|Saut de miroir|officielle|
|[GElFkGSpAoSzMnqX.htm](agents-of-edgewatch-bestiary-items/GElFkGSpAoSzMnqX.htm)|Push|Bousculade|officielle|
|[gi71u7E3lwMd8bQT.htm](agents-of-edgewatch-bestiary-items/gi71u7E3lwMd8bQT.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[GjE2VyfYPjLsNFGi.htm](agents-of-edgewatch-bestiary-items/GjE2VyfYPjLsNFGi.htm)|Jaws|Mâchoires|officielle|
|[glyWTuwYyEk8AaC1.htm](agents-of-edgewatch-bestiary-items/glyWTuwYyEk8AaC1.htm)|Dubious Shifting|Changement suspect|officielle|
|[gMEgsUUpGodwiTBG.htm](agents-of-edgewatch-bestiary-items/gMEgsUUpGodwiTBG.htm)|Club|Gourdin|officielle|
|[gMURrHnMDkhqOaJp.htm](agents-of-edgewatch-bestiary-items/gMURrHnMDkhqOaJp.htm)|At-Will Spells|Sorts à volonté|officielle|
|[GneEI6sPR4fF7SgD.htm](agents-of-edgewatch-bestiary-items/GneEI6sPR4fF7SgD.htm)|Low-Light Vision|Vision nocturne|officielle|
|[GnobVyc3rnNPsKdE.htm](agents-of-edgewatch-bestiary-items/GnobVyc3rnNPsKdE.htm)|Grab|Empoignade|officielle|
|[GnvMkeIBg6ZpYXzJ.htm](agents-of-edgewatch-bestiary-items/GnvMkeIBg6ZpYXzJ.htm)|Pseudopod Burst|Salve de pseudopodes|officielle|
|[gPHdCqQsilHM5yrV.htm](agents-of-edgewatch-bestiary-items/gPHdCqQsilHM5yrV.htm)|Improved Grab|Empoignade améliorée|officielle|
|[gpQwZhqK70OriQ2S.htm](agents-of-edgewatch-bestiary-items/gpQwZhqK70OriQ2S.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[Gqv7Fk6y6KrpOB3W.htm](agents-of-edgewatch-bestiary-items/Gqv7Fk6y6KrpOB3W.htm)|Occult Prepared Spells|Sorts occultes préparés|libre|
|[GrbNOULOdg2VBphU.htm](agents-of-edgewatch-bestiary-items/GrbNOULOdg2VBphU.htm)|Sneak Attack|Attaque sournoise|officielle|
|[gTlSQCqmrYTzoJgt.htm](agents-of-edgewatch-bestiary-items/gTlSQCqmrYTzoJgt.htm)|Jaws|Mâchoires|officielle|
|[GtoDWNmX34925xMc.htm](agents-of-edgewatch-bestiary-items/GtoDWNmX34925xMc.htm)|Unbalancing Blow|Coup déséquilibrant|officielle|
|[GTs7sVH9yhH8mKkM.htm](agents-of-edgewatch-bestiary-items/GTs7sVH9yhH8mKkM.htm)|Nonlethal Training|Entraînement non létal|officielle|
|[GtVYqqSXE71FA0xR.htm](agents-of-edgewatch-bestiary-items/GtVYqqSXE71FA0xR.htm)|Thunderstone|Pierre à tonnerre|officielle|
|[GUrHLtuqnl4x3KHH.htm](agents-of-edgewatch-bestiary-items/GUrHLtuqnl4x3KHH.htm)|Burst of Speed|Pointe de vitesse|officielle|
|[GutcPWbxQjMoihFA.htm](agents-of-edgewatch-bestiary-items/GutcPWbxQjMoihFA.htm)|Water Step|Foulée sur l'eau|officielle|
|[Gv8gIlACS3hfoYJF.htm](agents-of-edgewatch-bestiary-items/Gv8gIlACS3hfoYJF.htm)|Greater Holy Blade|Lame sainte supérieure|officielle|
|[GVWSny9n33dg608R.htm](agents-of-edgewatch-bestiary-items/GVWSny9n33dg608R.htm)|Dagger|Dague|officielle|
|[Gy4RoLuOaQU79ESu.htm](agents-of-edgewatch-bestiary-items/Gy4RoLuOaQU79ESu.htm)|Sneak Attack|Attaque sournoise|officielle|
|[gzMwqFLG38aJHUIa.htm](agents-of-edgewatch-bestiary-items/gzMwqFLG38aJHUIa.htm)|Purple Worm Venom|Dard du ver pourpre|officielle|
|[gZWXLF5Vb3kNdm0I.htm](agents-of-edgewatch-bestiary-items/gZWXLF5Vb3kNdm0I.htm)|Focus Spells|Sorts focalisés|libre|
|[H1Aapl7br22RF2Pe.htm](agents-of-edgewatch-bestiary-items/H1Aapl7br22RF2Pe.htm)|Primal Spontaneous Spells|Sorts primordiaux spontanés|libre|
|[H2cjvx6x95qaiQFo.htm](agents-of-edgewatch-bestiary-items/H2cjvx6x95qaiQFo.htm)|Marrow Rot|Pourrissement de la moelle|officielle|
|[h8NOsLMsLkhSoPDm.htm](agents-of-edgewatch-bestiary-items/h8NOsLMsLkhSoPDm.htm)|Change Shape|Changement de forme|officielle|
|[haax0MkiNqA6MN2L.htm](agents-of-edgewatch-bestiary-items/haax0MkiNqA6MN2L.htm)|Rallying Call|Appel de ralliement|officielle|
|[HaPAHwncLeNsBABj.htm](agents-of-edgewatch-bestiary-items/HaPAHwncLeNsBABj.htm)|Acid Spit|Projection d'acide|officielle|
|[HAvR3sxXnbRxAmUE.htm](agents-of-edgewatch-bestiary-items/HAvR3sxXnbRxAmUE.htm)|Claw|Griffe|officielle|
|[hbg1FrfQIQ6EAWuz.htm](agents-of-edgewatch-bestiary-items/hbg1FrfQIQ6EAWuz.htm)|Opportune Backstab|Coup de poignard opportuniste|officielle|
|[HCuDpfMGf5F5xT6L.htm](agents-of-edgewatch-bestiary-items/HCuDpfMGf5F5xT6L.htm)|At-Will Spells|Sorts à volonté|officielle|
|[hDbNl3kiEPn8AUos.htm](agents-of-edgewatch-bestiary-items/hDbNl3kiEPn8AUos.htm)|Capsize|Chavirer|officielle|
|[HeE0E8xiY1o4FYiX.htm](agents-of-edgewatch-bestiary-items/HeE0E8xiY1o4FYiX.htm)|Dagger|Dague|officielle|
|[HePUOrJQdIAXDxrv.htm](agents-of-edgewatch-bestiary-items/HePUOrJQdIAXDxrv.htm)|Gang Lore|Connaissance des gangs|officielle|
|[HffwSLkW7mUAJAlB.htm](agents-of-edgewatch-bestiary-items/HffwSLkW7mUAJAlB.htm)|Wing|Aile|officielle|
|[HGIf9bu9E1RjDKaF.htm](agents-of-edgewatch-bestiary-items/HGIf9bu9E1RjDKaF.htm)|Dominate|Domination|officielle|
|[HhajioTT4qQe67v3.htm](agents-of-edgewatch-bestiary-items/HhajioTT4qQe67v3.htm)|Improved Grab|Empoignade améliorée|officielle|
|[HHDLYWqh0LHr49Uu.htm](agents-of-edgewatch-bestiary-items/HHDLYWqh0LHr49Uu.htm)|Stitch Skin|Coudre la peau|officielle|
|[hhg7o2qV6JntAswy.htm](agents-of-edgewatch-bestiary-items/hhg7o2qV6JntAswy.htm)|+2 Status Bonus on Saves vs. Trip|+2 de statut aux JdS contre Croc-en-jambe|officielle|
|[HIbGLn4unRGk8EZL.htm](agents-of-edgewatch-bestiary-items/HIbGLn4unRGk8EZL.htm)|Dart Volley|Volée de fléchettes|officielle|
|[hJJc012AkwlepsfP.htm](agents-of-edgewatch-bestiary-items/hJJc012AkwlepsfP.htm)|Odorless|Inodore|officielle|
|[HjL44xV6BvZNBNH8.htm](agents-of-edgewatch-bestiary-items/HjL44xV6BvZNBNH8.htm)|Frost Vial|Bombe de froid|officielle|
|[HLhPjN1be3uh5emL.htm](agents-of-edgewatch-bestiary-items/HLhPjN1be3uh5emL.htm)|Flurry of Blows|Déluge de coups|officielle|
|[HLLn9CHqmRrWk4IN.htm](agents-of-edgewatch-bestiary-items/HLLn9CHqmRrWk4IN.htm)|Nimble Dodge|Esquive agile|officielle|
|[HMCS1B66LSvBKspC.htm](agents-of-edgewatch-bestiary-items/HMCS1B66LSvBKspC.htm)|Splatter|Gerbe|officielle|
|[hnspRHRc7JzzGcNY.htm](agents-of-edgewatch-bestiary-items/hnspRHRc7JzzGcNY.htm)|Knockdown|Renversement|officielle|
|[HNYjS5WAb54frbpw.htm](agents-of-edgewatch-bestiary-items/HNYjS5WAb54frbpw.htm)|Charming Liar|Menteur charismatique|officielle|
|[hoABDC4bNqLsgmbp.htm](agents-of-edgewatch-bestiary-items/hoABDC4bNqLsgmbp.htm)|Dagger|Dague|officielle|
|[hozDdAMhO9qf2Buo.htm](agents-of-edgewatch-bestiary-items/hozDdAMhO9qf2Buo.htm)|Decapitate|Décapitation|officielle|
|[Hq1vagZwNS6eDSNA.htm](agents-of-edgewatch-bestiary-items/Hq1vagZwNS6eDSNA.htm)|Tainted Backlash|Contrecoup corrompu|officielle|
|[hrbhUgMW0KZvWovb.htm](agents-of-edgewatch-bestiary-items/hrbhUgMW0KZvWovb.htm)|Katar Specialist|Spécialiste du katar|officielle|
|[HrV9IU9zNImvgkv7.htm](agents-of-edgewatch-bestiary-items/HrV9IU9zNImvgkv7.htm)|Grab|Empoignade|officielle|
|[hScPOOUe2XVGeyIa.htm](agents-of-edgewatch-bestiary-items/hScPOOUe2XVGeyIa.htm)|Bronze Fist|Poing en bronze|officielle|
|[Ht4guo51Eky0gFrz.htm](agents-of-edgewatch-bestiary-items/Ht4guo51Eky0gFrz.htm)|Partial Amphibian|Moitié-amphibie|officielle|
|[hup5Hnb2EZPEeNg7.htm](agents-of-edgewatch-bestiary-items/hup5Hnb2EZPEeNg7.htm)|Darkvision|Vision dans le noir|officielle|
|[hvjSsYJt6z0pbDL0.htm](agents-of-edgewatch-bestiary-items/hvjSsYJt6z0pbDL0.htm)|Mandibles|Mandibules|officielle|
|[hvsbuonawLt5vOLp.htm](agents-of-edgewatch-bestiary-items/hvsbuonawLt5vOLp.htm)|Brutish Shove|Poussée brutale|officielle|
|[hwDOgihBpaGyHO9Q.htm](agents-of-edgewatch-bestiary-items/hwDOgihBpaGyHO9Q.htm)|Dagger|Dague|officielle|
|[hxG5VYuZdf33T05P.htm](agents-of-edgewatch-bestiary-items/hxG5VYuZdf33T05P.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[HxSPrzqufbKb8gOo.htm](agents-of-edgewatch-bestiary-items/HxSPrzqufbKb8gOo.htm)|Negative Healing|Soins négatifs|officielle|
|[hytdhHAVEMZ2IlnQ.htm](agents-of-edgewatch-bestiary-items/hytdhHAVEMZ2IlnQ.htm)|Claws|Griffes|officielle|
|[hzLoXfk0XZ09nziz.htm](agents-of-edgewatch-bestiary-items/hzLoXfk0XZ09nziz.htm)|Terrifying Gaze|Regard terrifiant|officielle|
|[i0POO0dwNFDOcYuV.htm](agents-of-edgewatch-bestiary-items/i0POO0dwNFDOcYuV.htm)|Summon Devil|Convocation de diable|officielle|
|[i1fagw7VusZxwIdz.htm](agents-of-edgewatch-bestiary-items/i1fagw7VusZxwIdz.htm)|+1 Status to All Saves vs. Magic|+1 de statut aux JdS contre la magie|officielle|
|[I23rYMszuvKL97k7.htm](agents-of-edgewatch-bestiary-items/I23rYMszuvKL97k7.htm)|Protean Anatomy|Anatomie protéenne|officielle|
|[I5UOcgBhEV16sOvv.htm](agents-of-edgewatch-bestiary-items/I5UOcgBhEV16sOvv.htm)|Studied Strike|Frappe réfléchie|officielle|
|[I7zUPwVMtrP17lu5.htm](agents-of-edgewatch-bestiary-items/I7zUPwVMtrP17lu5.htm)|Eschew Materials|Dispense de composant matériel|officielle|
|[IBTLrjDIdwriv1Sh.htm](agents-of-edgewatch-bestiary-items/IBTLrjDIdwriv1Sh.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[iccpDuEAwMrtLyBh.htm](agents-of-edgewatch-bestiary-items/iccpDuEAwMrtLyBh.htm)|Wire Catch|Fil de saisie|officielle|
|[IcNV9BTE0VD3ZNXy.htm](agents-of-edgewatch-bestiary-items/IcNV9BTE0VD3ZNXy.htm)|Intimidating Stare|Regard intimidant|officielle|
|[IDckQwFfttdnib8W.htm](agents-of-edgewatch-bestiary-items/IDckQwFfttdnib8W.htm)|Poisoned Dart|Fléchette empoisonnée|officielle|
|[Idet44bdRqy8cmfx.htm](agents-of-edgewatch-bestiary-items/Idet44bdRqy8cmfx.htm)|Dagger|Dague|officielle|
|[IdUgaBFlawtCWK5m.htm](agents-of-edgewatch-bestiary-items/IdUgaBFlawtCWK5m.htm)|Constant Spells|Sorts constants|officielle|
|[IEuErW2Q6KaGlk21.htm](agents-of-edgewatch-bestiary-items/IEuErW2Q6KaGlk21.htm)|Hook and Flay|Accrocher et écorcher|officielle|
|[If0QHT3SwMga7arR.htm](agents-of-edgewatch-bestiary-items/If0QHT3SwMga7arR.htm)|Darkvision|Vision dans le noir|officielle|
|[IfCbrxEtWCoJCOgv.htm](agents-of-edgewatch-bestiary-items/IfCbrxEtWCoJCOgv.htm)|Longsword|Épée longue|officielle|
|[iFX6Z8UsTWukjUxp.htm](agents-of-edgewatch-bestiary-items/iFX6Z8UsTWukjUxp.htm)|Quick Alchemy|Alchimie rapide|officielle|
|[iG9uOtBJolj3P4CM.htm](agents-of-edgewatch-bestiary-items/iG9uOtBJolj3P4CM.htm)|Spew Cloud|Expulsion de gaz|officielle|
|[igIwGgzOBvjtlLdx.htm](agents-of-edgewatch-bestiary-items/igIwGgzOBvjtlLdx.htm)|Transparent|Transparent|officielle|
|[IH1SiADzFCDakX8w.htm](agents-of-edgewatch-bestiary-items/IH1SiADzFCDakX8w.htm)|Out You Go|Sortez de là|officielle|
|[II1iKHEPt0qdurXn.htm](agents-of-edgewatch-bestiary-items/II1iKHEPt0qdurXn.htm)|Plunder Life|Extorsion de vie|officielle|
|[IiO8ytUL6tJgUAXR.htm](agents-of-edgewatch-bestiary-items/IiO8ytUL6tJgUAXR.htm)|Vargouille Venom|Venin de vargouille|officielle|
|[iJUSlDT7ZVFtjNSy.htm](agents-of-edgewatch-bestiary-items/iJUSlDT7ZVFtjNSy.htm)|Spider Swarm Host|Porteuse d'une nuée d'araignées|officielle|
|[Ikix1zKqDpYhzRYA.htm](agents-of-edgewatch-bestiary-items/Ikix1zKqDpYhzRYA.htm)|Infector|Infector|officielle|
|[IKQ2kDBIPfAILZjf.htm](agents-of-edgewatch-bestiary-items/IKQ2kDBIPfAILZjf.htm)|Alchemical Bomb|Bombe alchimique|officielle|
|[ikVC2siqOrYTYZtS.htm](agents-of-edgewatch-bestiary-items/ikVC2siqOrYTYZtS.htm)|Second Chance|Deuxième chance|officielle|
|[Iky2gPwt5DqOgbkl.htm](agents-of-edgewatch-bestiary-items/Iky2gPwt5DqOgbkl.htm)|Push 10 feet|Bousculade 3 m|officielle|
|[IlMxWLhPQQrxWb4s.htm](agents-of-edgewatch-bestiary-items/IlMxWLhPQQrxWb4s.htm)|Wind|Remonter|officielle|
|[INFGwX7PUOvEy0iD.htm](agents-of-edgewatch-bestiary-items/INFGwX7PUOvEy0iD.htm)|Trail of Flame|Traînée de feu|officielle|
|[iNUgfYO7f8c2RdLm.htm](agents-of-edgewatch-bestiary-items/iNUgfYO7f8c2RdLm.htm)|Rend (jaws)|Éventration (mâchoires)|officielle|
|[iOt4Y0EmIGeBhl6u.htm](agents-of-edgewatch-bestiary-items/iOt4Y0EmIGeBhl6u.htm)|Darkvision|Vision dans le noir|officielle|
|[iprvaUuVaBlST8Su.htm](agents-of-edgewatch-bestiary-items/iprvaUuVaBlST8Su.htm)|Low-Light Vision|Vision nocturne|officielle|
|[iqSjrlDup1I8i5VX.htm](agents-of-edgewatch-bestiary-items/iqSjrlDup1I8i5VX.htm)|Bloody Feet|Pied sanglant|officielle|
|[ISXykb5wK1DoaIOw.htm](agents-of-edgewatch-bestiary-items/ISXykb5wK1DoaIOw.htm)|Terrain Advantage|Maîtrise du terrain|officielle|
|[ITQk9n2tp7J5FDQa.htm](agents-of-edgewatch-bestiary-items/ITQk9n2tp7J5FDQa.htm)|Change Shape|Changement de forme|officielle|
|[iUGSF5wsY0hhEr0g.htm](agents-of-edgewatch-bestiary-items/iUGSF5wsY0hhEr0g.htm)|Grab|Empoignade|officielle|
|[iV2kvaEO0RYmWI8X.htm](agents-of-edgewatch-bestiary-items/iV2kvaEO0RYmWI8X.htm)|Blade|Lame|officielle|
|[IVbYmIS1u62AEFZm.htm](agents-of-edgewatch-bestiary-items/IVbYmIS1u62AEFZm.htm)|Scalding Spray|Jet brûlant|officielle|
|[iYdGVR75KvoZWFO3.htm](agents-of-edgewatch-bestiary-items/iYdGVR75KvoZWFO3.htm)|Toxic Mastery|Maîtrise toxique|officielle|
|[IZqqliLOZ1AtFOJF.htm](agents-of-edgewatch-bestiary-items/IZqqliLOZ1AtFOJF.htm)|Sap|Matraque|officielle|
|[j2lpHYGglR2Lc1Ut.htm](agents-of-edgewatch-bestiary-items/j2lpHYGglR2Lc1Ut.htm)|Darkvision|Vision dans le noir|officielle|
|[J6CumHMGaEuUCyrp.htm](agents-of-edgewatch-bestiary-items/J6CumHMGaEuUCyrp.htm)|Open Hatch|Ouvrir la trappe|officielle|
|[j72OO3UZ0KrTLLO6.htm](agents-of-edgewatch-bestiary-items/j72OO3UZ0KrTLLO6.htm)|Ghoul Fever|Fièvre des goules|officielle|
|[j7j4zEpOdEbRtRuk.htm](agents-of-edgewatch-bestiary-items/j7j4zEpOdEbRtRuk.htm)|+1 Status to All Saves vs. Magic|+1 de statut aux JdS contre la magie|officielle|
|[j7oe7252bXGLrvgk.htm](agents-of-edgewatch-bestiary-items/j7oe7252bXGLrvgk.htm)|Reaper's Lancet Blade|Lame du Bistouri de l'éventreur|officielle|
|[jC7yNSXPp6Y4dqOF.htm](agents-of-edgewatch-bestiary-items/jC7yNSXPp6Y4dqOF.htm)|Nightmare Assault|Assaut cauchemardesque|officielle|
|[jcfMZ2vEUL2nEfFQ.htm](agents-of-edgewatch-bestiary-items/jcfMZ2vEUL2nEfFQ.htm)|Surreptitious Siege|Siège furtif|officielle|
|[JCwAfoQqJDmPTVLF.htm](agents-of-edgewatch-bestiary-items/JCwAfoQqJDmPTVLF.htm)|Crystal Sense (Imprecise) 30 feet|Perception des cristaux (imprécis) 9 m|officielle|
|[jDE564VbgKyhi1ch.htm](agents-of-edgewatch-bestiary-items/jDE564VbgKyhi1ch.htm)|+1 Status to All Saves vs. Magic|+1 de statut aux JdS contre la magie|officielle|
|[jfhtS3mRJ8Vf1K2F.htm](agents-of-edgewatch-bestiary-items/jfhtS3mRJ8Vf1K2F.htm)|Longsword|Épée longue|officielle|
|[jgogVBpbS3zZv6XG.htm](agents-of-edgewatch-bestiary-items/jgogVBpbS3zZv6XG.htm)|Rapier|Rapière|officielle|
|[JHDUCoAwvNv120KU.htm](agents-of-edgewatch-bestiary-items/JHDUCoAwvNv120KU.htm)|At-Will Spells|Sorts à volonté|officielle|
|[jirCmoCjYEZm147H.htm](agents-of-edgewatch-bestiary-items/jirCmoCjYEZm147H.htm)|Darkvision|Vision dans le noir|officielle|
|[JJiuY4l7O3Ujlg8S.htm](agents-of-edgewatch-bestiary-items/JJiuY4l7O3Ujlg8S.htm)|Speed|Vitesse|officielle|
|[JJluFLD9A2iKiNi4.htm](agents-of-edgewatch-bestiary-items/JJluFLD9A2iKiNi4.htm)|Double Punch|Double coup de poing|officielle|
|[jJSKOhaRoCp1WwP4.htm](agents-of-edgewatch-bestiary-items/jJSKOhaRoCp1WwP4.htm)|Inhabit Vessel|Possession du réceptacle|officielle|
|[jkHMXDAED128Pydk.htm](agents-of-edgewatch-bestiary-items/jkHMXDAED128Pydk.htm)|Heavy Crossbow|Arbalète lourde|officielle|
|[Jkps95l9DKtpNrr8.htm](agents-of-edgewatch-bestiary-items/Jkps95l9DKtpNrr8.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[JNMs0qEBFo57y0tr.htm](agents-of-edgewatch-bestiary-items/JNMs0qEBFo57y0tr.htm)|Rend|Éventration|officielle|
|[JnTr0G2JvwVYzdfR.htm](agents-of-edgewatch-bestiary-items/JnTr0G2JvwVYzdfR.htm)|Detaining Strike|Frappe de détention|officielle|
|[jq9NuOTE6q7jHwO1.htm](agents-of-edgewatch-bestiary-items/jq9NuOTE6q7jHwO1.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[JqbE9PdaDo8dWRhu.htm](agents-of-edgewatch-bestiary-items/JqbE9PdaDo8dWRhu.htm)|Jaws|Mâchoires|officielle|
|[JQLaH8N9rTPhEzqj.htm](agents-of-edgewatch-bestiary-items/JQLaH8N9rTPhEzqj.htm)|Sword Cane Duelist|Duelliste à la canne épée|officielle|
|[JRKYJD9kEhVvnpPF.htm](agents-of-edgewatch-bestiary-items/JRKYJD9kEhVvnpPF.htm)|Jaws|Mâchoires|officielle|
|[JsmcVomHbSSmqqvS.htm](agents-of-edgewatch-bestiary-items/JsmcVomHbSSmqqvS.htm)|Absalom Lore|Connaissance d'Absalom|officielle|
|[JUNDTV3wwrM9gFjE.htm](agents-of-edgewatch-bestiary-items/JUNDTV3wwrM9gFjE.htm)|Crossbow|Arbalète|officielle|
|[JXdEhHs5I4scpvbs.htm](agents-of-edgewatch-bestiary-items/JXdEhHs5I4scpvbs.htm)|Shortsword|Épée courte|officielle|
|[JXIun04dusvrOS6n.htm](agents-of-edgewatch-bestiary-items/JXIun04dusvrOS6n.htm)|Kiss of the Speakers|Baiser des Orateurs|officielle|
|[jybYCirNRpxTm5fl.htm](agents-of-edgewatch-bestiary-items/jybYCirNRpxTm5fl.htm)|Fist|Poing|officielle|
|[jzOEMi9hAVAe1K1V.htm](agents-of-edgewatch-bestiary-items/jzOEMi9hAVAe1K1V.htm)|Poisoned Needle|Aiguille empoisonnée|officielle|
|[jZQm4Bhn7fdp3YRx.htm](agents-of-edgewatch-bestiary-items/jZQm4Bhn7fdp3YRx.htm)|+1 Status to All Saves vs. Magic|+1 de statut aux JdS contre la magie|officielle|
|[k0BFzCiBWr8fRZ4m.htm](agents-of-edgewatch-bestiary-items/k0BFzCiBWr8fRZ4m.htm)|Obscuring Grab|Masqué en empoignant|officielle|
|[k1KchqR9i8uSyT6e.htm](agents-of-edgewatch-bestiary-items/k1KchqR9i8uSyT6e.htm)|Combustible|Combustible|officielle|
|[k1m2BVdO1iqRouch.htm](agents-of-edgewatch-bestiary-items/k1m2BVdO1iqRouch.htm)|Emotion Sense 120 feet|Perception des émotions 36 m|officielle|
|[K287MomJCMfPXPLf.htm](agents-of-edgewatch-bestiary-items/K287MomJCMfPXPLf.htm)|Smoke Vision|Vision malgré la fumée|officielle|
|[K2NRmCiVWR4uMsRg.htm](agents-of-edgewatch-bestiary-items/K2NRmCiVWR4uMsRg.htm)|Vanish in Reflections|Volatilisation des reflets|officielle|
|[K2xZ73LiKk2uqR28.htm](agents-of-edgewatch-bestiary-items/K2xZ73LiKk2uqR28.htm)|Blackfinger's Prayer|Prière de Noirs Doigts|officielle|
|[K4f9RbGiZucMUGD8.htm](agents-of-edgewatch-bestiary-items/K4f9RbGiZucMUGD8.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[K68kyyTQ4cOmTtUk.htm](agents-of-edgewatch-bestiary-items/K68kyyTQ4cOmTtUk.htm)|Mind-Numbing Grasp|Poigne abrutissante|officielle|
|[k6q40GuzzYWifu3y.htm](agents-of-edgewatch-bestiary-items/k6q40GuzzYWifu3y.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[K7CFCrn5nlo3dzku.htm](agents-of-edgewatch-bestiary-items/K7CFCrn5nlo3dzku.htm)|Scourge|Chat à neuf queues|officielle|
|[kckDvYIQBBGcjLNo.htm](agents-of-edgewatch-bestiary-items/kckDvYIQBBGcjLNo.htm)|Scimitar|Cimeterre|officielle|
|[kcrnWNWqaiPR3tg9.htm](agents-of-edgewatch-bestiary-items/kcrnWNWqaiPR3tg9.htm)|Arcane Spontaneous Spells|Sorts arcaniques spontanés|libre|
|[KcYjsnv1Da60rZqp.htm](agents-of-edgewatch-bestiary-items/KcYjsnv1Da60rZqp.htm)|Low-Light Vision|Vision nocturne|officielle|
|[kEiDcm42Prsww2ja.htm](agents-of-edgewatch-bestiary-items/kEiDcm42Prsww2ja.htm)|Alchemical Reaction|Réaction alchimique|officielle|
|[kELQNLDmBgZ4uIU5.htm](agents-of-edgewatch-bestiary-items/kELQNLDmBgZ4uIU5.htm)|Improved Grab|Empoignade améliorée|officielle|
|[KeuEtlVRXnSoTjPo.htm](agents-of-edgewatch-bestiary-items/KeuEtlVRXnSoTjPo.htm)|Darkvision|Vision dans le noir|officielle|
|[kfBkomhbVA2SgJJO.htm](agents-of-edgewatch-bestiary-items/kfBkomhbVA2SgJJO.htm)|Athletics|Athlétisme|officielle|
|[KFdVbNhK30Xo6GCp.htm](agents-of-edgewatch-bestiary-items/KFdVbNhK30Xo6GCp.htm)|Ooze Tendril|Vrille de mucus|officielle|
|[Kfw3xJtFTIHlmDoM.htm](agents-of-edgewatch-bestiary-items/Kfw3xJtFTIHlmDoM.htm)|Improved Grab|Empoignade améliorée|officielle|
|[kfZVz80JupwQ5DE1.htm](agents-of-edgewatch-bestiary-items/kfZVz80JupwQ5DE1.htm)|Bloody Chain Aura|Aura de chaîne sanglante|officielle|
|[kIPI4tNPGahloPyK.htm](agents-of-edgewatch-bestiary-items/kIPI4tNPGahloPyK.htm)|Darkvision|Vision dans le noir|officielle|
|[kK15wEyRUz3vGzjl.htm](agents-of-edgewatch-bestiary-items/kK15wEyRUz3vGzjl.htm)|+1 Status to All Saves vs. Magic|+1 de statut aux JdS contre la magie|officielle|
|[KkQu3TqvmvRwh5Gx.htm](agents-of-edgewatch-bestiary-items/KkQu3TqvmvRwh5Gx.htm)|Crush of Hundreds|Foule écrasante|officielle|
|[kMMwsDVDDuFjQhm3.htm](agents-of-edgewatch-bestiary-items/kMMwsDVDDuFjQhm3.htm)|Fist|Poing|officielle|
|[KOfo0zVfgBhhWGjT.htm](agents-of-edgewatch-bestiary-items/KOfo0zVfgBhhWGjT.htm)|Quill Cannon|Lance-piquants|officielle|
|[Kp4jOhCQVcuNVP2j.htm](agents-of-edgewatch-bestiary-items/Kp4jOhCQVcuNVP2j.htm)|Vital Transfusion|Transfusion vitale|officielle|
|[kPfYHs4PPwutz153.htm](agents-of-edgewatch-bestiary-items/kPfYHs4PPwutz153.htm)|Grabbing Trunk|Empoignade à la trompe|officielle|
|[krn8dqAmFwPEmo0O.htm](agents-of-edgewatch-bestiary-items/krn8dqAmFwPEmo0O.htm)|Darkvision|Vision dans le noir|officielle|
|[KRYmDgL0v3sgSPhE.htm](agents-of-edgewatch-bestiary-items/KRYmDgL0v3sgSPhE.htm)|Explosion|Explosion|officielle|
|[kS3RkK66eSUF1gOu.htm](agents-of-edgewatch-bestiary-items/kS3RkK66eSUF1gOu.htm)|Tail|Queue|officielle|
|[kspfnXbpzKtPxBp2.htm](agents-of-edgewatch-bestiary-items/kspfnXbpzKtPxBp2.htm)|Darkvision|Vision dans le noir|officielle|
|[kVdw249CmzAbj17U.htm](agents-of-edgewatch-bestiary-items/kVdw249CmzAbj17U.htm)|Riptide|Vers le fond|officielle|
|[KWEh7hh4al6ViDpu.htm](agents-of-edgewatch-bestiary-items/KWEh7hh4al6ViDpu.htm)|Heavy Crossbow|Arbalète lourde|officielle|
|[KZ4Ixv66QE0EQIct.htm](agents-of-edgewatch-bestiary-items/KZ4Ixv66QE0EQIct.htm)|Pseudopod|Pseudopode|officielle|
|[kzNiR7bLBxL4aWfR.htm](agents-of-edgewatch-bestiary-items/kzNiR7bLBxL4aWfR.htm)|Quick Draw|Arme en main|officielle|
|[kZPMz3gjTr4SFKIs.htm](agents-of-edgewatch-bestiary-items/kZPMz3gjTr4SFKIs.htm)|Darkvision|Vision dans le noir|officielle|
|[l5qCiwvxOStA4H26.htm](agents-of-edgewatch-bestiary-items/l5qCiwvxOStA4H26.htm)|Claw|Griffe|officielle|
|[l5xkjpvgvFHWi6Yj.htm](agents-of-edgewatch-bestiary-items/l5xkjpvgvFHWi6Yj.htm)|Innate Divine Spells|Sorts divins innés|libre|
|[l7wT4LDdfJXJETHf.htm](agents-of-edgewatch-bestiary-items/l7wT4LDdfJXJETHf.htm)|Clockwork Brain|Cerveau mécanique|officielle|
|[LaDAOhsZJlBZ3eiZ.htm](agents-of-edgewatch-bestiary-items/LaDAOhsZJlBZ3eiZ.htm)|Shortsword|Épée courte|officielle|
|[laNWVOBt5DoTHn3G.htm](agents-of-edgewatch-bestiary-items/laNWVOBt5DoTHn3G.htm)|Warpwave Strike|Frappe de vagues de distorsion|officielle|
|[LBQk96wXoxYrTUZi.htm](agents-of-edgewatch-bestiary-items/LBQk96wXoxYrTUZi.htm)|Distort Magic|Déformation de la magie|officielle|
|[LCEc1hLLB843Ltt3.htm](agents-of-edgewatch-bestiary-items/LCEc1hLLB843Ltt3.htm)|Fist|Poing|officielle|
|[LCKRgv7IeeWygjY3.htm](agents-of-edgewatch-bestiary-items/LCKRgv7IeeWygjY3.htm)|Spirit Blast Ray|Rayon de coup spirituel|officielle|
|[LFIJhcEKJhKijVm1.htm](agents-of-edgewatch-bestiary-items/LFIJhcEKJhKijVm1.htm)|Shock|Foudre|officielle|
|[lftFJteRXqRBmg0d.htm](agents-of-edgewatch-bestiary-items/lftFJteRXqRBmg0d.htm)|Telepathy 100 feet|Télépathie à 30 mètres|officielle|
|[lHgISCDkIa7T34nH.htm](agents-of-edgewatch-bestiary-items/lHgISCDkIa7T34nH.htm)|Spawn Mirror Duplicate|Engendrer un double|officielle|
|[LiFDw1yyHumRLJFc.htm](agents-of-edgewatch-bestiary-items/LiFDw1yyHumRLJFc.htm)|Grab|Empoignade|officielle|
|[lJYPy9JbnZy8cvV0.htm](agents-of-edgewatch-bestiary-items/lJYPy9JbnZy8cvV0.htm)|Dagger|Dague|officielle|
|[lMrRe942X6FotrEf.htm](agents-of-edgewatch-bestiary-items/lMrRe942X6FotrEf.htm)|Darkvision|Vision dans le noir|officielle|
|[lmVllcsiFHe8ybRy.htm](agents-of-edgewatch-bestiary-items/lmVllcsiFHe8ybRy.htm)|Wretched Weeps|Suintements effroyables|officielle|
|[LnwbIbIzWsV3rQvP.htm](agents-of-edgewatch-bestiary-items/LnwbIbIzWsV3rQvP.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[loHKBZQtnioHzbFm.htm](agents-of-edgewatch-bestiary-items/loHKBZQtnioHzbFm.htm)|Quick Bomber|Artificier rapide|officielle|
|[LPMvePNhEQEvIeA1.htm](agents-of-edgewatch-bestiary-items/LPMvePNhEQEvIeA1.htm)|Alchemical Injection|Injection alchimique|officielle|
|[lpqsopfJ3k9tHtel.htm](agents-of-edgewatch-bestiary-items/lpqsopfJ3k9tHtel.htm)|Innate Divine Spells|Sorts divins innés|libre|
|[LQEhpFQTxwqQ8b2q.htm](agents-of-edgewatch-bestiary-items/LQEhpFQTxwqQ8b2q.htm)|Deflective Dodge|Esquive déflectrice|officielle|
|[LveVTPzs4hEN09k6.htm](agents-of-edgewatch-bestiary-items/LveVTPzs4hEN09k6.htm)|Darkvision|Vision dans le noir|officielle|
|[LvmDMsbTpm0IyK9u.htm](agents-of-edgewatch-bestiary-items/LvmDMsbTpm0IyK9u.htm)|Fast Swallow|Gober rapidement|officielle|
|[lVTqn24kqmGrjWJI.htm](agents-of-edgewatch-bestiary-items/lVTqn24kqmGrjWJI.htm)|Spit|Crachat|officielle|
|[lVvJa2dDy1MeSjQ1.htm](agents-of-edgewatch-bestiary-items/lVvJa2dDy1MeSjQ1.htm)|Splash Poison|Projection empoisonnée|officielle|
|[LvYtP53GFOEynRGV.htm](agents-of-edgewatch-bestiary-items/LvYtP53GFOEynRGV.htm)|Skip Between|Insinuation|officielle|
|[LWnz2t8s48aN2oK3.htm](agents-of-edgewatch-bestiary-items/LWnz2t8s48aN2oK3.htm)|Harrow Card|Carte du jeu du tourment|officielle|
|[LxrfittzDR3SPd3W.htm](agents-of-edgewatch-bestiary-items/LxrfittzDR3SPd3W.htm)|Fill Lungs|Oedème pulmonaire|officielle|
|[LYl3vW92ci5HFVXk.htm](agents-of-edgewatch-bestiary-items/LYl3vW92ci5HFVXk.htm)|Alchemical Chambers|Compartiments alchimiques|officielle|
|[LyRSaBlkqwNMCaQT.htm](agents-of-edgewatch-bestiary-items/LyRSaBlkqwNMCaQT.htm)|Slam|Claquement|officielle|
|[lYvWF5v5Vah9KWsN.htm](agents-of-edgewatch-bestiary-items/lYvWF5v5Vah9KWsN.htm)|Golem Antimagic|Antimagie du golem|officielle|
|[LzeE4Oi1C9zb69rd.htm](agents-of-edgewatch-bestiary-items/LzeE4Oi1C9zb69rd.htm)|Darkvision|Vision dans le noir|officielle|
|[LzNIxnRUBN5s28I4.htm](agents-of-edgewatch-bestiary-items/LzNIxnRUBN5s28I4.htm)|Darkvision|Vision dans le noir|officielle|
|[lZznKQkFobPXBxDT.htm](agents-of-edgewatch-bestiary-items/lZznKQkFobPXBxDT.htm)|Warpwave Strike|Frappe de vagues de distorsion|officielle|
|[M0jhr5pNpF64FnCK.htm](agents-of-edgewatch-bestiary-items/M0jhr5pNpF64FnCK.htm)|Legal Lore|Connaissance juridique|officielle|
|[M0P9lsWNp222StTi.htm](agents-of-edgewatch-bestiary-items/M0P9lsWNp222StTi.htm)|Pseudopod|Pseudopode|officielle|
|[M0y7VtVeytggUvVq.htm](agents-of-edgewatch-bestiary-items/M0y7VtVeytggUvVq.htm)|Jaws|Mâchoires|officielle|
|[M2i1Ktvj26SGDm7s.htm](agents-of-edgewatch-bestiary-items/M2i1Ktvj26SGDm7s.htm)|+1 Status to All Saves vs. Magic|+1 de statut aux JdS contre la magie|officielle|
|[m6L9nqkoxL1pP5V6.htm](agents-of-edgewatch-bestiary-items/m6L9nqkoxL1pP5V6.htm)|Attack of Opportunity (Trip Only)|Attaque d'opportunité (Croc-en-jambe uniquement)|officielle|
|[M7E6dxLskMLaX43G.htm](agents-of-edgewatch-bestiary-items/M7E6dxLskMLaX43G.htm)|Dimensional Pit|Fosse dimensionnelle|officielle|
|[m81fVuHWpnJwd8cc.htm](agents-of-edgewatch-bestiary-items/m81fVuHWpnJwd8cc.htm)|At-Will Spells|Sorts à volonté|officielle|
|[M9ciwVyPg7uZSM44.htm](agents-of-edgewatch-bestiary-items/M9ciwVyPg7uZSM44.htm)|Web|Toile|officielle|
|[Ma9MtDSiauZ5onNI.htm](agents-of-edgewatch-bestiary-items/Ma9MtDSiauZ5onNI.htm)|Nimble Swim|Nage rapide|officielle|
|[mBGuEJPcBGcngTxE.htm](agents-of-edgewatch-bestiary-items/mBGuEJPcBGcngTxE.htm)|Horn|Corne|officielle|
|[MClpWaT5c8jTmcFK.htm](agents-of-edgewatch-bestiary-items/MClpWaT5c8jTmcFK.htm)|Flush|Élimination|officielle|
|[McLV2Lx51f0UF1YX.htm](agents-of-edgewatch-bestiary-items/McLV2Lx51f0UF1YX.htm)|Poison Ink|Encre empoisonnée|officielle|
|[mdqTsawy6o4sNLqp.htm](agents-of-edgewatch-bestiary-items/mdqTsawy6o4sNLqp.htm)|Nimble Dodge|Esquive agile|officielle|
|[mDzfLBnuMo5KYFSr.htm](agents-of-edgewatch-bestiary-items/mDzfLBnuMo5KYFSr.htm)|Cast Down|Incantation renversante|officielle|
|[mHKZKkSzSU0RL4MI.htm](agents-of-edgewatch-bestiary-items/mHKZKkSzSU0RL4MI.htm)|Sap|Matraque|officielle|
|[MIGvbijvRTovVpJE.htm](agents-of-edgewatch-bestiary-items/MIGvbijvRTovVpJE.htm)|Vomit Blood|Vomir du sang|officielle|
|[mIVZhiAlmqc8WWY7.htm](agents-of-edgewatch-bestiary-items/mIVZhiAlmqc8WWY7.htm)|Flay|Écorcher|officielle|
|[mjEOGwxDnDNV6VB0.htm](agents-of-edgewatch-bestiary-items/mjEOGwxDnDNV6VB0.htm)|Swarming|Pullulement|officielle|
|[mk8H61X7q0MpYFgq.htm](agents-of-edgewatch-bestiary-items/mk8H61X7q0MpYFgq.htm)|Prepared Divine Spells|Sorts divins préparés|libre|
|[Mlm12XchuKW2Zkqk.htm](agents-of-edgewatch-bestiary-items/Mlm12XchuKW2Zkqk.htm)|Lifesense 30 feet|Perception de la vie 9 m|officielle|
|[mmdbAeOOVVjLALCx.htm](agents-of-edgewatch-bestiary-items/mmdbAeOOVVjLALCx.htm)|Rapid Repair|Réparation rapide|officielle|
|[mNgkZuWHNH6FJsSb.htm](agents-of-edgewatch-bestiary-items/mNgkZuWHNH6FJsSb.htm)|Trickster's Ace|Atout du trompeur|officielle|
|[mOEFev3A3LvUINAz.htm](agents-of-edgewatch-bestiary-items/mOEFev3A3LvUINAz.htm)|Deep Breath|Inspiration profonde|officielle|
|[mOEnnV4W8qmCRdYl.htm](agents-of-edgewatch-bestiary-items/mOEnnV4W8qmCRdYl.htm)|Claw|Griffe|officielle|
|[MpraNid9IzBbGn7L.htm](agents-of-edgewatch-bestiary-items/MpraNid9IzBbGn7L.htm)|Dagger|Dague|officielle|
|[MQ2Rp3l2BaBlZCiE.htm](agents-of-edgewatch-bestiary-items/MQ2Rp3l2BaBlZCiE.htm)|Darkvision|Vision dans le noir|officielle|
|[mqpmTykaUCnVLmSj.htm](agents-of-edgewatch-bestiary-items/mqpmTykaUCnVLmSj.htm)|Claw|Griffe|officielle|
|[mRbro73gnpsZOfZa.htm](agents-of-edgewatch-bestiary-items/mRbro73gnpsZOfZa.htm)|Claw|Griffe|officielle|
|[mRPwn3ixbA1j5Wfz.htm](agents-of-edgewatch-bestiary-items/mRPwn3ixbA1j5Wfz.htm)|Study Target|Étudier la cible|officielle|
|[MRSyx2ZsaFXN3kSn.htm](agents-of-edgewatch-bestiary-items/MRSyx2ZsaFXN3kSn.htm)|Power Attack|Attaque en puissance|officielle|
|[mryw4DfSBV3Xx30n.htm](agents-of-edgewatch-bestiary-items/mryw4DfSBV3Xx30n.htm)|Grab|Empoignade|officielle|
|[MRzdtZQQLPCyGK5c.htm](agents-of-edgewatch-bestiary-items/MRzdtZQQLPCyGK5c.htm)|Rituals|Rituels|libre|
|[Mt5gA0Ecgq48bbxm.htm](agents-of-edgewatch-bestiary-items/Mt5gA0Ecgq48bbxm.htm)|Bardic Lore|Connaissance bardique|officielle|
|[mTP4bYZoCnN514ZB.htm](agents-of-edgewatch-bestiary-items/mTP4bYZoCnN514ZB.htm)|Spit|Crachat|officielle|
|[MvGg9ZU03PHCDYj9.htm](agents-of-edgewatch-bestiary-items/MvGg9ZU03PHCDYj9.htm)|Quick Stitch|Couture rapide|officielle|
|[MwIhQXzCMNpAds0Q.htm](agents-of-edgewatch-bestiary-items/MwIhQXzCMNpAds0Q.htm)|Constant Spells|Sorts constants|officielle|
|[mYdklqhaT2LcRlN3.htm](agents-of-edgewatch-bestiary-items/mYdklqhaT2LcRlN3.htm)|Determination|Détermination|officielle|
|[mYFvnq9cvcGTpyjV.htm](agents-of-edgewatch-bestiary-items/mYFvnq9cvcGTpyjV.htm)|Constant Spells|Sorts constants|officielle|
|[MyTJRF05gqbWnyPq.htm](agents-of-edgewatch-bestiary-items/MyTJRF05gqbWnyPq.htm)|Metallify|Métallisation|officielle|
|[MZuAw11uvBDLHKj7.htm](agents-of-edgewatch-bestiary-items/MZuAw11uvBDLHKj7.htm)|Shortsword|Épée courte|officielle|
|[mZVd3ZcajfGKAm79.htm](agents-of-edgewatch-bestiary-items/mZVd3ZcajfGKAm79.htm)|At-Will Spells|Sorts à volonté|officielle|
|[n0iAPmFsdZlSVqHV.htm](agents-of-edgewatch-bestiary-items/n0iAPmFsdZlSVqHV.htm)|Dagger|Dague|officielle|
|[n4IYsyn6jwdp0itB.htm](agents-of-edgewatch-bestiary-items/n4IYsyn6jwdp0itB.htm)|Blade Legs|Pattes coupantes|officielle|
|[N5zf8LJKXg8KdskQ.htm](agents-of-edgewatch-bestiary-items/N5zf8LJKXg8KdskQ.htm)|Kharnas's Blessing|Bénédiction de Kharnas|officielle|
|[n72S7jEvm5AQeVky.htm](agents-of-edgewatch-bestiary-items/n72S7jEvm5AQeVky.htm)|Dagger|Dague|officielle|
|[N85UwJsuAe8nwuWV.htm](agents-of-edgewatch-bestiary-items/N85UwJsuAe8nwuWV.htm)|Spell Circle|Cercle magique|officielle|
|[n8gYgKZb2msziuyh.htm](agents-of-edgewatch-bestiary-items/n8gYgKZb2msziuyh.htm)|Darkvision|Vision dans le noir|officielle|
|[N8sxUVjIH1fEWXxu.htm](agents-of-edgewatch-bestiary-items/N8sxUVjIH1fEWXxu.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[n9d7kqBHgMv3tOmv.htm](agents-of-edgewatch-bestiary-items/n9d7kqBHgMv3tOmv.htm)|Choking Smog|Smog étouffant|officielle|
|[n9oO2EI8HwHSgtLl.htm](agents-of-edgewatch-bestiary-items/n9oO2EI8HwHSgtLl.htm)|Terinav Root Poison|Poison de racine de terrinave|officielle|
|[nA0QdxrRaEjv6RhP.htm](agents-of-edgewatch-bestiary-items/nA0QdxrRaEjv6RhP.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[Nbx9ouRvtXU7r7XP.htm](agents-of-edgewatch-bestiary-items/Nbx9ouRvtXU7r7XP.htm)|Dart Volley|Volée de fléchettes|officielle|
|[nCwbF6k6DjALgFuy.htm](agents-of-edgewatch-bestiary-items/nCwbF6k6DjALgFuy.htm)|Rage|Rage|officielle|
|[NDjY88hxyNPqDwPf.htm](agents-of-edgewatch-bestiary-items/NDjY88hxyNPqDwPf.htm)|Quick Bomber|Artificier rapide|officielle|
|[NeqCKB9myPD8TeNF.htm](agents-of-edgewatch-bestiary-items/NeqCKB9myPD8TeNF.htm)|Harrow Lore|Connaissance du jeu du tourment|officielle|
|[Ng3bVzRpGnsUr9AL.htm](agents-of-edgewatch-bestiary-items/Ng3bVzRpGnsUr9AL.htm)|Dagger|Dague|officielle|
|[NGB5wLXcmJCijnEn.htm](agents-of-edgewatch-bestiary-items/NGB5wLXcmJCijnEn.htm)|Jaws|Mâchoires|officielle|
|[NGOG0Y8bjtNkNrjz.htm](agents-of-edgewatch-bestiary-items/NGOG0Y8bjtNkNrjz.htm)|Spear|Lance|officielle|
|[NHDyB7FzdAXPJvx3.htm](agents-of-edgewatch-bestiary-items/NHDyB7FzdAXPJvx3.htm)|At-Will Spells|Sorts à volonté|officielle|
|[nHj4mI5oMJrWP9jm.htm](agents-of-edgewatch-bestiary-items/nHj4mI5oMJrWP9jm.htm)|Grab|Empoignade|officielle|
|[NhWlBh7zqwLP19mb.htm](agents-of-edgewatch-bestiary-items/NhWlBh7zqwLP19mb.htm)|Mindtwisting Utterance|Déclaration psychotrope|officielle|
|[nhz5F428cTKGkc9v.htm](agents-of-edgewatch-bestiary-items/nhz5F428cTKGkc9v.htm)|Inexorable March|Marche inexorable|officielle|
|[NI333m4B4OMeocLf.htm](agents-of-edgewatch-bestiary-items/NI333m4B4OMeocLf.htm)|Darkvision|Vision dans le noir|officielle|
|[NILfejzFJ409TN4i.htm](agents-of-edgewatch-bestiary-items/NILfejzFJ409TN4i.htm)|Poison Conversion|Transformation en poison|officielle|
|[niqal0ySwUPyqG0P.htm](agents-of-edgewatch-bestiary-items/niqal0ySwUPyqG0P.htm)|Eye Probe|Sonde oculaire|officielle|
|[NIWpD2eqbaBc5M8m.htm](agents-of-edgewatch-bestiary-items/NIWpD2eqbaBc5M8m.htm)|Fist|Poing|officielle|
|[Nl7SDGVnqgpHC581.htm](agents-of-edgewatch-bestiary-items/Nl7SDGVnqgpHC581.htm)|At-Will Spells|Sorts à volonté|officielle|
|[nO9dKSqf8ZjJRuCy.htm](agents-of-edgewatch-bestiary-items/nO9dKSqf8ZjJRuCy.htm)|Change Shape|Changement de forme|officielle|
|[NPDwckg35b7RN3Hb.htm](agents-of-edgewatch-bestiary-items/NPDwckg35b7RN3Hb.htm)|Sports Lore|Connaissance des sports|officielle|
|[NPjuafWJNiaGNe5E.htm](agents-of-edgewatch-bestiary-items/NPjuafWJNiaGNe5E.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[nqhGLlFeLoFFBN9g.htm](agents-of-edgewatch-bestiary-items/nqhGLlFeLoFFBN9g.htm)|Light Form|Forme lumineuse|officielle|
|[nqTM0MryJthdq3ZY.htm](agents-of-edgewatch-bestiary-items/nqTM0MryJthdq3ZY.htm)|Extra Reaction|Surcroît de réaction|officielle|
|[NRUwbsWBSVtCGgrp.htm](agents-of-edgewatch-bestiary-items/NRUwbsWBSVtCGgrp.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[nRzUgQJS26xT7C1q.htm](agents-of-edgewatch-bestiary-items/nRzUgQJS26xT7C1q.htm)|Innate Occult Spells|Sorts occultes innés|libre|
|[nsnL0aVILG6H6pb2.htm](agents-of-edgewatch-bestiary-items/nsnL0aVILG6H6pb2.htm)|Maelstrom Lore|Connaissance du Maelstrom|officielle|
|[NsvkBq0sahLrYsTg.htm](agents-of-edgewatch-bestiary-items/NsvkBq0sahLrYsTg.htm)|Elven Curve Blade|Lame courbée elfique|officielle|
|[Ntt0pX48FlDnOFb3.htm](agents-of-edgewatch-bestiary-items/Ntt0pX48FlDnOFb3.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[nurJkCpZvHiPZo76.htm](agents-of-edgewatch-bestiary-items/nurJkCpZvHiPZo76.htm)|Project False Image|Projection de fausse image|officielle|
|[NvOUKM0pOaTFyrWw.htm](agents-of-edgewatch-bestiary-items/NvOUKM0pOaTFyrWw.htm)|Close Cutter|Raser de près|officielle|
|[nvUzHv6therpBM38.htm](agents-of-edgewatch-bestiary-items/nvUzHv6therpBM38.htm)|Aquatic Ambush|Embuscade aquatique|officielle|
|[nW0moOpMRYhgNi82.htm](agents-of-edgewatch-bestiary-items/nW0moOpMRYhgNi82.htm)|Trunk|Trompe|officielle|
|[nXrzkclnA7IMvnFu.htm](agents-of-edgewatch-bestiary-items/nXrzkclnA7IMvnFu.htm)|Pitfall and Plunger|Élimination|officielle|
|[nXZygeRYzYRESgcP.htm](agents-of-edgewatch-bestiary-items/nXZygeRYzYRESgcP.htm)|Fist|Poing|officielle|
|[nyahaCV93O7YYyau.htm](agents-of-edgewatch-bestiary-items/nyahaCV93O7YYyau.htm)|Breach Vulnerability|Trop-plein|officielle|
|[NyD5wXkVBGB2Wu0I.htm](agents-of-edgewatch-bestiary-items/NyD5wXkVBGB2Wu0I.htm)|Protean Anatomy 20|Anatomie protéenne 20|officielle|
|[NZtjQV6Cb5pHneZI.htm](agents-of-edgewatch-bestiary-items/NZtjQV6Cb5pHneZI.htm)|Engineering Lore|Connaissance de l'ingénierie|officielle|
|[o03yd3s67EeP9WOh.htm](agents-of-edgewatch-bestiary-items/o03yd3s67EeP9WOh.htm)|Prescient Revision|Remise en question presciente|officielle|
|[o11O0K7oIAQznUJo.htm](agents-of-edgewatch-bestiary-items/o11O0K7oIAQznUJo.htm)|Bloody Spew|Crachat de sang|officielle|
|[O19b688nMXlfxdQP.htm](agents-of-edgewatch-bestiary-items/O19b688nMXlfxdQP.htm)|Twin Takedown|Agression jumelée|officielle|
|[o1pfLDPNAQy8FT0p.htm](agents-of-edgewatch-bestiary-items/o1pfLDPNAQy8FT0p.htm)|Point Blank|À bout portant|officielle|
|[o1vU8G41mZhtjXsb.htm](agents-of-edgewatch-bestiary-items/o1vU8G41mZhtjXsb.htm)|Sling|Fronde|officielle|
|[O3FvbwzFjYGBuqWx.htm](agents-of-edgewatch-bestiary-items/O3FvbwzFjYGBuqWx.htm)|Shield Block|Blocage au bouclier|officielle|
|[O6wQ2EWNzgJGn6Ta.htm](agents-of-edgewatch-bestiary-items/O6wQ2EWNzgJGn6Ta.htm)|Hidden Paragon|Parangon de discrétion|officielle|
|[O97E3bY0dRr5pLCH.htm](agents-of-edgewatch-bestiary-items/O97E3bY0dRr5pLCH.htm)|Inflict Warpwave|Infliger des vagues de distorsion|officielle|
|[oaYvNv83OSKkOI6V.htm](agents-of-edgewatch-bestiary-items/oaYvNv83OSKkOI6V.htm)|Performance Anxiety|Anxiété artistique|officielle|
|[oAz0VaKaZ2Hym3GL.htm](agents-of-edgewatch-bestiary-items/oAz0VaKaZ2Hym3GL.htm)|Dagger|Dague|officielle|
|[ob3CKat2QrsjJqn4.htm](agents-of-edgewatch-bestiary-items/ob3CKat2QrsjJqn4.htm)|Innate Occult Spells|Sorts occultes innés|libre|
|[oBDa2RafFTX6BBua.htm](agents-of-edgewatch-bestiary-items/oBDa2RafFTX6BBua.htm)|Grab|Empoignade|officielle|
|[ObuiuOqfP6nhvs9S.htm](agents-of-edgewatch-bestiary-items/ObuiuOqfP6nhvs9S.htm)|Tug|Tirer|officielle|
|[OCmlinuwWYcoVWLc.htm](agents-of-edgewatch-bestiary-items/OCmlinuwWYcoVWLc.htm)|Scurry|Détaler|officielle|
|[odph4tUxJEzbuuiq.htm](agents-of-edgewatch-bestiary-items/odph4tUxJEzbuuiq.htm)|Impaler|Empaleur|officielle|
|[OdVJynq0IATGWujh.htm](agents-of-edgewatch-bestiary-items/OdVJynq0IATGWujh.htm)|Foot|Pied|officielle|
|[OESsshXWIJSX1LE7.htm](agents-of-edgewatch-bestiary-items/OESsshXWIJSX1LE7.htm)|Blood-Fueled Titter|Gloussement face au sang|officielle|
|[of0eB3PZMwreV0L4.htm](agents-of-edgewatch-bestiary-items/of0eB3PZMwreV0L4.htm)|Furious Pacifier|Pacificateur furieux|officielle|
|[OF5xPm4it251t0zo.htm](agents-of-edgewatch-bestiary-items/OF5xPm4it251t0zo.htm)|Reflective Plating|Plaque réfléchissante|officielle|
|[OF6N85WER1HxJgCK.htm](agents-of-edgewatch-bestiary-items/OF6N85WER1HxJgCK.htm)|Integrated Launcher|Lanceur intégré|officielle|
|[ofiTdcCz7bSlRXmy.htm](agents-of-edgewatch-bestiary-items/ofiTdcCz7bSlRXmy.htm)|Marrow Rot|Pourrissement de la moelle|officielle|
|[oFx3vt7MvXsfxzSp.htm](agents-of-edgewatch-bestiary-items/oFx3vt7MvXsfxzSp.htm)|Fist|Poing|officielle|
|[OfYcuvZbwxbjF6cG.htm](agents-of-edgewatch-bestiary-items/OfYcuvZbwxbjF6cG.htm)|Vein Walker|Marcheur veineux|officielle|
|[OGtpbVhb7Iohn4OQ.htm](agents-of-edgewatch-bestiary-items/OGtpbVhb7Iohn4OQ.htm)|Stealth|Discrétion|officielle|
|[oIOsV3m1VCQehtGp.htm](agents-of-edgewatch-bestiary-items/oIOsV3m1VCQehtGp.htm)|Magic Horn|Corne magique|officielle|
|[OIT5GvqodAryKJpv.htm](agents-of-edgewatch-bestiary-items/OIT5GvqodAryKJpv.htm)|Sling|Fronde|officielle|
|[ojlesPsjiDHjASWl.htm](agents-of-edgewatch-bestiary-items/ojlesPsjiDHjASWl.htm)|Arcane Spontaneous Spells|Sorts arcaniques spontanés|libre|
|[oJWZY02OG1m5LrP6.htm](agents-of-edgewatch-bestiary-items/oJWZY02OG1m5LrP6.htm)|Telekinetic Reach|Allonge télékinésique|officielle|
|[oKdyZTnDb5xbqpF3.htm](agents-of-edgewatch-bestiary-items/oKdyZTnDb5xbqpF3.htm)|Hamstring|Tranche-tendon|officielle|
|[OksVpY2e7p3fhW8q.htm](agents-of-edgewatch-bestiary-items/OksVpY2e7p3fhW8q.htm)|Dream Lore|Connaissance des rêves|officielle|
|[OmGZxI6hAdkJlkwY.htm](agents-of-edgewatch-bestiary-items/OmGZxI6hAdkJlkwY.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[OMRu84X8qswYsTyJ.htm](agents-of-edgewatch-bestiary-items/OMRu84X8qswYsTyJ.htm)|Fangs|Crocs|officielle|
|[OMtflrg7VM1ogHqZ.htm](agents-of-edgewatch-bestiary-items/OMtflrg7VM1ogHqZ.htm)|Tegresin's Greeting|Accueil de Tégrésin|officielle|
|[OofTe2WDiLiEdYTm.htm](agents-of-edgewatch-bestiary-items/OofTe2WDiLiEdYTm.htm)|Halfling Luck|Chance halfeline|officielle|
|[oOj2D6OXRFRGfvRp.htm](agents-of-edgewatch-bestiary-items/oOj2D6OXRFRGfvRp.htm)|Vomit Blood|Vomir du sang|officielle|
|[OpV99ABQlIY3FF4G.htm](agents-of-edgewatch-bestiary-items/OpV99ABQlIY3FF4G.htm)|Escort from the Premises|Escorter hors des lieux|officielle|
|[oq1IG7RwkiKMV9mP.htm](agents-of-edgewatch-bestiary-items/oq1IG7RwkiKMV9mP.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[OqEu80bVGGZXPfVs.htm](agents-of-edgewatch-bestiary-items/OqEu80bVGGZXPfVs.htm)|Devastating Blast|Déflagration dévastatrice|officielle|
|[oQgehOwZMcJQ5i4V.htm](agents-of-edgewatch-bestiary-items/oQgehOwZMcJQ5i4V.htm)|Scent (Imprecise) 30 feet|Odorat (imprécis) 9 m|officielle|
|[oQJvVNt4Rn1ZygrC.htm](agents-of-edgewatch-bestiary-items/oQJvVNt4Rn1ZygrC.htm)|Deception|Duperie|officielle|
|[Os5QOeYEAod9Yg1T.htm](agents-of-edgewatch-bestiary-items/Os5QOeYEAod9Yg1T.htm)|Tentacle|Tentacule|officielle|
|[osA98Km4yrxpF38T.htm](agents-of-edgewatch-bestiary-items/osA98Km4yrxpF38T.htm)|Steel Quill|Piquant d'acier|officielle|
|[ossSXTeiyXQFkTmA.htm](agents-of-edgewatch-bestiary-items/ossSXTeiyXQFkTmA.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[OTSaRNEQPlVfldSC.htm](agents-of-edgewatch-bestiary-items/OTSaRNEQPlVfldSC.htm)|At-Will Spells|Sorts à volonté|officielle|
|[oTX4OOEDagBTll1x.htm](agents-of-edgewatch-bestiary-items/oTX4OOEDagBTll1x.htm)|Prepared Arcane Spells|Sorts arcaniques préparés|libre|
|[oXzm9KqLtbgKvm14.htm](agents-of-edgewatch-bestiary-items/oXzm9KqLtbgKvm14.htm)|Summon Elementals|Convocation d'élémentaires|officielle|
|[ozjGo4jmzdNhJfAA.htm](agents-of-edgewatch-bestiary-items/ozjGo4jmzdNhJfAA.htm)|Shortsword|Épée courte|officielle|
|[P2D3nIr4b8QtzEFx.htm](agents-of-edgewatch-bestiary-items/P2D3nIr4b8QtzEFx.htm)|Overflowing Boiling Water|Projections d'eau bouillante|officielle|
|[p777SbqGil3xrtzd.htm](agents-of-edgewatch-bestiary-items/p777SbqGil3xrtzd.htm)|Flay|Écorcher|officielle|
|[P7QkpppdP81lVkfJ.htm](agents-of-edgewatch-bestiary-items/P7QkpppdP81lVkfJ.htm)|Poison Lore|Connaissance des poisons|officielle|
|[p8i5dtNaUJaS4W11.htm](agents-of-edgewatch-bestiary-items/p8i5dtNaUJaS4W11.htm)|Battle Axe|Hache d'armes|officielle|
|[p8PqMvUhFUkgJL4b.htm](agents-of-edgewatch-bestiary-items/p8PqMvUhFUkgJL4b.htm)|Fist|Poing|officielle|
|[p9JgBbLy85y8zTXK.htm](agents-of-edgewatch-bestiary-items/p9JgBbLy85y8zTXK.htm)|Norgorber Lore|Connaissance de Norgorber|officielle|
|[Pc7yhV1GBS53uadp.htm](agents-of-edgewatch-bestiary-items/Pc7yhV1GBS53uadp.htm)|Crossbow|Arbalète|officielle|
|[PCytOdylz3vMj2WE.htm](agents-of-edgewatch-bestiary-items/PCytOdylz3vMj2WE.htm)|Claw|Griffe|officielle|
|[pEH5ZGr2J9sZg6ce.htm](agents-of-edgewatch-bestiary-items/pEH5ZGr2J9sZg6ce.htm)|Stabbing Beast Venom|Venin de Dardeuse|officielle|
|[pEr8VvYetu8yNn5t.htm](agents-of-edgewatch-bestiary-items/pEr8VvYetu8yNn5t.htm)|Efficient Winding|Remontage efficace|officielle|
|[PFD814sBXDi1v2I7.htm](agents-of-edgewatch-bestiary-items/PFD814sBXDi1v2I7.htm)|Sneak Attack|Attaque sournoise|officielle|
|[PfYigOWR2ynFziwC.htm](agents-of-edgewatch-bestiary-items/PfYigOWR2ynFziwC.htm)|Spear|Lance|officielle|
|[pgSTLlpCYo6JjJeZ.htm](agents-of-edgewatch-bestiary-items/pgSTLlpCYo6JjJeZ.htm)|Improved Grab|Empoignade améliorée|officielle|
|[phWjGBiRvnlb7wDJ.htm](agents-of-edgewatch-bestiary-items/phWjGBiRvnlb7wDJ.htm)|Constrict|Constriction|officielle|
|[PigzABnoSkZuUAGP.htm](agents-of-edgewatch-bestiary-items/PigzABnoSkZuUAGP.htm)|Expanded Splash|Éclaboussure élargie|officielle|
|[pJcNWcLAWBXtvefA.htm](agents-of-edgewatch-bestiary-items/pJcNWcLAWBXtvefA.htm)|Clockwork Reconstruction|Reconstruction mécanique|officielle|
|[PJdaCe5La2FhB5AN.htm](agents-of-edgewatch-bestiary-items/PJdaCe5La2FhB5AN.htm)|Swift Leap|Bond rapide|officielle|
|[pjqAB7UZDOSJ8Dvg.htm](agents-of-edgewatch-bestiary-items/pjqAB7UZDOSJ8Dvg.htm)|Scissors|Ciseaux|officielle|
|[pk7WhgmEvUXgGRiz.htm](agents-of-edgewatch-bestiary-items/pk7WhgmEvUXgGRiz.htm)|No MAP|Pas de PAM|officielle|
|[PlFu6E3ZCVF3JCam.htm](agents-of-edgewatch-bestiary-items/PlFu6E3ZCVF3JCam.htm)|Nightmare Rider|Cavalière de destrier noir|officielle|
|[pNlE1um147bT0E6P.htm](agents-of-edgewatch-bestiary-items/pNlE1um147bT0E6P.htm)|Animated Statues|Statues animées|officielle|
|[pnx6alHWYJ3kLnBW.htm](agents-of-edgewatch-bestiary-items/pnx6alHWYJ3kLnBW.htm)|Proven Devotion|Dévotion établie|officielle|
|[PoQgBkVgVkqOOV5e.htm](agents-of-edgewatch-bestiary-items/PoQgBkVgVkqOOV5e.htm)|Fling Offal|Projection de déchets|officielle|
|[PQM8ukSoaiRa6PDB.htm](agents-of-edgewatch-bestiary-items/PQM8ukSoaiRa6PDB.htm)|Innate Occult Spells|Sorts occultes innés|libre|
|[psPKDFy6x0wj7ko0.htm](agents-of-edgewatch-bestiary-items/psPKDFy6x0wj7ko0.htm)|Cleaver|Fendoir|officielle|
|[Ptf4oQIP2T6623QL.htm](agents-of-edgewatch-bestiary-items/Ptf4oQIP2T6623QL.htm)|Leaping Charge|Charge bondissante|officielle|
|[PtGiCuINX35GsQBg.htm](agents-of-edgewatch-bestiary-items/PtGiCuINX35GsQBg.htm)|Darkvision|Vision dans le noir|officielle|
|[pu72YVc5Xv4KyIqf.htm](agents-of-edgewatch-bestiary-items/pu72YVc5Xv4KyIqf.htm)|Dagger|Dague|officielle|
|[pUqjSa0MTNAe7fLX.htm](agents-of-edgewatch-bestiary-items/pUqjSa0MTNAe7fLX.htm)|Guillotine Blade|Lame de guillotine|officielle|
|[PvgGyV08NoFmrceo.htm](agents-of-edgewatch-bestiary-items/PvgGyV08NoFmrceo.htm)|Jaws|Mâchoires|officielle|
|[PvjXCmuXDpKKHSIf.htm](agents-of-edgewatch-bestiary-items/PvjXCmuXDpKKHSIf.htm)|Innate Primal Spells|Sorts primordiaux innés|libre|
|[pWqtIfqDJ0Fag2CW.htm](agents-of-edgewatch-bestiary-items/pWqtIfqDJ0Fag2CW.htm)|Waylay|Agripper les jambes|officielle|
|[PWYoCF0aSsWVwxCI.htm](agents-of-edgewatch-bestiary-items/PWYoCF0aSsWVwxCI.htm)|Sneak Attack|Attaque sournoise|officielle|
|[PxaUCfocdE8pfDRJ.htm](agents-of-edgewatch-bestiary-items/PxaUCfocdE8pfDRJ.htm)|Dagger|Dague|officielle|
|[pxtdz9Cb1KUHXsrM.htm](agents-of-edgewatch-bestiary-items/pxtdz9Cb1KUHXsrM.htm)|Corpse Wallow|Ablution d'immondices|officielle|
|[py2SpejCikEONHQm.htm](agents-of-edgewatch-bestiary-items/py2SpejCikEONHQm.htm)|Constant Spells|Sorts constants|officielle|
|[Q0NhoV8dgyILx0Gd.htm](agents-of-edgewatch-bestiary-items/Q0NhoV8dgyILx0Gd.htm)|Underworld Lore|Connaissance de la pègre|officielle|
|[q0zeFHZ9WaJ92IKN.htm](agents-of-edgewatch-bestiary-items/q0zeFHZ9WaJ92IKN.htm)|Jaws|Mâchoires|officielle|
|[Q10SWwitCSafoJ4L.htm](agents-of-edgewatch-bestiary-items/Q10SWwitCSafoJ4L.htm)|Shared Diversion|Diversion commune|officielle|
|[Q1MxJ1mg3fC5zwcl.htm](agents-of-edgewatch-bestiary-items/Q1MxJ1mg3fC5zwcl.htm)|Grab|Empoignade|officielle|
|[q47XrkqT8jn5rAqq.htm](agents-of-edgewatch-bestiary-items/q47XrkqT8jn5rAqq.htm)|Grab|Empoignade|officielle|
|[Q4P1CH4AQAlYpZ0X.htm](agents-of-edgewatch-bestiary-items/Q4P1CH4AQAlYpZ0X.htm)|Golem Antimagic|Antimagie du golem|officielle|
|[q4WljID3qrZ5Fpnr.htm](agents-of-edgewatch-bestiary-items/q4WljID3qrZ5Fpnr.htm)|Wind-Up|Remontage|officielle|
|[qALAo7rXGBdfG3m4.htm](agents-of-edgewatch-bestiary-items/qALAo7rXGBdfG3m4.htm)|Dagger|Dague|officielle|
|[Qc7N0GKOoFezaIK1.htm](agents-of-edgewatch-bestiary-items/Qc7N0GKOoFezaIK1.htm)|Fist|Poing|officielle|
|[QCKNcu26UUfwBynU.htm](agents-of-edgewatch-bestiary-items/QCKNcu26UUfwBynU.htm)|Calculated Splash|Éclaboussure calculée|officielle|
|[qCVeUkgwYXq7lu8z.htm](agents-of-edgewatch-bestiary-items/qCVeUkgwYXq7lu8z.htm)|Bloody Handprint|Empreinte de main sanglante|officielle|
|[QDbM7WGGXh21h9Xw.htm](agents-of-edgewatch-bestiary-items/QDbM7WGGXh21h9Xw.htm)|Nimble Dodge|Esquive agile|officielle|
|[qFILNq2kmX5xF0Dt.htm](agents-of-edgewatch-bestiary-items/qFILNq2kmX5xF0Dt.htm)|Black Ink Delirium|Délire d'encre noire|officielle|
|[Qg91eQ3wUA0CISzT.htm](agents-of-edgewatch-bestiary-items/Qg91eQ3wUA0CISzT.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[QgE3ujjsFZSthy7z.htm](agents-of-edgewatch-bestiary-items/QgE3ujjsFZSthy7z.htm)|Illusory Copies|Copies illusoires|officielle|
|[qgStk0bh308sgMGP.htm](agents-of-edgewatch-bestiary-items/qgStk0bh308sgMGP.htm)|Dagger|Dague|officielle|
|[qgu0AUWtGhup6Nyf.htm](agents-of-edgewatch-bestiary-items/qgu0AUWtGhup6Nyf.htm)|Spine|Épine|officielle|
|[qHyTxio8Rf0fGBM5.htm](agents-of-edgewatch-bestiary-items/qHyTxio8Rf0fGBM5.htm)|Constant Spells|Sorts constants|officielle|
|[qi5ozQVZsQZavdpC.htm](agents-of-edgewatch-bestiary-items/qi5ozQVZsQZavdpC.htm)|Moderate Acid Flask|Fiole d'acide moyenne|officielle|
|[QJApLqpe9NsQa7B2.htm](agents-of-edgewatch-bestiary-items/QJApLqpe9NsQa7B2.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[qLsi1de5hGC6uHjL.htm](agents-of-edgewatch-bestiary-items/qLsi1de5hGC6uHjL.htm)|Spell Ambush|Embûche magique|officielle|
|[QMeKA8gRtzqiRWQ5.htm](agents-of-edgewatch-bestiary-items/QMeKA8gRtzqiRWQ5.htm)|Tail|Queue|officielle|
|[QMmywYM7lzMllSGS.htm](agents-of-edgewatch-bestiary-items/QMmywYM7lzMllSGS.htm)|Darkvision|Vision dans le noir|officielle|
|[qnVCozwzjFK6gieU.htm](agents-of-edgewatch-bestiary-items/qnVCozwzjFK6gieU.htm)|Dagger|Dague|officielle|
|[qOiRJ4goVAPViPIt.htm](agents-of-edgewatch-bestiary-items/qOiRJ4goVAPViPIt.htm)|Hurl|Projeter|officielle|
|[qoxTaaEWLHDHtzML.htm](agents-of-edgewatch-bestiary-items/qoxTaaEWLHDHtzML.htm)|Claw|Griffe|officielle|
|[QozBQD2QZP7kMYOG.htm](agents-of-edgewatch-bestiary-items/QozBQD2QZP7kMYOG.htm)|Control Body|Contrôler le corps|officielle|
|[QP06eTmf8XusaVPV.htm](agents-of-edgewatch-bestiary-items/QP06eTmf8XusaVPV.htm)|Explode|Exploser|officielle|
|[QprgJpKB6Wfofr3t.htm](agents-of-edgewatch-bestiary-items/QprgJpKB6Wfofr3t.htm)|Focus Spells|Sorts focalisés|libre|
|[QSKXWJ8br4qwHbtf.htm](agents-of-edgewatch-bestiary-items/QSKXWJ8br4qwHbtf.htm)|Paralysis|Paralysie|officielle|
|[quBDtUjWWKx7qiFU.htm](agents-of-edgewatch-bestiary-items/quBDtUjWWKx7qiFU.htm)|Innate Primal Spells|Sorts primordiaux innés|libre|
|[Quf28S7PJstH1L0b.htm](agents-of-edgewatch-bestiary-items/Quf28S7PJstH1L0b.htm)|Spill Eyeballs|Déversement de globes oculaires|officielle|
|[qUO8L9YDXGIF28f3.htm](agents-of-edgewatch-bestiary-items/qUO8L9YDXGIF28f3.htm)|Feeblemind Ray|Rayon de débilité|officielle|
|[QURekRpCFthbGcMS.htm](agents-of-edgewatch-bestiary-items/QURekRpCFthbGcMS.htm)|Scatterbrain Palm|Claque brouille-cervelle|officielle|
|[Qv8OyAidG7v9lnwp.htm](agents-of-edgewatch-bestiary-items/Qv8OyAidG7v9lnwp.htm)|Tear Flesh|Arracher la chair|officielle|
|[qVB5KclnPFeoDGPo.htm](agents-of-edgewatch-bestiary-items/qVB5KclnPFeoDGPo.htm)|Darkvision|Vision dans le noir|officielle|
|[r1GptaN7xdyOuJwe.htm](agents-of-edgewatch-bestiary-items/r1GptaN7xdyOuJwe.htm)|Tail|Queue|officielle|
|[R1OmPKgnrpEPWGr6.htm](agents-of-edgewatch-bestiary-items/R1OmPKgnrpEPWGr6.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[r2CSyvxtD75InoFP.htm](agents-of-edgewatch-bestiary-items/r2CSyvxtD75InoFP.htm)|Sneak Attack|Attaque sournoise|officielle|
|[r47iaCjc2jC8iNnL.htm](agents-of-edgewatch-bestiary-items/r47iaCjc2jC8iNnL.htm)|Draconic Momentum|Impulsion draconique|officielle|
|[RB7QxnNxpj30rjOL.htm](agents-of-edgewatch-bestiary-items/RB7QxnNxpj30rjOL.htm)|Constant Spells|Sorts constants|officielle|
|[rbbCn6nAeaPo4dUD.htm](agents-of-edgewatch-bestiary-items/rbbCn6nAeaPo4dUD.htm)|Pseudopod|Pseudopode|officielle|
|[rBy8DTqkrMtivM2r.htm](agents-of-edgewatch-bestiary-items/rBy8DTqkrMtivM2r.htm)|At-Will Spells|Sorts à volonté|officielle|
|[rc2xxGTBv3TdEinV.htm](agents-of-edgewatch-bestiary-items/rc2xxGTBv3TdEinV.htm)|Crafting|Artisanat|officielle|
|[rDNFst5KVOAvifdy.htm](agents-of-edgewatch-bestiary-items/rDNFst5KVOAvifdy.htm)|+1 Status to All Saves vs. Magic|+1 de statut aux JdS contre la magie|officielle|
|[rDNKRU6LA2e9G96b.htm](agents-of-edgewatch-bestiary-items/rDNKRU6LA2e9G96b.htm)|Jaws|Mâchoires|officielle|
|[RDwGhyZoG46giLcH.htm](agents-of-edgewatch-bestiary-items/RDwGhyZoG46giLcH.htm)|Absalom Lore|Connaissance d'Absalom|officielle|
|[REB6Z7mAMxxSKiun.htm](agents-of-edgewatch-bestiary-items/REB6Z7mAMxxSKiun.htm)|At-Will Spells|Sorts à volonté|officielle|
|[RfaP98Gi8H94iPk3.htm](agents-of-edgewatch-bestiary-items/RfaP98Gi8H94iPk3.htm)|Crossbow|Arbalète|officielle|
|[RgfNXAlieDWY02I4.htm](agents-of-edgewatch-bestiary-items/RgfNXAlieDWY02I4.htm)|Alchemical Bomb|Bombe alchimique|officielle|
|[RGHyYuZOOFXsZGfS.htm](agents-of-edgewatch-bestiary-items/RGHyYuZOOFXsZGfS.htm)|Fist|Poing|officielle|
|[rhQHRT4MPNImnl8e.htm](agents-of-edgewatch-bestiary-items/rhQHRT4MPNImnl8e.htm)|True Appearance|Véritable apparence|officielle|
|[Rif86iiefabTTeXm.htm](agents-of-edgewatch-bestiary-items/Rif86iiefabTTeXm.htm)|Hand Crossbow|Arbalète de poing|officielle|
|[RiF9Uw43dbvL8TEa.htm](agents-of-edgewatch-bestiary-items/RiF9Uw43dbvL8TEa.htm)|Consume Poison|Consommation de poison|officielle|
|[rJILDneSIHcw62xJ.htm](agents-of-edgewatch-bestiary-items/rJILDneSIHcw62xJ.htm)|Dagger|Dague|officielle|
|[rJu8a6w24iFChezA.htm](agents-of-edgewatch-bestiary-items/rJu8a6w24iFChezA.htm)|Transfusion Aura|Aura de transfusion|officielle|
|[rJXLqXZbLYPfyPpc.htm](agents-of-edgewatch-bestiary-items/rJXLqXZbLYPfyPpc.htm)|Lifesense 60 feet|Perception de la vie 18 m|officielle|
|[RkUX9VYzuGsIycUb.htm](agents-of-edgewatch-bestiary-items/RkUX9VYzuGsIycUb.htm)|Blood Scent|Perception du sang|officielle|
|[rlDEDPkmEQiODiMh.htm](agents-of-edgewatch-bestiary-items/rlDEDPkmEQiODiMh.htm)|Norgorber Lore|Connaissance de Norgorber|officielle|
|[rlsHyYTiGdVwRStH.htm](agents-of-edgewatch-bestiary-items/rlsHyYTiGdVwRStH.htm)|Grab|Empoignade|officielle|
|[rlZ9VxWPwn9XfGgL.htm](agents-of-edgewatch-bestiary-items/rlZ9VxWPwn9XfGgL.htm)|Breath Weapon|Souffle|officielle|
|[rm9LdZwyao3hq2RK.htm](agents-of-edgewatch-bestiary-items/rm9LdZwyao3hq2RK.htm)|Absalom Lore|Connaissance d'Absalom|officielle|
|[rMKwSTFsZNjo251o.htm](agents-of-edgewatch-bestiary-items/rMKwSTFsZNjo251o.htm)|Dueling Parry|Parade en duel|officielle|
|[RPBXXyVoR76aOb4H.htm](agents-of-edgewatch-bestiary-items/RPBXXyVoR76aOb4H.htm)|Rapier|Rapière|officielle|
|[RPGIrySxh4Qa0yaP.htm](agents-of-edgewatch-bestiary-items/RPGIrySxh4Qa0yaP.htm)|Sneak Attack|Attaque sournoise|officielle|
|[rpruEIwmxeOzTZOp.htm](agents-of-edgewatch-bestiary-items/rpruEIwmxeOzTZOp.htm)|Dream Haunting|Hanter les rêves|officielle|
|[rPZdz4pgzoaAanDn.htm](agents-of-edgewatch-bestiary-items/rPZdz4pgzoaAanDn.htm)|Swallow Whole|Gober|officielle|
|[RQ0R47s8yyK8gYS8.htm](agents-of-edgewatch-bestiary-items/RQ0R47s8yyK8gYS8.htm)|Stretching Step|Déplacement élastique|officielle|
|[rr5bpEab2TNVmDUt.htm](agents-of-edgewatch-bestiary-items/rr5bpEab2TNVmDUt.htm)|Constrict|Constriction|officielle|
|[rtBp1EzJQp3x5EsU.htm](agents-of-edgewatch-bestiary-items/rtBp1EzJQp3x5EsU.htm)|Darkvision|Vision dans le noir|officielle|
|[rwp43QY136LRMqdW.htm](agents-of-edgewatch-bestiary-items/rwp43QY136LRMqdW.htm)|Darkvision|Vision dans le noir|officielle|
|[rWzEv0M5A3nxCW6P.htm](agents-of-edgewatch-bestiary-items/rWzEv0M5A3nxCW6P.htm)|Constant Spells|Sorts constants|officielle|
|[rxkGSEBo9iXMnETl.htm](agents-of-edgewatch-bestiary-items/rxkGSEBo9iXMnETl.htm)|At-Will Spells|Sorts à volonté|officielle|
|[rY3uWiPQqOCSmYTu.htm](agents-of-edgewatch-bestiary-items/rY3uWiPQqOCSmYTu.htm)|Lore Master|Maîtresse du savoir|officielle|
|[Ry9m2AVrebalVrSx.htm](agents-of-edgewatch-bestiary-items/Ry9m2AVrebalVrSx.htm)|Fist|Poing|officielle|
|[RywUmURTSevVYI7y.htm](agents-of-edgewatch-bestiary-items/RywUmURTSevVYI7y.htm)|Poisoner's Staff|Bâton de l'empoisonneur|officielle|
|[rZ6LtOq5W7Ef8R1W.htm](agents-of-edgewatch-bestiary-items/rZ6LtOq5W7Ef8R1W.htm)|Whirling Slashes|Tourbillon d'entailles|officielle|
|[RZJPsQgzSjhqSzZx.htm](agents-of-edgewatch-bestiary-items/RZJPsQgzSjhqSzZx.htm)|Engineering Lore|Connaissance de l'ingénierie|officielle|
|[S00gbcUTroHxccVJ.htm](agents-of-edgewatch-bestiary-items/S00gbcUTroHxccVJ.htm)|Pummeling Assault|Assaut fracassant|officielle|
|[s1576Kzeq8JMVNz7.htm](agents-of-edgewatch-bestiary-items/s1576Kzeq8JMVNz7.htm)|Empathetic Response|Réaction empathique|officielle|
|[s4YFi2Scm3NoZSHU.htm](agents-of-edgewatch-bestiary-items/s4YFi2Scm3NoZSHU.htm)|Darting Flurry|Déluge lancinant|officielle|
|[s5ejDTLYAXnRuoub.htm](agents-of-edgewatch-bestiary-items/s5ejDTLYAXnRuoub.htm)|Jaws|Mâchoires|officielle|
|[s6jGCfKSGTptJo1G.htm](agents-of-edgewatch-bestiary-items/s6jGCfKSGTptJo1G.htm)|Blinding Bile|Bile aveuglante|officielle|
|[s7eTzERmLr5TUCJL.htm](agents-of-edgewatch-bestiary-items/s7eTzERmLr5TUCJL.htm)|Jaws|Mâchoires|officielle|
|[s7QApXyE5DjGF9aY.htm](agents-of-edgewatch-bestiary-items/s7QApXyE5DjGF9aY.htm)|Morningstar|Morgenstern|officielle|
|[S8lzbxGXfumHXYYq.htm](agents-of-edgewatch-bestiary-items/S8lzbxGXfumHXYYq.htm)|Flay|Écorcher|officielle|
|[sbYDSGZlPbw5odWZ.htm](agents-of-edgewatch-bestiary-items/sbYDSGZlPbw5odWZ.htm)|Vulnerable to Shatter|Vulnérable à Fracassement|officielle|
|[SczLJpfQwYYTKEvD.htm](agents-of-edgewatch-bestiary-items/SczLJpfQwYYTKEvD.htm)|Watery Simulacra|Simulacres aqueux|officielle|
|[sDzBK6rs6LhvAFPu.htm](agents-of-edgewatch-bestiary-items/sDzBK6rs6LhvAFPu.htm)|All-Around Vision|Vision à 360°|officielle|
|[SEDzDCcVcLItgSUy.htm](agents-of-edgewatch-bestiary-items/SEDzDCcVcLItgSUy.htm)|Reap|Moissonner|officielle|
|[Sf1u8xUKN1LLV2FB.htm](agents-of-edgewatch-bestiary-items/Sf1u8xUKN1LLV2FB.htm)|Grab|Empoignade|officielle|
|[sFeJf8GnNPDabG1D.htm](agents-of-edgewatch-bestiary-items/sFeJf8GnNPDabG1D.htm)|Fast Healing 10|Guérison accélérée 10|officielle|
|[SGQATSbzafV9SYlH.htm](agents-of-edgewatch-bestiary-items/SGQATSbzafV9SYlH.htm)|Coordinated Distraction|Diversion coordonnée|officielle|
|[sgRdRygQY6ggn4fy.htm](agents-of-edgewatch-bestiary-items/sgRdRygQY6ggn4fy.htm)|Change Shape|Changement de forme|officielle|
|[SGuyP6Rc9SZRoRYv.htm](agents-of-edgewatch-bestiary-items/SGuyP6Rc9SZRoRYv.htm)|Lesser Alchemist's Fire|Feu grégeois inférieur|officielle|
|[shJIHzhh4j6cG3tv.htm](agents-of-edgewatch-bestiary-items/shJIHzhh4j6cG3tv.htm)|Flurry of Claws|Déluge de griffes|officielle|
|[siibhLVqReWpmq6r.htm](agents-of-edgewatch-bestiary-items/siibhLVqReWpmq6r.htm)|Darkvision|Vision dans le noir|officielle|
|[SLWi3J2O0e3QXfYB.htm](agents-of-edgewatch-bestiary-items/SLWi3J2O0e3QXfYB.htm)|Clobber|Matraquer|officielle|
|[SMg5BMoXiwN9nxmJ.htm](agents-of-edgewatch-bestiary-items/SMg5BMoXiwN9nxmJ.htm)|Running Reload|Rechargement en courant|officielle|
|[SMwhRQyOrogJbUal.htm](agents-of-edgewatch-bestiary-items/SMwhRQyOrogJbUal.htm)|Lifedrinker|Buveur de vie|officielle|
|[snJ95Lkh1TmrNndQ.htm](agents-of-edgewatch-bestiary-items/snJ95Lkh1TmrNndQ.htm)|Splash of Color|Éclaboussure de couleur|officielle|
|[SNqbJ3OBpkCDVRSr.htm](agents-of-edgewatch-bestiary-items/SNqbJ3OBpkCDVRSr.htm)|Shortsword|Épée courte|officielle|
|[sODZwisWeLEBKV8P.htm](agents-of-edgewatch-bestiary-items/sODZwisWeLEBKV8P.htm)|Prepared Arcane Spells|Sorts arcaniques préparés|libre|
|[SoEJRrsphmHa4aoG.htm](agents-of-edgewatch-bestiary-items/SoEJRrsphmHa4aoG.htm)|Rejuvenation|Reconstruction|officielle|
|[SoNwHVeZMLSrrDEw.htm](agents-of-edgewatch-bestiary-items/SoNwHVeZMLSrrDEw.htm)|Items|Objets|officielle|
|[spdJfofzwtFFM3Cs.htm](agents-of-edgewatch-bestiary-items/spdJfofzwtFFM3Cs.htm)|At-Will Spells|Sorts à volonté|officielle|
|[SriOV0CxqQFmpPCv.htm](agents-of-edgewatch-bestiary-items/SriOV0CxqQFmpPCv.htm)|Sneak Attack|Attaque sournoise|officielle|
|[STPSnFrl7yD39Fni.htm](agents-of-edgewatch-bestiary-items/STPSnFrl7yD39Fni.htm)|Holy Greatsword|Épée à deux mains sainte|officielle|
|[sTXhA6HFH3RVnqjV.htm](agents-of-edgewatch-bestiary-items/sTXhA6HFH3RVnqjV.htm)|Final Blow|Dernière attaque|officielle|
|[sVBakTcoJU3kPGlY.htm](agents-of-edgewatch-bestiary-items/sVBakTcoJU3kPGlY.htm)|Whirlwind Kick|Cyclone de coups de pied|officielle|
|[SvCc6HA58iZaOg9T.htm](agents-of-edgewatch-bestiary-items/SvCc6HA58iZaOg9T.htm)|Unrelativity Field|Champ de non-relativité|officielle|
|[sZjoXHynGmaJPLpi.htm](agents-of-edgewatch-bestiary-items/sZjoXHynGmaJPLpi.htm)|Combustible|Combustible|officielle|
|[sZRQVUQ7xiN6ZTK9.htm](agents-of-edgewatch-bestiary-items/sZRQVUQ7xiN6ZTK9.htm)|Grab|Empoignade|officielle|
|[T2NZbbINwDvovbim.htm](agents-of-edgewatch-bestiary-items/T2NZbbINwDvovbim.htm)|Darkvision|Vision dans le noir|officielle|
|[T2pFSnOpz24hsKkz.htm](agents-of-edgewatch-bestiary-items/T2pFSnOpz24hsKkz.htm)|Hand Crossbow|Arbalète de poing|officielle|
|[T4zHj7DFiaCBwf6f.htm](agents-of-edgewatch-bestiary-items/T4zHj7DFiaCBwf6f.htm)|Go Dark|S'éteindre|officielle|
|[t5jAYbO0kLhWne6I.htm](agents-of-edgewatch-bestiary-items/t5jAYbO0kLhWne6I.htm)|Fist|Poing|officielle|
|[T5JT4UBwqBoN3X7e.htm](agents-of-edgewatch-bestiary-items/T5JT4UBwqBoN3X7e.htm)|Gang Lore|Connaissance des gangs|officielle|
|[t5skSqnTS4aKuuan.htm](agents-of-edgewatch-bestiary-items/t5skSqnTS4aKuuan.htm)|Surprise Attack|Attaque surprise|officielle|
|[t7R0LcoxHegwmHyq.htm](agents-of-edgewatch-bestiary-items/t7R0LcoxHegwmHyq.htm)|Rapier|Rapière|officielle|
|[T8yXNvNM2mPGZfoq.htm](agents-of-edgewatch-bestiary-items/T8yXNvNM2mPGZfoq.htm)|At-Will Spells|Sorts à volonté|officielle|
|[TADC3gK2VlA4Jp9l.htm](agents-of-edgewatch-bestiary-items/TADC3gK2VlA4Jp9l.htm)|Mirror Duplicate|Double|officielle|
|[TAic4n8t4egJtNJX.htm](agents-of-edgewatch-bestiary-items/TAic4n8t4egJtNJX.htm)|Deny Advantage|Refus d'avantage|officielle|
|[TBb2TTPl5fRKJQJU.htm](agents-of-edgewatch-bestiary-items/TBb2TTPl5fRKJQJU.htm)|Grasping Bites|Morsures agrippantes|officielle|
|[Tbf45tR5JDVDyJCc.htm](agents-of-edgewatch-bestiary-items/Tbf45tR5JDVDyJCc.htm)|Rotting Aura|Aura de pourrissement|officielle|
|[tBGcGGksEkKqFPe7.htm](agents-of-edgewatch-bestiary-items/tBGcGGksEkKqFPe7.htm)|Alietta|Alietta|officielle|
|[TcOqYdc5uITUBa9m.htm](agents-of-edgewatch-bestiary-items/TcOqYdc5uITUBa9m.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[TeXV6pOiXpdWsvDV.htm](agents-of-edgewatch-bestiary-items/TeXV6pOiXpdWsvDV.htm)|Darkvision|Vision dans le noir|officielle|
|[TfGlX9oclwIPR62r.htm](agents-of-edgewatch-bestiary-items/TfGlX9oclwIPR62r.htm)|Toxin-Inured|Insensible aux toxines|officielle|
|[Tfnx5jxpKaKtZ7ES.htm](agents-of-edgewatch-bestiary-items/Tfnx5jxpKaKtZ7ES.htm)|Choose Weakness|Choix des faiblesses|officielle|
|[TFvB8Q4vaggsZeH2.htm](agents-of-edgewatch-bestiary-items/TFvB8Q4vaggsZeH2.htm)|Infused Items|Objets imprégnés|officielle|
|[TGZnX69H3vH84pPZ.htm](agents-of-edgewatch-bestiary-items/TGZnX69H3vH84pPZ.htm)|Ooze Tendril|Vrille de mucus|officielle|
|[ThVw3eJTb5RzjlG8.htm](agents-of-edgewatch-bestiary-items/ThVw3eJTb5RzjlG8.htm)|Rapier Hand|Main rapière|officielle|
|[tIl9HHbyeahh21jc.htm](agents-of-edgewatch-bestiary-items/tIl9HHbyeahh21jc.htm)|Tremorsense (Imprecise) 60 feet|Perception des vibrations (imprécis) 18 m|officielle|
|[Tillz0YYXHueJmui.htm](agents-of-edgewatch-bestiary-items/Tillz0YYXHueJmui.htm)|Dimensional Slide|Glissement dimensionnel|officielle|
|[TiZR6umibymo5txc.htm](agents-of-edgewatch-bestiary-items/TiZR6umibymo5txc.htm)|Sickle|Serpe|officielle|
|[TLLEMhBLZebrCg2V.htm](agents-of-edgewatch-bestiary-items/TLLEMhBLZebrCg2V.htm)|Clutching Cobbles|Accroche des pavés|officielle|
|[TmEWLqRkXgHmnIV4.htm](agents-of-edgewatch-bestiary-items/TmEWLqRkXgHmnIV4.htm)|Change Shape|Changement de forme|officielle|
|[tMhjT7tf9r9vZX4C.htm](agents-of-edgewatch-bestiary-items/tMhjT7tf9r9vZX4C.htm)|Vargouille Transformation|Transformation en vargouille|officielle|
|[tnOFl9MIpqf5OkVX.htm](agents-of-edgewatch-bestiary-items/tnOFl9MIpqf5OkVX.htm)|+1 Status to All Saves vs. Magic|+1 de statut aux JdS contre la magie|officielle|
|[Tq4JjTyPFg3Q57Z4.htm](agents-of-edgewatch-bestiary-items/Tq4JjTyPFg3Q57Z4.htm)|Trident|Trident|officielle|
|[Tr0RBSqA2yKIpS47.htm](agents-of-edgewatch-bestiary-items/Tr0RBSqA2yKIpS47.htm)|Farming Lore|Connaissance agricole|officielle|
|[Ts4PjUT1udov76YX.htm](agents-of-edgewatch-bestiary-items/Ts4PjUT1udov76YX.htm)|Absalom Lore|Connaissance d'Absalom|officielle|
|[tvVxQhJ0X1FuWMhv.htm](agents-of-edgewatch-bestiary-items/tvVxQhJ0X1FuWMhv.htm)|Dagger|Dague|officielle|
|[tWZVevRequPfr7TC.htm](agents-of-edgewatch-bestiary-items/tWZVevRequPfr7TC.htm)|Innate Primal Spells|Sorts primordiaux innés|libre|
|[TxhGff0LtzQslOci.htm](agents-of-edgewatch-bestiary-items/TxhGff0LtzQslOci.htm)|Easy to Call|Facile à appeler|officielle|
|[TXU2NotAQBux9iMZ.htm](agents-of-edgewatch-bestiary-items/TXU2NotAQBux9iMZ.htm)|Darkvision|Vision dans le noir|officielle|
|[tyx67m7w5wjz8Xc5.htm](agents-of-edgewatch-bestiary-items/tyx67m7w5wjz8Xc5.htm)|Spear|Lance|officielle|
|[tZ9QMTStg7ZyDCGS.htm](agents-of-edgewatch-bestiary-items/tZ9QMTStg7ZyDCGS.htm)|Underworld Lore|Connaissance de la pègre|officielle|
|[tZNd4FllHFLFPMgx.htm](agents-of-edgewatch-bestiary-items/tZNd4FllHFLFPMgx.htm)|Claw|Griffe|officielle|
|[U0v9mqG5NfAe3NgW.htm](agents-of-edgewatch-bestiary-items/U0v9mqG5NfAe3NgW.htm)|Fast Healing 15|Guérison accélérée 15|officielle|
|[U0xgJtIHGK7aaxUB.htm](agents-of-edgewatch-bestiary-items/U0xgJtIHGK7aaxUB.htm)|Negative Healing|Soins négatifs|officielle|
|[U1E1Xka1zLQxC4tT.htm](agents-of-edgewatch-bestiary-items/U1E1Xka1zLQxC4tT.htm)|Activate Attractor|Activer l'attracteur|officielle|
|[u1ffp4yNEVawu9dE.htm](agents-of-edgewatch-bestiary-items/u1ffp4yNEVawu9dE.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[U3Bd6ShOOPqzffIg.htm](agents-of-edgewatch-bestiary-items/U3Bd6ShOOPqzffIg.htm)|Legal Lore|Connaissance juridique|officielle|
|[u4BY6CXSH5AXUTMv.htm](agents-of-edgewatch-bestiary-items/u4BY6CXSH5AXUTMv.htm)|Continuous Barrage|Tir de barrage ininterrompu|officielle|
|[u4Or7r9xHL9MH7uK.htm](agents-of-edgewatch-bestiary-items/u4Or7r9xHL9MH7uK.htm)|Negative Healing|Soins négatifs|officielle|
|[U5JrBAObBwJKEgSJ.htm](agents-of-edgewatch-bestiary-items/U5JrBAObBwJKEgSJ.htm)|Claw|Griffe|officielle|
|[U64bIK9noEw8M2Ro.htm](agents-of-edgewatch-bestiary-items/U64bIK9noEw8M2Ro.htm)|Blood Chain|Chaîne sanglante|officielle|
|[U7PjzQ5AjISpvEhR.htm](agents-of-edgewatch-bestiary-items/U7PjzQ5AjISpvEhR.htm)|Greatpick|Grand pic de guerre|officielle|
|[U8JHbhefcDKeNERu.htm](agents-of-edgewatch-bestiary-items/U8JHbhefcDKeNERu.htm)|Constant Spells|Sorts constants|officielle|
|[UAnT1rt4pcCFs6br.htm](agents-of-edgewatch-bestiary-items/UAnT1rt4pcCFs6br.htm)|Draconic Frenzy|Frénésie draconique|officielle|
|[udFVDwS0vDvYW3tw.htm](agents-of-edgewatch-bestiary-items/udFVDwS0vDvYW3tw.htm)|Bloody Rebel|Rebelle sanguinaire|officielle|
|[uffmVAC7FAC8GDmU.htm](agents-of-edgewatch-bestiary-items/uffmVAC7FAC8GDmU.htm)|At-Will Spells|Sorts à volonté|officielle|
|[UFTgVHYhhpDzdMpd.htm](agents-of-edgewatch-bestiary-items/UFTgVHYhhpDzdMpd.htm)|Darkvision|Vision dans le noir|officielle|
|[uIpZm7bCNAs8c7BV.htm](agents-of-edgewatch-bestiary-items/uIpZm7bCNAs8c7BV.htm)|Vorpal Garrote|Garrot vorpal|officielle|
|[UiWgpQaBrzfpuuvN.htm](agents-of-edgewatch-bestiary-items/UiWgpQaBrzfpuuvN.htm)|Dagger|Dague|officielle|
|[ukjOJLhlz5hOZcuy.htm](agents-of-edgewatch-bestiary-items/ukjOJLhlz5hOZcuy.htm)|Crystallize Blood|Cristalliser le sang|officielle|
|[uLs1qWcXHyZE14d2.htm](agents-of-edgewatch-bestiary-items/uLs1qWcXHyZE14d2.htm)|Darkvision|Vision dans le noir|officielle|
|[uOIdVHoijKlxOrxM.htm](agents-of-edgewatch-bestiary-items/uOIdVHoijKlxOrxM.htm)|Abyssal Plague|Peste abyssale|officielle|
|[UOmYO7g7ej3NwdzH.htm](agents-of-edgewatch-bestiary-items/UOmYO7g7ej3NwdzH.htm)|Protean Anatomy 25|Anatomie Protéenne 25|officielle|
|[uP5k61RUIateqmiN.htm](agents-of-edgewatch-bestiary-items/uP5k61RUIateqmiN.htm)|Wavesense (Imprecise) 30 feet|Perception des ondes (imprécis) 9 m|officielle|
|[UP6qek6RNSS32oOG.htm](agents-of-edgewatch-bestiary-items/UP6qek6RNSS32oOG.htm)|Darkvision|Vision dans le noir|officielle|
|[UTB8Ay3kwYWUQkpJ.htm](agents-of-edgewatch-bestiary-items/UTB8Ay3kwYWUQkpJ.htm)|Darkvision|Vision dans le noir|officielle|
|[utBhBKO5t1Pl4TVo.htm](agents-of-edgewatch-bestiary-items/utBhBKO5t1Pl4TVo.htm)|Planar Coven|Nom: Cercle planaire|officielle|
|[UUw22Ccx1nqHZgFR.htm](agents-of-edgewatch-bestiary-items/UUw22Ccx1nqHZgFR.htm)|Unbalancing Blow|Coup déséquilibrant|officielle|
|[uv8lYUKGJVNvxD5Q.htm](agents-of-edgewatch-bestiary-items/uv8lYUKGJVNvxD5Q.htm)|Darkvision|Vision dans le noir|officielle|
|[uVoFX3A8pbUaPYi4.htm](agents-of-edgewatch-bestiary-items/uVoFX3A8pbUaPYi4.htm)|Divine Rituals|Rituels divins|libre|
|[uW2CbK6jDKtg1sG4.htm](agents-of-edgewatch-bestiary-items/uW2CbK6jDKtg1sG4.htm)|Storm Bringer|Porteur de Tempête|officielle|
|[UX4F0eelHvADT40O.htm](agents-of-edgewatch-bestiary-items/UX4F0eelHvADT40O.htm)|Gearblade|Engrenalame|officielle|
|[uXNPc5LpmbvMlOa5.htm](agents-of-edgewatch-bestiary-items/uXNPc5LpmbvMlOa5.htm)|Breathe Death|Souffle de mort|officielle|
|[UylfhYpRBAz42DrH.htm](agents-of-edgewatch-bestiary-items/UylfhYpRBAz42DrH.htm)|Flurry of Blows|Déluge de coups|officielle|
|[UYn0m6msQksZeSqS.htm](agents-of-edgewatch-bestiary-items/UYn0m6msQksZeSqS.htm)|Exorcism|Exorcisme|officielle|
|[uypEiarY3kwsOyuI.htm](agents-of-edgewatch-bestiary-items/uypEiarY3kwsOyuI.htm)|Divine Rituals|Rituels divins|libre|
|[uYvjUwSl8dqCBiVf.htm](agents-of-edgewatch-bestiary-items/uYvjUwSl8dqCBiVf.htm)|Temple Sword|Épée du temple|officielle|
|[V00sOZ0n6SQD1gNl.htm](agents-of-edgewatch-bestiary-items/V00sOZ0n6SQD1gNl.htm)|Poison Lore|Connaissance des poisons|officielle|
|[V3S7DLZ4aWxgcg1R.htm](agents-of-edgewatch-bestiary-items/V3S7DLZ4aWxgcg1R.htm)|Poison Ink|Encre empoisonnée|officielle|
|[vA2R6FhhaArEEKGJ.htm](agents-of-edgewatch-bestiary-items/vA2R6FhhaArEEKGJ.htm)|Claw|Griffe|officielle|
|[va3CnUqjuHNbajzr.htm](agents-of-edgewatch-bestiary-items/va3CnUqjuHNbajzr.htm)|Impromptu Toxin|Toxine improvisée|officielle|
|[vCow6RPYhJCBoynk.htm](agents-of-edgewatch-bestiary-items/vCow6RPYhJCBoynk.htm)|Staff|Bâton|officielle|
|[VdYfW7jwgrSYwMvK.htm](agents-of-edgewatch-bestiary-items/VdYfW7jwgrSYwMvK.htm)|Grab|Empoignade|officielle|
|[VeiaoxMMsn7EH0Ri.htm](agents-of-edgewatch-bestiary-items/VeiaoxMMsn7EH0Ri.htm)|Purple Worm Sting|Dard du ver pourpre|officielle|
|[vfIc0za5DwDp2KUV.htm](agents-of-edgewatch-bestiary-items/vfIc0za5DwDp2KUV.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[vGPvwYP6vUTp3ILa.htm](agents-of-edgewatch-bestiary-items/vGPvwYP6vUTp3ILa.htm)|Composite Shortbow|Arc court composite|officielle|
|[Vgy7PoKunyfdAFR8.htm](agents-of-edgewatch-bestiary-items/Vgy7PoKunyfdAFR8.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[Vhh4ycX46oiBHKeS.htm](agents-of-edgewatch-bestiary-items/Vhh4ycX46oiBHKeS.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[VHpzidpp88FeTXoa.htm](agents-of-edgewatch-bestiary-items/VHpzidpp88FeTXoa.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[viGvtlLGXHhsAVh8.htm](agents-of-edgewatch-bestiary-items/viGvtlLGXHhsAVh8.htm)|Quick Stow|Rangement rapide|officielle|
|[Vj7wAYFFJXEJe0Ag.htm](agents-of-edgewatch-bestiary-items/Vj7wAYFFJXEJe0Ag.htm)|Stinger|Dard|officielle|
|[vL5RasoMgasgdDwB.htm](agents-of-edgewatch-bestiary-items/vL5RasoMgasgdDwB.htm)|Innate Arcane Spells|Sorts arcaniques innés|libre|
|[VL9GemmMcofwTOB1.htm](agents-of-edgewatch-bestiary-items/VL9GemmMcofwTOB1.htm)|Chaos Hand|Main chaotique|officielle|
|[vLQku20flQOsiRfM.htm](agents-of-edgewatch-bestiary-items/vLQku20flQOsiRfM.htm)|Jaws|Mâchoires|officielle|
|[Vlw7l0vJKqGsX2Ig.htm](agents-of-edgewatch-bestiary-items/Vlw7l0vJKqGsX2Ig.htm)|Warfare Lore|Connaissance de la guerre|officielle|
|[vMASeC7y1dcAleR5.htm](agents-of-edgewatch-bestiary-items/vMASeC7y1dcAleR5.htm)|Games Lore|Connaissance ludique|officielle|
|[vObvjZsVBRVevJEo.htm](agents-of-edgewatch-bestiary-items/vObvjZsVBRVevJEo.htm)|Grab|Empoignade|officielle|
|[Vqq99svjAY5zIGFW.htm](agents-of-edgewatch-bestiary-items/Vqq99svjAY5zIGFW.htm)|Dagger|Dague|officielle|
|[VrU5JQWGpeNs37xp.htm](agents-of-edgewatch-bestiary-items/VrU5JQWGpeNs37xp.htm)|Constant Spells|Sorts constants|officielle|
|[vRwV27JhbYTjeiSz.htm](agents-of-edgewatch-bestiary-items/vRwV27JhbYTjeiSz.htm)|Ritual|Rituel|libre|
|[VSaPPvdgJDTYrZHJ.htm](agents-of-edgewatch-bestiary-items/VSaPPvdgJDTYrZHJ.htm)|Knockdown|Renversement|officielle|
|[VsMHrDx86310JmtX.htm](agents-of-edgewatch-bestiary-items/VsMHrDx86310JmtX.htm)|Sneak Attack|Attaque sournoise|officielle|
|[vu11UJ4Yclcvha1F.htm](agents-of-edgewatch-bestiary-items/vu11UJ4Yclcvha1F.htm)|Prepared Arcane Spells|Sorts arcaniques préparés|libre|
|[VV329B6w0rioBP7u.htm](agents-of-edgewatch-bestiary-items/VV329B6w0rioBP7u.htm)|Shortsword|Épée courte|officielle|
|[vvMB5rLp5hejSAmy.htm](agents-of-edgewatch-bestiary-items/vvMB5rLp5hejSAmy.htm)|Arrogant Taunts|Railleries arrogantes|officielle|
|[VVMmL1VxeAzF1IBI.htm](agents-of-edgewatch-bestiary-items/VVMmL1VxeAzF1IBI.htm)|Demanding Orders|Ordres exigeants|officielle|
|[Vw3bmxYidV4ocAb1.htm](agents-of-edgewatch-bestiary-items/Vw3bmxYidV4ocAb1.htm)|Drain Wand|Baguette de drainage|officielle|
|[vW6qQ6HvTQdopnBl.htm](agents-of-edgewatch-bestiary-items/vW6qQ6HvTQdopnBl.htm)|Bloody Sneak Attack|Attaque sournoise sanglante|officielle|
|[Vy9EtFybJQqo55xn.htm](agents-of-edgewatch-bestiary-items/Vy9EtFybJQqo55xn.htm)|Hypnotic Glow|Lueur hypnotique|officielle|
|[Vyd3GhdzugQrKNRt.htm](agents-of-edgewatch-bestiary-items/Vyd3GhdzugQrKNRt.htm)|Prepared Arcane Spells|Sorts arcaniques préparés|libre|
|[vynPyIOZZDBVXGPa.htm](agents-of-edgewatch-bestiary-items/vynPyIOZZDBVXGPa.htm)|Scent|Odorat|officielle|
|[VyojjmuQFpZR3NHk.htm](agents-of-edgewatch-bestiary-items/VyojjmuQFpZR3NHk.htm)|Statue Shortsword|Épée courte de statue|officielle|
|[vYvC5Vb8SsSJOIxk.htm](agents-of-edgewatch-bestiary-items/vYvC5Vb8SsSJOIxk.htm)|Innate Arcane Spells|Sorts arcaniques innés|libre|
|[VzGeaeMNgzVgxvGo.htm](agents-of-edgewatch-bestiary-items/VzGeaeMNgzVgxvGo.htm)|Lusca Venom|Venin de lusque|officielle|
|[VZSNVQOWLrMXeg03.htm](agents-of-edgewatch-bestiary-items/VZSNVQOWLrMXeg03.htm)|Alchemy Lore|Connaissance alchimique|officielle|
|[vZXFIePI3zP2Og1M.htm](agents-of-edgewatch-bestiary-items/vZXFIePI3zP2Og1M.htm)|Web Trap|Piège de toile|officielle|
|[W211sY7aEqXIhFeB.htm](agents-of-edgewatch-bestiary-items/W211sY7aEqXIhFeB.htm)|Darkvision|Vision dans le noir|officielle|
|[w3uomQxkiwXJF2hj.htm](agents-of-edgewatch-bestiary-items/w3uomQxkiwXJF2hj.htm)|Light Ray|Rayon de lumière|officielle|
|[w53ToXCIso2kLmSW.htm](agents-of-edgewatch-bestiary-items/w53ToXCIso2kLmSW.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[w5G4Ty8RBA7MDSTS.htm](agents-of-edgewatch-bestiary-items/w5G4Ty8RBA7MDSTS.htm)|Pitfall|Chausse-trappe|officielle|
|[W5xPRbSycfvcG1bj.htm](agents-of-edgewatch-bestiary-items/W5xPRbSycfvcG1bj.htm)|Dart|Fléchette|officielle|
|[WaEIc7jlHfXb63gH.htm](agents-of-edgewatch-bestiary-items/WaEIc7jlHfXb63gH.htm)|Blasphemous Arms|Bras blasphématoires|officielle|
|[wAYzRxbZPCt548k7.htm](agents-of-edgewatch-bestiary-items/wAYzRxbZPCt548k7.htm)|Dual Assault|Assaut double|officielle|
|[wazlV57b7ZEUGjn6.htm](agents-of-edgewatch-bestiary-items/wazlV57b7ZEUGjn6.htm)|Steady Spellcasting|Incantation fiable|officielle|
|[Wbny5fScmVtKCw6w.htm](agents-of-edgewatch-bestiary-items/Wbny5fScmVtKCw6w.htm)|Whirring Doom|Vrombissement apocalyptique|officielle|
|[wBPluFhMyu7RSQgs.htm](agents-of-edgewatch-bestiary-items/wBPluFhMyu7RSQgs.htm)|Light Mace|Masse d'armes légère|officielle|
|[Wc0IrjjTSicQkUfW.htm](agents-of-edgewatch-bestiary-items/Wc0IrjjTSicQkUfW.htm)|Wind Up|Remontage|officielle|
|[WCFZ8IEU5wGPGAZK.htm](agents-of-edgewatch-bestiary-items/WCFZ8IEU5wGPGAZK.htm)|Opportune Backstab|Coup de poignard opportuniste|officielle|
|[wCtIIrUW3W5fxJS5.htm](agents-of-edgewatch-bestiary-items/wCtIIrUW3W5fxJS5.htm)|Psychokinetic Trumpet|Barrissement psychokinétique|officielle|
|[wdrujBr7WxOu0zlx.htm](agents-of-edgewatch-bestiary-items/wdrujBr7WxOu0zlx.htm)|Swallow Whole|Gober|officielle|
|[WeIVzFTJRVOOXc8w.htm](agents-of-edgewatch-bestiary-items/WeIVzFTJRVOOXc8w.htm)|Magic Susceptibility|Vulnérabilité magique|officielle|
|[wFFcTblSQv1w7t5j.htm](agents-of-edgewatch-bestiary-items/wFFcTblSQv1w7t5j.htm)|Lava Bomb|Bombe de lave|officielle|
|[wG13bULump9xDoJR.htm](agents-of-edgewatch-bestiary-items/wG13bULump9xDoJR.htm)|+1 Status to All Saves vs. Magic|+1 de statut aux JdS contre la magie|officielle|
|[Wg2JQdgjmSOEpeH4.htm](agents-of-edgewatch-bestiary-items/Wg2JQdgjmSOEpeH4.htm)|Longsword|Épée longue|officielle|
|[wGuVhWBsG7ERdKlr.htm](agents-of-edgewatch-bestiary-items/wGuVhWBsG7ERdKlr.htm)|Extending Chandeliers|Lustres à rallonge|officielle|
|[wh8aWKGdA2HjMBi7.htm](agents-of-edgewatch-bestiary-items/wh8aWKGdA2HjMBi7.htm)|Cleric Domain Spells|Sorts de domaine de prêtre|libre|
|[wHtIgMdmPGGys5hu.htm](agents-of-edgewatch-bestiary-items/wHtIgMdmPGGys5hu.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[WibchVOYf3MLcdy7.htm](agents-of-edgewatch-bestiary-items/WibchVOYf3MLcdy7.htm)|Spike|Pique|officielle|
|[WJcrXfekpCSuZzoT.htm](agents-of-edgewatch-bestiary-items/WJcrXfekpCSuZzoT.htm)|[Two Actions] Water Jet|[Two Actions] Projection d'eau|officielle|
|[wjucZTZEvK64kwlz.htm](agents-of-edgewatch-bestiary-items/wjucZTZEvK64kwlz.htm)|Exile|Exil|officielle|
|[WjzMU3WoQ0I6gVRQ.htm](agents-of-edgewatch-bestiary-items/WjzMU3WoQ0I6gVRQ.htm)|Bloody Spew|Crachat sanglant|officielle|
|[WkhPyo0GeiUUil24.htm](agents-of-edgewatch-bestiary-items/WkhPyo0GeiUUil24.htm)|Architecture Lore|Connaissance de l'architecture|officielle|
|[wKQPyzZESm0w8vni.htm](agents-of-edgewatch-bestiary-items/wKQPyzZESm0w8vni.htm)|Protean Anatomy|Anatomie protéenne|officielle|
|[WlrYnJI19Nc7qelL.htm](agents-of-edgewatch-bestiary-items/WlrYnJI19Nc7qelL.htm)|Longsword|Épée longue|officielle|
|[WNRDgclsqI4zJ8Zn.htm](agents-of-edgewatch-bestiary-items/WNRDgclsqI4zJ8Zn.htm)|Greater Flaming Greataxe|Grande hache enflammée supérieure|officielle|
|[wPp48cMhLkEqI4UE.htm](agents-of-edgewatch-bestiary-items/wPp48cMhLkEqI4UE.htm)|Binding Blast|Explosion aveuglante|officielle|
|[wpS81V45xNq0wZpT.htm](agents-of-edgewatch-bestiary-items/wpS81V45xNq0wZpT.htm)|Venomous Spit|Crachat venimeux|officielle|
|[WQ6JLrvLYwJnndgB.htm](agents-of-edgewatch-bestiary-items/WQ6JLrvLYwJnndgB.htm)|At-Will Spells|Sorts à volonté|officielle|
|[WQakf5K0a5EhkRkV.htm](agents-of-edgewatch-bestiary-items/WQakf5K0a5EhkRkV.htm)|Sneak Attack|Attaque sournoise|officielle|
|[wqR1FOPVr3IfWeO6.htm](agents-of-edgewatch-bestiary-items/wqR1FOPVr3IfWeO6.htm)|Innate Occult Spells|Sorts occultes innés|libre|
|[WSvQFfsRQ4UpAb4L.htm](agents-of-edgewatch-bestiary-items/WSvQFfsRQ4UpAb4L.htm)|At-Will Spells|Sorts à volonté|officielle|
|[wtewhF2XR7w6V0Wm.htm](agents-of-edgewatch-bestiary-items/wtewhF2XR7w6V0Wm.htm)|Incendiary Spit|Crachat incendiaire|officielle|
|[wTQI2GpK1QxS5tKd.htm](agents-of-edgewatch-bestiary-items/wTQI2GpK1QxS5tKd.htm)|Constant Spells|Sorts constants|officielle|
|[WUdhVBp748zzwj4z.htm](agents-of-edgewatch-bestiary-items/WUdhVBp748zzwj4z.htm)|Negative Healing|Soins négatifs|officielle|
|[wuytt71AuRJcaryt.htm](agents-of-edgewatch-bestiary-items/wuytt71AuRJcaryt.htm)|Sidestep|Pas de côté|officielle|
|[wvGApQMwNilwaRG1.htm](agents-of-edgewatch-bestiary-items/wvGApQMwNilwaRG1.htm)|Massive Scythe|Faux énorme|officielle|
|[wvqzKo67ZU0rKlN6.htm](agents-of-edgewatch-bestiary-items/wvqzKo67ZU0rKlN6.htm)|Jaws|Mâchoires|officielle|
|[wW8lBy7vi5CcDHTN.htm](agents-of-edgewatch-bestiary-items/wW8lBy7vi5CcDHTN.htm)|Retch of Foulness|Renvoi de crasse|officielle|
|[wWBBBQdGjY5Kvg0N.htm](agents-of-edgewatch-bestiary-items/wWBBBQdGjY5Kvg0N.htm)|Constant Spells|Sorts constants|officielle|
|[WWLbZOyOAUEoJrVO.htm](agents-of-edgewatch-bestiary-items/WWLbZOyOAUEoJrVO.htm)|Fast Swallow|Gober rapidement|officielle|
|[WxDAGBp5686PFyEA.htm](agents-of-edgewatch-bestiary-items/WxDAGBp5686PFyEA.htm)|Claw|Griffe|officielle|
|[x2ySAWMOcn67UNgo.htm](agents-of-edgewatch-bestiary-items/x2ySAWMOcn67UNgo.htm)|Innate Primal Spells|Sorts primordiaux innés|libre|
|[X6LedzeSnW4xbem7.htm](agents-of-edgewatch-bestiary-items/X6LedzeSnW4xbem7.htm)|Sneak Attack|Attaque sournoise|officielle|
|[X7yV3UIdROaskajA.htm](agents-of-edgewatch-bestiary-items/X7yV3UIdROaskajA.htm)|Suicidal Obedience|Obéissance suicidaire|officielle|
|[x85871wfT5AGGHSb.htm](agents-of-edgewatch-bestiary-items/x85871wfT5AGGHSb.htm)|Predictive Impediment|Obstacle prédictif|officielle|
|[X8bVkdHSJwnQfIoj.htm](agents-of-edgewatch-bestiary-items/X8bVkdHSJwnQfIoj.htm)|Mirror Senses|Perception des miroirs|officielle|
|[xaK2BOT4rtFista9.htm](agents-of-edgewatch-bestiary-items/xaK2BOT4rtFista9.htm)|Blade of the Rabbit Prince|Lame du Prince lapin|officielle|
|[xB5y0S4TFwSTIZuN.htm](agents-of-edgewatch-bestiary-items/xB5y0S4TFwSTIZuN.htm)|Swallow Whole|Gober|officielle|
|[XbpdSS6FXHoqmiIy.htm](agents-of-edgewatch-bestiary-items/XbpdSS6FXHoqmiIy.htm)|Rend|Éventration|officielle|
|[Xc5ErxpysKbiLVjW.htm](agents-of-edgewatch-bestiary-items/Xc5ErxpysKbiLVjW.htm)|Tentacle|Tentacule|officielle|
|[XC6rH5AiexqlIUrJ.htm](agents-of-edgewatch-bestiary-items/XC6rH5AiexqlIUrJ.htm)|Crowded Mob|Foule immense|officielle|
|[xcCXMK0pQFPWRPA9.htm](agents-of-edgewatch-bestiary-items/xcCXMK0pQFPWRPA9.htm)|Grab|Empoignade|officielle|
|[XCR8NvW2d1UwWElX.htm](agents-of-edgewatch-bestiary-items/XCR8NvW2d1UwWElX.htm)|Arm Activation|Activation d'un bras|officielle|
|[XEmgx6gs9UBM9Fqd.htm](agents-of-edgewatch-bestiary-items/XEmgx6gs9UBM9Fqd.htm)|Storm of Claws|Tempête de griffes|libre|
|[xhZKk4oQRgs08i0T.htm](agents-of-edgewatch-bestiary-items/xhZKk4oQRgs08i0T.htm)|Nimble Dodge|Esquive agile|officielle|
|[XjKcKhw6OKjMZYZd.htm](agents-of-edgewatch-bestiary-items/XjKcKhw6OKjMZYZd.htm)|Jaws|Mâchoires|officielle|
|[xkC8ZjLdXCpQeb8J.htm](agents-of-edgewatch-bestiary-items/xkC8ZjLdXCpQeb8J.htm)|Regeneration 20 (Deactivated by Evil)|Régénération 20 (désactivée par mauvais)|officielle|
|[XKe1YQHD2E6lSSDn.htm](agents-of-edgewatch-bestiary-items/XKe1YQHD2E6lSSDn.htm)|Shriek|Hurlement|officielle|
|[xm43sElSiUh9TZcn.htm](agents-of-edgewatch-bestiary-items/xm43sElSiUh9TZcn.htm)|Prepared Divine Spells|Sorts divins préparés|libre|
|[xpaIY6o97hpi3r6c.htm](agents-of-edgewatch-bestiary-items/xpaIY6o97hpi3r6c.htm)|Club|Gourdin|officielle|
|[XpTvRTQsAv9H9zpJ.htm](agents-of-edgewatch-bestiary-items/XpTvRTQsAv9H9zpJ.htm)|Constrict|Constriction|officielle|
|[XPZi9QdRnv0anTSq.htm](agents-of-edgewatch-bestiary-items/XPZi9QdRnv0anTSq.htm)|Greater Flaming Javelin|Javeline enflammée supérieure|officielle|
|[XrbjsVj4ZB7Fx9Dk.htm](agents-of-edgewatch-bestiary-items/XrbjsVj4ZB7Fx9Dk.htm)|Dagger|Dague|officielle|
|[XrNCDIzaEOy3x2U7.htm](agents-of-edgewatch-bestiary-items/XrNCDIzaEOy3x2U7.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[XrrgRKZ5Cs3t7Wzc.htm](agents-of-edgewatch-bestiary-items/XrrgRKZ5Cs3t7Wzc.htm)|Entropy Sense (Imprecise) 60 feet|Perception de l'entropie (imprécis) 18 m|officielle|
|[XsO5LFwrPWmXxkjs.htm](agents-of-edgewatch-bestiary-items/XsO5LFwrPWmXxkjs.htm)|Innate Divine Spells|Sorts divins innés|libre|
|[XVBhNwitUtia7zY6.htm](agents-of-edgewatch-bestiary-items/XVBhNwitUtia7zY6.htm)|+2 Circumstance Bonus on Saves vs. Poison|+2 de circonstances aux JdS contre poison|officielle|
|[xYJikLuNmwyBRVgt.htm](agents-of-edgewatch-bestiary-items/xYJikLuNmwyBRVgt.htm)|Ghostly Hand|Main spectrale|officielle|
|[xzlfpLa29fzw4xH5.htm](agents-of-edgewatch-bestiary-items/xzlfpLa29fzw4xH5.htm)|Swipe|Frappe transversale|officielle|
|[xzuxIMk5QK7iPQ9j.htm](agents-of-edgewatch-bestiary-items/xzuxIMk5QK7iPQ9j.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[y4FvJN07K6t62yW1.htm](agents-of-edgewatch-bestiary-items/y4FvJN07K6t62yW1.htm)|Rolling Assault|Roulade mortelle|officielle|
|[Y7LWmp8wJgF4O24C.htm](agents-of-edgewatch-bestiary-items/Y7LWmp8wJgF4O24C.htm)|Slam Shut|Fermeture violente|officielle|
|[Y7P7UpMZajGcxNz4.htm](agents-of-edgewatch-bestiary-items/Y7P7UpMZajGcxNz4.htm)|Sneak Attack|Attaque sournoise|officielle|
|[Y83KtMbjPwV7yApV.htm](agents-of-edgewatch-bestiary-items/Y83KtMbjPwV7yApV.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[Y86gdLAapKCoO1Pl.htm](agents-of-edgewatch-bestiary-items/Y86gdLAapKCoO1Pl.htm)|Barrel|Barrique|officielle|
|[y8wwK8yOkMWsAZoY.htm](agents-of-edgewatch-bestiary-items/y8wwK8yOkMWsAZoY.htm)|Tail|Queue|officielle|
|[Y974djSHleUUJ1cg.htm](agents-of-edgewatch-bestiary-items/Y974djSHleUUJ1cg.htm)|Claws|Griffes|officielle|
|[yAHJOd3dkfOae1Kf.htm](agents-of-edgewatch-bestiary-items/yAHJOd3dkfOae1Kf.htm)|Magic Immunity|Immunité contre la magie|officielle|
|[yAihXkZJ3wXkWXOm.htm](agents-of-edgewatch-bestiary-items/yAihXkZJ3wXkWXOm.htm)|Jaws|Mâchoires|officielle|
|[Yb2OjQReLk3HgLoc.htm](agents-of-edgewatch-bestiary-items/Yb2OjQReLk3HgLoc.htm)|Feed on Fear|Se nourrir de la peur|officielle|
|[YDAmsDKjMjg1NU8x.htm](agents-of-edgewatch-bestiary-items/YDAmsDKjMjg1NU8x.htm)|Head Hunter|Chasseur de têtes|officielle|
|[YE6DApvJToive72E.htm](agents-of-edgewatch-bestiary-items/YE6DApvJToive72E.htm)|Paint the Masses|Peindre les masses|officielle|
|[YEhIEhTt74V6zF2F.htm](agents-of-edgewatch-bestiary-items/YEhIEhTt74V6zF2F.htm)|Pseudopod|Pseudopode|officielle|
|[YH6BaTJoWcGrOFjD.htm](agents-of-edgewatch-bestiary-items/YH6BaTJoWcGrOFjD.htm)|Crossbow|Arbalète|officielle|
|[YhPJNRCs20xqntX6.htm](agents-of-edgewatch-bestiary-items/YhPJNRCs20xqntX6.htm)|Motion Sense 60 feet|Perception du mouvement 18 m|officielle|
|[yhVKDXeyQEVchPJW.htm](agents-of-edgewatch-bestiary-items/yhVKDXeyQEVchPJW.htm)|Dismember|Démembrer|officielle|
|[Yid1THCUAuHFu0h8.htm](agents-of-edgewatch-bestiary-items/Yid1THCUAuHFu0h8.htm)|Survival|Survie|officielle|
|[yIeOGWTZXqk546wz.htm](agents-of-edgewatch-bestiary-items/yIeOGWTZXqk546wz.htm)|Claw|Griffe|officielle|
|[yJXSa8oHuL9yaPqg.htm](agents-of-edgewatch-bestiary-items/yJXSa8oHuL9yaPqg.htm)|Wary|Méfiant|officielle|
|[ykHarPFAY9t176Uv.htm](agents-of-edgewatch-bestiary-items/ykHarPFAY9t176Uv.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[ylBiUlJT4zFUEOca.htm](agents-of-edgewatch-bestiary-items/ylBiUlJT4zFUEOca.htm)|Dagger|Dague|officielle|
|[YlkEautzPcw7ota2.htm](agents-of-edgewatch-bestiary-items/YlkEautzPcw7ota2.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[YMK6yeIqUwKCrXpF.htm](agents-of-edgewatch-bestiary-items/YMK6yeIqUwKCrXpF.htm)|Composite Shortbow|Arc court composite|officielle|
|[yMXbFPudLdFxCrfJ.htm](agents-of-edgewatch-bestiary-items/yMXbFPudLdFxCrfJ.htm)|Prehensile Tail|Queue préhensile|officielle|
|[YNkhegfdFfE7yfGn.htm](agents-of-edgewatch-bestiary-items/YNkhegfdFfE7yfGn.htm)|Formation Attack|Attaque en formation|officielle|
|[yoqc9IMPx8HSR6k1.htm](agents-of-edgewatch-bestiary-items/yoqc9IMPx8HSR6k1.htm)|Grab|Empoignade|officielle|
|[yOvz4I15CCPk78ns.htm](agents-of-edgewatch-bestiary-items/yOvz4I15CCPk78ns.htm)|Smoke Vision|Vision malgré la fumée|officielle|
|[yqKz1nsBwvpywBaj.htm](agents-of-edgewatch-bestiary-items/yqKz1nsBwvpywBaj.htm)|Prepared Arcane Spells|Sorts arcaniques préparés|libre|
|[yqYxldud8C52XbLb.htm](agents-of-edgewatch-bestiary-items/yqYxldud8C52XbLb.htm)|Darkvision|Vision dans le noir|officielle|
|[Yra4blZoY3d1QYtS.htm](agents-of-edgewatch-bestiary-items/Yra4blZoY3d1QYtS.htm)|Constant Spells|Sorts constants|officielle|
|[Ys0t5quGKBZoMSOI.htm](agents-of-edgewatch-bestiary-items/Ys0t5quGKBZoMSOI.htm)|Pseudopod|Pseudopode|officielle|
|[Ys9PpeeVpBUmvYSZ.htm](agents-of-edgewatch-bestiary-items/Ys9PpeeVpBUmvYSZ.htm)|At-Will Spells|Sorts à volonté|officielle|
|[ysA7ztYuAl3O3J7A.htm](agents-of-edgewatch-bestiary-items/ysA7ztYuAl3O3J7A.htm)|Nightmare Cudgel|Matraque cauchemardesque|officielle|
|[yTV624ydUotNS7Qa.htm](agents-of-edgewatch-bestiary-items/yTV624ydUotNS7Qa.htm)|Vital Transfusion|Transfusion vitale|officielle|
|[yut11i3oSOwRk5mZ.htm](agents-of-edgewatch-bestiary-items/yut11i3oSOwRk5mZ.htm)|Najra Lizard Venom|Venin de lézard najra|officielle|
|[yVDBxObK8ZPhiFLN.htm](agents-of-edgewatch-bestiary-items/yVDBxObK8ZPhiFLN.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[yVT8Le2gpdDtMHay.htm](agents-of-edgewatch-bestiary-items/yVT8Le2gpdDtMHay.htm)|Shield Block|Blocage au bouclier|officielle|
|[YW2WcfCcpCi1cDl2.htm](agents-of-edgewatch-bestiary-items/YW2WcfCcpCi1cDl2.htm)|Thoughtsense (Imprecise) 120 feet|Perception des pensées (imprécis) 36 m|officielle|
|[yX6VKlUamzU5KOQr.htm](agents-of-edgewatch-bestiary-items/yX6VKlUamzU5KOQr.htm)|Bonesense (Imprecise) 30 feet|Perception des os (imprécis) 9 m|officielle|
|[YXf0epw5INfNsVTi.htm](agents-of-edgewatch-bestiary-items/YXf0epw5INfNsVTi.htm)|Vermicular Movement|Déplacement vermiculaire|officielle|
|[YY5ytAHtfhcxwxto.htm](agents-of-edgewatch-bestiary-items/YY5ytAHtfhcxwxto.htm)|Sneak Attack|Attaque sournoise|officielle|
|[yyZMl8O8UfRO0cwF.htm](agents-of-edgewatch-bestiary-items/yyZMl8O8UfRO0cwF.htm)|Tremorsense (Imprecise) 120 feet|Perception des vibrations (imprécis) 36 m|officielle|
|[yzD6DHeUdHo1tXNp.htm](agents-of-edgewatch-bestiary-items/yzD6DHeUdHo1tXNp.htm)|Summon Steed|Invocation du destrier|officielle|
|[YzsKsO21XfN1KAry.htm](agents-of-edgewatch-bestiary-items/YzsKsO21XfN1KAry.htm)|Dread Gaze|Regard effrayant|officielle|
|[yzzm5eV3r1RGpar6.htm](agents-of-edgewatch-bestiary-items/yzzm5eV3r1RGpar6.htm)|+1 Status Bonus on Saves vs. Incapacitation Effects|+1 de statut aux JdS contre les effets de mise hors de combat|officielle|
|[Z0dIBhDdvys895pC.htm](agents-of-edgewatch-bestiary-items/Z0dIBhDdvys895pC.htm)|Rituals|Rituels|libre|
|[z0sQG5rpiS29mSKB.htm](agents-of-edgewatch-bestiary-items/z0sQG5rpiS29mSKB.htm)|Law Lore|Connaissance de la loi|officielle|
|[Z3kggQWxBiytny0l.htm](agents-of-edgewatch-bestiary-items/Z3kggQWxBiytny0l.htm)|Norgorber Lore|Connaissance de Norgorber|officielle|
|[z53waMQlGY7ahvCA.htm](agents-of-edgewatch-bestiary-items/z53waMQlGY7ahvCA.htm)|Hook and Flay|Accrocher et écorcher|officielle|
|[Z5GiE6QWjQtsaPbJ.htm](agents-of-edgewatch-bestiary-items/Z5GiE6QWjQtsaPbJ.htm)|Capture Subject|Sujet de capture|officielle|
|[z7mNGwtluLbrFYpO.htm](agents-of-edgewatch-bestiary-items/z7mNGwtluLbrFYpO.htm)|Sling|Fronde|officielle|
|[ZaivUgHp2bK6CmBQ.htm](agents-of-edgewatch-bestiary-items/ZaivUgHp2bK6CmBQ.htm)|Protean Anatomy 10|Anatomie protéenne 10|officielle|
|[zaQRIy856WuUiHBw.htm](agents-of-edgewatch-bestiary-items/zaQRIy856WuUiHBw.htm)|Overdrive Engine|Surcharge moteur|officielle|
|[zBa632069HwaM4cT.htm](agents-of-edgewatch-bestiary-items/zBa632069HwaM4cT.htm)|Dagger|Dague|officielle|
|[ZBfbG1IkIDHWmhM2.htm](agents-of-edgewatch-bestiary-items/ZBfbG1IkIDHWmhM2.htm)|Wall Run|Course à la verticale|officielle|
|[ZbNsjTdTl9JRvGxk.htm](agents-of-edgewatch-bestiary-items/ZbNsjTdTl9JRvGxk.htm)|Bullyrag Beatdown|Passage à tabac|officielle|
|[Zc0ocF6ln2ziU1iE.htm](agents-of-edgewatch-bestiary-items/Zc0ocF6ln2ziU1iE.htm)|Agonizing Wail|Plainte d'agonie|officielle|
|[ZCDanTgpw20dpNUH.htm](agents-of-edgewatch-bestiary-items/ZCDanTgpw20dpNUH.htm)|Regeneration 30 (Deactivated by Lawful)|Régénération 30 (Désactivée par Loyal)|officielle|
|[zchaebxlIBynZinm.htm](agents-of-edgewatch-bestiary-items/zchaebxlIBynZinm.htm)|Toxic Fumes|Fumées toxiques|officielle|
|[zDBvkCFJMYUXSbxN.htm](agents-of-edgewatch-bestiary-items/zDBvkCFJMYUXSbxN.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[zdKpdDk0vI7szcPH.htm](agents-of-edgewatch-bestiary-items/zdKpdDk0vI7szcPH.htm)|Dagger|Dague|officielle|
|[zdtAExquN9fICX40.htm](agents-of-edgewatch-bestiary-items/zdtAExquN9fICX40.htm)|Telepathy 100 feet|Télépathie à 30 mètres|officielle|
|[Ze4gxM4HMIEzvJgg.htm](agents-of-edgewatch-bestiary-items/Ze4gxM4HMIEzvJgg.htm)|Main-Gauche|Main-Gauche|officielle|
|[ZEbYuhDFGc3fADE1.htm](agents-of-edgewatch-bestiary-items/ZEbYuhDFGc3fADE1.htm)|Sneak Attack|Attaque sournoise|officielle|
|[zeH3OrBLioIRoYb5.htm](agents-of-edgewatch-bestiary-items/zeH3OrBLioIRoYb5.htm)|Deny Advantage|Refus d'avantage|officielle|
|[zehVjK1f2C0jpnAB.htm](agents-of-edgewatch-bestiary-items/zehVjK1f2C0jpnAB.htm)|Innate Occult Spells|Sorts occultes innés|libre|
|[ZFx4kB8JAYfSX4xr.htm](agents-of-edgewatch-bestiary-items/ZFx4kB8JAYfSX4xr.htm)|Extend Legs|Jambes extensibles|officielle|
|[zghmY4aHHsaTIPLa.htm](agents-of-edgewatch-bestiary-items/zghmY4aHHsaTIPLa.htm)|Quick Alchemy|Alchimie rapide|officielle|
|[zGvdZdakNLZQ0BMF.htm](agents-of-edgewatch-bestiary-items/zGvdZdakNLZQ0BMF.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[ZHs955g8FhuO2EvW.htm](agents-of-edgewatch-bestiary-items/ZHs955g8FhuO2EvW.htm)|Shield Block|Blocage au bouclier|officielle|
|[Zi59HbzpU7nXEarp.htm](agents-of-edgewatch-bestiary-items/Zi59HbzpU7nXEarp.htm)|Fast Healing 20|Guérison accélérée 20|officielle|
|[zJ4cmL8xK1QGb5IF.htm](agents-of-edgewatch-bestiary-items/zJ4cmL8xK1QGb5IF.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[ZjdHv1h2KMIjC0bA.htm](agents-of-edgewatch-bestiary-items/ZjdHv1h2KMIjC0bA.htm)|Prepared Arcane Spells|Sorts arcaniques préparés|libre|
|[ZJvsVjbPWteUphTK.htm](agents-of-edgewatch-bestiary-items/ZJvsVjbPWteUphTK.htm)|Concussive Beatdown|Coup assommant|officielle|
|[ZkE9BJn0zrQtzRMW.htm](agents-of-edgewatch-bestiary-items/ZkE9BJn0zrQtzRMW.htm)|Writhing Arms|Membres grouillants|officielle|
|[zKzd8af55LIetqCL.htm](agents-of-edgewatch-bestiary-items/zKzd8af55LIetqCL.htm)|Poison Lore|Connaissance des poisons|officielle|
|[zMxrSkK2U9CDEp0u.htm](agents-of-edgewatch-bestiary-items/zMxrSkK2U9CDEp0u.htm)|Bone Drink|Boire les os|officielle|
|[zn2a2GvxqbknqXbo.htm](agents-of-edgewatch-bestiary-items/zn2a2GvxqbknqXbo.htm)|Entropy Sense (Imprecise) 60 feet|Perception de l'entropie (imprécis) 18 m|officielle|
|[ZNWERIfXN1fj8u4P.htm](agents-of-edgewatch-bestiary-items/ZNWERIfXN1fj8u4P.htm)|Chain Expert|Experte des chaînes|officielle|
|[ZoDu12928nPzUoAD.htm](agents-of-edgewatch-bestiary-items/ZoDu12928nPzUoAD.htm)|Zealous Restoration|Restauration zélée|officielle|
|[zqI0OQ5w5KAQApsl.htm](agents-of-edgewatch-bestiary-items/zqI0OQ5w5KAQApsl.htm)|Shortsword|Épée courte|officielle|
|[ZtFghl2g5noJE7Cw.htm](agents-of-edgewatch-bestiary-items/ZtFghl2g5noJE7Cw.htm)|Darkvision|Vision dans le noir|officielle|
|[ztgpLrKdJjctex5J.htm](agents-of-edgewatch-bestiary-items/ztgpLrKdJjctex5J.htm)|Pollution Infusion|Imprégnation de pollution|officielle|
|[zvkxJ9ZreNjX3yJL.htm](agents-of-edgewatch-bestiary-items/zvkxJ9ZreNjX3yJL.htm)|Flurry of Pods|Déluge de pseudopodes|officielle|
|[ZX0T6lbLJyKn8KuN.htm](agents-of-edgewatch-bestiary-items/ZX0T6lbLJyKn8KuN.htm)|Unbalancing Blow|Coup déséquilibrant|officielle|
|[ZX4MxqemrRYlaJEf.htm](agents-of-edgewatch-bestiary-items/ZX4MxqemrRYlaJEf.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[zX6RvKVyn16ug6vJ.htm](agents-of-edgewatch-bestiary-items/zX6RvKVyn16ug6vJ.htm)|Change Shape|Changement de forme|officielle|
|[zxw1AsQiZy4HbIBa.htm](agents-of-edgewatch-bestiary-items/zxw1AsQiZy4HbIBa.htm)|Motion Sense 100 feet|Perception du mouvement 30 m|officielle|
