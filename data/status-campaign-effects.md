# État de la traduction (campaign-effects)

 * **aucune**: 23


Dernière mise à jour: 2023-03-05 17:26 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[4vnF6BFM4Xg4Eg0k.htm](campaign-effects/4vnF6BFM4Xg4Eg0k.htm)|Effect: Improved reflexes|
|[5DBYunGL2HV8aekO.htm](campaign-effects/5DBYunGL2HV8aekO.htm)|Resonant Reflection: Reflection of Stone|
|[8eMGYNilWV3dFAUI.htm](campaign-effects/8eMGYNilWV3dFAUI.htm)|Resonant Reflection: Reflection of Life|
|[cESIlNrgjo1uW71D.htm](campaign-effects/cESIlNrgjo1uW71D.htm)|Mixed Drink: Guidance|
|[CS1XbH7GYfP6Ve61.htm](campaign-effects/CS1XbH7GYfP6Ve61.htm)|Effect: Burned Mouth and Throat (No Singing or Yelling)|
|[DOxl0FDd21VMDOMP.htm](campaign-effects/DOxl0FDd21VMDOMP.htm)|Effect: Reflection of Life (Fast Healing)|
|[e8K0YTKxzf0bhayh.htm](campaign-effects/e8K0YTKxzf0bhayh.htm)|Mixed Drink: Luck|
|[j8hA7CsmSY90tBqw.htm](campaign-effects/j8hA7CsmSY90tBqw.htm)|Mixed Drink: Prestidigitation|
|[JnQi8whNqY7sPW7x.htm](campaign-effects/JnQi8whNqY7sPW7x.htm)|Mixed Drink: Dancing Lights|
|[Luti1qjFOKYwZio0.htm](campaign-effects/Luti1qjFOKYwZio0.htm)|Effect: Extreme stomach cramps|
|[lZ4DA81o4kbL8nve.htm](campaign-effects/lZ4DA81o4kbL8nve.htm)|Effect: Burned Tongue (Linguistic)|
|[meTIFa2VsIzRVywE.htm](campaign-effects/meTIFa2VsIzRVywE.htm)|Effect: Hope or Despair (Critical Success)|
|[NwF6m7SyaqwjdQxT.htm](campaign-effects/NwF6m7SyaqwjdQxT.htm)|Effect: Manipulate Luck - Drusilla (Good)|
|[nwFhhgZRggDoDgdk.htm](campaign-effects/nwFhhgZRggDoDgdk.htm)|Effect: Burned Mouth and Throat (Linguistic)|
|[Px7sSipQxHdOMSjk.htm](campaign-effects/Px7sSipQxHdOMSjk.htm)|Effect: Hope or Despair (Failure or Critical Failure)|
|[QWW4hNCtLkOK1k71.htm](campaign-effects/QWW4hNCtLkOK1k71.htm)|Effect: Immediate and Intense Headache|
|[Rz35d02dUtAjKtMB.htm](campaign-effects/Rz35d02dUtAjKtMB.htm)|Effect: Heightened awareness|
|[scgEAXAqHEr3Egeb.htm](campaign-effects/scgEAXAqHEr3Egeb.htm)|Resonant Reflection: Reflection of Water|
|[SvR7Ez1lfnN4You5.htm](campaign-effects/SvR7Ez1lfnN4You5.htm)|Geb's Blessing|
|[uDQw7YPMiKPXCbaV.htm](campaign-effects/uDQw7YPMiKPXCbaV.htm)|Resonant Reflection: Reflection of Light|
|[UXBOvlJbnI76UoGp.htm](campaign-effects/UXBOvlJbnI76UoGp.htm)|Resonant Reflection: Reflection of Storm|
|[XOE1gp3phFyxL4Dq.htm](campaign-effects/XOE1gp3phFyxL4Dq.htm)|Effect: Burned Tongue (No Singing or Yelling)|
|[yJWWTfZkAF4raa4R.htm](campaign-effects/yJWWTfZkAF4raa4R.htm)|Effect: Keen insight|

## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
