# État de la traduction (equipment-effects)

 * **libre**: 342
 * **vide**: 6
 * **aucune**: 59


Dernière mise à jour: 2023-03-05 17:26 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[9mIS76oZkxXQ4g3T.htm](equipment-effects/9mIS76oZkxXQ4g3T.htm)|Effect: Jolt Coil - Armor|
|[bMZ6Gw58sK8sFp5n.htm](equipment-effects/bMZ6Gw58sK8sFp5n.htm)|Effect: Crackling Bubble Gum (Failure)|
|[ceIvpmxWqBJpBHIn.htm](equipment-effects/ceIvpmxWqBJpBHIn.htm)|Effect: Jolt Coil - Weapon (Major)|
|[DaN3N9bOzqDhOng0.htm](equipment-effects/DaN3N9bOzqDhOng0.htm)|Effect: Pickled Demon Tongue - Weapon (Major)|
|[dfnhwI5pJgLtXh2k.htm](equipment-effects/dfnhwI5pJgLtXh2k.htm)|Effect: Silversheen|
|[ECGvrM0eAaJlm1VC.htm](equipment-effects/ECGvrM0eAaJlm1VC.htm)|Effect: Desolation Locket - Armor (Greater)|
|[EjLVjt3GMeHM0Ai3.htm](equipment-effects/EjLVjt3GMeHM0Ai3.htm)|Effect: Ghostcaller's Planchette - Armor|
|[EPqnA5OlNpwr41Os.htm](equipment-effects/EPqnA5OlNpwr41Os.htm)|Effect: Beastmaster's Sigil - Ranged Weapon|
|[EwHufLQI1z1QzqZU.htm](equipment-effects/EwHufLQI1z1QzqZU.htm)|Effect: Jolt Coil - Weapon (Greater)|
|[ft5LjQSa8mZkklhM.htm](equipment-effects/ft5LjQSa8mZkklhM.htm)|Effect: Polished Demon Horn - Armor|
|[gaEXUewHgPpM3zfW.htm](equipment-effects/gaEXUewHgPpM3zfW.htm)|Effect: Jyoti's Feather - Armor (Greater)|
|[grXFmNl8Zy3VRVpR.htm](equipment-effects/grXFmNl8Zy3VRVpR.htm)|Effect: Ghostcaller's Planchette - Weapon|
|[gU3uZE2ihLnpQN0b.htm](equipment-effects/gU3uZE2ihLnpQN0b.htm)|Effect: Beastmaster's Sigil - Melee Weapon (Greater)|
|[GUHNFlNYiR38sTDE.htm](equipment-effects/GUHNFlNYiR38sTDE.htm)|Effect: Crackling Bubble Gum|
|[gZOED4T3o6giterN.htm](equipment-effects/gZOED4T3o6giterN.htm)|Effect: Beastmaster's Sigil - Armor (Greater)|
|[hozXQvKqp62DnawX.htm](equipment-effects/hozXQvKqp62DnawX.htm)|Effect: Jyoti's Feather - Armor|
|[i5agc4lBE6GfeCXq.htm](equipment-effects/i5agc4lBE6GfeCXq.htm)|Effect: Cold Iron Blanch (Lesser)|
|[IlNjAwsIZShlVsCT.htm](equipment-effects/IlNjAwsIZShlVsCT.htm)|Effect: Architect's Pattern Book|
|[inqXnzrqYzbUBuOj.htm](equipment-effects/inqXnzrqYzbUBuOj.htm)|Effect: Diplomat's Charcuterie|
|[iOY4XqVZNiQ5esdu.htm](equipment-effects/iOY4XqVZNiQ5esdu.htm)|Effect: Pickled Demon Tongue - Armor (Major)|
|[KJXNLvJAl0mNnGvn.htm](equipment-effects/KJXNLvJAl0mNnGvn.htm)|Effect: Jyoti's Feather - Weapon|
|[LbaYzs0dQuFj8FXJ.htm](equipment-effects/LbaYzs0dQuFj8FXJ.htm)|Effect: Pickled Demon Tongue - Weapon|
|[LjaEu7gAGO77uVs2.htm](equipment-effects/LjaEu7gAGO77uVs2.htm)|Effect: Hexing Jar|
|[M1HKNPpqkjFI9A4q.htm](equipment-effects/M1HKNPpqkjFI9A4q.htm)|Effect: Beastmaster's Sigil - Armor|
|[mHIdEC7RX6isILiM.htm](equipment-effects/mHIdEC7RX6isILiM.htm)|Effect: Jolt Coil - Weapon|
|[NBxTbdCCqmilAxqA.htm](equipment-effects/NBxTbdCCqmilAxqA.htm)|Effect: Whispering Staff (Enemy)|
|[NddLhLIQYgZYrPTR.htm](equipment-effects/NddLhLIQYgZYrPTR.htm)|Effect: Pickled Demon Tongue - Weapon (Greater)|
|[NwEVRZmLbM9QKoIH.htm](equipment-effects/NwEVRZmLbM9QKoIH.htm)|Effect: Desolation Locket - Weapon|
|[OFJVaPxdafc4ezWB.htm](equipment-effects/OFJVaPxdafc4ezWB.htm)|Effect: Cold Iron Blanch (Greater)|
|[P882YXPpESinSvrJ.htm](equipment-effects/P882YXPpESinSvrJ.htm)|Effect: Polished Demon Horn - Weapon (Major)|
|[pgWhwSGZd8JT5IlF.htm](equipment-effects/pgWhwSGZd8JT5IlF.htm)|Effect: Ghostcaller's Planchette - Weapon (Greater)|
|[pnBdSjOtQb9T1ajL.htm](equipment-effects/pnBdSjOtQb9T1ajL.htm)|Effect: Jolt Coil - Armor (Major)|
|[pr12dSHV4nIyVG5n.htm](equipment-effects/pr12dSHV4nIyVG5n.htm)|Effect: Polished Demon Horn - Armor (Greater)|
|[QCqYiSIR9DVPAHgR.htm](equipment-effects/QCqYiSIR9DVPAHgR.htm)|Effect: Assassin Vine Wine|
|[qFT6TJU5EObpoixe.htm](equipment-effects/qFT6TJU5EObpoixe.htm)|Effect: Ghostcaller's Planchette - Armor (Greater)|
|[qLl1jwybXY6EbOoI.htm](equipment-effects/qLl1jwybXY6EbOoI.htm)|Effect: Bewitching Bloom (Magnolia)|
|[rdHzCYZEWpy2rTfI.htm](equipment-effects/rdHzCYZEWpy2rTfI.htm)|Effect: Beastmaster's Sigil - Melee Weapon|
|[Rfw1T5NXIoeUbJzt.htm](equipment-effects/Rfw1T5NXIoeUbJzt.htm)|Effect: Polished Demon Horn - Armor (Major)|
|[ri5qxyVViva60ilN.htm](equipment-effects/ri5qxyVViva60ilN.htm)|Effect: Bewitching Bloom (Lotus)|
|[rQV8Azb3FeUJJ3fG.htm](equipment-effects/rQV8Azb3FeUJJ3fG.htm)|Effect: Delve Scale|
|[RxtpVyOywdrt29Q6.htm](equipment-effects/RxtpVyOywdrt29Q6.htm)|Effect: Desolation Locket - Armor (Major)|
|[T38SHe842S43a8bB.htm](equipment-effects/T38SHe842S43a8bB.htm)|Effect: Beastmaster's Sigil - Ranged Weapon (Major)|
|[tNaFPSbNkcyHS50y.htm](equipment-effects/tNaFPSbNkcyHS50y.htm)|Effect: Polished Demon Horn - Weapon (Greater)|
|[TU67AK08CUsP7pl4.htm](equipment-effects/TU67AK08CUsP7pl4.htm)|Effect: Beastmaster's Sigil - Ranged Weapon (Greater)|
|[uHZ23fBG9HIdK5ht.htm](equipment-effects/uHZ23fBG9HIdK5ht.htm)|Effect: Tremorsensors|
|[uijpoXaiKXcCYrSD.htm](equipment-effects/uijpoXaiKXcCYrSD.htm)|Effect: Auric Noodles|
|[uK2vXk4WnleihqYI.htm](equipment-effects/uK2vXk4WnleihqYI.htm)|Effect: Cold Iron Blanch (Moderate)|
|[UPMZe0oKVpUgDaOE.htm](equipment-effects/UPMZe0oKVpUgDaOE.htm)|Effect: Pickled Demon Tongue - Armor (Greater)|
|[VKdiRnhrsgQTFSCM.htm](equipment-effects/VKdiRnhrsgQTFSCM.htm)|Effect: Whispering Staff (Ally)|
|[VVXjPCummVHQp7hG.htm](equipment-effects/VVXjPCummVHQp7hG.htm)|Effect: Bloodhound Olfactory Stimulators|
|[WARLTi8unmPgmnNw.htm](equipment-effects/WARLTi8unmPgmnNw.htm)|Effect: Polished Demon Horn - Weapon|
|[wcjEjFKLcPisk4jK.htm](equipment-effects/wcjEjFKLcPisk4jK.htm)|Effect: Jyoti's Feather - Weapon (Major)|
|[WGmrfhdQzlNzyMrq.htm](equipment-effects/WGmrfhdQzlNzyMrq.htm)|Effect: Jyoti's Feather - Weapon (Greater)|
|[WJ9L6rgUTZVV7vEE.htm](equipment-effects/WJ9L6rgUTZVV7vEE.htm)|Effect: Desolation Locket - Armor|
|[WRvZ2Nq3wquisD4Y.htm](equipment-effects/WRvZ2Nq3wquisD4Y.htm)|Effect: Pickled Demon Tongue - Armor|
|[xSw7cTboMvP8sJAq.htm](equipment-effects/xSw7cTboMvP8sJAq.htm)|Effect: Beastmaster's Sigil - Melee Weapon (Major)|
|[YAZ1iri403S8XcrH.htm](equipment-effects/YAZ1iri403S8XcrH.htm)|Effect: Jyoti's Feather - Armor (Major)|
|[YflZZ7EG7JJkdX0d.htm](equipment-effects/YflZZ7EG7JJkdX0d.htm)|Effect: Jolt Coil - Armor (Greater)|
|[yzENPvcYIxegPflt.htm](equipment-effects/yzENPvcYIxegPflt.htm)|Effect: Beastmaster's Sigil - Armor (Major)|

## Liste des éléments vides ne pouvant pas être traduits

| Fichier   | Nom (EN)    | État |
|-----------|-------------|:----:|
|[8YZX34sJOIH32VwI.htm](equipment-effects/8YZX34sJOIH32VwI.htm)|Effect: Illuminated Folio|vide|
|[GqXIV46JqB8x8eEN.htm](equipment-effects/GqXIV46JqB8x8eEN.htm)|Effect: Book of Warding Prayers|vide|
|[jlYPMOHplgkvzLa9.htm](equipment-effects/jlYPMOHplgkvzLa9.htm)|Effect: Standard of the Primeval Howl|vide|
|[N54jx6GEz2NpGobK.htm](equipment-effects/N54jx6GEz2NpGobK.htm)|Effect: Breastplate of the Mountain|vide|
|[PT1g0Ar47FVo2O4D.htm](equipment-effects/PT1g0Ar47FVo2O4D.htm)|Effect: Batsbreath Cane|vide|
|[yykiQBIGqwxIDRZq.htm](equipment-effects/yykiQBIGqwxIDRZq.htm)|Effect: Viper Rapier|vide|

## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0qJEtpXWPb7JJBbY.htm](equipment-effects/0qJEtpXWPb7JJBbY.htm)|Effect: Aromatic Ammunition|Effet : Munition aromatique|libre|
|[0YbNzbW0HSKtgStQ.htm](equipment-effects/0YbNzbW0HSKtgStQ.htm)|Effect: Dragon's Blood Pudding (Major)|Effet : Poudding de sang de dragon majeur|libre|
|[16tOZk4qy329s2aK.htm](equipment-effects/16tOZk4qy329s2aK.htm)|Effect: Shielding Salve|Effet : Onguent de bouclier|libre|
|[19ECULG5Zp593jQX.htm](equipment-effects/19ECULG5Zp593jQX.htm)|Effect: Dragon's Blood Pudding (Moderate)|Effet : Poudding de sang de dragon modéré|libre|
|[1ihy7Jvw5PY4WYbP.htm](equipment-effects/1ihy7Jvw5PY4WYbP.htm)|Effect: Eye of the Unseen (Greater)|Effet : Oeil de l'invisible supérieur|libre|
|[1l139A2Qik4lBHKO.htm](equipment-effects/1l139A2Qik4lBHKO.htm)|Effect: Juggernaut Mutagen (Lesser)|Effet : Mutagène de juggernaut inférieur|libre|
|[1mKjaWC65KWPuFR4.htm](equipment-effects/1mKjaWC65KWPuFR4.htm)|Effect: Antidote (Major)|Effet : Antidote majeur|libre|
|[1N28rGPbAl2IkGUf.htm](equipment-effects/1N28rGPbAl2IkGUf.htm)|Effect: Slime Whip|Effet : Fouet gluant|libre|
|[1ouUo8lLK6H79Rqh.htm](equipment-effects/1ouUo8lLK6H79Rqh.htm)|Effect: Bestial Mutagen (Major)|Effet : Mutagène bestial majeur|libre|
|[1S51uIRb9bnZtpFU.htm](equipment-effects/1S51uIRb9bnZtpFU.htm)|Effect: Winged Boots|Effet : Bottes ailées|libre|
|[1tweTwYQuQUV45wJ.htm](equipment-effects/1tweTwYQuQUV45wJ.htm)|Effect: Rowan Rifle (Cold)|Effet : Fusil de Rowan (froid)|libre|
|[1xHHvQlW4pRR89qj.htm](equipment-effects/1xHHvQlW4pRR89qj.htm)|Effect: Stone Body Mutagen (Moderate)|Effet : Mutagène corps-de-pierre modéré|libre|
|[2Bds6d4UGQZqYSZM.htm](equipment-effects/2Bds6d4UGQZqYSZM.htm)|Effect: Quicksilver Mutagen (Greater)|Effet : Mutagène de vif-argent supérieur|libre|
|[2C1HuKDQDGFZuv7l.htm](equipment-effects/2C1HuKDQDGFZuv7l.htm)|Effect: Boulderhead Bock|Effet : Bock têtederoc|libre|
|[2iR5uP6vgPzgKKNO.htm](equipment-effects/2iR5uP6vgPzgKKNO.htm)|Effect: Red-Rib Gill Mask (Moderate)|Effet : Masque branchies de côte rouge modéré|libre|
|[2PNo8u4wxSbz5WEs.htm](equipment-effects/2PNo8u4wxSbz5WEs.htm)|Effect: Juggernaut Mutagen (Major)|Effet : Mutagène de juggernaut majeur|libre|
|[2YgXoHvJfrDHucMr.htm](equipment-effects/2YgXoHvJfrDHucMr.htm)|Effect: Raise a Shield|Effet : Lever un bouclier|libre|
|[2ytxPqhGyLtEjYxW.htm](equipment-effects/2ytxPqhGyLtEjYxW.htm)|Effect: Static Snare|Effet : piège artisanal statique|libre|
|[3LhreroLRmI4atE6.htm](equipment-effects/3LhreroLRmI4atE6.htm)|Effect: Clockwork Cloak|Effet : Cape mécanique|libre|
|[3O5lvuX4VHqtpCkU.htm](equipment-effects/3O5lvuX4VHqtpCkU.htm)|Effect: Lover's Gloves|Effet : Gants de l'amant|libre|
|[4aSqtBgvQr2TI3XT.htm](equipment-effects/4aSqtBgvQr2TI3XT.htm)|Effect: Grit (Stage 2)|Effet : Grit (stade 2)|libre|
|[4G9qnI0oRyL6eKFQ.htm](equipment-effects/4G9qnI0oRyL6eKFQ.htm)|Effect: Frost Vial (Major)|Effet : Fiole givrée majeure|libre|
|[4JULykNCgQoypsu8.htm](equipment-effects/4JULykNCgQoypsu8.htm)|Effect: Spiderfoot Brew (Major)|Effet : Infusion de pattes d'araignée majeure|libre|
|[4RnEUeYEzC919GZR.htm](equipment-effects/4RnEUeYEzC919GZR.htm)|Effect: Energizing Rune (Sonic)|Effet : Rune énergisante (son)|libre|
|[4tepFOJLhZSelPoa.htm](equipment-effects/4tepFOJLhZSelPoa.htm)|Effect: Dragon Turtle Scale|Effet : Écaille de tortue dragon|libre|
|[4uy4Ygf5KD2WrtGW.htm](equipment-effects/4uy4Ygf5KD2WrtGW.htm)|Effect: Nosoi Charm (Diplomacy)|Effet : Charme de nosoi (Diplomatie)|libre|
|[5dhm66yN0LQTOePw.htm](equipment-effects/5dhm66yN0LQTOePw.htm)|Effect: Holy Steam Ball|Effet : Bille de vapeur sacrée|libre|
|[5Gof60StUppR2Xn9.htm](equipment-effects/5Gof60StUppR2Xn9.htm)|Effect: Skeptic's Elixir (Lesser)|Effet : Élixir de l'incrédule inférieur|libre|
|[5JYchreCttBg7RcD.htm](equipment-effects/5JYchreCttBg7RcD.htm)|Effect: Goo Grenade|Effet : Grenade gluante|libre|
|[5KXsyN9J78glG25I.htm](equipment-effects/5KXsyN9J78glG25I.htm)|Effect: Ochre Fulcrum Lens|Effet : Lentille à pivot ocre|libre|
|[5lZWAvm0oGxvF4bm.htm](equipment-effects/5lZWAvm0oGxvF4bm.htm)|Effect: Exsanguinating Ammunition (Greater)|Effet : Munition exsangue supérieure|libre|
|[5o33sch67Z8j5Vom.htm](equipment-effects/5o33sch67Z8j5Vom.htm)|Effect: Lastwall Soup (Improved)|Effet : Soupe de Dernier-Rempart améliorée|libre|
|[5OABp099y6w3didN.htm](equipment-effects/5OABp099y6w3didN.htm)|Effect: Soulspark Candle|Effet : Cierge lueur d'âme|libre|
|[5oYKYXAexr0vhx84.htm](equipment-effects/5oYKYXAexr0vhx84.htm)|Effect: Treat Disease (Critical Failure)|Effet : Soigner les blessures (échec critique)|libre|
|[5uK3fmGlfJrbWQz4.htm](equipment-effects/5uK3fmGlfJrbWQz4.htm)|Effect: Stalwart's Ring|Effet : Anneau de résolution|libre|
|[5WLda1tGUiKoSj1K.htm](equipment-effects/5WLda1tGUiKoSj1K.htm)|Effect: Stalk Goggles (Major)|Effet : Lunettes à tiges majeures|libre|
|[5xgapIXn5DwbXHKh.htm](equipment-effects/5xgapIXn5DwbXHKh.htm)|Effect: Serene Mutagen (Lesser)|Effet : Mutagène de sérénité inférieur|libre|
|[68xcDyxsNgD3JddD.htm](equipment-effects/68xcDyxsNgD3JddD.htm)|Effect: Energizing Rune (Cold)|Effet : Rune énergisante de froid|libre|
|[6A8jsLR7upLGuRiv.htm](equipment-effects/6A8jsLR7upLGuRiv.htm)|Effect: Lastwall Soup|Effet : Soupe de Dernier-Rempart|libre|
|[6alqXfVq0qWQC359.htm](equipment-effects/6alqXfVq0qWQC359.htm)|Effect: Energy Robe of Fire|Effet : Robe d'énergie de feu|libre|
|[6dsPjRKjCPd9BWPt.htm](equipment-effects/6dsPjRKjCPd9BWPt.htm)|Effect: Greater Healer's Gel|Effet : Gel du guérisseur supérieur|libre|
|[6p2Sjl7XxCc55ft4.htm](equipment-effects/6p2Sjl7XxCc55ft4.htm)|Effect: Mudrock Snare (Success)|Effet : Piège artisanal de boue solidifiée (Succès)|libre|
|[6PNLBIdlqqWNCFMy.htm](equipment-effects/6PNLBIdlqqWNCFMy.htm)|Effect: Quicksilver Mutagen (Lesser)|Effet : Mutagène de vif-argent inférieur|libre|
|[7dLsA9PAb5ij7Bc6.htm](equipment-effects/7dLsA9PAb5ij7Bc6.htm)|Effect: Dueling Cape|Effet : Cape de duel|libre|
|[7MgpgF8tOXOiDEwv.htm](equipment-effects/7MgpgF8tOXOiDEwv.htm)|Effect: Vaultbreaker's Harness|Effet : Harnais du brise-coffre|libre|
|[7UL8belWmo7U5YGM.htm](equipment-effects/7UL8belWmo7U5YGM.htm)|Effect: Darkvision Elixir (Lesser)|Effet : Élixir de vision dans le noir inférieur|libre|
|[7vCenP9j6FuHRv5C.htm](equipment-effects/7vCenP9j6FuHRv5C.htm)|Effect: Darkvision Elixir (Greater)|Effet : Élixir de Vision dans le noir supérieur|libre|
|[7z1iY4AaNEAIKuAU.htm](equipment-effects/7z1iY4AaNEAIKuAU.htm)|Effect: Antidote (Lesser)|Effet : Antidote inférieur|libre|
|[88kqcDmsoAEddzUt.htm](equipment-effects/88kqcDmsoAEddzUt.htm)|Effect: Boots of Elvenkind|Effet : Bottes elfiques|libre|
|[8ersuvNJXX00XaIQ.htm](equipment-effects/8ersuvNJXX00XaIQ.htm)|Effect: Euryale (Curse) Card|Effet : Carte Euryale (Malédiction)|libre|
|[8kfSF8P4NOh09YvZ.htm](equipment-effects/8kfSF8P4NOh09YvZ.htm)|Effect: Grim Sandglass - Weapon (Greater)|Effet : Sablier sinistre supérieur - arme|libre|
|[8RNPIAuV7ixaXeq5.htm](equipment-effects/8RNPIAuV7ixaXeq5.htm)|Effect: War Blood Mutagen (Greater)|Effet : Mutagène de sang guerrier supérieur|libre|
|[988f6NpOo4YzFzIr.htm](equipment-effects/988f6NpOo4YzFzIr.htm)|Effect: Quicksilver Mutagen (Major)|Effet : Mutagène de vif-argent majeur|libre|
|[9BsFdrEc7hkPWgSd.htm](equipment-effects/9BsFdrEc7hkPWgSd.htm)|Effect: Energizing Rune (Electricity)|Effet : Rune d'énergie d'électricité|libre|
|[9e6iVkPpGqJYwMyb.htm](equipment-effects/9e6iVkPpGqJYwMyb.htm)|Effect: Brewer's Regret|Effet : Regret du brasseur|libre|
|[9FfFhu2kl2wMTsiI.htm](equipment-effects/9FfFhu2kl2wMTsiI.htm)|Effect: Silvertongue Mutagen (Major)|Effet : Mutagène de langue dorée majeur|libre|
|[9j1uTGBGAc7GIhjm.htm](equipment-effects/9j1uTGBGAc7GIhjm.htm)|Effect: Dragonfly Potion|Effet : Potion libellule|libre|
|[9keegq0GdS1eSrNr.htm](equipment-effects/9keegq0GdS1eSrNr.htm)|Effect: Sea Touch Elixir (Moderate)|Effet: Élixir de caresse marine modéré|libre|
|[9kOgG7BPEfIyWyqm.htm](equipment-effects/9kOgG7BPEfIyWyqm.htm)|Effect: Rowan Rifle (Electricity)|Effet : Fusil de Rowan (électricité)|libre|
|[9MeHc072G4L8AJkp.htm](equipment-effects/9MeHc072G4L8AJkp.htm)|Effect: Elixir of Life (True)|Effet : Élixir de vie ultime|libre|
|[9PASRixhNM0ogqmG.htm](equipment-effects/9PASRixhNM0ogqmG.htm)|Effect: Triton's Conch|Effet : Conque de triton|libre|
|[agDVcRyoS4NTHkht.htm](equipment-effects/agDVcRyoS4NTHkht.htm)|Effect: Trinity Geode - Armor (Major)|Effet : Géode de la trinité majeure - Armure|libre|
|[ah41XCrV4LFsVyzl.htm](equipment-effects/ah41XCrV4LFsVyzl.htm)|Effect: Shield of the Unified Legion|Effet : Bouclier de la Légion unifiée|libre|
|[aIZsC56OdotiGb9M.htm](equipment-effects/aIZsC56OdotiGb9M.htm)|Effect: War Blood Mutagen (Lesser)|Effet : Mutagène de sang guerrier inférieur|libre|
|[AJx8i8QX35vsG5Q4.htm](equipment-effects/AJx8i8QX35vsG5Q4.htm)|Effect: Stonethroat Ammunition (Success)|Effet : Munition coince-gueule (Succès)|libre|
|[AMhUb42NAJ1aisZp.htm](equipment-effects/AMhUb42NAJ1aisZp.htm)|Effect: Stone Fist Elixir|Effet : Élixir de poing de pierre|libre|
|[ApGnHnZEK7nv3IqL.htm](equipment-effects/ApGnHnZEK7nv3IqL.htm)|Effect: Greater Codex of Unimpeded Sight|Effet : Codex de vision sans entraves supérieur|libre|
|[ascxqSlMEN9R6OOy.htm](equipment-effects/ascxqSlMEN9R6OOy.htm)|Effect: Energizing Rune (Fire)|Effet : Rune énergisante de feu|libre|
|[AUoiLqENVZlZohsn.htm](equipment-effects/AUoiLqENVZlZohsn.htm)|Effect: Spined Shield Spines|Effet : Pointes de bouclier de la manticore|libre|
|[AvXNZ9I6s1H8C4wd.htm](equipment-effects/AvXNZ9I6s1H8C4wd.htm)|Effect: War Blood Mutagen (Moderate)|Effet : Mutagène de sang guerrier modéré|libre|
|[aXDtl9vMp1vIznya.htm](equipment-effects/aXDtl9vMp1vIznya.htm)|Effect: Eye of the Unseen|Effet : Oeil de l'invisible|libre|
|[b9DTIJyBT8kvIBpj.htm](equipment-effects/b9DTIJyBT8kvIBpj.htm)|Effect: Stone Body Mutagen (Greater)|Effet : Mutagène corps-de-pierre supérieur|libre|
|[bcxVvIbuZWOvsKcA.htm](equipment-effects/bcxVvIbuZWOvsKcA.htm)|Effect: Darkvision Elixir (Moderate)|Effet : Élixir de Vision dans le noir modéré|libre|
|[Bg4hNMqBx0yqmWYJ.htm](equipment-effects/Bg4hNMqBx0yqmWYJ.htm)|Effect: Clockwork Goggles (Major)|Effet : Lunettes mécaniques majeures|libre|
|[bP40jr6wE6MCsRvY.htm](equipment-effects/bP40jr6wE6MCsRvY.htm)|Effect: Golden Legion Epaulet|Effet : Épaulette de la Légion dorée|libre|
|[bri7UVNCfHhCIvXN.htm](equipment-effects/bri7UVNCfHhCIvXN.htm)|Effect: Immolation Clan Pistol|Effet : Pistolet de clan d'immolation|libre|
|[buJnkFBzL4e22ASp.htm](equipment-effects/buJnkFBzL4e22ASp.htm)|Effect: Gecko Potion|Effet : Potion de Gecko|libre|
|[BV8RPntjc9FUzD3g.htm](equipment-effects/BV8RPntjc9FUzD3g.htm)|Effect: Drakeheart Mutagen (Moderate)|Effet : Mutagène de coeur de drake modéré|libre|
|[c0URo81HpSmCkuQc.htm](equipment-effects/c0URo81HpSmCkuQc.htm)|Effect: Energy Robe of Electricity|Effet : Robe d'énergie d'électricité|libre|
|[C9Tnl6Q7Z5Sbw5EY.htm](equipment-effects/C9Tnl6Q7Z5Sbw5EY.htm)|Effect: Energy Mutagen (Lesser)|Effet : Mutagène d'énergie inférieur|libre|
|[ccMa75bqXo3ZnlHM.htm](equipment-effects/ccMa75bqXo3ZnlHM.htm)|Effect: Five-Feather Wreath - Armor (Major)|Effet : Couronne à cinq plumes majeure - Armure|libre|
|[cg5qyeMJUh6b4fta.htm](equipment-effects/cg5qyeMJUh6b4fta.htm)|Effect: Belt of the Five Kings (Wearer)|Effet : Ceinture des cinq rois (Porteur)|libre|
|[CIfqUEC0mITBjwmL.htm](equipment-effects/CIfqUEC0mITBjwmL.htm)|Effect: Sarkorian God-Caller Garb|Effet : Tenue d'invocateur divin Sarkorien|libre|
|[cjQHrvoXDCGOsptN.htm](equipment-effects/cjQHrvoXDCGOsptN.htm)|Effect: Flask of Fellowship|Effet : Gourde de fraternité|libre|
|[ClsVhp5baFRjZQ23.htm](equipment-effects/ClsVhp5baFRjZQ23.htm)|Effect: Energizing Rune (Acid)|Effet : Rune énergisante d'acide|libre|
|[cOcHWeogJFIkEI0d.htm](equipment-effects/cOcHWeogJFIkEI0d.htm)|Effect: Energizing Lattice|Effet : Maille énérgisante|libre|
|[cozi2kUELY40Dcv3.htm](equipment-effects/cozi2kUELY40Dcv3.htm)|Effect: Malleable Mixture (Lesser)|Effet : Mixture de mollesse inférieure|libre|
|[csA4UAD2tQq7RjT8.htm](equipment-effects/csA4UAD2tQq7RjT8.htm)|Effect: Tanglefoot Bag (Greater)|Effet : Sacoche immobilisante supérieure|libre|
|[Cxa7MdgMCUoMqbKm.htm](equipment-effects/Cxa7MdgMCUoMqbKm.htm)|Effect: Bronze Bull Pendant|Effet : Pendentif de taureau de bronze|libre|
|[cy42NXgx1vjYzSxN.htm](equipment-effects/cy42NXgx1vjYzSxN.htm)|Effect: Suit of Armoire Frustration|Effet : Frustration de la panoplie d'armoire|libre|
|[d7BDxmsnM1BUoEeT.htm](equipment-effects/d7BDxmsnM1BUoEeT.htm)|Effect: Goggles of Night (Greater)|Effet : Lunettes de nycatlope supérieures|libre|
|[D7teqZ68L21aZCpd.htm](equipment-effects/D7teqZ68L21aZCpd.htm)|Effect: Glittering Snare (Failure)|Effet : Piège scintillant (Échec)|libre|
|[dchlrZqQ2oEmgNlN.htm](equipment-effects/dchlrZqQ2oEmgNlN.htm)|Effect: Silkspinner's Shield (Animated Strike)|Effet : Bouclier de la fileuse de soie (Frappe animée)|libre|
|[DfAyZW2vkhTygZVC.htm](equipment-effects/DfAyZW2vkhTygZVC.htm)|Effect: Prepared Camouflage Suit (Superb)|Effet : Tenue de camouflage superbe préparée|libre|
|[DlqcczhwjfaEf7G1.htm](equipment-effects/DlqcczhwjfaEf7G1.htm)|Effect: Ablative Armor Plating (Greater)|Effet : Blindage ablatif d'armure supérieur|libre|
|[dpIrjd1UPY7EnWUD.htm](equipment-effects/dpIrjd1UPY7EnWUD.htm)|Effect: Silvertongue Mutagen (Lesser)|Effet : Mutagène de langue dorée inférieur|libre|
|[dv0IKm5syOdP759w.htm](equipment-effects/dv0IKm5syOdP759w.htm)|Effect: Frost Vial (Moderate)|Effet : Fiole givrée modérée|libre|
|[E2uy6gqOXi1HRVBU.htm](equipment-effects/E2uy6gqOXi1HRVBU.htm)|Effect: Clockwork Goggles (Greater)|Effet : Lunettes mécaniques supérieures|libre|
|[e3RzlURndODzBnMt.htm](equipment-effects/e3RzlURndODzBnMt.htm)|Effect: Grit (Stage 1)|Effet : Grit (stade 1)|libre|
|[E4B02mJmNexQLa8F.htm](equipment-effects/E4B02mJmNexQLa8F.htm)|Effect: Inspiring Spotlight|Effet : Projecteur inspirant|libre|
|[e6dXfbKzv5sNr1zh.htm](equipment-effects/e6dXfbKzv5sNr1zh.htm)|Effect: Vermin Repellent Agent (Major)|Effet : Agent répulsif de vermine majeur|libre|
|[Ee2xfKX1yyqGIDZj.htm](equipment-effects/Ee2xfKX1yyqGIDZj.htm)|Effect: Treat Disease (Success)|Effet : Soigner une maladie (succès)|libre|
|[eeGWTG9ZAha4IIOY.htm](equipment-effects/eeGWTG9ZAha4IIOY.htm)|Effect: Cloak of Elvenkind|Effet : Cape elfique|libre|
|[eh7EqmDBDW30ShCu.htm](equipment-effects/eh7EqmDBDW30ShCu.htm)|Effect: Bravo's Brew (Lesser)|Effet : Breuvage de bravoure inférieur|libre|
|[ehYmO1rFBt35zoOw.htm](equipment-effects/ehYmO1rFBt35zoOw.htm)|Effect: Server's Stew|Effet : Ragoût du serveur|libre|
|[eLQABqabYp41Mw1R.htm](equipment-effects/eLQABqabYp41Mw1R.htm)|Effect: Immortal Bastion|Effet : Bastion immortel|libre|
|[eNVSBXuOiAaN152C.htm](equipment-effects/eNVSBXuOiAaN152C.htm)|Effect: Energized Cartridge (Electricity)|Effet : Cartouche énergisée (électricité)|libre|
|[EpB7yJPEuG6ez4z3.htm](equipment-effects/EpB7yJPEuG6ez4z3.htm)|Effect: Elixir of Life (Lesser)|Effet : Élixir de vie inférieur|libre|
|[EpNflrkmWzQ0lEb4.htm](equipment-effects/EpNflrkmWzQ0lEb4.htm)|Effect: Glaive of the Artist|Effet : Glaive de l'artiste|libre|
|[eQqi3tWSHwV4SHqK.htm](equipment-effects/eQqi3tWSHwV4SHqK.htm)|Effect: South Wind's Scorch Song (Speed Boost)|Effet : Chanson brûlante du vent du sud (Boost de vitesse)|libre|
|[eSIYyxi6uTKiP6W5.htm](equipment-effects/eSIYyxi6uTKiP6W5.htm)|Effect: Improvised Weapon|Effet : Arme improvisée|libre|
|[ESuBosh3t1pXEcBj.htm](equipment-effects/ESuBosh3t1pXEcBj.htm)|Effect: Treat Poison (Critical Failure)|Effet : Soigner l'empoisonnement (échec critique)|libre|
|[etJW0w4CiSFgMrWP.htm](equipment-effects/etJW0w4CiSFgMrWP.htm)|Effect: Aeon Stone (Orange Prism) (Nature)|Effect: Pierre d'éternité (Prisme orange) (Nature)|libre|
|[exwQF6E1FWmuxwBc.htm](equipment-effects/exwQF6E1FWmuxwBc.htm)|Effect: Protective Barrier|Effet : Barrière protectrice|libre|
|[f2U0pvTwqrLYyOlC.htm](equipment-effects/f2U0pvTwqrLYyOlC.htm)|Effect: Azarim|Effet : Azarim|libre|
|[F8nQOLVWmpp9G5hZ.htm](equipment-effects/F8nQOLVWmpp9G5hZ.htm)|Effect: Dragon's Blood Pudding (Greater)|Effet : Poudding de sang de dragon supérieur|libre|
|[FbFl95WRpzrrijh3.htm](equipment-effects/FbFl95WRpzrrijh3.htm)|Effect: Aeon Stone (Orange Prism) (Religion)|Effect: Pierre d'éternité (Prisme orange) (Religion)|libre|
|[fbSFwwp60AuDDKpK.htm](equipment-effects/fbSFwwp60AuDDKpK.htm)|Effect: Belt of the Five Kings (Allies)|Effet : Ceinture des cinq rois (Alliés)|libre|
|[fIpzDpuwLdIS4tW5.htm](equipment-effects/fIpzDpuwLdIS4tW5.htm)|Effect: Bestial Mutagen (Lesser)|Effet : Mutagène bestial inférieur|libre|
|[Fngb79C1VDGLJ1EQ.htm](equipment-effects/Fngb79C1VDGLJ1EQ.htm)|Effect: Feyfoul (Lesser)|Effet : Cafouillefée inférieur|libre|
|[FOZXp7QQDnny1600.htm](equipment-effects/FOZXp7QQDnny1600.htm)|Effect: Fire and Iceberg|Effet : Feu et iceberg|libre|
|[fRlvmul3LbLo2xvR.htm](equipment-effects/fRlvmul3LbLo2xvR.htm)|Effect: Parry|Effet : Parade|libre|
|[fuQVJiPPUsvL6fi5.htm](equipment-effects/fuQVJiPPUsvL6fi5.htm)|Effect: Sulfur Bomb (Failure)|Effet : Bombe de soufre (Échec)|libre|
|[fUrZ4xcMJz0CfTyG.htm](equipment-effects/fUrZ4xcMJz0CfTyG.htm)|Effect: Juggernaut Mutagen (Moderate)|Effet : Mutagène de juggernaut modéré|libre|
|[fYe48HmFgfmcqbvL.htm](equipment-effects/fYe48HmFgfmcqbvL.htm)|Effect: Taljjae's Mask (The Hero)|Effet : Masque de Taljjae (le héros)|libre|
|[fYjvLx9DHIdCHdDx.htm](equipment-effects/fYjvLx9DHIdCHdDx.htm)|Effect: Applereed Mutagen (Moderate)|Effet : Mutagène pousse-roseau modéré|libre|
|[fYZIanbYu0Vc4JEL.htm](equipment-effects/fYZIanbYu0Vc4JEL.htm)|Effect: Tanglefoot Bag (Lesser)|Effet : Sacoche immobilisante inférieure|libre|
|[Fz3cSffzDAxhCh2D.htm](equipment-effects/Fz3cSffzDAxhCh2D.htm)|Effect: Exsanguinating Ammunition|Effet : Munition exsangue|libre|
|[G0lG7IIZnCZtYi6v.htm](equipment-effects/G0lG7IIZnCZtYi6v.htm)|Effect: Breastplate of Command|Effet : Cuirasse de commandement|libre|
|[g8JS6wsw5sRWOJLg.htm](equipment-effects/g8JS6wsw5sRWOJLg.htm)|Effect: Stalk Goggles|Effet: Lunettes à tiges|libre|
|[GBBjw61g4ekJymT0.htm](equipment-effects/GBBjw61g4ekJymT0.htm)|Effect: Drakeheart Mutagen (Lesser)|Effet : Mutagène de coeur de drake inférieur|libre|
|[gDefAEEMXVVZgqXH.htm](equipment-effects/gDefAEEMXVVZgqXH.htm)|Effect: Celestial Armor|Effet : Armure céleste|libre|
|[Gj6u2Za5okFlsTvT.htm](equipment-effects/Gj6u2Za5okFlsTvT.htm)|Effect: Deadweight Snare (Failure/Critical Failure)|Effet : Piège du poids-mort (Échec/Échec critique)|libre|
|[GNFNDyx8nfNXrgV6.htm](equipment-effects/GNFNDyx8nfNXrgV6.htm)|Effect: Glittering Snare (Critical Failure)|Effet : Piège scintillant (Échec critique)|libre|
|[h0Zh8tDF9zJBHZXA.htm](equipment-effects/h0Zh8tDF9zJBHZXA.htm)|Effect: Flaming Star - Weapon (Major)|Effet : Étoile enflammée majeure - arme|libre|
|[H29JukjrSpHe5DXR.htm](equipment-effects/H29JukjrSpHe5DXR.htm)|Effect: Impossible Cake|Effet : Gâteau de l'impossible|libre|
|[haywlcUtG6hV1LAy.htm](equipment-effects/haywlcUtG6hV1LAy.htm)|Effect: Trinity Geode - Armor (Greater)|Effet : Géode de la trinité supérieure - Armure|libre|
|[HaZ5LB1wh1LY5wUy.htm](equipment-effects/HaZ5LB1wh1LY5wUy.htm)|Effect: Potion of Minute Echoes|Effet : Potion de l'écho-minute|libre|
|[hD0dUWYKM8FrVDZY.htm](equipment-effects/hD0dUWYKM8FrVDZY.htm)|Effect: Crown of the Kobold King|Effet : Couronne du roi kobold|libre|
|[HeRHBo2NaKy5IxhU.htm](equipment-effects/HeRHBo2NaKy5IxhU.htm)|Effect: Antiplague (Moderate)|Effet : Antimaladie modéré|libre|
|[Hnt3Trd7TiFICB06.htm](equipment-effects/Hnt3Trd7TiFICB06.htm)|Effect: Vermin Repellent Agent (Moderate)|Effet : Agent répulsif de vermine modéré|libre|
|[HoZWmT4yvGso7pHM.htm](equipment-effects/HoZWmT4yvGso7pHM.htm)|Effect: Flaming Star - Weapon (Greater)|Effet : Étoile enflammée supérieure - arme|libre|
|[hPxrIpuL54XRlA2h.htm](equipment-effects/hPxrIpuL54XRlA2h.htm)|Effect: Earplugs|Effet : bouchons d'oreilles|libre|
|[Hx4MOTujp5z6SlQu.htm](equipment-effects/Hx4MOTujp5z6SlQu.htm)|Effect: Arboreal's Revenge (Speed Penalty)|Effet : Vengeance de l'arboréen (pénalité à la vitesse)|libre|
|[hy6LAC13QIJNDYXm.htm](equipment-effects/hy6LAC13QIJNDYXm.htm)|Effect: South Wind's Scorch Song (Damage)|Effet : Chanson brûlante du vent du sud (Dégâts)|libre|
|[i0tm2ZHekp7rGGR3.htm](equipment-effects/i0tm2ZHekp7rGGR3.htm)|Effect: Stole of Civility|Effet : Étole de civilité|libre|
|[id20P4pj7zDKeLmy.htm](equipment-effects/id20P4pj7zDKeLmy.htm)|Effect: Treat Disease (Critical Success)|Effet : Soigner la maladie (succès critique)|libre|
|[iEkH8BKLMUa2wxLX.htm](equipment-effects/iEkH8BKLMUa2wxLX.htm)|Effect: Glamorous Buckler|Effet : Targe étincelante|libre|
|[iK6JeCsZwm5Vakks.htm](equipment-effects/iK6JeCsZwm5Vakks.htm)|Effect: Anklets of Alacrity|Effet : Chaînes de chevilles d'alacrité|libre|
|[IlTS2LTwYTyGXY49.htm](equipment-effects/IlTS2LTwYTyGXY49.htm)|Effect: Feyfoul (Moderate)|Effet : Cafouillefée modéré|libre|
|[ioGzmVSmMGXWWBYb.htm](equipment-effects/ioGzmVSmMGXWWBYb.htm)|Effect: Cloak of the Bat|Effet : Cape de la chauve-souris|libre|
|[ITAFsW3dQPupJ3DW.htm](equipment-effects/ITAFsW3dQPupJ3DW.htm)|Effect: Tanglefoot Bag (Major)|Effet : Sacoche immobilisante majeure|libre|
|[IZkHdaqWBJIIWO7F.htm](equipment-effects/IZkHdaqWBJIIWO7F.htm)|Effect: Ebon Fulcrum Lens (Reaction)|Effet : Lentille à pivot ébène (Réaction)|libre|
|[J0YS8mQsQ1BmT6Xv.htm](equipment-effects/J0YS8mQsQ1BmT6Xv.htm)|Effect: Emberheart|Effet : Coeur de braise|libre|
|[j9zVZwRBVAcnpEkE.htm](equipment-effects/j9zVZwRBVAcnpEkE.htm)|Effect: Cheetah's Elixir (Moderate)|Effet : Élixir du guépard modéré|libre|
|[Ja3PlqLuD9aSaPNZ.htm](equipment-effects/Ja3PlqLuD9aSaPNZ.htm)|Effect: Brewer's Regret (Greater)|Effet : Regret du brasseur supérieur|libre|
|[jaBMZKdoywOTrQvP.htm](equipment-effects/jaBMZKdoywOTrQvP.htm)|Effect: Cognitive Mutagen (Lesser)|Effet : Mutagène cognitif inférieur|libre|
|[JbJykktAYMR4BRav.htm](equipment-effects/JbJykktAYMR4BRav.htm)|Effect: Energy Mutagen (Greater)|Effet : Mutagène d'énergie supérieur|libre|
|[jgaDboqENQJaS1sW.htm](equipment-effects/jgaDboqENQJaS1sW.htm)|Effect: Prepared Camouflage Suit|Effet : Tenue de camouflage préparée|libre|
|[jlVYoiPVRRVGBj5G.htm](equipment-effects/jlVYoiPVRRVGBj5G.htm)|Effect: Ablative Armor Plating (Lesser)|Effet : Blindage ablatif d'armure inférieur|libre|
|[JnnyamqQrAEcyI6F.htm](equipment-effects/JnnyamqQrAEcyI6F.htm)|Effect: Grim Sandglass - Armor (Greater)|Effet : Sablier sinistre supérieur - Armure|libre|
|[JvwzM4rJWwtB9HAP.htm](equipment-effects/JvwzM4rJWwtB9HAP.htm)|Effect: Wolfjaw Armor|Effet : Armure Gueule de loup|libre|
|[jw6Tr9FbErjLAFLQ.htm](equipment-effects/jw6Tr9FbErjLAFLQ.htm)|Effect: Serene Mutagen (Greater)|Effet : Mutagène de sérénité supérieur|libre|
|[K21XQMoDVSPqzRla.htm](equipment-effects/K21XQMoDVSPqzRla.htm)|Effect: Sage's Lash (Turquoise)|Effet : Ceinture du sage (turquoise)|libre|
|[KAEWiyE8TQwofNj9.htm](equipment-effects/KAEWiyE8TQwofNj9.htm)|Effect: Impossible Cake (Greater)|Effet : Gâteau de l'impossible supérieur|libre|
|[KFnOWk5e7nwXT8IE.htm](equipment-effects/KFnOWk5e7nwXT8IE.htm)|Effect: Feyfoul (Greater)|Effet : Cafouillefey (Supérieur)|libre|
|[kgEOxqF1q4Sy6r97.htm](equipment-effects/kgEOxqF1q4Sy6r97.htm)|Effect: Flaming Star - Armor (Major)|Effet : Étoile enflammée majeure - armure|libre|
|[kgotU0sFmtAHYySB.htm](equipment-effects/kgotU0sFmtAHYySB.htm)|Effect: Eagle Eye Elixir (Greater)|Effet : Élixir d'oeil du faucon supérieur|libre|
|[KiurLemTV8GV7OyM.htm](equipment-effects/KiurLemTV8GV7OyM.htm)|Effect: Rowan Rifle (Sonic)|Effet : Fusil de Rowan (sonique)|libre|
|[kkDbalYEavzRpYlp.htm](equipment-effects/kkDbalYEavzRpYlp.htm)|Effect: Antiplague (Lesser)|Effet : Antimaladie inférieur|libre|
|[kMPPl4AqFb6GclOL.htm](equipment-effects/kMPPl4AqFb6GclOL.htm)|Effect: Malleable Mixture (Greater)|Effet : Mixture de mollesse supérieure|libre|
|[KSvkfMqMQ8mlGLiz.htm](equipment-effects/KSvkfMqMQ8mlGLiz.htm)|Effect: Goggles of Night|Effet : Lunettes de nyctalope|libre|
|[kwD0wuW5Ndkc9YXB.htm](equipment-effects/kwD0wuW5Ndkc9YXB.htm)|Effect: Bestial Mutagen (Greater)|Effet : Mutagène bestial supérieur|libre|
|[kwOtHtmlH69ctK0O.htm](equipment-effects/kwOtHtmlH69ctK0O.htm)|Effect: Sun Orchid Poultice|Effet : Cataplasme d'orchidée solaire|libre|
|[kyLLXUQ9zSEvC4py.htm](equipment-effects/kyLLXUQ9zSEvC4py.htm)|Effect: Stalk Goggles (Greater)|Effet : Lunettes à tiges supérieures|libre|
|[lBMhT2W2raYMa8JS.htm](equipment-effects/lBMhT2W2raYMa8JS.htm)|Effect: Spellguard Shield|Effet : Bouclier Gardesort|libre|
|[lgvjbbQiHBGKR3C6.htm](equipment-effects/lgvjbbQiHBGKR3C6.htm)|Effect: Rhino Shot|Effet : Tir de rhino|libre|
|[LH0IDLLF4RsT3KvM.htm](equipment-effects/LH0IDLLF4RsT3KvM.htm)|Effect: Energized Cartridge (Fire)|Effet : Cartouche énergisée (feu)|libre|
|[lLP56tbG689TNKW5.htm](equipment-effects/lLP56tbG689TNKW5.htm)|Effect: Bracelet of Dashing|Effet : Bracelets de fougue|libre|
|[lNWACCNe9RYgaFxb.htm](equipment-effects/lNWACCNe9RYgaFxb.htm)|Effect: Cheetah's Elixir (Lesser)|Effet : Élixir du guépard inférieur|libre|
|[lO95TwgihBdTilAB.htm](equipment-effects/lO95TwgihBdTilAB.htm)|Effect: Thurible of Revelation|Effet : Encensoir de révélation inférieur|libre|
|[lPRuIRbu0rHBkoKY.htm](equipment-effects/lPRuIRbu0rHBkoKY.htm)|Effect: Elixir of Life (Minor)|Effet : Élixir de vie mineur|libre|
|[LVy8SfUF8Jrd6X18.htm](equipment-effects/LVy8SfUF8Jrd6X18.htm)|Effect: Leopard's Armor|Effet : Armure du léopard|libre|
|[M0hhLRC86sASVOk7.htm](equipment-effects/M0hhLRC86sASVOk7.htm)|Effect: Tteokguk of Time Advancement|Effet : Tteokguk du déroulement du temps|libre|
|[M3EFomnN5xArdQmV.htm](equipment-effects/M3EFomnN5xArdQmV.htm)|Effect: Moderate Healer's Gel|Effet : Gel du guérisseur modéré|libre|
|[m4WpxepWRV1u1Kcw.htm](equipment-effects/m4WpxepWRV1u1Kcw.htm)|Effect: Grim Sandglass - Weapon|Effet : Sablier sinistre - Arme|libre|
|[MCny5ohCGf09a7Wl.htm](equipment-effects/MCny5ohCGf09a7Wl.htm)|Effect: Salve of Slipperiness|Effet : Onguent d'insaisissabilité|libre|
|[MEreOgnjoRiXPEuq.htm](equipment-effects/MEreOgnjoRiXPEuq.htm)|Effect: Tanglefoot Bag (Moderate)|Effet : Sacoche immobilisante modéré|libre|
|[Mf9EBLhYmZerf0nS.htm](equipment-effects/Mf9EBLhYmZerf0nS.htm)|Effect: Potion of Flying (Standard)|Effet : Potion de vol de base|libre|
|[mG6S6zm6hxaF7Tla.htm](equipment-effects/mG6S6zm6hxaF7Tla.htm)|Effect: Skeptic's Elixir (Moderate)|Effet : Élixir de l'incrédule modéré|libre|
|[mi4Md1fB2XThCand.htm](equipment-effects/mi4Md1fB2XThCand.htm)|Effect: Antidote (Moderate)|Effet : Antidote modéré|libre|
|[MI5OCkF9IXmD2lPF.htm](equipment-effects/MI5OCkF9IXmD2lPF.htm)|Effect: Bloodhound Mask (Greater)|Effet : Masque du limier supérieur|libre|
|[mkjcgwDBeaOUolVe.htm](equipment-effects/mkjcgwDBeaOUolVe.htm)|Effect: Major Fanged Rune Animal Form|Effet : Forme d'animal de rune de crocs majeur|libre|
|[mn39aML7EWKbttKT.htm](equipment-effects/mn39aML7EWKbttKT.htm)|Effect: Ablative Armor Plating (Moderate)|Effet : Blindage ablatif d'armure modéré|libre|
|[ModBoFdCi7YQU4gP.htm](equipment-effects/ModBoFdCi7YQU4gP.htm)|Effect: Potion of Swimming (Greater)|Effet : Potion de nage supérieure|libre|
|[mQJk8R0vHzvpTz0e.htm](equipment-effects/mQJk8R0vHzvpTz0e.htm)|Effect: Chatterer of Follies|Effet : Bavardeur de folies|libre|
|[mrwg2XftLtSLj197.htm](equipment-effects/mrwg2XftLtSLj197.htm)|Effect: Exsanguinating Ammunition (Major)|Effet : Munition exsangue majeure|libre|
|[na2gf5mSkilFoHXk.htm](equipment-effects/na2gf5mSkilFoHXk.htm)|Effect: Energy Mutagen (Moderate)|Effet : Mutagène d'énergie modéré|libre|
|[Nbgf8zvHimdQqIu6.htm](equipment-effects/Nbgf8zvHimdQqIu6.htm)|Effect: Skyrider Sword|Effet : Chevaucheur d'épée|libre|
|[NdfhpKCjSS80LiUz.htm](equipment-effects/NdfhpKCjSS80LiUz.htm)|Effect: Nosoi Charm (Greater) (Diplomacy)|Effet : Charme nosoi supérieur (Diplomatie)|libre|
|[NE7Fm5YnUhD4ySX3.htm](equipment-effects/NE7Fm5YnUhD4ySX3.htm)|Effect: Earplugs (PFS Guide)|Effet : bouchons d'oreilles (Guide de la PFS)|libre|
|[neZPoQF4hW3A31dd.htm](equipment-effects/neZPoQF4hW3A31dd.htm)|Effect: Lastwall Soup (Greater)|Effet : Soupe de Dernier-Rempart supérieure|libre|
|[nJRoiSyd67eQ1dYj.htm](equipment-effects/nJRoiSyd67eQ1dYj.htm)|Effect: Frost Vial (Greater)|Effet : Fiole givrée supérieure|libre|
|[no7vnIiNBwWjh3w8.htm](equipment-effects/no7vnIiNBwWjh3w8.htm)|Effect: Grasp of Droskar|Effet : Poigne de Droskar|libre|
|[nQ6vM1CRLyvQdGLG.htm](equipment-effects/nQ6vM1CRLyvQdGLG.htm)|Effect: Five-Feather Wreath - Armor|Effet : Couronne à cinq plumes - Armure|libre|
|[NYOi1F9cW3axHrdc.htm](equipment-effects/NYOi1F9cW3axHrdc.htm)|Effect: Deadweight Snare (Success)|Effet : Piège du poids-mort (Succès)|libre|
|[oAewXfq9c0ecaSfw.htm](equipment-effects/oAewXfq9c0ecaSfw.htm)|Effect: Silvertongue Mutagen (Greater)|Effet : Mutagène de langue dorée supérieur|libre|
|[OAN5Fj21PJPhIqRU.htm](equipment-effects/OAN5Fj21PJPhIqRU.htm)|Effect: Vermin Repellent Agent (Lesser)|Effet : Agent répulsif de vermine inférieur|libre|
|[oaR6YGiZKg8a2971.htm](equipment-effects/oaR6YGiZKg8a2971.htm)|Effect: Clockwork Goggles|Effet : Lunettes mécaniques|libre|
|[ohMdE8BmQHuLs40b.htm](equipment-effects/ohMdE8BmQHuLs40b.htm)|Effect: Impossible Cake (Major)|Effet : Gâteau de l'impossible majeur|libre|
|[oiO3cQfqp8MuxR82.htm](equipment-effects/oiO3cQfqp8MuxR82.htm)|Effect: Blast Boots (Major)|Effet : Bottes d'explosion majeures|libre|
|[OMW71UJzYCUr4ubh.htm](equipment-effects/OMW71UJzYCUr4ubh.htm)|Effect: Ablative Armor Plating (Major)|Effet : Blindage ablatif d'armure majeur|libre|
|[omyZyfTnx3uYVgiP.htm](equipment-effects/omyZyfTnx3uYVgiP.htm)|Effect: Arachnid Harness|Effet : Harnais arachnide|libre|
|[oqwrw6XztVlS9tEG.htm](equipment-effects/oqwrw6XztVlS9tEG.htm)|Effect: Trinity Geode - Armor|Effet : Géode de la trinité - Armure|libre|
|[ORdhj3IAvYACNGkJ.htm](equipment-effects/ORdhj3IAvYACNGkJ.htm)|Effect: Shrine Inarizushi|Effet : Autel d'Inarizushi|libre|
|[OxCVZSvWVJsOGAZN.htm](equipment-effects/OxCVZSvWVJsOGAZN.htm)|Effect: Flaming Star - Weapon|Effet : Étoile enflammée - Arme|libre|
|[p2aGtovaY1feytws.htm](equipment-effects/p2aGtovaY1feytws.htm)|Effect: Aeon Stone (Black Pearl)|Effet : Pierre d'éternité (Perle noire)|libre|
|[P7Y7pO2ulZ5wBgxU.htm](equipment-effects/P7Y7pO2ulZ5wBgxU.htm)|Effect: Barding of the Zephyr|Effet : Barde du zéphir|libre|
|[p9jROgkqozXB52UJ.htm](equipment-effects/p9jROgkqozXB52UJ.htm)|Effect: Trinity Geode - Weapon (Greater)|Effet : Géode de la trinité supérieure - arme|libre|
|[pAMyEbJzWBoYoGhs.htm](equipment-effects/pAMyEbJzWBoYoGhs.htm)|Effect: Diplomat's Badge|Effet : Insigne du diplomate|libre|
|[PeiuJ951kkBPTCSM.htm](equipment-effects/PeiuJ951kkBPTCSM.htm)|Effect: Bracers of Missile Deflection|Effet : Protège-bras de déviation de projectiles|libre|
|[PEPOd38VfVzQMKG5.htm](equipment-effects/PEPOd38VfVzQMKG5.htm)|Effect: Stone Body Mutagen (Lesser)|Effet : Mutagène corps-de-pierre inférieur|libre|
|[PeuUz7JaabCgl6Yh.htm](equipment-effects/PeuUz7JaabCgl6Yh.htm)|Effect: Cheetah's Elixir (Greater)|Effet : Élixir du guépard supérieur|libre|
|[Pkk8m79MoT1RgtfW.htm](equipment-effects/Pkk8m79MoT1RgtfW.htm)|Effect: Succubus Kiss (Stage 1)|Effet : Baiser de la succube (Stade 1)|libre|
|[pmWJxDjz3gqL29OM.htm](equipment-effects/pmWJxDjz3gqL29OM.htm)|Effect: Bottled Omen|Effet : Présage en bouteille|libre|
|[PpLxndUSgzgs6dd0.htm](equipment-effects/PpLxndUSgzgs6dd0.htm)|Effect: Elixir of Life (Major)|Effet : Élixir de vie majeur|libre|
|[PuWZyFzJCkbq1Inj.htm](equipment-effects/PuWZyFzJCkbq1Inj.htm)|Effect: Flaming Star - Armor|Effet : Étoile enflammée - Armure|libre|
|[q1EhQ716bPSgJVnC.htm](equipment-effects/q1EhQ716bPSgJVnC.htm)|Effect: Bravo's Brew (Greater)|Effet : Breuvage de bravoure supérieur|libre|
|[q58ahUEjUzTXffRN.htm](equipment-effects/q58ahUEjUzTXffRN.htm)|Effect: Perfect Droplet - Armor (Greater)|Effet : Goutte parfaite supérieure - Armure|libre|
|[QapoFh0tbUgMwSIB.htm](equipment-effects/QapoFh0tbUgMwSIB.htm)|Effect: Thurible of Revelation (Greater)|Effet : Encensoir de révélation supérieur|libre|
|[qit1mLbJUyRTYcPU.htm](equipment-effects/qit1mLbJUyRTYcPU.htm)|Effect: Cognitive Mutagen (Greater)|Effet : Mutagène cognitif supérieur|libre|
|[qOBdeZ4FXYc5qHsm.htm](equipment-effects/qOBdeZ4FXYc5qHsm.htm)|Effect: Private Workshop (Using for Crafting)|Effet : Atelier privé (utilisation pour l'artisanat)|libre|
|[QrsPKOFuo3qzgxw5.htm](equipment-effects/QrsPKOFuo3qzgxw5.htm)|Effect: Red-Rib Gill Mask (Greater)|Effet : Masque branchies de côte rouge supérieur|libre|
|[QuZ5frBMJF3gi7RY.htm](equipment-effects/QuZ5frBMJF3gi7RY.htm)|Effect: Antidote (Greater)|Effet : Antidote supérieur|libre|
|[qVKrrKpTghgMIuGH.htm](equipment-effects/qVKrrKpTghgMIuGH.htm)|Effect: Antiplague (Major)|Effet : Antimaladie majeur|libre|
|[qwoLV4awdezlEJ60.htm](equipment-effects/qwoLV4awdezlEJ60.htm)|Effect: Drakeheart Mutagen (Greater)|Effet : Mutagène de coeur de drake supérieur|libre|
|[QXJLvL2k3WqlF0SN.htm](equipment-effects/QXJLvL2k3WqlF0SN.htm)|Effect: Grim Sandglass - Armor (Major)|Effet : Sablier sinistre majeur - Armure|libre|
|[qzRcSQ0HTTp58hV2.htm](equipment-effects/qzRcSQ0HTTp58hV2.htm)|Effect: Sixfingers Elixir (Moderate)|Effet : Élixir Six-doigts modéré|libre|
|[R106i7WCXvHLGMTu.htm](equipment-effects/R106i7WCXvHLGMTu.htm)|Effect: Antiplague (Greater)|Effet : Antimaladie supérieur|libre|
|[R1kZsBMZdGZ3ATkA.htm](equipment-effects/R1kZsBMZdGZ3ATkA.htm)|Effect: Flaming Star - Armor (Greater)|Effet : Étoile enflammée supérieure - armure|libre|
|[R5ugeFK3MPwkbv0s.htm](equipment-effects/R5ugeFK3MPwkbv0s.htm)|Effect: Potency Crystal|Effet : Cristal de puissance|libre|
|[r6hDgfVLod0AmU7J.htm](equipment-effects/r6hDgfVLod0AmU7J.htm)|Effect: Heartripper Blade|Effet : Lame arrachecoeur|libre|
|[rH6RHxy6sNTLusKX.htm](equipment-effects/rH6RHxy6sNTLusKX.htm)|Effect: Emerald Fulcrum Lens (Saving Throw)|Effet : Lentille à pivot émeraude (jet de sauvegarde)|libre|
|[RIozNOntRJok5ZJt.htm](equipment-effects/RIozNOntRJok5ZJt.htm)|Effect: Energy Mutagen (Major)|Effet : Mutagène d'énergie majeur|libre|
|[RLsdvhmTh64Mmty9.htm](equipment-effects/RLsdvhmTh64Mmty9.htm)|Effect: Frost Vial (Lesser)|Effet : Fiole givrée inférieure|libre|
|[RRusoN3HEGnDO1Dg.htm](equipment-effects/RRusoN3HEGnDO1Dg.htm)|Effect: Sea Touch Elixir (Greater)|Effet: Élixir de caresse marine supérieur|libre|
|[RT1BxXrbbGgk40Ti.htm](equipment-effects/RT1BxXrbbGgk40Ti.htm)|Effect: Cognitive Mutagen (Major)|Effet : Mutagène cognitif majeur|libre|
|[rVFDLzYrJVYLiQBL.htm](equipment-effects/rVFDLzYrJVYLiQBL.htm)|Effect: Qat (Stage 1)|Effet : Qat (Stade 1)|libre|
|[rXM6njevpwqSMNRt.htm](equipment-effects/rXM6njevpwqSMNRt.htm)|Effect: Tallowheart Mass|Effet : Agglomérat de suif|libre|
|[S3Sv7SYwxozbG554.htm](equipment-effects/S3Sv7SYwxozbG554.htm)|Effect: War Blood Mutagen (Major)|Effet : Mutagène de sang guerrier majeur|libre|
|[S4MZzALqFoXJsr6o.htm](equipment-effects/S4MZzALqFoXJsr6o.htm)|Effect: Bloodhound Mask (Lesser)|Effet : Masque du limier inférieur|libre|
|[s95P3L72BDKvzYhn.htm](equipment-effects/s95P3L72BDKvzYhn.htm)|Effect: Curse of Potent Poison|Effet : Malédiction du poison puissant|libre|
|[SbYcOry1cxbndSve.htm](equipment-effects/SbYcOry1cxbndSve.htm)|Effect: Silkspinner's Shield (Climb)|Effet : Bouclier de la fileuse de soie (Escalade)|libre|
|[sOwAqyQ6MaoSqaY1.htm](equipment-effects/sOwAqyQ6MaoSqaY1.htm)|Effect: Lesser Healer's Gel|Effet : Gel du guérisseur inférieur|libre|
|[SqN1FGSgdNlyvRu9.htm](equipment-effects/SqN1FGSgdNlyvRu9.htm)|Effect: Containment Contraption|Effet : Appareil de confinement|libre|
|[T27uDXdMVc5ZFwKw.htm](equipment-effects/T27uDXdMVc5ZFwKw.htm)|Effect: Enhanced Hearing Aids|Effet : Appareil auditif augmenté|libre|
|[t7VUJHSUT6bkVUjg.htm](equipment-effects/t7VUJHSUT6bkVUjg.htm)|Effect: Serene Mutagen (Major)|Effet : Mutagène de sérénité majeur|libre|
|[tcHG8NlsYmHdziko.htm](equipment-effects/tcHG8NlsYmHdziko.htm)|Effect: Grim Sandglass - Weapon (Major)|Effet : Sablier sinistre majeur - Arme|libre|
|[tGeMT4iHJcsjVbl3.htm](equipment-effects/tGeMT4iHJcsjVbl3.htm)|Effect: Metuak's Pendant|Effet : Pendentif de Metuak|libre|
|[Thd0XXhunYNk6jD7.htm](equipment-effects/Thd0XXhunYNk6jD7.htm)|Effect: Cinnamon Seers|Effet : Voyants à la cannelle|libre|
|[thOpQunbQr77XWdF.htm](equipment-effects/thOpQunbQr77XWdF.htm)|Effect: Sea Touch Elixir (Lesser)|Effet: Élixir de caresse marine inférieur|libre|
|[Tioloj3bTlFnQDqa.htm](equipment-effects/Tioloj3bTlFnQDqa.htm)|Effect: Perfect Droplet - Armor (Major)|Effet : Goutte parfaite majeure (Armure)|libre|
|[TkRuKKYyPHTGPfgf.htm](equipment-effects/TkRuKKYyPHTGPfgf.htm)|Effect: Sixfingers Elixir (Greater)|Effet : Élixir six-doigts supérieur|libre|
|[tLGzSCcfxflLSqzw.htm](equipment-effects/tLGzSCcfxflLSqzw.htm)|Effect: Energized Cartridge (Cold)|Effet : Cartouche énergisée (froid)|libre|
|[TsWUTODTVi487SEz.htm](equipment-effects/TsWUTODTVi487SEz.htm)|Effect: Skeptic's Elixir (Greater)|Effet : Élixir de l'incrédule supérieur|libre|
|[tTBJ33UGtzXjWOJp.htm](equipment-effects/tTBJ33UGtzXjWOJp.htm)|Effect: Applereed Mutagen (Greater)|Effet : Mutagène pousse-roseau supérieur|libre|
|[u35Qzft0c84UySq2.htm](equipment-effects/u35Qzft0c84UySq2.htm)|Effect: Ebon Fulcrum Lens (2 Action)|Effet : Lentille à pivot ébène (2 Actions)|libre|
|[u7200u7lh40am0jb.htm](equipment-effects/u7200u7lh40am0jb.htm)|Effect: Trinity Geode - Weapon (Major)|Effet : Géode de la trinité majeure - arme|libre|
|[Uadsb25G18pKdZ2e.htm](equipment-effects/Uadsb25G18pKdZ2e.htm)|Effect: Clandestine Cloak|Effet : Cape de clandestinité|libre|
|[uC6KjfiWrTBXYtP8.htm](equipment-effects/uC6KjfiWrTBXYtP8.htm)|Effect: Storm Chair|Effet : Chaise de tempête|libre|
|[UDfVCATxdLdSzJYJ.htm](equipment-effects/UDfVCATxdLdSzJYJ.htm)|Effect: Red-Rib Gill Mask (Lesser)|Effet : Masque branchies de côte rouge inférieur|libre|
|[UlalLihKzDxcOdXL.htm](equipment-effects/UlalLihKzDxcOdXL.htm)|Effect: Thurible of Revelation (Moderate)|Effet : Encensoir de révélation modéré|libre|
|[UTtX0xLGYci6P43I.htm](equipment-effects/UTtX0xLGYci6P43I.htm)|Effect: Mudrock Snare (Failure)|Effet : Piège artisanal de boue solidifiée (Échec)|libre|
|[uVxs1qFMQsGWXNs6.htm](equipment-effects/uVxs1qFMQsGWXNs6.htm)|Effect: Potion of Stable Form|Effet : Potion de forme stable|libre|
|[uXEp1rPU5fY4OiBf.htm](equipment-effects/uXEp1rPU5fY4OiBf.htm)|Effect: Clandestine Cloak (Greater)|Effect: Cape de clandestinité supérieure|libre|
|[UzSrsR9S2pgMDbbp.htm](equipment-effects/UzSrsR9S2pgMDbbp.htm)|Effect: Cape of the Open Sky|Effet : Cape du ciel ouvert|libre|
|[V4JoVnOfKze8cRan.htm](equipment-effects/V4JoVnOfKze8cRan.htm)|Effect: Grim Sandglass - Armor|Effet : Sablier sinistre - Armure|libre|
|[v5Ht1V4MZvRKRBjL.htm](equipment-effects/v5Ht1V4MZvRKRBjL.htm)|Effect: Silvertongue Mutagen (Moderate)|Effet : Mutagène de langue dorée modéré|libre|
|[VCypzSu659eC6jNi.htm](equipment-effects/VCypzSu659eC6jNi.htm)|Effect: Eagle Eye Elixir (Lesser)|Effet : Élixir d'oeil du faucon inférieur|libre|
|[vFOr2JAJxiVvvn2E.htm](equipment-effects/vFOr2JAJxiVvvn2E.htm)|Effect: Drakeheart Mutagen (Major)|Effet : Mutagène de coeur de drake majeur|libre|
|[vH4bEu3EnAhNpKEQ.htm](equipment-effects/vH4bEu3EnAhNpKEQ.htm)|Effect: Dragonscale Amulet|Effet : Amulette en écailles de dragons|libre|
|[viCX9fZzTWGuoO85.htm](equipment-effects/viCX9fZzTWGuoO85.htm)|Effect: Cloak of Elvenkind (Greater)|Effet : Cape elfique supérieure|libre|
|[VIzeuA9tQEQ7V1Ib.htm](equipment-effects/VIzeuA9tQEQ7V1Ib.htm)|Effect: Wand of Fey Flames|Effet : Baguette de feux féériques|libre|
|[vj2hkcSbwwRYNLk5.htm](equipment-effects/vj2hkcSbwwRYNLk5.htm)|Effect: Jack's Tattered Cape|Effet : Cape en loques de Jack|libre|
|[vKooOkXHvtqCgZYg.htm](equipment-effects/vKooOkXHvtqCgZYg.htm)|Effect: Energized Cartridge (Acid)|Effet : Cartouche énergisée (acide)|libre|
|[VlfuBfWkygsG8u5h.htm](equipment-effects/VlfuBfWkygsG8u5h.htm)|Effect: Blaze|Effet : Brûlure|libre|
|[vOgD9wfStLX1utte.htm](equipment-effects/vOgD9wfStLX1utte.htm)|Effect: Skyrider Sword (Greater)|Effet : Chevaucheur d'épée supérieur|libre|
|[vOU4Yv2MyAfYBbmF.htm](equipment-effects/vOU4Yv2MyAfYBbmF.htm)|Effect: Aeon Stone (Orange Prism) (Occultism)|Effet : Pierre d'éternité (Prisme orange) (Occultisme)|libre|
|[VPtsrpbP0AE642al.htm](equipment-effects/VPtsrpbP0AE642al.htm)|Effect: Quicksilver Mutagen (Moderate)|Effet : Mutagène de vif-argent modéré|libre|
|[VrYfR2WuyA15zFhq.htm](equipment-effects/VrYfR2WuyA15zFhq.htm)|Effect: Vermin Repellent Agent (Greater)|Effet : Agent répulsif de vermine supérieur|libre|
|[VsHhBBLApZsOCJRL.htm](equipment-effects/VsHhBBLApZsOCJRL.htm)|Effect: Fire and Iceberg (Greater)|Effet : Feu et iceberg supérieur|libre|
|[VVWvXiNudYYGV9sJ.htm](equipment-effects/VVWvXiNudYYGV9sJ.htm)|Effect: Nosoi Charm (Lifesense)|Effet : Charme nosoi (Perception de la vie)|libre|
|[VZCcjwsQX1wnYlTn.htm](equipment-effects/VZCcjwsQX1wnYlTn.htm)|Effect: Perfect Droplet - Armor|Effet : Goutte parfaite - Armure|libre|
|[vZPyQAt5T2L0Dfmq.htm](equipment-effects/vZPyQAt5T2L0Dfmq.htm)|Effect: Topology Protoplasm|Effet : Morphologie protoplasmique|libre|
|[w0LUnfS2whVhDBUF.htm](equipment-effects/w0LUnfS2whVhDBUF.htm)|Effect: Glittering Snare (Success)|Effet : Piège scintillant (Succès)|libre|
|[W3BCLbX6j1IqL0uB.htm](equipment-effects/W3BCLbX6j1IqL0uB.htm)|Effect: Slippers of Spider Climbing|Effet : Chaussons de l'araignée|libre|
|[W3xQBLj5hLOtb6Tj.htm](equipment-effects/W3xQBLj5hLOtb6Tj.htm)|Effect: Potion of Swimming (Moderate)|Effet : Potion de nage modéré|libre|
|[W9tKQlA7tVIcAuzw.htm](equipment-effects/W9tKQlA7tVIcAuzw.htm)|Effect: Greater Potion of Stable Form|Effet : Potion de forme stable supérieure|libre|
|[Wa4317cqU4lJ8vAQ.htm](equipment-effects/Wa4317cqU4lJ8vAQ.htm)|Effect: Eagle Eye Elixir (Moderate)|Effet : Élixir d'oeil du faucon modéré|libre|
|[wacGBDbbQ1HaNZbX.htm](equipment-effects/wacGBDbbQ1HaNZbX.htm)|Effect: Hyldarf's Fang|Effet : Croc de Hyldarf|libre|
|[wFF0SZs1Hcf87Kk1.htm](equipment-effects/wFF0SZs1Hcf87Kk1.htm)|Effect: Bloodhound Mask (Moderate)|Effet : Masque du limier modéré|libre|
|[wFP3SqPoO0bCPmyK.htm](equipment-effects/wFP3SqPoO0bCPmyK.htm)|Effect: Kraken's Guard|Effet : Gardien kraken|libre|
|[WHAp9cDOqnJ1VCcg.htm](equipment-effects/WHAp9cDOqnJ1VCcg.htm)|Effect: Orchestral Brooch|Effet : Broche orchestrale|libre|
|[WMKjWH4gyUrky4Hy.htm](equipment-effects/WMKjWH4gyUrky4Hy.htm)|Effect: Demon Dust (Stage 1)|Effet : Poussière de démon (Stade 1)|libre|
|[wNCxSxruzLVGtLE4.htm](equipment-effects/wNCxSxruzLVGtLE4.htm)|Effect: Spiderfoot Brew (Lesser)|Effet : Infusion de pattes d'araignée inférieure|libre|
|[WQ0DxUzMgAvo0zy9.htm](equipment-effects/WQ0DxUzMgAvo0zy9.htm)|Effect: Apricot of Bestial Might|Effet : Abricot de puissance bestiale|libre|
|[WRV0XjiEHdlBpduS.htm](equipment-effects/WRV0XjiEHdlBpduS.htm)|Effect: Trinity Geode - Weapon|Effet : Géode de la trinité - Arme|libre|
|[wTZnKkT0K4Tdy8mD.htm](equipment-effects/wTZnKkT0K4Tdy8mD.htm)|Effect: Bravo's Brew (Moderate)|Effet : Breuvage de bravoure modéré|libre|
|[WXrqEuLT4uP48Bvo.htm](equipment-effects/WXrqEuLT4uP48Bvo.htm)|Effect: Goggles of Night (Major)|Effet : Lunettes de nyctalope majeures|libre|
|[WXsWkFosSGBrptwF.htm](equipment-effects/WXsWkFosSGBrptwF.htm)|Effect: Ivory Baton|Effet: Bâton d'ivoire|libre|
|[wyLEew86nhNUXASu.htm](equipment-effects/wyLEew86nhNUXASu.htm)|Effect: Eagle Eye Elixir (Major)|Effet : Élixir d'oeil du faucon majeur|libre|
|[X2ZZgTqanpoCMDmd.htm](equipment-effects/X2ZZgTqanpoCMDmd.htm)|Effect: Taljjae's Mask (The General)|Effet : Masque de Taljjae (le général)|libre|
|[xFQRiVU6h8EA6Lw9.htm](equipment-effects/xFQRiVU6h8EA6Lw9.htm)|Effect: Bestial Mutagen (Moderate)|Effet : Mutagène bestial modéré|libre|
|[XKsqQrabRlg9klGp.htm](equipment-effects/XKsqQrabRlg9klGp.htm)|Effect: Draft of Stellar Radiance|Effet : Ébauche d'éclat stellaire|libre|
|[XlHbUOTbK6PfBfCv.htm](equipment-effects/XlHbUOTbK6PfBfCv.htm)|Effect: Demon Dust (Stage 2)|Effet : Poussière de démon (Stade 2)|libre|
|[xLilBqqf34ZJYO9i.htm](equipment-effects/xLilBqqf34ZJYO9i.htm)|Effect: Juggernaut Mutagen (Greater)|Effet : Mutagène de juggernaut supérieur|libre|
|[XrlChFETfe8avLsX.htm](equipment-effects/XrlChFETfe8avLsX.htm)|Effect: Sixfingers Elixir (Lesser)|Effet : Élixir six-doigts inférieur|libre|
|[xVAdPzFaSvJXPMKv.htm](equipment-effects/xVAdPzFaSvJXPMKv.htm)|Effect: Applereed Mutagen (Lesser)|Effet : Mutagène pousse-roseau inférieur|libre|
|[XwCBalKJf3CiEiFa.htm](equipment-effects/XwCBalKJf3CiEiFa.htm)|Effect: Treat Poison (Critical Success)|Effet : Soigner l'empoisonnement (succès critique)|libre|
|[XWenziR7J3mwKV4W.htm](equipment-effects/XWenziR7J3mwKV4W.htm)|Effect: Treat Poison (Success)|Effet : Soigner l'empoisonnement (succès)|libre|
|[XzxADtNpUlRff972.htm](equipment-effects/XzxADtNpUlRff972.htm)|Effect: Greater Fanged Rune Animal Form|Effet : Forme d'animal à runes de crocs supérieure|libre|
|[YI7QQqXO6nosaAKr.htm](equipment-effects/YI7QQqXO6nosaAKr.htm)|Effect: Spiderfoot Brew (Greater)|Effet : Infusion de pattes d'araignée supérieure|libre|
|[yP45Rqu4jvCfXBkp.htm](equipment-effects/yP45Rqu4jvCfXBkp.htm)|Effect: Fire and Iceberg (Major)|Effet : Feu et iceberg majeur|libre|
|[yrbz0rZzp8aZEqbv.htm](equipment-effects/yrbz0rZzp8aZEqbv.htm)|Effect: Serene Mutagen (Moderate)|Effet : Mutagène de sérénité modéré|libre|
|[yt8meGTS7wLa6Fg2.htm](equipment-effects/yt8meGTS7wLa6Fg2.htm)|Effect: Ablative Armor Plating (True)|Effet : Blindage ablatif d'armure ultime|libre|
|[ytqvHyF0oMKbo65P.htm](equipment-effects/ytqvHyF0oMKbo65P.htm)|Effect: Crimson Fulcrum Lens|Effet : Lentille cramoisie|libre|
|[yvabfuAO74pvH8hh.htm](equipment-effects/yvabfuAO74pvH8hh.htm)|Effect: Aeon Stone (Orange Prism) (Arcana)|Effect: Pierre d'éternité (Prisme orange) (Arcanes)|libre|
|[Yxssrnh9UZJAM0V7.htm](equipment-effects/Yxssrnh9UZJAM0V7.htm)|Effect: Elixir of Life (Moderate)|Effet : Élixir de vie modéré|libre|
|[z3ATL8DcRVrT0Uzt.htm](equipment-effects/z3ATL8DcRVrT0Uzt.htm)|Effect: Disarm (Success)|Effet : Désarmer (succès)|libre|
|[z8FvdsKEY4lB2L8b.htm](equipment-effects/z8FvdsKEY4lB2L8b.htm)|Effect: Phoenix Flask|Effet : Flasque de phénix|libre|
|[Z9oPh462q82IYIZ6.htm](equipment-effects/Z9oPh462q82IYIZ6.htm)|Effect: Elixir of Life (Greater)|Effet : Élixir de vie supérieur|libre|
|[Zb8RYgmzCI6fQE0o.htm](equipment-effects/Zb8RYgmzCI6fQE0o.htm)|Effect: Throne Card|Effet : Carte trône|libre|
|[zd85Ny1RS46OL0TD.htm](equipment-effects/zd85Ny1RS46OL0TD.htm)|Effect: Shrinking Potion (Greater)|Effect: Potion de rétrécissement supérieur|libre|
|[Zdh2uO1vVYJmaqld.htm](equipment-effects/Zdh2uO1vVYJmaqld.htm)|Effect: Potion of Flying (Greater)|Effet : Potion de vol supérieure|libre|
|[zDuuHVeHgd175pGf.htm](equipment-effects/zDuuHVeHgd175pGf.htm)|Effect: Succubus Kiss (Stage 2)|Effet : Baiser de la succube (Stade 2)|libre|
|[zlSNbMDIlTOpcO8R.htm](equipment-effects/zlSNbMDIlTOpcO8R.htm)|Effect: Skinstitch Salve|Effet : Pommade suture|libre|
|[ZP9Uq4PVTgzJ3wEi.htm](equipment-effects/ZP9Uq4PVTgzJ3wEi.htm)|Effect: Five-Feather Wreath - Armor (Greater)|Effet : Couronne à cinq plumes supérieure|libre|
|[zqKzWGLODgIvtiKf.htm](equipment-effects/zqKzWGLODgIvtiKf.htm)|Effect: Spellguard Blade|Effet : Lame contre-sort|libre|
|[ztxW3lBPRcesF7wK.htm](equipment-effects/ztxW3lBPRcesF7wK.htm)|Effect: Cognitive Mutagen (Moderate)|Effet : Mutagène cognitif modéré|libre|
|[ZV9rtVOD1eDTwcY4.htm](equipment-effects/ZV9rtVOD1eDTwcY4.htm)|Effect: Grit (Stage 3)|Effet : Grit (stade 3)|libre|
|[zY7cemRcFD2zAVbC.htm](equipment-effects/zY7cemRcFD2zAVbC.htm)|Effect: Oath of the Devoted|Effet : Serment du dévoué|libre|
|[zZsdex5orF5Odpus.htm](equipment-effects/zZsdex5orF5Odpus.htm)|Effect: Mask of Allure|Effet : Masque d'allure|libre|
